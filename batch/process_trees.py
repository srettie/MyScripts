import sys
import math
import os
import argparse
import glob
import datetime as dt
import ROOT
import tracking_submitJobs

debug = False

tracks = tracking_submitJobs.tracks
track_selections = ['baseline','HF','FROMB','FROMC','PUFAKE','LowPt']
jet_selections = ['baseline','bjet','cjet','ljet', 'isojet','bjetsemilep_mu','bjetsemilep_el','bjetsemilep_mu_withCghost','bjetsemilep_el_withCghost','bjetsemilep_mu_newLabels','bjetsemilep_el_newLabels']

# Baseline cuts
jet_pt_min  = 100.
jet_pt_max  = 5000.
jet_eta_min = 0.
jet_eta_max = 2.1
jet_dRiso_cut = 1.0
trk_pt_min = 1.

# Binning; nbins, min, max
nbins_njets,min_njets,max_njets                = 20,0,20
nbins_PVx,min_PVx,max_PVx                      = 100,-0.6,-0.4
nbins_PVy,min_PVy,max_PVy                      = 100,-0.6,-0.4
nbins_PVz,min_PVz,max_PVz                      = 500,-200,200
nbins_pt,min_pt,max_pt                         = 200,0,5000
nbins_trk_pt,min_trk_pt,max_trk_pt             = 400,0,400
nbins_eta,min_eta,max_eta                      = 100,-5,5
nbins_phi,min_phi,max_phi                      = 100,-math.pi,math.pi
nbins_ntrk,min_ntrk,max_ntrk                   = 120,0,120
nbins_Lxy,min_Lxy,max_Lxy                      = 200,0,200
nbins_L3d,min_L3d,max_L3d                      = 300,0,300
nbins_sig3d,min_sig3d,max_sig3d                = 200,0,200
nbins_nvtx,min_nvtx,max_nvtx                   = 10,0,10
nbins_efc,min_efc,max_efc                      = 100,0,1
nbins_d0,min_d0,max_d0                         = 100,-5,5
nbins_d0Err,min_d0Err,max_d0Err                = 100,0,1
nbins_d0sig,min_d0sig,max_d0sig                = 100,-10,10
nbins_d0res,min_d0res,max_d0res                = 100,-0.5,0.5
nbins_d0pull,min_d0pull,max_d0pull             = 100,-5,5
nbins_z0sintheta,min_z0sintheta,max_z0sintheta = 100,-5,5
nbins_z0,min_z0,max_z0                         = 100,-5,5
nbins_z0Err,min_z0Err,max_z0Err                = 100,0,1
nbins_z0sig,min_z0sig,max_z0sig                = 100,-10,10
nbins_z0res,min_z0res,max_z0res                = 100,-2,2
nbins_z0pull,min_z0pull,max_z0pull             = 100,-10,10
nbins_phiErr,min_phiErr,max_phiErr             = 100,0,0.05
nbins_theta,min_theta,max_theta                = 100,0,math.pi
nbins_thetaErr,min_thetaErr,max_thetaErr       = 100,0,0.02
nbins_qop,min_qop,max_qop                      = 100,-5,5
nbins_qopErr,min_qopErr,max_qopErr             = 100,0,0.1
nbins_vtx_trk,min_vtx_trk,max_vtx_trk          = 20,0,20
nbins_vtxx,min_vtxx,max_vtxx                   = 300,-150,150
nbins_vtxy,min_vtxy,max_vtxy                   = 300,-150,150
nbins_dRiso,min_dRiso,max_dRiso                = 60,0,6
nbins_nIBLHits,min_nIBLHits,max_nIBLHits       = 5,0,5
nbins_nPixHits,min_nPixHits,max_nPixHits       = 12,0,12
nbins_nSCTHits,min_nSCTHits,max_nSCTHits       = 25,0,25
nbins_chi2,min_chi2,max_chi2                   = 200,0,200
nbins_chi2DOF,min_chi2DOF,max_chi2DOF          = 80,0,8
nbins_holes,min_holes,max_holes                = 5,0,5
nbins_TMP, min_TMP, max_TMP                    = 200,0,1
nbins_decay,min_decay,max_decay                = 5,0,5
# Impact parameters
nbins_ipX, min_ipX, max_ipX                    = 200,-20,20
nbins_ipX_c, min_ipX_c, max_ipX_c              = 200,-15,15
nbins_ipX_cu, min_ipX_cu, max_ipX_cu           = 200,-10,10
# SV1
nbins_sv1_m, min_sv1_m, max_sv1_m              = 60,0,6
nbins_sv1_ntrkv, min_sv1_ntrkv, max_sv1_ntrkv  = 15,0,15
nbins_sv1_n2t, min_sv1_n2t, max_sv1_n2t        = 60,0,60
nbins_sv1_deltaR,min_sv1_deltaR,max_sv1_deltaR = 100,0,1
nbins_sv1_llr, min_sv1_llr, max_sv1_llr        = 200, -5, 15
# B hadron
nbins_pt_frac,min_pt_frac,max_pt_frac          = 200, 0, 2
nbins_nbhadron,min_nbhadron,max_nbhadron       = 5, 0, 5
nbins_dRjet,min_dRjet,max_dRjet                = 40, 0, 0.4

algos = ['sv1','jf']
jet_flavours = ['bjet','cjet','ljet']
decay_modes = {
    'bjethadronic'   : 0,
    'bjetsemilep_mu' : 1,
    'bjetsemilep_el' : 2,
}
pt_bins = {
    '0pT150'     : [0,150],
    '150pT400'   : [150,400],
    '400pT1000'  : [400,1000],
    '1000pT1750' : [1000,1750],
    '1750pT2750' : [1750,2750],
    '2750pTInf'  : [2750,5000],
}

debugLxy_cutoff = 30.

plot_skeletons = {

    # Event histograms
    'njets'                         : ROOT.TH1D('njets','njets;Number of jets;Events',nbins_njets,min_njets,max_njets),
    'PVx'                           : ROOT.TH1D('PVx','PVx;Primary Vertex X Position [mm];Events',nbins_PVx,min_PVx,max_PVx),
    'PVy'                           : ROOT.TH1D('PVy','PVy;Primary Vertex Y Position [mm];Events',nbins_PVy,min_PVy,max_PVy),
    'PVz'                           : ROOT.TH1D('PVz','PVz;Primary Vertex Z Position [mm];Events',nbins_PVz,min_PVz,max_PVz),
    
    # Jet histograms
    'jet_pt'                        : ROOT.TH1D('jet_pt','jet_pt;Jet p_{T} [GeV];Entries',nbins_pt,min_pt,max_pt),
    'jet_eta'                       : ROOT.TH1D('jet_eta','jet_eta;Jet #eta;Entries',nbins_eta,min_eta,max_eta),
    'jet_phi'                       : ROOT.TH1D('jet_phi','jet_phi;Jet #phi;Entries',nbins_phi,min_phi,max_phi),
    'jet_ntrk'                      : ROOT.TH1D('jet_ntrk','jet_ntrk;Number of tracks in jet;Entries',nbins_ntrk,min_ntrk,max_ntrk),
    'jet_dRiso'                     : ROOT.TH1D('jet_dRiso','jet_dRiso;Minimum dR between jet and other jets;Entries',nbins_dRiso,min_dRiso,max_dRiso),
    'jet_nHFtracks'                 : ROOT.TH1D('jet_nHFtracks','jet_nHFtracks;Number of HF tracks in jet;Entries',nbins_ntrk,min_ntrk,max_ntrk),
    'jet_nPUFAKEtracks'             : ROOT.TH1D('jet_nPUFAKEtracks','jet_nPUFAKEtracks;Number of PU/FAKE tracks in jet;Entries',nbins_ntrk,min_ntrk,max_ntrk),
    'jet_nOthertracks'              : ROOT.TH1D('jet_nOthertracks','jet_nOthertracks;Number of non-HF tracks in jet;Entries',nbins_ntrk,min_ntrk,max_ntrk),
    'jet_nLowPtTracks'              : ROOT.TH1D('jet_nLowPtTracks','jet_nLowPtTracks;Number of tracks with p_{T} < 10 GeV in jet;Entries',nbins_ntrk,min_ntrk,max_ntrk),
    'jet_decayMode'                 : ROOT.TH1D('jet_decayMode','jet_decayMode;Jet decay mode;Entries',nbins_decay,min_decay,max_decay),

    # Impact parameters
    'jet_ip2'                       : ROOT.TH1D('jet_ip2','jet_ip2;IP2D log(P_{b}/P_{light});Entries',nbins_ipX, min_ipX, max_ipX),
    'jet_ip2_c'                     : ROOT.TH1D('jet_ip2_c','jet_ip2_c;IP2D log(P_{b}/P_{c});Entries',nbins_ipX_c, min_ipX_c, max_ipX_c),
    'jet_ip2_cu'                    : ROOT.TH1D('jet_ip2_cu','jet_ip2_cu;IP2D log(P_{c}/P_{light});Entries',nbins_ipX_cu, min_ipX_cu, max_ipX_cu),
    'jet_ip3'                       : ROOT.TH1D('jet_ip3','jet_ip3;IP3D log(P_{b}/P_{light});Entries',nbins_ipX, min_ipX, max_ipX),
    'jet_ip3_c'                     : ROOT.TH1D('jet_ip3_c','jet_ip3_c;IP3D log(P_{b}/P_{c});Entries',nbins_ipX_c, min_ipX_c, max_ipX_c),
    'jet_ip3_cu'                    : ROOT.TH1D('jet_ip3_cu','jet_ip3_cu;IP3D log(P_{c}/P_{light});Entries',nbins_ipX_cu, min_ipX_cu, max_ipX_cu),

    # SV1
    'jet_sv1_Nvtx'                  : ROOT.TH1D('jet_sv1_Nvtx','jet_sv1_Nvtx;Number of SV1 vertices in jet;Entries',nbins_nvtx,min_nvtx,max_nvtx),
    'jet_sv1_m'                     : ROOT.TH1D('jet_sv1_m','jet_sv1_m;SV1 m(SV) [GeV];Entries',nbins_sv1_m, min_sv1_m, max_sv1_m),
    'jet_sv1_efc'                   : ROOT.TH1D('jet_sv1_efc','jet_sv1_efc;Fraction of charged jet energy in SV, SV1 f_{E}(SV);Entries',nbins_efc, min_efc, max_efc),
    'jet_sv1_ntrkv'                 : ROOT.TH1D('jet_sv1_ntrkv','jet_sv1_ntrkv;Number of tracks used in SV, SV1 N_{TrkAtVtx}(SV);Entries',nbins_sv1_ntrkv, min_sv1_ntrkv, max_sv1_ntrkv),
    'jet_sv1_n2t'                   : ROOT.TH1D('jet_sv1_n2t','jet_sv1_n2t;Number of two-track vertex candidates, SV1 N_{2TrkVtx}(SV);Entries',nbins_sv1_n2t, min_sv1_n2t, max_sv1_n2t),
    'jet_sv1_Lxy'                   : ROOT.TH1D('jet_sv1_Lxy','jet_sv1_Lxy;Jet L_{xy}^{SV1} [mm];Entries',nbins_Lxy,min_Lxy,max_Lxy),
    'jet_sv1_L3d'                   : ROOT.TH1D('jet_sv1_L3d','jet_sv1_L3d;Jet L_{3d}^{SV1} [mm];Entries',nbins_L3d,min_L3d,max_L3d),
    'jet_sv1_sig3d'                 : ROOT.TH1D('jet_sv1_sig3d','jet_sv1_sig3d;Jet S_{3d}^{SV1};Entries',nbins_sig3d,min_sig3d,max_sig3d),
    'jet_sv1_deltaR'                : ROOT.TH1D('jet_sv1_deltaR','jet_sv1_deltaR;SV1 #DeltaR(jet,SV);Entries',nbins_sv1_deltaR,min_sv1_deltaR,max_sv1_deltaR),
    'jet_sv1_llr'                   : ROOT.TH1D('jet_sv1_llr','jet_sv1_llr;Jet SV1 LLR;Entries',nbins_sv1_llr,min_sv1_llr,max_sv1_llr),

    # JF
    'jet_jf_nvtx'                   : ROOT.TH1D('jet_jf_nvtx','jet_jf_nvts;Number of JF vertices in jet;Entries',nbins_nvtx,min_nvtx,max_nvtx),
    'jet_jf_efc'                    : ROOT.TH1D('jet_jf_efc','jet_jf_efc;JF energy fraction of jet;Entries',nbins_efc,min_efc,max_efc),

    # Track histograms
    'jet_trk_ip3d_d0sig'            : ROOT.TH1D('jet_trk_ip3d_d0sig','jet_trk_ip3d_d0sig;Track IP3D d_{0}^{sig};Entries',nbins_d0sig,min_d0sig,max_d0sig),
    'jet_trk_d0'                    : ROOT.TH1D('jet_trk_d0','jet_trk_d0;Track d_{0} [mm];Entries',nbins_d0,min_d0,max_d0),
    'jet_trk_d0Truth'               : ROOT.TH1D('jet_trk_d0Truth','jet_trk_d0Truth;Track Truth d_{0} [mm];Entries',nbins_d0,min_d0,max_d0),
    'jet_trk_d0Err'                 : ROOT.TH1D('jet_trk_d0Err','jet_trk_d0Err;Track #sigma(d_{0}) [mm];Entries',nbins_d0Err,min_d0Err,max_d0Err),
    'jet_trk_d0res'                 : ROOT.TH1D('jet_trk_d0res','jet_trk_d0res;Track d_{0}^{reco} - d_{0}^{truth} [mm];Entries',nbins_d0res,min_d0res,max_d0res),
    'jet_trk_d0pull'                : ROOT.TH1D('jet_trk_d0pull','jet_trk_d0pull;Track (d_{0}^{reco} - d_{0}^{truth})/#sigma(d_{0});Entries',nbins_d0pull,min_d0pull,max_d0pull),

    'jet_trk_ip3d_z0sig'            : ROOT.TH1D('jet_trk_ip3d_z0sig','jet_trk_ip3d_z0sig;Track IP3D z_{0}^{sig};Entries',nbins_z0sig,min_z0sig,max_z0sig),
    'jet_trk_z0'                    : ROOT.TH1D('jet_trk_z0','jet_trk_z0;Track z_{0} [mm];Entries',nbins_z0,min_z0,max_z0),
    'jet_trk_z0Truth'               : ROOT.TH1D('jet_trk_z0Truth','jet_trk_z0Truth;Track Truth z_{0} [mm];Entries',nbins_z0,min_z0,max_z0),
    'jet_trk_z0sintheta'            : ROOT.TH1D('jet_trk_z0sintheta','jet_trk_z0sintheta;Track z_{0}*sin(#theta) [mm];Entries',nbins_z0sintheta,min_z0sintheta,max_z0sintheta),
    'jet_trk_z0Err'                 : ROOT.TH1D('jet_trk_z0Err','jet_trk_z0Err;Track #sigma(z_{0}) [mm];Entries',nbins_z0Err,min_z0Err,max_z0Err),
    'jet_trk_z0res'                 : ROOT.TH1D('jet_trk_z0res','jet_trk_z0res;Track z_{0}^{reco} - z_{0}^{truth} [mm];Entries',nbins_z0res,min_z0res,max_z0res),
    'jet_trk_z0pull'                : ROOT.TH1D('jet_trk_z0pull','jet_trk_z0pull;Track (z_{0}^{reco} - z_{0}^{truth})/#sigma(z_{0});Entries',nbins_z0pull,min_z0pull,max_z0pull),

    'jet_trk_phi'                   : ROOT.TH1D('jet_trk_phi','jet_trk_phi;Track #phi;Entries',nbins_phi,min_phi,max_phi),
    'jet_trk_phiErr'                : ROOT.TH1D('jet_trk_phiErr','jet_trk_phiErr;Track #sigma(#phi);Entries',nbins_phiErr,min_phiErr,max_phiErr),
    'jet_trk_theta'                 : ROOT.TH1D('jet_trk_theta','jet_trk_theta;Track #theta;Entries',nbins_theta,min_theta,max_theta),
    'jet_trk_thetaErr'              : ROOT.TH1D('jet_trk_thetaErr','jet_trk_thetaErr;Track #sigma(#theta);Entries',nbins_thetaErr,min_thetaErr,max_thetaErr),
    'jet_trk_qop'                   : ROOT.TH1D('jet_trk_qop','jet_trk_qop;Track q/p [GeV^{-1}];Entries',nbins_qop,min_qop,max_qop),
    'jet_trk_qopErr'                : ROOT.TH1D('jet_trk_qopErr','jet_trk_qopErr;Track #sigma(q/p) [GeV^{-1}];Entries',nbins_qopErr,min_qopErr,max_qopErr),
    'jet_trk_pt'                    : ROOT.TH1D('jet_trk_pt','jet_trk_pt;Track p_{T} [GeV];Entries',nbins_trk_pt,min_trk_pt,max_trk_pt),

    'jet_trk_nIBLHits'              : ROOT.TH1D('jet_trk_nIBLHits','jet_trk_nIBLHits;Number of IBL hits in track;Entries',nbins_nIBLHits,min_nIBLHits,max_nIBLHits),
    'jet_trk_nPixHits'              : ROOT.TH1D('jet_trk_nPixHits','jet_trk_nPixHits;Number of pixel hits in track;Entries',nbins_nPixHits,min_nPixHits,max_nPixHits),
    'jet_trk_nsharedPixHits'        : ROOT.TH1D('jet_trk_nsharedPixHits','jet_trk_nsharedPixHits;Number of shared pixel hits in track;Entries',nbins_nPixHits,min_nPixHits,max_nPixHits),
    'jet_trk_nsplitPixHits'         : ROOT.TH1D('jet_trk_nsplitPixHits','jet_trk_nsplitPixHits;Number of split pixel hits in track;Entries',nbins_nPixHits,min_nPixHits,max_nPixHits),
    'jet_trk_nPixHoles'             : ROOT.TH1D('jet_trk_nPixHoles','jet_trk_nPixHoles;Number of pixel holes in track;Entries',nbins_holes,min_holes,max_holes),
    'jet_trk_nSCTHits'              : ROOT.TH1D('jet_trk_nSCTHits','jet_trk_nSCTHits;Number of SCT hits in track;Entries',nbins_nSCTHits,min_nSCTHits,max_nSCTHits),
    'jet_trk_nsharedSCTHits'        : ROOT.TH1D('jet_trk_nsharedSCTHits','jet_trk_nsharedSCTHits;Number of shared SCT hits in track;Entries',nbins_nSCTHits,min_nSCTHits,max_nSCTHits),
    'jet_trk_nSCTHoles'             : ROOT.TH1D('jet_trk_nSCTHoles','jet_trk_nSCTHoles;Number of SCT holes in track;Entries',nbins_holes,min_holes,max_holes),
    'jet_trk_nPixSCTHits'           : ROOT.TH1D('jet_trk_nPixSCTHits','jet_trk_nPixSCTHits;Number of (pixel + SCT) hits in track;Entries',nbins_nSCTHits,min_nSCTHits,max_nSCTHits),
    'jet_trk_chi2'                  : ROOT.TH1D('jet_trk_chi2','jet_trk_chi2;Track #chi^{2};Entries',nbins_chi2,min_chi2,max_chi2),
    'jet_trk_chi2OverDOF'           : ROOT.TH1D('jet_trk_chi2OverDOF','jet_trk_chi2OverDOF;Track #chi^{2}/DOF;Entries',nbins_chi2DOF,min_chi2DOF,max_chi2DOF),
    'jet_trk_truthMatchProbability' : ROOT.TH1D('jet_trk_truthMatchProbability','jet_trk_truthMatchProbability;Track Truth Match Probability;Entries',nbins_TMP,min_TMP,max_TMP),

    # Vertex histograms
    'jet_jf_vtx_ntrk'               : ROOT.TH1D('jet_jf_vtx_ntrk','jet_jf_vtx_ntrk;Number of tracks at JF vertex;Entries',nbins_vtx_trk,min_vtx_trk,max_vtx_trk),

    # Truth B hadron histograms
    'jet_bH_nBHadron'               : ROOT.TH1D('jet_bH_nBHadron','jet_bH_nBHadron;Number of B hadrons;Entries',nbins_nbhadron,min_nbhadron,max_nbhadron),
    'jet_bH_nHFtracks'              : ROOT.TH1D('jet_bH_nHFtracks','jet_bH_nHFtracks;Number of HF tracks from truth bH;Entries',nbins_vtx_trk,min_vtx_trk,max_vtx_trk),
    'jet_bH_Lxy'                    : ROOT.TH1D('jet_bH_Lxy','jet_bH_Lxy;Jet L_{xy}^{bH} [mm];Entries',nbins_Lxy,min_Lxy,max_Lxy),
    'jet_bH_pt_frac'                : ROOT.TH1D('jet_bH_pt_frac','jet_bH_pt_frac;p_{T}^{bH}/p_{T}^{Jet};Entries',nbins_pt_frac,min_pt_frac,max_pt_frac),
    'jet_bH_dRjet'                  : ROOT.TH1D('jet_bH_dRjet','jet_bH_dRjet;dR(B hadron,jet) ;Entries',nbins_dRjet,min_dRjet,max_dRjet),
    'jet_bH_pt'                     : ROOT.TH1D('jet_bH_pt','jet_bH_pt;B hadron p_{T} [GeV];Entries',nbins_pt,min_pt,max_pt),

    # Rates
    'jet_rate_pt'                   : ROOT.TH1D('jet_rate_pt','jet_rate_pt;Jet p_{T} [GeV];Rate',nbins_pt,min_pt,max_pt),
    'jet_rate_dRiso'                : ROOT.TH1D('jet_rate_dRiso','jet_rate_dRiso;Jet dRiso;Rate',nbins_dRiso,min_dRiso,max_dRiso),

    # 2D
    'jet_pt_vs_sv1_Lxy'             : ROOT.TH2D('jet_pt_vs_sv1_Lxy','jet_pt_vs_sv1_Lxy;Jet L_{xy}^{SV1} [mm];Jet p_{T} [GeV];Entries',nbins_Lxy,min_Lxy,max_Lxy,nbins_pt,min_pt,max_pt),
    'jet_pt_vs_ntrk'                : ROOT.TH2D('jet_pt_vs_ntrk','jet_pt_vs_ntrk;Number of tracks in jet;Jet p_{T} [GeV];Entries',nbins_ntrk,min_ntrk,max_ntrk,nbins_pt,min_pt,max_pt),
    'jet_ntrk_vs_sv1_Lxy'           : ROOT.TH2D('jet_ntrk_vs_sv1_Lxy','jet_ntrk_vs_sv1_Lxy;Jet L_{xy}^{SV1} [mm];Number of tracks in jet;Entries',nbins_Lxy,min_Lxy,max_Lxy,nbins_ntrk,min_ntrk,max_ntrk),
    'jet_sv1_vtx_y_vs_x'            : ROOT.TH2D('jet_sv1_vtx_y_vs_x','jet_sv1_vtx_y_vs_x;Jet SV1 vertex x [mm];Jet SV1 vertex y [mm];Entries',nbins_vtxx,min_vtxx,max_vtxx,nbins_vtxy,min_vtxy,max_vtxy),
    'jet_jf_vtx_y_vs_x'             : ROOT.TH2D('jet_jf_vtx_y_vs_x','jet_jf_vtx_y_vs_x;Jet JF vertex x [mm];Jet JF vertex y [mm];Entries',nbins_vtxx,min_vtxx,max_vtxx,nbins_vtxy,min_vtxy,max_vtxy),
    'jet_nHFtracks_vs_ntrk'         : ROOT.TH2D('jet_nHFtracks_vs_ntrk','jet_nHFtracks_vs_ntrk;Number of tracks in jet;Number of HF tracks in jet;Entries',nbins_ntrk,min_ntrk,max_ntrk,nbins_ntrk,min_ntrk,max_ntrk),
    'jet_pt_vs_jet_eta'             : ROOT.TH2D('jet_pt_vs_eta','jet_pt_vs_eta;Jet #eta;Jet p_{T} [GeV];Entries',nbins_eta,min_eta,max_eta,nbins_pt,min_pt,max_pt),
    'jet_bH_pt_vs_jet_eta'          : ROOT.TH2D('jet_bH_pt_vs_eta','jet_bH_pt_vs_eta;Jet #eta;B hadron p_{T} [GeV];Entries',nbins_eta,min_eta,max_eta,nbins_pt,min_pt,max_pt),
    'jet_bH_pt_vs_jet_pt'           : ROOT.TH2D('jet_bH_pt_vs_pt','jet_bH_pt_vs_pt;Jet p_{T} [GeV];B hadron p_{T} [GeV];Entries',nbins_pt,min_pt,max_pt,nbins_pt,min_pt,max_pt),

    # 3D
    'jet_bH_pt_vs_jet_pt_vs_jet_eta' : ROOT.TH3D('jet_bH_pt_vs_eta_vs_pt','jet_bH_pt_vs_eta_vs_pt;Jet #eta;Jet p_{T} [GeV];B hadron p_{T} [GeV];Entries',nbins_eta,min_eta,max_eta,nbins_pt,min_pt,max_pt,nbins_pt,min_pt,max_pt),
    # TODO
    # Do this for HF and frag+PU; HF == p_match>0.75 && barcode<2000000
    #'jet_ntrk_ip3d_vs_bHLxy' : ROOT.TH1D('jet_ntrk_ip3d_vs_bHLxy','jet_ntrk_ip3d_vs_bHLxy',12,0,120),
    #'ljetrej_vs_bjeteff'     : ROOT.TH1D('ljetrej_vs_bjeteff','ljetrej_vs_bjeteff',10,0,1)
}

'''
jet_jf_n2t     : Number of 2-track vertex candidates
jet_jf_ntrkv   : Number of tracks from displaced vertices with at least 2 tracks
jet_jf_nvtx    : Number of displaced vertices with more than 1 track
jet_jf_dR      : DeltaR between the jet axis and vectorial sum of all tracks momenta attached to the displaced vertices 
jet_jf_nvtx1t  : Number of displaced vertices (including one-track vertices)
jet_jf_mass    : Invariant mass of tracks from displaced vertices
jet_jf_efrc    : Fraction of charged jet energy in secondary vertices
jet_jf_sig3    : Significance of the average distance between PV and displaced vertices
jet_LabDr_HadF : jet flavour; 0 = light jet, 4 = c jet, 5 = b jet
jet_trk_orig   : -1 = PUFAKE, 0 = FROMB, 1 = FROMC, 2 = FRAG, 3 = GEANT
jet_dRiso      : Minimum dR between this jet and all other jets in the event
'''

def select_event(event):
    if debug:
        print('Event selection')
        print('Number of jets in event: '+str(event.njets))
    return True

def select_jet(event, ijet, selection = 'baseline'):
    jet_pt = event.jet_pt[ijet]/1000.
    jet_eta = abs(event.jet_eta[ijet])
    jet_LabDr_HadF = event.jet_LabDr_HadF[ijet]
    jet_decay_label = event.jet_decay_label[ijet]
    jet_dRiso = event.jet_dRiso[ijet]
    n_tracks = len(event.jet_trk_d0[ijet])
    
    if debug:
        print('Jet selection of jet '+str(ijet))
        print('Pt of jet in GeV: '+str(jet_pt))
        print('Number of tracks in jet: '+str(n_tracks))
    if selection not in jet_selections:
        raise ValueError('UNKNOWN JET SELECTION: {}'.format(selection))

    # Baseline selection
    if jet_pt < jet_pt_min or jet_pt > jet_pt_max:
        return False
    if jet_eta < jet_eta_min or jet_eta > jet_eta_max:
        return False

    if selection == 'bjet':
        if jet_LabDr_HadF == 5:
            return True
        else:
            return False
    if selection == 'cjet':
        if jet_LabDr_HadF == 4:
            return True
        else:
            return False
    if selection == 'ljet':
        if jet_LabDr_HadF == 0:
            return True
        else:
            return False
    if selection == 'isojet':
        if jet_dRiso > jet_dRiso_cut:
            return True
        else:
            return False
    if 'bjetsemilep' in selection:
        if jet_LabDr_HadF != 5:
            return False
        # Check B hadron children tracks in jet for pdgid (inclusive of C hadron children from B ghosts)
        if selection in ['bjetsemilep_mu','bjetsemilep_el']:
            for jtrk in range(len(event.jet_bH_child_pdg_id[ijet])):
                pdgid = abs(event.jet_bH_child_pdg_id[ijet][jtrk])
                # Only check for electrons (11) and muons (13)
                if pdgid == 13 and selection == 'bjetsemilep_mu':
                    return True
                if pdgid == 11 and selection == 'bjetsemilep_el':
                    return True
        if '_withCghost' in selection:
            for jtrk in range(len(event.jet_bH_child_pdg_id[ijet])):
                pdgid = abs(event.jet_bH_child_pdg_id[ijet][jtrk])
                # Only check for electrons (11) and muons (13)
                if pdgid == 13 and selection == 'bjetsemilep_mu_withCghost':
                    return True
                if pdgid == 11 and selection == 'bjetsemilep_el_withCghost':
                    return True
            # Also loop over C ghosts
            for jtrk in range(len(event.jet_cH_child_pdg_id[ijet])):
                pdgid = abs(event.jet_cH_child_pdg_id[ijet][jtrk])
                # Only check for electrons (11) and muons (13)
                if pdgid == 13 and selection == 'bjetsemilep_mu_withCghost':
                    return True
                if pdgid == 11 and selection == 'bjetsemilep_el_withCghost':
                    return True
        if '_newLabels' in selection:
            if selection == 'bjetsemilep_mu_newLabels' and jet_decay_label == 13:
                return True
            if selection == 'bjetsemilep_el_newLabels' and jet_decay_label == 11:
                return True

        return False

    return True

def get_bjet_decay(event, ijet):
    if event.jet_LabDr_HadF[ijet] != 5:
        raise ValueError('TRYING TO GET DECAY MODE OF NON-BJET: {}'.format(jet_LabDr_HadF))
    if select_jet(event, ijet, 'bjetsemilep_mu'):
        return decay_modes['bjetsemilep_mu']
    if select_jet(event, ijet, 'bjetsemilep_el'):
        return decay_modes['bjetsemilep_el']
    # If not semileptonic, return hadronic
    return decay_modes['bjethadronic']

def select_track(event, ijet, jtrack, selection = 'baseline'):
    if debug:
        print('Track selection of track '+str(jtrack)+' in jet '+str(ijet))
        print('ip3d_d0sig: '+str(event.jet_trk_ip3d_d0sig[ijet][jtrack]))
    if selection not in track_selections:
        raise ValueError('UNKNOWN SELECTION: {}'.format(selection))
    #jet_trk_orig   : PUFAKE = -1, FROMB = 0, FROMC = 1, FRAG = 2, GEANT = 3
    if selection == 'HF':
        if event.jet_trk_orig[ijet][jtrack] == 0 or event.jet_trk_orig[ijet][jtrack] == 1:
            return True
        else:
            return False
    if selection == 'FROMB':
        if event.jet_trk_orig[ijet][jtrack] == 0:
            return True
        else:
            return False
    if selection == 'FROMC':
        if event.jet_trk_orig[ijet][jtrack] == 1:
            return True
        else:
            return False
    if selection == 'PUFAKE':
        if event.jet_trk_orig[ijet][jtrack] == -1:
            return True
        else:
            return False
    if selection == 'LowPt':
        if event.jet_trk_pt[ijet][jtrack]/1000. < 10.:
            return True
        else:
            return False
    return True

def select_vertex(event, ijet, jvtx, vertex_type = 'sv1'):

    # true_Lxy = 1
    # vtx_Lxy = event.jet_sv1_Lxy[ijet]
    # vtx_Lxy_err = 0

    # if vertex_type == 'jf':
    #     print('jf vertex selection not implemented yet')
    #     return False

    # # Loop over the B hadrons and see if the vertex is close to on of the Lxy's
    # for jbhad in range(n_bHadrons):
    #         bH_Lxy     = event.jet_bH_Lxy[ijet][jbhad]
    #         HF_ntracks = event.jet_bH_nBtracks[ijet][jbhad] + event.jet_bH_nCtracks[ijet][jbhad]
    #         bH_nHFtracks += HF_ntracks
    #         hists[file_id+'_jet_bH_nHFtracks'].Fill(HF_ntracks)
    #         hists[file_id+'_jet_bH_Lxy'].Fill(bH_Lxy)
    #         if is_bjet:
    #             hists[file_id+'_jet_bH_nHFtracks_bjet'].Fill(HF_ntracks)
    #             hists[file_id+'_jet_bH_Lxy_bjet'].Fill(bH_Lxy) 


    # if abs(true_Lxy - vtx_Lxy) < (3*vtx_Lxy_err):
    #     return True

    return False

def book_histograms(file_id, skeletons):
    # TODO: MODULARIZE INDIVIDUAL BOOKING; FUNCTION THAT TAKES HISTS, H_ID, AND SUFFIX, AND SKELETON AND CLONES/SETS DIRECTORY
    # Book histograms
    hists = {}
    for var, h_skeleton in skeletons.items():
        h_id = file_id + '_' + var

        # Rates
        if '_rate_' in var:
            selections = []
            if 'dRiso' in var:
                selections.append('')
            else:
                selections.append('')
                selections.append('_iso')
                #selections.append('_debugLxySmall')
                #selections.append('_debugLxyLarge')
            for selection in selections:
                for jflav in jet_flavours:
                    # Denominators
                    hists[h_id+'_'+jflav+selection] = h_skeleton.Clone()
                    hists[h_id+'_'+jflav+selection].SetDirectory(0)
                    # Numerators
                    for alg in algos:
                        hists[h_id+'_'+jflav+'_'+alg+selection] = h_skeleton.Clone()
                        hists[h_id+'_'+jflav+'_'+alg+selection].SetDirectory(0)
                for pt_bin in pt_bins:
                    for jflav in jet_flavours:
                        # Denominators
                        hists[h_id+'_'+jflav+selection+'_'+pt_bin] = h_skeleton.Clone()
                        hists[h_id+'_'+jflav+selection+'_'+pt_bin].SetDirectory(0)
                        # Numerators
                        for alg in algos:
                            hists[h_id+'_'+jflav+'_'+alg+selection+'_'+pt_bin] = h_skeleton.Clone()
                            hists[h_id+'_'+jflav+'_'+alg+selection+'_'+pt_bin].SetDirectory(0)
            continue
        # B hadron histograms
        if 'jet_bH' in var and var != 'jet_bH_nBHadron':
            hists[h_id+'_bjet'] = h_skeleton.Clone()
            hists[h_id+'_bjet'].SetDirectory(0)
            hists[h_id+'_bjet_leadingJet'] = h_skeleton.Clone()
            hists[h_id+'_bjet_leadingJet'].SetDirectory(0)
            hists[h_id+'_bjet_subleadingJet'] = h_skeleton.Clone()
            hists[h_id+'_bjet_subleadingJet'].SetDirectory(0)
            # Special 1bH selection
            hists[h_id+'_bjet_1bH'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH'].SetDirectory(0)
            # Special isojet selection
            hists[h_id+'_bjet_isojet'] = h_skeleton.Clone()
            hists[h_id+'_bjet_isojet'].SetDirectory(0)
            # 1bH and isojet
            hists[h_id+'_bjet_1bH_isojet'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet'].SetDirectory(0)
            # Jets with mismeasured pT
            hists[h_id+'_bjet_1bH_isojet_largeFrag'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet_largeFrag'].SetDirectory(0)
            # Semileptonic decays
            hists[h_id+'_bjet_1bH_isojet_semilep_mu'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet_semilep_mu'].SetDirectory(0)
            hists[h_id+'_bjet_1bH_isojet_semilep_el'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet_semilep_el'].SetDirectory(0)
            hists[h_id+'_bjet_1bH_isojet_hadronic'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet_hadronic'].SetDirectory(0)
            hists[h_id+'_bjet_1bH_isojet_semilep_mu_withCghost'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet_semilep_mu_withCghost'].SetDirectory(0)
            hists[h_id+'_bjet_1bH_isojet_semilep_el_withCghost'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet_semilep_el_withCghost'].SetDirectory(0)
            hists[h_id+'_bjet_1bH_isojet_hadronic_withCghost'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet_hadronic_withCghost'].SetDirectory(0)
            hists[h_id+'_bjet_1bH_isojet_semilep_mu_newLabels'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet_semilep_mu_newLabels'].SetDirectory(0)
            hists[h_id+'_bjet_1bH_isojet_semilep_el_newLabels'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet_semilep_el_newLabels'].SetDirectory(0)
            hists[h_id+'_bjet_1bH_isojet_hadronic_newLabels'] = h_skeleton.Clone()
            hists[h_id+'_bjet_1bH_isojet_hadronic_newLabels'].SetDirectory(0)
            for pt_bin in pt_bins:
                hists[h_id+'_bjet_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_leadingJet_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_leadingJet_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_subleadingJet_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_subleadingJet_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_isojet_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_isojet_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_largeFrag_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_largeFrag_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_semilep_mu_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_semilep_mu_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_semilep_el_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_semilep_el_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_hadronic_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_hadronic_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_semilep_mu_withCghost_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_semilep_mu_withCghost_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_semilep_el_withCghost_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_semilep_el_withCghost_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_hadronic_withCghost_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_hadronic_withCghost_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_semilep_mu_newLabels_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_semilep_mu_newLabels_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_semilep_el_newLabels_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_semilep_el_newLabels_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_1bH_isojet_hadronic_newLabels_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_1bH_isojet_hadronic_newLabels_'+pt_bin].SetDirectory(0)
            continue
     
        # Book inclusive and pT-binned histograms
        hists[h_id] = h_skeleton.Clone()
        hists[h_id].SetDirectory(0)
        for pt_bin in pt_bins:
            if var == 'njets': continue
            hists[h_id+'_'+pt_bin] = h_skeleton.Clone()
            hists[h_id+'_'+pt_bin].SetDirectory(0)
        # Exclusive in jet flavour
        for jflav in jet_flavours:
            hists[h_id+'_'+jflav] = h_skeleton.Clone()
            hists[h_id+'_'+jflav].SetDirectory(0)
            for pt_bin in pt_bins:
                if var == 'njets': continue
                hists[h_id+'_'+jflav+'_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_'+jflav+'_'+pt_bin].SetDirectory(0)
        # Additional bH plots
        if var == 'jet_jf_vtx_ntrk' or var == 'jet_sv1_ntrkv':
            hists[h_id+'_bH'] = h_skeleton.Clone()
            hists[h_id+'_bH'].SetDirectory(0)
            for pt_bin in pt_bins:
                hists[h_id+'_bH_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bH_'+pt_bin].SetDirectory(0)
        if var == 'jet_bH_nBHadron' or var == 'jet_decayMode':
            hists[h_id+'_bjet_leadingJet'] = h_skeleton.Clone()
            hists[h_id+'_bjet_leadingJet'].SetDirectory(0)
            hists[h_id+'_bjet_subleadingJet'] = h_skeleton.Clone()
            hists[h_id+'_bjet_subleadingJet'].SetDirectory(0)
            for pt_bin in pt_bins:
                hists[h_id+'_bjet_leadingJet_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_leadingJet_'+pt_bin].SetDirectory(0)
                hists[h_id+'_bjet_subleadingJet_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_bjet_subleadingJet_'+pt_bin].SetDirectory(0)
        # Event-level variables
        if var == 'njets':
            hists[h_id+'_weighted'] = h_skeleton.Clone()
            hists[h_id+'_weighted'].SetDirectory(0)
            for jflav in jet_flavours:
                hists[h_id+'_'+jflav+'_weighted'] = h_skeleton.Clone()
                hists[h_id+'_'+jflav+'_weighted'].GetXaxis().SetTitle('Number of {}-jets'.format(jflav[0]))
                hists[h_id+'_'+jflav+'_weighted'].SetDirectory(0)
                hists[h_id+'_'+jflav].GetXaxis().SetTitle('Number of {}-jets'.format(jflav[0]))
                if jflav == 'bjet':
                    hists[h_id+'_'+jflav+'_semilep_mu'] = h_skeleton.Clone()
                    hists[h_id+'_'+jflav+'_semilep_mu'].GetXaxis().SetTitle('Number of semileptonic (#mu) b-jets')
                    hists[h_id+'_'+jflav+'_semilep_mu'].SetDirectory(0)
                    hists[h_id+'_'+jflav+'_semilep_el'] = h_skeleton.Clone()
                    hists[h_id+'_'+jflav+'_semilep_el'].GetXaxis().SetTitle('Number of semileptonic (e) b-jets')
                    hists[h_id+'_'+jflav+'_semilep_el'].SetDirectory(0)
                    hists[h_id+'_'+jflav+'_semilep_mu_weighted'] = h_skeleton.Clone()
                    hists[h_id+'_'+jflav+'_semilep_mu_weighted'].GetXaxis().SetTitle('Number of semileptonic (#mu) b-jets')
                    hists[h_id+'_'+jflav+'_semilep_mu_weighted'].SetDirectory(0)
                    hists[h_id+'_'+jflav+'_semilep_el_weighted'] = h_skeleton.Clone()
                    hists[h_id+'_'+jflav+'_semilep_el_weighted'].GetXaxis().SetTitle('Number of semileptonic (e) b-jets')
                    hists[h_id+'_'+jflav+'_semilep_el_weighted'].SetDirectory(0)
                    hists[h_id+'_'+jflav+'_hadronic'] = h_skeleton.Clone()
                    hists[h_id+'_'+jflav+'_hadronic'].GetXaxis().SetTitle('Number of hadronic b-jets')
                    hists[h_id+'_'+jflav+'_hadronic'].SetDirectory(0)
                    hists[h_id+'_'+jflav+'_hadronic_weighted'] = h_skeleton.Clone()
                    hists[h_id+'_'+jflav+'_hadronic_weighted'].GetXaxis().SetTitle('Number of hadronic b-jets')
                    hists[h_id+'_'+jflav+'_hadronic_weighted'].SetDirectory(0)
        # Primary vertex quantities
        if 'PV' in var:
            hists[h_id+'_weighted'] = h_skeleton.Clone()
            hists[h_id+'_weighted'].SetDirectory(0)
            hists[h_id+'_HF'] = h_skeleton.Clone()
            hists[h_id+'_HF'].SetDirectory(0)
            hists[h_id+'_HF_weighted'] = h_skeleton.Clone()
            hists[h_id+'_HF_weighted'].SetDirectory(0)
            for jflav in jet_flavours:
                hists[h_id+'_'+jflav+'_weighted'] = h_skeleton.Clone()
                hists[h_id+'_'+jflav+'_weighted'].SetDirectory(0)
        # Track-level variables
        if 'jet_trk' in var:
            hists[h_id+'_HF'] = h_skeleton.Clone()
            hists[h_id+'_HF'].SetDirectory(0)
            hists[h_id+'_nonHF'] = h_skeleton.Clone()
            hists[h_id+'_nonHF'].SetDirectory(0)
            for pt_bin in pt_bins:
                hists[h_id+'_HF_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_HF_'+pt_bin].SetDirectory(0)
                hists[h_id+'_nonHF_'+pt_bin] = h_skeleton.Clone()
                hists[h_id+'_nonHF_'+pt_bin].SetDirectory(0)

        # Debug small Lxy tracks...
        # if 'jet_pt' in var or 'jet_eta' in var or 'jet_phi' in var or 'jet_ntrk' in var or 'jet_dRiso' in var or 'jet_trk' in var:
        #     hists[h_id+'_debugLxySmall'] = h_skeleton.Clone()
        #     hists[h_id+'_debugLxySmall'].SetDirectory(0)
        #     hists[h_id+'_debugLxyLarge'] = h_skeleton.Clone()
        #     hists[h_id+'_debugLxyLarge'].SetDirectory(0)

    return hists

def fill_SV1_histograms(t, ijet, is_jet_flavours, hists, file_id, weight = 1., pt_bin = ''):
    jet_pt = t.jet_pt[ijet]/1000.
    jet_ntracks = t.jet_btag_ntrk[ijet]

    sv1_vtxx = t.jet_sv1_vtx_x[ijet][0]
    sv1_vtxy = t.jet_sv1_vtx_y[ijet][0]
    sv1_vtxz = t.jet_sv1_vtx_z[ijet][0]

    jet_sv1_m = t.jet_sv1_m[ijet]/1000.
    jet_sv1_efc = t.jet_sv1_efc[ijet]
    jet_sv1_ntrkv = t.jet_sv1_ntrkv[ijet]
    jet_sv1_n2t = t.jet_sv1_n2t[ijet]
    jet_sv1_Lxy = t.jet_sv1_Lxy[ijet]
    jet_sv1_L3d = t.jet_sv1_L3d[ijet]
    jet_sv1_sig3d = t.jet_sv1_sig3d[ijet]
    jet_sv1_deltaR = t.jet_sv1_deltaR[ijet]
    jet_sv1_llr = t.sv1_llr[ijet]

    hists[file_id+'_jet_sv1_m'+pt_bin].Fill(jet_sv1_m, weight)
    hists[file_id+'_jet_sv1_efc'+pt_bin].Fill(jet_sv1_efc, weight)
    hists[file_id+'_jet_sv1_ntrkv'+pt_bin].Fill(jet_sv1_ntrkv, weight)
    hists[file_id+'_jet_sv1_n2t'+pt_bin].Fill(jet_sv1_n2t, weight)
    hists[file_id+'_jet_sv1_Lxy'+pt_bin].Fill(jet_sv1_Lxy, weight)
    hists[file_id+'_jet_sv1_L3d'+pt_bin].Fill(jet_sv1_L3d, weight)
    hists[file_id+'_jet_sv1_sig3d'+pt_bin].Fill(jet_sv1_sig3d, weight)
    hists[file_id+'_jet_sv1_deltaR'+pt_bin].Fill(jet_sv1_deltaR, weight)
    hists[file_id+'_jet_sv1_llr'+pt_bin].Fill(jet_sv1_llr, weight)
    # 2D
    hists[file_id+'_jet_sv1_vtx_y_vs_x'+pt_bin].Fill(sv1_vtxx, sv1_vtxy, weight)
    hists[file_id+'_jet_pt_vs_sv1_Lxy'+pt_bin].Fill(jet_sv1_Lxy, jet_pt, weight)
    hists[file_id+'_jet_ntrk_vs_sv1_Lxy'+pt_bin].Fill(jet_sv1_Lxy, jet_ntracks, weight)

    for jflav, is_jflav in zip(jet_flavours, is_jet_flavours):
        if is_jflav:
            hists[file_id+'_jet_sv1_m_'+jflav+pt_bin].Fill(jet_sv1_m, weight)
            hists[file_id+'_jet_sv1_efc_'+jflav+pt_bin].Fill(jet_sv1_efc, weight)
            hists[file_id+'_jet_sv1_ntrkv_'+jflav+pt_bin].Fill(jet_sv1_ntrkv, weight)
            hists[file_id+'_jet_sv1_n2t_'+jflav+pt_bin].Fill(jet_sv1_n2t, weight)
            hists[file_id+'_jet_sv1_Lxy_'+jflav+pt_bin].Fill(jet_sv1_Lxy, weight)
            hists[file_id+'_jet_sv1_L3d_'+jflav+pt_bin].Fill(jet_sv1_L3d, weight)
            hists[file_id+'_jet_sv1_sig3d_'+jflav+pt_bin].Fill(jet_sv1_sig3d, weight)
            hists[file_id+'_jet_sv1_deltaR_'+jflav+pt_bin].Fill(jet_sv1_deltaR, weight)
            hists[file_id+'_jet_sv1_llr_'+jflav+pt_bin].Fill(jet_sv1_llr, weight)
            # 2D
            hists[file_id+'_jet_sv1_vtx_y_vs_x_'+jflav+pt_bin].Fill(sv1_vtxx, sv1_vtxy, weight)
            hists[file_id+'_jet_pt_vs_sv1_Lxy_'+jflav+pt_bin].Fill(jet_sv1_Lxy, jet_pt, weight)
            hists[file_id+'_jet_ntrk_vs_sv1_Lxy_'+jflav+pt_bin].Fill(jet_sv1_Lxy, jet_ntracks, weight)

def fill_JF_histograms(t, ijet, jvtx, is_jet_flavours, hists, file_id, weight = 1., pt_bin = ''):
    jf_vtxx = t.jet_jf_vtx_x[ijet][jvtx]
    jf_vtxy = t.jet_jf_vtx_y[ijet][jvtx]
    jf_vtx_ntrk = t.jet_jf_vtx_ntrk[ijet][jvtx]
    hists[file_id+'_jet_jf_vtx_y_vs_x'+pt_bin].Fill(jf_vtxx, jf_vtxy, weight)
    hists[file_id+'_jet_jf_vtx_ntrk'+pt_bin].Fill(jf_vtx_ntrk, weight)
    for jflav, is_jflav in zip(jet_flavours, is_jet_flavours):
        if is_jflav:
            hists[file_id+'_jet_jf_vtx_y_vs_x_'+jflav+pt_bin].Fill(jf_vtxx, jf_vtxy, weight)
            hists[file_id+'_jet_jf_vtx_ntrk_'+jflav+pt_bin].Fill(jf_vtx_ntrk, weight)

def fill_bH_histograms(t, ijet, jbhad, is_jet_flavours, hists, file_id, weight = 1., selection = ''):
    if not is_jet_flavours[0]: raise ValueError('FILLING B HADRON HISTOGRAM FOR NON-B-JET')
    jet_pt     = t.jet_pt[ijet]/1000.
    jet_eta    = t.jet_eta[ijet]
    bH_Lxy     = t.jet_bH_Lxy[ijet][jbhad]
    HF_ntracks = t.jet_bH_nBtracks[ijet][jbhad] + t.jet_bH_nCtracks[ijet][jbhad]
    bH_pt      = t.jet_bH_pt[ijet][jbhad]/1000.
    bH_dRjet   = t.jet_bH_dRjet[ijet][jbhad]
    hists[file_id+'_jet_bH_nHFtracks_bjet'+selection].Fill(HF_ntracks, weight)
    hists[file_id+'_jet_bH_Lxy_bjet'+selection].Fill(bH_Lxy, weight)
    hists[file_id+'_jet_bH_pt_frac_bjet'+selection].Fill(bH_pt/jet_pt, weight)
    hists[file_id+'_jet_bH_pt_bjet'+selection].Fill(bH_pt, weight)
    hists[file_id+'_jet_bH_dRjet_bjet'+selection].Fill(bH_dRjet, weight)
    hists[file_id+'_jet_bH_pt_vs_jet_eta_bjet'+selection].Fill(jet_eta, bH_pt, weight)
    hists[file_id+'_jet_bH_pt_vs_jet_pt_bjet'+selection].Fill(jet_pt, bH_pt, weight)

def fill_track_histograms(t, ijet, jtrk, is_jet_flavours, hists, file_id, weight = 1., pt_bin = '', debugLxy=False):    
    # Need this to correct truth z0
    truth_PVz = t.truth_PVz

    is_HF_track = select_track(t, ijet, jtrk, 'HF')
    
    trk_theta          = t.jet_trk_theta[ijet][jtrk]

    trk_ip3d_d0sig     = t.jet_trk_ip3d_d0sig[ijet][jtrk]
    trk_d0             = t.jet_trk_d0[ijet][jtrk]
    trk_d0Truth        = t.jet_trk_d0Truth[ijet][jtrk]
    # Not sure why these two are not equal: math.sqrt(t.jet_trk_cov_d0d0[ijet][jtrk])
    trk_d0Err          = t.jet_trk_d0Err[ijet][jtrk]
    trk_d0res          = trk_d0 - trk_d0Truth
    trk_d0pull         = trk_d0res/trk_d0Err

    trk_ip3d_z0sig     = t.jet_trk_ip3d_z0sig[ijet][jtrk]
    # Try NOTE2 from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/Run2BtagOptimisationFramework?rev=48
    trk_z0             = t.jet_trk_z0[ijet][jtrk]
    trk_z0Truth        = t.jet_trk_z0Truth[ijet][jtrk] - truth_PVz
    trk_z0sintheta     = trk_z0*math.sin(trk_theta)
    # Not sure why these two are not equal: math.sqrt(t.jet_trk_cov_z0z0[ijet][jtrk])
    trk_z0Err          = t.jet_trk_z0Err[ijet][jtrk]
    trk_z0res          = trk_z0 - trk_z0Truth
    trk_z0pull         = trk_z0res/trk_z0Err
    trk_phi            = t.jet_trk_phi[ijet][jtrk]
    trk_phiErr         = math.sqrt(t.jet_trk_cov_phiphi[ijet][jtrk])
    trk_theta          = t.jet_trk_theta[ijet][jtrk]
    trk_thetaErr       = math.sqrt(t.jet_trk_cov_thetatheta[ijet][jtrk])
    trk_qop            = t.jet_trk_qoverp[ijet][jtrk]*1000.
    trk_qopErr         = math.sqrt(t.jet_trk_cov_qoverpqoverp[ijet][jtrk])*1000.
    trk_pt             = t.jet_trk_pt[ijet][jtrk]/1000.
    trk_nIBLHits       = t.jet_trk_nInnHits[ijet][jtrk]
    trk_nPixHits       = t.jet_trk_nPixHits[ijet][jtrk]
    trk_nsharedPixHits = t.jet_trk_nsharedPixHits[ijet][jtrk]
    trk_nsplitPixHits  = t.jet_trk_nsplitPixHits[ijet][jtrk]
    trk_nPixHoles      = t.jet_trk_nPixHoles[ijet][jtrk]
    trk_nSCTHits       = t.jet_trk_nSCTHits[ijet][jtrk]
    trk_nsharedSCTHits = t.jet_trk_nsharedSCTHits[ijet][jtrk]
    trk_nSCTHoles      = t.jet_trk_nSCTHoles[ijet][jtrk]
    trk_nPixSCTHits    = trk_nPixHits + trk_nSCTHits
    trk_chi2           = t.jet_trk_chi2[ijet][jtrk]
    trk_ndf            = t.jet_trk_ndf[ijet][jtrk]
    trk_TMP            = t.jet_trk_truthMatchProbability[ijet][jtrk]
    try:
        trk_chi2OverDOF = trk_chi2/trk_ndf
    except ZeroDivisionError:
        print('Division by zero, setting chi2/DOF to chi2: chi2 = {}, DOF = {}'.format(trk_chi2, trk_ndf))
        trk_chi2OverDOF = trk_chi2
    
    hists[file_id+'_jet_trk_ip3d_d0sig'+pt_bin].Fill(trk_ip3d_d0sig, weight)
    hists[file_id+'_jet_trk_d0'+pt_bin].Fill(trk_d0, weight)
    hists[file_id+'_jet_trk_d0Truth'+pt_bin].Fill(trk_d0Truth, weight)
    hists[file_id+'_jet_trk_d0Err'+pt_bin].Fill(trk_d0Err, weight)
    hists[file_id+'_jet_trk_d0res'+pt_bin].Fill(trk_d0res, weight)
    hists[file_id+'_jet_trk_d0pull'+pt_bin].Fill(trk_d0pull, weight)

    hists[file_id+'_jet_trk_ip3d_z0sig'+pt_bin].Fill(trk_ip3d_z0sig, weight)
    hists[file_id+'_jet_trk_z0'+pt_bin].Fill(trk_z0, weight)
    hists[file_id+'_jet_trk_z0Truth'+pt_bin].Fill(trk_z0Truth, weight)
    hists[file_id+'_jet_trk_z0sintheta'+pt_bin].Fill(trk_z0sintheta, weight)
    hists[file_id+'_jet_trk_z0Err'+pt_bin].Fill(trk_z0Err, weight)
    hists[file_id+'_jet_trk_z0res'+pt_bin].Fill(trk_z0res, weight)
    hists[file_id+'_jet_trk_z0pull'+pt_bin].Fill(trk_z0pull, weight)

    hists[file_id+'_jet_trk_phi'+pt_bin].Fill(trk_phi, weight)
    hists[file_id+'_jet_trk_phiErr'+pt_bin].Fill(trk_phiErr, weight)
    hists[file_id+'_jet_trk_theta'+pt_bin].Fill(trk_theta, weight)
    hists[file_id+'_jet_trk_thetaErr'+pt_bin].Fill(trk_thetaErr, weight)
    hists[file_id+'_jet_trk_qop'+pt_bin].Fill(trk_qop, weight)
    hists[file_id+'_jet_trk_qopErr'+pt_bin].Fill(trk_qopErr, weight)
    hists[file_id+'_jet_trk_pt'+pt_bin].Fill(trk_pt, weight)

    hists[file_id+'_jet_trk_nIBLHits'+pt_bin].Fill(trk_nIBLHits, weight)
    hists[file_id+'_jet_trk_nPixHits'+pt_bin].Fill(trk_nPixHits, weight)
    hists[file_id+'_jet_trk_nsharedPixHits'+pt_bin].Fill(trk_nsharedPixHits, weight)
    hists[file_id+'_jet_trk_nsplitPixHits'+pt_bin].Fill(trk_nsplitPixHits, weight)
    hists[file_id+'_jet_trk_nPixHoles'+pt_bin].Fill(trk_nPixHoles, weight)
    hists[file_id+'_jet_trk_nSCTHits'+pt_bin].Fill(trk_nSCTHits, weight)
    hists[file_id+'_jet_trk_nsharedSCTHits'+pt_bin].Fill(trk_nsharedSCTHits, weight)
    hists[file_id+'_jet_trk_nSCTHoles'+pt_bin].Fill(trk_nSCTHoles, weight)
    hists[file_id+'_jet_trk_nPixSCTHits'+pt_bin].Fill(trk_nPixSCTHits, weight)
    hists[file_id+'_jet_trk_chi2'+pt_bin].Fill(trk_chi2, weight)
    hists[file_id+'_jet_trk_chi2OverDOF'+pt_bin].Fill(trk_chi2OverDOF, weight)
    hists[file_id+'_jet_trk_truthMatchProbability'+pt_bin].Fill(trk_TMP, weight)

    for jflav, is_jflav in zip(jet_flavours, is_jet_flavours):
        if is_jflav:
            hists[file_id+'_jet_trk_ip3d_d0sig_'+jflav+pt_bin].Fill(trk_ip3d_d0sig, weight)
            hists[file_id+'_jet_trk_d0_'+jflav+pt_bin].Fill(trk_d0, weight)
            hists[file_id+'_jet_trk_d0Truth_'+jflav+pt_bin].Fill(trk_d0Truth, weight)
            hists[file_id+'_jet_trk_d0Err_'+jflav+pt_bin].Fill(trk_d0Err, weight)
            hists[file_id+'_jet_trk_d0res_'+jflav+pt_bin].Fill(trk_d0res, weight)
            hists[file_id+'_jet_trk_d0pull_'+jflav+pt_bin].Fill(trk_d0pull, weight)

            hists[file_id+'_jet_trk_ip3d_z0sig_'+jflav+pt_bin].Fill(trk_ip3d_z0sig, weight)
            hists[file_id+'_jet_trk_z0_'+jflav+pt_bin].Fill(trk_z0, weight)
            hists[file_id+'_jet_trk_z0Truth_'+jflav+pt_bin].Fill(trk_z0Truth, weight)
            hists[file_id+'_jet_trk_z0sintheta_'+jflav+pt_bin].Fill(trk_z0sintheta, weight)
            hists[file_id+'_jet_trk_z0Err_'+jflav+pt_bin].Fill(trk_z0Err, weight)
            hists[file_id+'_jet_trk_z0res_'+jflav+pt_bin].Fill(trk_z0res, weight)
            hists[file_id+'_jet_trk_z0pull_'+jflav+pt_bin].Fill(trk_z0pull, weight)

            hists[file_id+'_jet_trk_phi_'+jflav+pt_bin].Fill(trk_phi, weight)
            hists[file_id+'_jet_trk_phiErr_'+jflav+pt_bin].Fill(trk_phiErr, weight)
            hists[file_id+'_jet_trk_theta_'+jflav+pt_bin].Fill(trk_theta, weight)
            hists[file_id+'_jet_trk_thetaErr_'+jflav+pt_bin].Fill(trk_thetaErr, weight)
            hists[file_id+'_jet_trk_qop_'+jflav+pt_bin].Fill(trk_qop, weight)
            hists[file_id+'_jet_trk_qopErr_'+jflav+pt_bin].Fill(trk_qopErr, weight)
            hists[file_id+'_jet_trk_pt_'+jflav+pt_bin].Fill(trk_pt, weight)

            hists[file_id+'_jet_trk_nIBLHits_'+jflav+pt_bin].Fill(trk_nIBLHits, weight)
            hists[file_id+'_jet_trk_nPixHits_'+jflav+pt_bin].Fill(trk_nPixHits, weight)
            hists[file_id+'_jet_trk_nsharedPixHits_'+jflav+pt_bin].Fill(trk_nsharedPixHits, weight)
            hists[file_id+'_jet_trk_nsplitPixHits_'+jflav+pt_bin].Fill(trk_nsplitPixHits, weight)
            hists[file_id+'_jet_trk_nPixHoles_'+jflav+pt_bin].Fill(trk_nPixHoles, weight)
            hists[file_id+'_jet_trk_nSCTHits_'+jflav+pt_bin].Fill(trk_nSCTHits, weight)
            hists[file_id+'_jet_trk_nsharedSCTHits_'+jflav+pt_bin].Fill(trk_nsharedSCTHits, weight)
            hists[file_id+'_jet_trk_nSCTHoles_'+jflav+pt_bin].Fill(trk_nSCTHoles, weight)
            hists[file_id+'_jet_trk_nPixSCTHits_'+jflav+pt_bin].Fill(trk_nPixSCTHits, weight)
            hists[file_id+'_jet_trk_chi2_'+jflav+pt_bin].Fill(trk_chi2, weight)
            hists[file_id+'_jet_trk_chi2OverDOF_'+jflav+pt_bin].Fill(trk_chi2OverDOF, weight)
            hists[file_id+'_jet_trk_truthMatchProbability_'+jflav+pt_bin].Fill(trk_TMP, weight)

    if is_HF_track:
        hf_sel = '_HF'
    else:
        hf_sel = '_nonHF'
    hists[file_id+'_jet_trk_ip3d_d0sig'+hf_sel+pt_bin].Fill(trk_ip3d_d0sig, weight)
    hists[file_id+'_jet_trk_d0'+hf_sel+pt_bin].Fill(trk_d0, weight)
    hists[file_id+'_jet_trk_d0Truth'+hf_sel+pt_bin].Fill(trk_d0Truth, weight)
    hists[file_id+'_jet_trk_d0Err'+hf_sel+pt_bin].Fill(trk_d0Err, weight)
    hists[file_id+'_jet_trk_d0res'+hf_sel+pt_bin].Fill(trk_d0res, weight)
    hists[file_id+'_jet_trk_d0pull'+hf_sel+pt_bin].Fill(trk_d0pull, weight)
    
    hists[file_id+'_jet_trk_ip3d_z0sig'+hf_sel+pt_bin].Fill(trk_ip3d_z0sig, weight)
    hists[file_id+'_jet_trk_z0'+hf_sel+pt_bin].Fill(trk_z0, weight)
    hists[file_id+'_jet_trk_z0Truth'+hf_sel+pt_bin].Fill(trk_z0Truth, weight)
    hists[file_id+'_jet_trk_z0sintheta'+hf_sel+pt_bin].Fill(trk_z0sintheta, weight)
    hists[file_id+'_jet_trk_z0Err'+hf_sel+pt_bin].Fill(trk_z0Err, weight)
    hists[file_id+'_jet_trk_z0res'+hf_sel+pt_bin].Fill(trk_z0res, weight)
    hists[file_id+'_jet_trk_z0pull'+hf_sel+pt_bin].Fill(trk_z0pull, weight)

    hists[file_id+'_jet_trk_phi'+hf_sel+pt_bin].Fill(trk_phi, weight)
    hists[file_id+'_jet_trk_phiErr'+hf_sel+pt_bin].Fill(trk_phiErr, weight)
    hists[file_id+'_jet_trk_theta'+hf_sel+pt_bin].Fill(trk_theta, weight)
    hists[file_id+'_jet_trk_thetaErr'+hf_sel+pt_bin].Fill(trk_thetaErr, weight)
    hists[file_id+'_jet_trk_qop'+hf_sel+pt_bin].Fill(trk_qop, weight)
    hists[file_id+'_jet_trk_qopErr'+hf_sel+pt_bin].Fill(trk_qopErr, weight)
    hists[file_id+'_jet_trk_pt'+hf_sel+pt_bin].Fill(trk_pt, weight)

    hists[file_id+'_jet_trk_nIBLHits'+hf_sel+pt_bin].Fill(trk_nIBLHits, weight)
    hists[file_id+'_jet_trk_nPixHits'+hf_sel+pt_bin].Fill(trk_nPixHits, weight)
    hists[file_id+'_jet_trk_nsharedPixHits'+hf_sel+pt_bin].Fill(trk_nsharedPixHits, weight)
    hists[file_id+'_jet_trk_nsplitPixHits'+hf_sel+pt_bin].Fill(trk_nsplitPixHits, weight)
    hists[file_id+'_jet_trk_nPixHoles'+hf_sel+pt_bin].Fill(trk_nPixHoles, weight)
    hists[file_id+'_jet_trk_nSCTHits'+hf_sel+pt_bin].Fill(trk_nSCTHits, weight)
    hists[file_id+'_jet_trk_nsharedSCTHits'+hf_sel+pt_bin].Fill(trk_nsharedSCTHits, weight)
    hists[file_id+'_jet_trk_nSCTHoles'+hf_sel+pt_bin].Fill(trk_nSCTHoles, weight)
    hists[file_id+'_jet_trk_nPixSCTHits'+hf_sel+pt_bin].Fill(trk_nPixSCTHits, weight)
    hists[file_id+'_jet_trk_chi2'+hf_sel+pt_bin].Fill(trk_chi2, weight)
    hists[file_id+'_jet_trk_chi2OverDOF'+hf_sel+pt_bin].Fill(trk_chi2OverDOF, weight)
    hists[file_id+'_jet_trk_truthMatchProbability'+hf_sel+pt_bin].Fill(trk_TMP, weight)

    # Investigate small SV1 Lxy jets...
    if debugLxy and t.jet_sv1_Nvtx[ijet]>0:
        if t.jet_sv1_Lxy[ijet] < debugLxy_cutoff:
            hists[file_id+'_jet_trk_d0_debugLxySmall'+pt_bin].Fill(trk_d0, weight)
            hists[file_id+'_jet_trk_d0Err_debugLxySmall'+pt_bin].Fill(trk_d0Err, weight)
            hists[file_id+'_jet_trk_ip3d_d0sig_debugLxySmall'+pt_bin].Fill(trk_ip3d_d0sig, weight)
            hists[file_id+'_jet_trk_d0res_debugLxySmall'+pt_bin].Fill(trk_d0res, weight)
            hists[file_id+'_jet_trk_d0pull_debugLxySmall'+pt_bin].Fill(trk_d0pull, weight)

            hists[file_id+'_jet_trk_ip3d_z0sig_debugLxySmall'+pt_bin].Fill(trk_ip3d_z0sig, weight)
            hists[file_id+'_jet_trk_z0sintheta_debugLxySmall'+pt_bin].Fill(trk_z0sintheta, weight)
            hists[file_id+'_jet_trk_z0Err_debugLxySmall'+pt_bin].Fill(trk_z0Err, weight)
            hists[file_id+'_jet_trk_z0res_debugLxySmall'+pt_bin].Fill(trk_z0res, weight)
            hists[file_id+'_jet_trk_z0pull_debugLxySmall'+pt_bin].Fill(trk_z0pull, weight)

            hists[file_id+'_jet_trk_phi_debugLxySmall'+pt_bin].Fill(trk_phi, weight)
            hists[file_id+'_jet_trk_phiErr_debugLxySmall'+pt_bin].Fill(trk_phiErr, weight)
            hists[file_id+'_jet_trk_theta_debugLxySmall'+pt_bin].Fill(trk_theta, weight)
            hists[file_id+'_jet_trk_thetaErr_debugLxySmall'+pt_bin].Fill(trk_thetaErr, weight)
            hists[file_id+'_jet_trk_qop_debugLxySmall'+pt_bin].Fill(trk_qop, weight)
            hists[file_id+'_jet_trk_qopErr_debugLxySmall'+pt_bin].Fill(trk_qopErr, weight)
            hists[file_id+'_jet_trk_pt_debugLxySmall'+pt_bin].Fill(trk_pt, weight)

            hists[file_id+'_jet_trk_nIBLHits_debugLxySmall'+pt_bin].Fill(trk_nIBLHits, weight)
            hists[file_id+'_jet_trk_nPixHits_debugLxySmall'+pt_bin].Fill(trk_nPixHits, weight)
            hists[file_id+'_jet_trk_nsharedPixHits_debugLxySmall'+pt_bin].Fill(trk_nsharedPixHits, weight)
            hists[file_id+'_jet_trk_nsplitPixHits_debugLxySmall'+pt_bin].Fill(trk_nsplitPixHits, weight)
            hists[file_id+'_jet_trk_nPixHoles_debugLxySmall'+pt_bin].Fill(trk_nPixHoles, weight)
            hists[file_id+'_jet_trk_nSCTHits_debugLxySmall'+pt_bin].Fill(trk_nSCTHits, weight)
            hists[file_id+'_jet_trk_nsharedSCTHits_debugLxySmall'+pt_bin].Fill(trk_nsharedSCTHits, weight)
            hists[file_id+'_jet_trk_nSCTHoles_debugLxySmall'+pt_bin].Fill(trk_nSCTHoles, weight)
            hists[file_id+'_jet_trk_nPixSCTHits_debugLxySmall'+pt_bin].Fill(trk_nPixSCTHits, weight)
            hists[file_id+'_jet_trk_chi2_debugLxySmall'+pt_bin].Fill(trk_chi2, weight)
            hists[file_id+'_jet_trk_chi2OverDOF_debugLxySmall'+pt_bin].Fill(trk_chi2OverDOF, weight)
            hists[file_id+'_jet_trk_truthMatchProbability_debugLxySmall'+pt_bin].Fill(trk_TMP, weight)
        else:
            hists[file_id+'_jet_trk_d0_debugLxyLarge'+pt_bin].Fill(trk_d0, weight)
            hists[file_id+'_jet_trk_d0Err_debugLxyLarge'+pt_bin].Fill(trk_d0Err, weight)
            hists[file_id+'_jet_trk_ip3d_d0sig_debugLxyLarge'+pt_bin].Fill(trk_ip3d_d0sig, weight)
            hists[file_id+'_jet_trk_d0res_debugLxyLarge'+pt_bin].Fill(trk_d0res, weight)
            hists[file_id+'_jet_trk_d0pull_debugLxyLarge'+pt_bin].Fill(trk_d0pull, weight)

            hists[file_id+'_jet_trk_ip3d_z0sig_debugLxyLarge'+pt_bin].Fill(trk_ip3d_z0sig, weight)
            hists[file_id+'_jet_trk_z0sintheta_debugLxyLarge'+pt_bin].Fill(trk_z0sintheta, weight)
            hists[file_id+'_jet_trk_z0Err_debugLxyLarge'+pt_bin].Fill(trk_z0Err, weight)
            hists[file_id+'_jet_trk_z0res_debugLxyLarge'+pt_bin].Fill(trk_z0res, weight)
            hists[file_id+'_jet_trk_z0pull_debugLxyLarge'+pt_bin].Fill(trk_z0pull, weight)

            hists[file_id+'_jet_trk_phi_debugLxyLarge'+pt_bin].Fill(trk_phi, weight)
            hists[file_id+'_jet_trk_phiErr_debugLxyLarge'+pt_bin].Fill(trk_phiErr, weight)
            hists[file_id+'_jet_trk_theta_debugLxyLarge'+pt_bin].Fill(trk_theta, weight)
            hists[file_id+'_jet_trk_thetaErr_debugLxyLarge'+pt_bin].Fill(trk_thetaErr, weight)
            hists[file_id+'_jet_trk_qop_debugLxyLarge'+pt_bin].Fill(trk_qop, weight)
            hists[file_id+'_jet_trk_qopErr_debugLxyLarge'+pt_bin].Fill(trk_qopErr, weight)
            hists[file_id+'_jet_trk_pt_debugLxyLarge'+pt_bin].Fill(trk_pt, weight)

            hists[file_id+'_jet_trk_nIBLHits_debugLxyLarge'+pt_bin].Fill(trk_nIBLHits, weight)
            hists[file_id+'_jet_trk_nPixHits_debugLxyLarge'+pt_bin].Fill(trk_nPixHits, weight)
            hists[file_id+'_jet_trk_nsharedPixHits_debugLxyLarge'+pt_bin].Fill(trk_nsharedPixHits, weight)
            hists[file_id+'_jet_trk_nsplitPixHits_debugLxyLarge'+pt_bin].Fill(trk_nsplitPixHits, weight)
            hists[file_id+'_jet_trk_nPixHoles_debugLxyLarge'+pt_bin].Fill(trk_nPixHoles, weight)
            hists[file_id+'_jet_trk_nSCTHits_debugLxyLarge'+pt_bin].Fill(trk_nSCTHits, weight)
            hists[file_id+'_jet_trk_nsharedSCTHits_debugLxyLarge'+pt_bin].Fill(trk_nsharedSCTHits, weight)
            hists[file_id+'_jet_trk_nSCTHoles_debugLxyLarge'+pt_bin].Fill(trk_nSCTHoles, weight)
            hists[file_id+'_jet_trk_nPixSCTHits_debugLxyLarge'+pt_bin].Fill(trk_nPixSCTHits, weight)
            hists[file_id+'_jet_trk_chi2_debugLxyLarge'+pt_bin].Fill(trk_chi2, weight)
            hists[file_id+'_jet_trk_chi2OverDOF_debugLxyLarge'+pt_bin].Fill(trk_chi2OverDOF, weight)
            hists[file_id+'_jet_trk_truthMatchProbability_debugLxyLarge'+pt_bin].Fill(trk_TMP, weight)

def fill_jet_histograms(t, ijet, is_jet_flavours, is_iso, hists, file_id, weight = 1., pt_bin = ''):
    # Get relevant jet quantities
    jet_pt = t.jet_pt[ijet]/1000.
    jet_eta = t.jet_eta[ijet]
    jet_phi = t.jet_phi[ijet]
    jet_ntracks = t.jet_btag_ntrk[ijet]
    jet_dRiso = t.jet_dRiso[ijet]

    jet_ip2 = t.jet_ip2[ijet]
    jet_ip2_c = t.jet_ip2_c[ijet]
    jet_ip2_cu = t.jet_ip2_cu[ijet]
    jet_ip3 = t.jet_ip3[ijet]
    jet_ip3_c = t.jet_ip3_c[ijet]
    jet_ip3_cu = t.jet_ip3_cu[ijet]

    jet_sv1_Nvtx = t.jet_sv1_Nvtx[ijet]# Number of sv1 vertices
    jet_jf_nvtx = t.jet_jf_nvtx[ijet]# Number of jet fitter vertices

    jet_nBHadrons = t.jet_nBHadr[ijet]

    # Fill relevant histograms
    hists[file_id+'_jet_pt'+pt_bin].Fill(jet_pt, weight)
    hists[file_id+'_jet_eta'+pt_bin].Fill(jet_eta, weight)
    hists[file_id+'_jet_phi'+pt_bin].Fill(jet_phi, weight)
    hists[file_id+'_jet_ntrk'+pt_bin].Fill(jet_ntracks, weight)
    hists[file_id+'_jet_dRiso'+pt_bin].Fill(jet_dRiso, weight)
    # IPX
    hists[file_id+'_jet_ip2'+pt_bin].Fill(jet_ip2, weight)
    hists[file_id+'_jet_ip2_c'+pt_bin].Fill(jet_ip2_c, weight)
    hists[file_id+'_jet_ip2_cu'+pt_bin].Fill(jet_ip2_cu, weight)
    hists[file_id+'_jet_ip3'+pt_bin].Fill(jet_ip3, weight)
    hists[file_id+'_jet_ip3_c'+pt_bin].Fill(jet_ip3_c, weight)
    hists[file_id+'_jet_ip3_cu'+pt_bin].Fill(jet_ip3_cu, weight)
    # Algos
    hists[file_id+'_jet_sv1_Nvtx'+pt_bin].Fill(jet_sv1_Nvtx, weight)
    hists[file_id+'_jet_jf_nvtx'+pt_bin].Fill(jet_jf_nvtx, weight)
    # 2D
    hists[file_id+'_jet_pt_vs_ntrk'+pt_bin].Fill(jet_ntracks, jet_pt, weight)
    # bH
    hists[file_id+'_jet_bH_nBHadron'+pt_bin].Fill(jet_nBHadrons, weight)

    for jflav, is_jflav in zip(jet_flavours, is_jet_flavours):
        if is_jflav:
            # Jet properties
            hists[file_id+'_jet_pt_'+jflav+pt_bin].Fill(jet_pt, weight)
            hists[file_id+'_jet_eta_'+jflav+pt_bin].Fill(jet_eta, weight)
            hists[file_id+'_jet_phi_'+jflav+pt_bin].Fill(jet_phi, weight)
            hists[file_id+'_jet_ntrk_'+jflav+pt_bin].Fill(jet_ntracks, weight)
            hists[file_id+'_jet_dRiso_'+jflav+pt_bin].Fill(jet_dRiso, weight)
            # IPX
            hists[file_id+'_jet_ip2_'+jflav+pt_bin].Fill(jet_ip2, weight)
            hists[file_id+'_jet_ip2_c_'+jflav+pt_bin].Fill(jet_ip2_c, weight)
            hists[file_id+'_jet_ip2_cu_'+jflav+pt_bin].Fill(jet_ip2_cu, weight)
            hists[file_id+'_jet_ip3_'+jflav+pt_bin].Fill(jet_ip3, weight)
            hists[file_id+'_jet_ip3_c_'+jflav+pt_bin].Fill(jet_ip3_c, weight)
            hists[file_id+'_jet_ip3_cu_'+jflav+pt_bin].Fill(jet_ip3_cu, weight)
            # Algos
            hists[file_id+'_jet_sv1_Nvtx_'+jflav+pt_bin].Fill(jet_sv1_Nvtx, weight)
            hists[file_id+'_jet_jf_nvtx_'+jflav+pt_bin].Fill(jet_jf_nvtx, weight)
            # 2D
            hists[file_id+'_jet_pt_vs_ntrk_'+jflav+pt_bin].Fill(jet_ntracks, jet_pt, weight)
            # bH
            hists[file_id+'_jet_bH_nBHadron_'+jflav+pt_bin].Fill(jet_nBHadrons, weight)

            # Rates
            hists[file_id+'_jet_rate_pt_'+jflav+pt_bin].Fill(jet_pt, weight)
            hists[file_id+'_jet_rate_dRiso_'+jflav+pt_bin].Fill(jet_dRiso, weight)
            if is_iso:
                hists[file_id+'_jet_rate_pt_'+jflav+'_iso'+pt_bin].Fill(jet_pt, weight)
            if jet_sv1_Nvtx>0:
                hists[file_id+'_jet_rate_pt_'+jflav+'_sv1'+pt_bin].Fill(jet_pt, weight)
                hists[file_id+'_jet_rate_dRiso_'+jflav+'_sv1'+pt_bin].Fill(jet_dRiso, weight)
                if is_iso:
                    hists[file_id+'_jet_rate_pt_'+jflav+'_sv1_iso'+pt_bin].Fill(jet_pt, weight)
            if jet_jf_nvtx>0:
                hists[file_id+'_jet_rate_pt_'+jflav+'_jf'+pt_bin].Fill(jet_pt, weight)
                hists[file_id+'_jet_rate_dRiso_'+jflav+'_jf'+pt_bin].Fill(jet_dRiso, weight)
                if is_iso:
                    hists[file_id+'_jet_rate_pt_'+jflav+'_jf_iso'+pt_bin].Fill(jet_pt, weight)

def fill_histograms(t, hists, file_id, reweight_quantity = ''):

    if len(jet_flavours) != 3:
        print('ERROR: NUMBER OF JET TYPES NOT EQUAL TO 3!!')
        sys.exit()
    njets = 0
    nbjets,ncjets,nljets = 0,0,0
    nbjets_semilep_mu,nbjets_semilep_el,nbjets_hadronic = 0,0,0
    event_weight = 0
    nbhadrons = 0
    leading_jet_idx = -1
    leading_jet_pt = 0.
    subleading_jet_idx = -1
    subleading_jet_pt = 0.
    tmp_ctr_mismatched = 0
    tmp_ctr_mismatched_sanity = 0
    tmp_ctr_total = 0
    # Loop over jets
    for ijet in range(t.njets):
        # Apply relevant jet selection
        if not select_jet(t, ijet):
            continue
        # Calculate relevant weight
        weight = 1.
        # Reweight extended Z' sample to no-pileup Z' sample for comparisons
        if reweight_quantity != '' and 'mc16_13TeV.427081' in file_id:
            if reweight_quantity == 'bcl_2d':
                weight = t.weights_jet_pt_bcl_2d[ijet]
            elif reweight_quantity == 'bcl_3d':
                weight = t.weights_jet_pt_bcl_3d[ijet]
            else:
                raise ValueError('UNKNOWN WEIGHT: {}'.format(reweight_quantity))
        if weight > 0.:
            event_weight += weight
        else:
            # Zero weight, so skip histogram filling etc.
            continue
        njets += 1

        # Used to fill relevant plots for b/c/l/iso-jets only
        jet_pt = t.jet_pt[ijet]/1000.
        is_bjet = select_jet(t, ijet, 'bjet')
        is_bjet_semilep_mu = select_jet(t, ijet, 'bjetsemilep_mu')
        is_bjet_semilep_el = select_jet(t, ijet, 'bjetsemilep_el') and not is_bjet_semilep_mu
        is_bjet_semilep_mu_withCghost = select_jet(t, ijet, 'bjetsemilep_mu_withCghost')
        is_bjet_semilep_el_withCghost = select_jet(t, ijet, 'bjetsemilep_el_withCghost') and not is_bjet_semilep_mu_withCghost
        is_bjet_semilep_mu_newLabels = select_jet(t, ijet, 'bjetsemilep_mu_newLabels')
        is_bjet_semilep_el_newLabels = select_jet(t, ijet, 'bjetsemilep_el_newLabels')
        if is_bjet:
            decay_label = t.jet_decay_label[ijet]
            tmp_ctr_total += 1
            if is_bjet_semilep_mu and not is_bjet_semilep_mu_withCghost:
                tmp_ctr_mismatched += 1
                print('CGHOST MISMATCH FOR SEMILEP MU!')
                print('\tnBHadron: {}'.format(t.jet_nBHadr[ijet]))
                print('\tNew label: {}'.format(decay_label))
                print('\tis_bjet,is_bjet_semilep_mu,is_bjet_semilep_el: {},{},{}'.format(is_bjet,is_bjet_semilep_mu,is_bjet_semilep_el))
                print('\tis_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost: {},{},{}'.format(is_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost))
            if is_bjet_semilep_el and not is_bjet_semilep_el_withCghost:
                tmp_ctr_mismatched += 1
                print('CGHOST MISMATCH FOR SEMILEP EL!')
                print('\tnBHadron: {}'.format(t.jet_nBHadr[ijet]))
                print('\tNew label: {}'.format(decay_label))
                print('\tis_bjet,is_bjet_semilep_mu,is_bjet_semilep_el: {},{},{}'.format(is_bjet,is_bjet_semilep_mu,is_bjet_semilep_el))
                print('\tis_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost: {},{},{}'.format(is_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost))
            if (not is_bjet_semilep_el and not is_bjet_semilep_mu) and not (not is_bjet_semilep_mu_withCghost and not is_bjet_semilep_el_withCghost):
                tmp_ctr_mismatched += 1
                print('CGHOST MISMATCH FOR HADRONIC!')
                print('\tnBHadron: {}'.format(t.jet_nBHadr[ijet]))
                print('\tNew label: {}'.format(decay_label))
                print('\tis_bjet,is_bjet_semilep_mu,is_bjet_semilep_el: {},{},{}'.format(is_bjet,is_bjet_semilep_mu,is_bjet_semilep_el))
                print('\tis_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost: {},{},{}'.format(is_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost))
            if is_bjet_semilep_mu_newLabels and not is_bjet_semilep_mu_withCghost:
                tmp_ctr_mismatched_sanity += 1
                print('SANITY MISMATCH FOR SEMILEP MU!')
                print('\tnBHadron: {}'.format(t.jet_nBHadr[ijet]))
                print('\tNew label: {}'.format(decay_label))
                print('\tis_bjet,is_bjet_semilep_mu_newLabels,is_bjet_semilep_el_newLabels: {},{},{}'.format(is_bjet,is_bjet_semilep_mu_newLabels,is_bjet_semilep_el_newLabels))
                print('\tis_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost: {},{},{}'.format(is_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost))
            if is_bjet_semilep_el_newLabels and not is_bjet_semilep_el_withCghost:
                tmp_ctr_mismatched_sanity += 1
                print('SANITY MISMATCH FOR SEMILEP EL!')
                print('\tnBHadron: {}'.format(t.jet_nBHadr[ijet]))
                print('\tNew label: {}'.format(decay_label))
                print('\tis_bjet,is_bjet_semilep_mu_newLabels,is_bjet_semilep_el_newLabels: {},{},{}'.format(is_bjet,is_bjet_semilep_mu_newLabels,is_bjet_semilep_el_newLabels))
                print('\tis_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost: {},{},{}'.format(is_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost))
            if (not is_bjet_semilep_el_newLabels and not is_bjet_semilep_mu_newLabels) and not (not is_bjet_semilep_mu_withCghost and not is_bjet_semilep_el_withCghost):
                tmp_ctr_mismatched_sanity += 1
                print('SANITY MISMATCH FOR HADRONIC!')
                print('\tnBHadron: {}'.format(t.jet_nBHadr[ijet]))
                print('\tNew label: {}'.format(decay_label))
                print('\tis_bjet,is_bjet_semilep_mu_newLabels,is_bjet_semilep_el_newLabels: {},{},{}'.format(is_bjet,is_bjet_semilep_mu_newLabels,is_bjet_semilep_el_newLabels))
                print('\tis_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost: {},{},{}'.format(is_bjet,is_bjet_semilep_mu_withCghost,is_bjet_semilep_el_withCghost))
        if is_bjet:
            nbjets += 1
            if is_bjet_semilep_mu:
                nbjets_semilep_mu += 1
            if is_bjet_semilep_el:
                nbjets_semilep_el += 1
            if not is_bjet_semilep_el and not is_bjet_semilep_mu:
                nbjets_hadronic += 1
        is_cjet = select_jet(t, ijet, 'cjet')
        if is_cjet: ncjets += 1
        is_ljet = select_jet(t, ijet, 'ljet')
        if is_ljet: nljets += 1
        is_jet_flavours = [is_bjet,is_cjet,is_ljet]
        is_iso  = select_jet(t, ijet, 'isojet')

        # print('\tJet {} (flav {}) has pT {}'.format(ijet, t.jet_LabDr_HadF[ijet], jet_pt))
        if jet_pt > leading_jet_pt:
            subleading_jet_idx = leading_jet_idx
            subleading_jet_pt = leading_jet_pt
            leading_jet_idx = ijet
            leading_jet_pt = jet_pt
        elif jet_pt > subleading_jet_pt:
            subleading_jet_idx = ijet
            subleading_jet_pt = jet_pt

        # Fill relevant histograms
        fill_jet_histograms(t, ijet, is_jet_flavours, is_iso, hists, file_id, weight)

        # There is at most one SV1 vertex
        jet_sv1_Nvtx = t.jet_sv1_Nvtx[ijet]# Number of sv1 vertices
        if jet_sv1_Nvtx > 1:
            print('MORE THAN ONE SV1 VERTEX!! --> NOT EXPECTED!!')
            sys.exit()
        if jet_sv1_Nvtx > 0:
            fill_SV1_histograms(t, ijet, is_jet_flavours, hists, file_id, weight)

        # Loop over JF vertices
        jet_jf_nvtx = t.jet_jf_nvtx[ijet]# Number of jet fitter vertices
        tot_jf_ntrkv = 0
        if jet_jf_nvtx>0:
            for jvtx in range(int(jet_jf_nvtx)):
                fill_JF_histograms(t, ijet, jvtx, is_jet_flavours, hists, file_id, weight, pt_bin = '')
                tot_jf_ntrkv += t.jet_jf_vtx_ntrk[ijet][jvtx]

        # Fill in tracks histograms
        jet_ntracks       = t.jet_btag_ntrk[ijet]
        jet_nHFtracks     = 0
        jet_nPUFAKEtracks = 0
        jet_nLowPtTracks  = 0
        # Loop over tracks in the jet
        for jtrk in range(jet_ntracks):
            if not select_track(t, ijet, jtrk):
                continue
            if select_track(t, ijet, jtrk, 'HF'):
                jet_nHFtracks += 1
            if select_track(t, ijet, jtrk, 'PUFAKE'):
                jet_nPUFAKEtracks += 1
            if select_track(t, ijet, jtrk, 'LowPt'):
                jet_nLowPtTracks += 1
            fill_track_histograms(t, ijet, jtrk, is_jet_flavours, hists, file_id, weight)

        jet_nOthertracks = jet_ntracks - jet_nHFtracks

        hists[file_id+'_jet_nHFtracks'].Fill(jet_nHFtracks, weight)
        hists[file_id+'_jet_nPUFAKEtracks'].Fill(jet_nPUFAKEtracks, weight)
        hists[file_id+'_jet_nOthertracks'].Fill(jet_nOthertracks, weight)
        hists[file_id+'_jet_nHFtracks_vs_ntrk'].Fill(jet_ntracks,jet_nHFtracks, weight)
        hists[file_id+'_jet_nLowPtTracks'].Fill(jet_nLowPtTracks, weight)
        for jflav, is_jflav in zip(jet_flavours, is_jet_flavours):
            if is_jflav:
                hists[file_id+'_jet_nHFtracks_'+jflav].Fill(jet_nHFtracks, weight)
                hists[file_id+'_jet_nPUFAKEtracks_'+jflav].Fill(jet_nPUFAKEtracks, weight)
                hists[file_id+'_jet_nOthertracks_'+jflav].Fill(jet_nOthertracks, weight)
                hists[file_id+'_jet_nHFtracks_vs_ntrk_'+jflav].Fill(jet_ntracks,jet_nHFtracks, weight)
                hists[file_id+'_jet_nLowPtTracks_'+jflav].Fill(jet_nLowPtTracks, weight)

        # Loop over truth B hadrons
        n_bHadrons = t.jet_nBHadr[ijet]
        # Keep track of number of B hadrons in the event
        nbhadrons += n_bHadrons
        for jbhad in range(n_bHadrons):
            fill_bH_histograms(t, ijet, jbhad, is_jet_flavours, hists, file_id, weight)
            if is_iso:
                fill_bH_histograms(t, ijet, jbhad, is_jet_flavours, hists, file_id, weight, '_isojet')
        if n_bHadrons == 1:
            fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH')
            if is_iso:
                fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet')
                jet_pt = t.jet_pt[ijet]/1000.
                bH_pt  = t.jet_bH_pt[ijet][0]/1000.
                if bH_pt/jet_pt > 1.0:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_largeFrag')
                if is_bjet_semilep_mu:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_mu')
                if is_bjet_semilep_el:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_el')
                if not is_bjet_semilep_el and not is_bjet_semilep_mu:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_hadronic')
                if is_bjet_semilep_mu_withCghost:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_mu_withCghost')
                if is_bjet_semilep_el_withCghost:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_el_withCghost')
                if not is_bjet_semilep_el_withCghost and not is_bjet_semilep_mu_withCghost:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_hadronic_withCghost')
                if is_bjet_semilep_mu_newLabels:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_mu_newLabels')
                if is_bjet_semilep_el_newLabels:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_el_newLabels')
                if not is_bjet_semilep_el_newLabels and not is_bjet_semilep_mu_newLabels:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_hadronic_newLabels')
        if n_bHadrons > 0:
            if jet_sv1_Nvtx>0:
                sv1_ntrkv = t.jet_sv1_ntrkv[ijet]
                hists[file_id+'_jet_sv1_ntrkv_bH'].Fill(sv1_ntrkv, weight)
            hists[file_id+'_jet_jf_vtx_ntrk_bH'].Fill(tot_jf_ntrkv, weight)
        if is_bjet:
            hists[file_id+'_jet_decayMode_bjet'].Fill(get_bjet_decay(t, ijet), weight)

        # Fill pT-binned histograms
        for pt_bin in pt_bins:
            pt_min, pt_max = pt_bins[pt_bin][0], pt_bins[pt_bin][1]
            # Check if jet pT is in relevant bin
            if jet_pt < pt_min or jet_pt >= pt_max:
                continue

            # Jet histograms pT-binned
            fill_jet_histograms(t, ijet, is_jet_flavours, is_iso, hists, file_id, weight, '_'+pt_bin)
            # SV1 pT-binned
            if jet_sv1_Nvtx > 0:
                fill_SV1_histograms(t, ijet, is_jet_flavours, hists, file_id, weight, '_'+pt_bin)
            # JF pT-binned
            if jet_jf_nvtx>0:
                for jvtx in range(int(jet_jf_nvtx)):
                    fill_JF_histograms(t, ijet, jvtx, is_jet_flavours, hists, file_id, weight, '_'+pt_bin)
            # Tracks pT-binned
            for jtrk in range(jet_ntracks):
                if not select_track(t, ijet, jtrk):
                    continue
                fill_track_histograms(t, ijet, jtrk, is_jet_flavours, hists, file_id, weight, '_'+pt_bin)
            hists[file_id+'_jet_nHFtracks_'+pt_bin].Fill(jet_nHFtracks, weight)
            hists[file_id+'_jet_nPUFAKEtracks_'+pt_bin].Fill(jet_nPUFAKEtracks, weight)
            hists[file_id+'_jet_nOthertracks_'+pt_bin].Fill(jet_nOthertracks, weight)
            hists[file_id+'_jet_nHFtracks_vs_ntrk_'+pt_bin].Fill(jet_ntracks,jet_nHFtracks, weight)
            hists[file_id+'_jet_nLowPtTracks_'+pt_bin].Fill(jet_nLowPtTracks, weight)
            for jflav, is_jflav in zip(jet_flavours, is_jet_flavours):
                if is_jflav:
                    hists[file_id+'_jet_nHFtracks_'+jflav+'_'+pt_bin].Fill(jet_nHFtracks, weight)
                    hists[file_id+'_jet_nPUFAKEtracks_'+jflav+'_'+pt_bin].Fill(jet_nPUFAKEtracks, weight)
                    hists[file_id+'_jet_nOthertracks_'+jflav+'_'+pt_bin].Fill(jet_nOthertracks, weight)
                    hists[file_id+'_jet_nHFtracks_vs_ntrk_'+jflav+'_'+pt_bin].Fill(jet_ntracks,jet_nHFtracks, weight)
                    hists[file_id+'_jet_nLowPtTracks_'+jflav+'_'+pt_bin].Fill(jet_nLowPtTracks, weight)
            # bH pT-binned
            n_bHadrons = t.jet_nBHadr[ijet]
            for jbhad in range(n_bHadrons):
                fill_bH_histograms(t, ijet, jbhad, is_jet_flavours, hists, file_id, weight, '_'+pt_bin)
                if is_iso:
                    fill_bH_histograms(t, ijet, jbhad, is_jet_flavours, hists, file_id, weight, '_isojet_'+pt_bin)
            if n_bHadrons == 1:
                fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_'+pt_bin)
                if is_iso:
                    fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_'+pt_bin)
                    jet_pt = t.jet_pt[ijet]/1000.
                    bH_pt  = t.jet_bH_pt[ijet][0]/1000.
                    if bH_pt/jet_pt > 1.0:
                        fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_largeFrag_'+pt_bin)
                    if is_bjet_semilep_mu:
                        fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_mu_'+pt_bin)
                    if is_bjet_semilep_el:
                        fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_el_'+pt_bin)
                    if not is_bjet_semilep_el and not is_bjet_semilep_mu:
                        fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_hadronic_'+pt_bin)
                    if is_bjet_semilep_mu_withCghost:
                        fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_mu_withCghost_'+pt_bin)
                    if is_bjet_semilep_el_withCghost:
                        fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_el_withCghost_'+pt_bin)
                    if not is_bjet_semilep_el_withCghost and not is_bjet_semilep_mu_withCghost:
                        fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_hadronic_withCghost_'+pt_bin)
                    if is_bjet_semilep_mu_newLabels:
                        fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_mu_newLabels_'+pt_bin)
                    if is_bjet_semilep_el_newLabels:
                        fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_semilep_el_newLabels_'+pt_bin)
                    if not is_bjet_semilep_el_newLabels and not is_bjet_semilep_mu_newLabels:
                        fill_bH_histograms(t, ijet, 0, is_jet_flavours, hists, file_id, weight, '_1bH_isojet_hadronic_newLabels_'+pt_bin)
            if n_bHadrons > 0:
                if jet_sv1_Nvtx>0:
                    sv1_ntrkv = t.jet_sv1_ntrkv[ijet]
                    hists[file_id+'_jet_sv1_ntrkv_bH_'+pt_bin].Fill(sv1_ntrkv, weight)
                hists[file_id+'_jet_jf_vtx_ntrk_bH_'+pt_bin].Fill(tot_jf_ntrkv, weight)
            if is_bjet:
                hists[file_id+'_jet_decayMode_bjet_'+pt_bin].Fill(get_bjet_decay(t, ijet), weight)

        # Loop over vertices in the jet
        # If == -1, the track in question is not associated to any JetFitter vertex
        # Any given track is associated to at most one JetFitter vertex in principle
        # ivtx = t.jet_trk_jf_Vertex[ijet][jtrk]
        # if ivtx != -1:
        #     # This track has a jf vertex associated to it, it is t.jet_trk_jf_Vertex[ijet][jtrk]...
        #     # Access vertex information for this vertex using ivtx, e.g.
        #     chi2 = t.jet_jf_vtx_chi2[ijet][ivtx]
        #     print(chi2)
        #     if debug:
        #         print('Index of vertex associated to the track: '+str(ivtx))
        #     if ivtx > (nvertices_jf + 1):
        #         print('IMPOSSIBLE!!!')

    # Done loop over jets, now fill event-level histograms
    hists[file_id+'_njets'].Fill(njets)
    PVx,PVy,PVz = t.PVx,t.PVy,t.PVz
    hists[file_id+'_PVx'].Fill(PVx)
    hists[file_id+'_PVy'].Fill(PVy)
    hists[file_id+'_PVz'].Fill(PVz)
    # Only fill if we have at least one jet, as zero jets will not have a valid weight for filling...
    if njets > 0:
        # Divide event weight by njets to account for jet multiplicity
        event_weight = event_weight/njets
        hists[file_id+'_njets_weighted'].Fill(njets,event_weight)
        hists[file_id+'_PVx_weighted'].Fill(PVx, event_weight)
        hists[file_id+'_PVy_weighted'].Fill(PVy, event_weight)
        hists[file_id+'_PVz_weighted'].Fill(PVz, event_weight)

    for jflav, njflav in zip(jet_flavours, [nbjets,ncjets,nljets]):
        hists[file_id+'_njets_'+jflav].Fill(njflav)
        if jflav == 'bjet':
            hists[file_id+'_njets_bjet_semilep_mu'].Fill(nbjets_semilep_mu)
            hists[file_id+'_njets_bjet_semilep_el'].Fill(nbjets_semilep_el)
            hists[file_id+'_njets_bjet_hadronic'].Fill(nbjets_hadronic)
        if njets > 0:
            # Only fill if we have at least one jet, as zero jets will not have a valid weight for filling...
            hists[file_id+'_njets_'+jflav+'_weighted'].Fill(njflav, event_weight)
            if jflav == 'bjet':
                hists[file_id+'_njets_bjet_semilep_mu_weighted'].Fill(nbjets_semilep_mu, event_weight)
                hists[file_id+'_njets_bjet_semilep_el_weighted'].Fill(nbjets_semilep_el, event_weight)
                hists[file_id+'_njets_bjet_hadronic_weighted'].Fill(nbjets_hadronic, event_weight)
        if njflav > 0:
            hists[file_id+'_PVx_'+jflav].Fill(PVx)
            hists[file_id+'_PVy_'+jflav].Fill(PVy)
            hists[file_id+'_PVz_'+jflav].Fill(PVz)
            hists[file_id+'_PVx_'+jflav+'_weighted'].Fill(PVx, event_weight)
            hists[file_id+'_PVy_'+jflav+'_weighted'].Fill(PVy, event_weight)
            hists[file_id+'_PVz_'+jflav+'_weighted'].Fill(PVz, event_weight)

    if nbhadrons > 0:
        hists[file_id+'_PVx_HF'].Fill(PVx)
        hists[file_id+'_PVy_HF'].Fill(PVy)
        hists[file_id+'_PVz_HF'].Fill(PVz)
        if njets > 0:
            hists[file_id+'_PVx_HF_weighted'].Fill(PVx, event_weight)
            hists[file_id+'_PVy_HF_weighted'].Fill(PVy, event_weight)
            hists[file_id+'_PVz_HF_weighted'].Fill(PVz, event_weight)

    if leading_jet_idx > -1 and subleading_jet_idx > -1:
        # Only look at events where two leading jets are b-jets
        if select_jet(t, leading_jet_idx, 'bjet') and select_jet(t, subleading_jet_idx, 'bjet'):
            leading_nBHad = t.jet_nBHadr[leading_jet_idx]
            leading_decayMode = get_bjet_decay(t, leading_jet_idx)
            subleading_nBHad = t.jet_nBHadr[subleading_jet_idx]
            subleading_decayMode = get_bjet_decay(t, subleading_jet_idx)
            is_jet_flavours_leading = [select_jet(t, leading_jet_idx, 'bjet'), select_jet(t, leading_jet_idx, 'cjet'), select_jet(t, leading_jet_idx, 'ljet')]
            is_jet_flavours_subleading = [select_jet(t, subleading_jet_idx, 'bjet'), select_jet(t, subleading_jet_idx, 'cjet'), select_jet(t, subleading_jet_idx, 'ljet')]
            if debug and leading_nBHad != 1:
                print('Leading jet in 2-bjet event has more than one B hadron!')
                print('\tLeading jet {} (pT {})'.format(leading_jet_idx, leading_jet_pt))
                print('\tSubleading jet {} (pT {})'.format(subleading_jet_idx, subleading_jet_pt))
                print('\t-->Filling with jet {} (pT {})'.format(leading_jet_idx, leading_jet_pt))
                print('\t\tHas {} B hadrons'.format(leading_nBHad))

            # # Fill leading b-jet histograms
            hists[file_id+'_jet_bH_nBHadron_bjet_leadingJet'].Fill(leading_nBHad, weight)
            hists[file_id+'_jet_decayMode_bjet_leadingJet'].Fill(leading_decayMode, weight)
            for pt_bin in pt_bins:
                pt_min, pt_max = pt_bins[pt_bin][0], pt_bins[pt_bin][1]
                if leading_jet_pt < pt_min or leading_jet_pt >= pt_max:
                    continue
                hists[file_id+'_jet_bH_nBHadron_bjet_leadingJet_'+pt_bin].Fill(leading_nBHad, weight)
                hists[file_id+'_jet_decayMode_bjet_leadingJet_'+pt_bin].Fill(leading_decayMode, weight)
            for jbhad in range(leading_nBHad):
                fill_bH_histograms(t, leading_jet_idx, jbhad, is_jet_flavours_leading, hists, file_id, weight, '_leadingJet')
                for pt_bin in pt_bins:
                    pt_min, pt_max = pt_bins[pt_bin][0], pt_bins[pt_bin][1]
                    if leading_jet_pt < pt_min or leading_jet_pt >= pt_max:
                        continue
                    fill_bH_histograms(t, leading_jet_idx, jbhad, is_jet_flavours_leading, hists, file_id, weight, '_leadingJet_'+pt_bin)
            # # Fill subleading b-jet histograms
            hists[file_id+'_jet_bH_nBHadron_bjet_subleadingJet'].Fill(subleading_nBHad, weight)
            hists[file_id+'_jet_decayMode_bjet_subleadingJet'].Fill(subleading_decayMode, weight)
            for pt_bin in pt_bins:
                pt_min, pt_max = pt_bins[pt_bin][0], pt_bins[pt_bin][1]
                if subleading_jet_pt < pt_min or subleading_jet_pt >= pt_max:
                    continue
                hists[file_id+'_jet_bH_nBHadron_bjet_subleadingJet_'+pt_bin].Fill(subleading_nBHad, weight)
                hists[file_id+'_jet_decayMode_bjet_subleadingJet_'+pt_bin].Fill(subleading_decayMode, weight)
            for jbhad in range(subleading_nBHad):
                fill_bH_histograms(t, subleading_jet_idx, jbhad, is_jet_flavours_subleading, hists, file_id, weight, '_subleadingJet')
                for pt_bin in pt_bins:
                    pt_min, pt_max = pt_bins[pt_bin][0], pt_bins[pt_bin][1]
                    if subleading_jet_pt < pt_min or subleading_jet_pt >= pt_max:
                        continue
                    fill_bH_histograms(t, subleading_jet_idx, jbhad, is_jet_flavours_subleading, hists, file_id, weight, '_subleadingJet_'+pt_bin)
    if nbjets > nbhadrons:
        print('STRANGE: nbjets = {}, nbhadrons = {}'.format(nbjets,nbhadrons))
    return tmp_ctr_mismatched, tmp_ctr_mismatched_sanity, tmp_ctr_total

def produce_histograms(sample, jet_type, track_type, max_evts, sub_job, skeletons, reweight_quantity = ''):

    t0 = dt.datetime.now()
    base_dir = tree_dir+'/'+sample+'/'+track_type+'/'
    filename = base_dir + 'all_flav_'+tracking_submitJobs.get_simple_name(sample)+'_'+tracking_submitJobs.get_short_jet_name(jet_type)+'_'+track_type+'.root'

    if not os.path.exists(filename):
        if debug: print('File {} does not exists, will TChain subdirectories...'.format(filename))
        filenames = [fn for fn in glob.glob(base_dir + 'trk_*/flav_{}.root'.format(tracking_submitJobs.get_short_jet_name(jet_type)))]
    else:
        if debug: print('Opening file: {}'.format(filename))
        filenames = [filename]
    if sub_job != 'NA':
        # Just run over one file, usually used with the batch system
        filenames = [base_dir + '/' + sub_job + '/flav_' + tracking_submitJobs.get_short_jet_name(jet_type) + '.root']

    # if 'mc16_13TeV.427081' in sample:
    #     filenames = [fn.replace('.root','_with_weights.root') for fn in filenames]

    t = ROOT.TChain('bTag_' + jet_type)
    for f in filenames:
        t.Add(f)

    nEvents = t.GetEntries()
    if debug: print('TChain has {} entries'.format(nEvents))
    if max_evts != -1:
        print('Looping over {} events'.format(max_evts))
    else:
        print('Looping over {} events'.format(nEvents))

    file_id = tracking_submitJobs.get_simple_name(filename.split('/')[-1])
    hists = book_histograms(file_id, skeletons)

    tot_ctr_mismatched, tot_ctr_sanity, tot_ctr_total = 0,0,0

    # Loop over events
    for i in range(nEvents):
        if (max_evts != -1) and (i >= max_evts):
            break
        if i % 1000 == 0:
            print('Processed {} events in {}'.format(i,str(dt.datetime.now()-t0)))

        # Get tree entry
        t.GetEntry(i)

        # Apply relevant event selection, confirming all three tree give the same result
        if not select_event(t):
            continue
        
        # Fill histograms
        mis, sanity, tot = fill_histograms(t, hists, file_id, reweight_quantity)
        tot_ctr_mismatched += mis
        tot_ctr_sanity += sanity
        tot_ctr_total += tot

    out_name = filename.replace('all_flav','hist')
    if sub_job != 'NA':
        out_name = base_dir + '/' + sub_job + '/' + 'hist_' + tracking_submitJobs.get_short_jet_name(jet_type) + '.root'
    if reweight_quantity != '':
        out_name = out_name.replace('.root','_rw_{}.root'.format(reweight_quantity))
    print('Output file: {}'.format(out_name))
    f_out = ROOT.TFile.Open(out_name,'RECREATE')
    f_out.cd()
    for h_name, h in hists.items():
        if reweight_quantity != '':
            h.Clone(h_name+'_rw_{}'.format(reweight_quantity)).Write()
        else:
            h.Clone(h_name).Write()
    f_out.Close()
    print('Produced histograms in {}'.format(dt.datetime.now()-t0))
    print('Mismatched: {}/{} ({}%)'.format(tot_ctr_mismatched,tot_ctr_total,100.0*tot_ctr_mismatched/tot_ctr_total))
    print('Sanity: {}/{} ({}%)'.format(tot_ctr_sanity,tot_ctr_total,100.0*tot_ctr_sanity/tot_ctr_total))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Produce histograms from TTrees')
    parser.add_argument('-s', '--sample', dest='sample', default='group.perf-idtracking.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.e5362_e5984_s3126_r11212_IdealNomV5_EXT0',
                        help='Sample to be used')
    parser.add_argument('-t', '--track_type', dest='track_type', default='nom',
                        help='Track type to be used')
    parser.add_argument('-n', '--nevts', dest='max_events', default=-1, type=int,
                        help='Number of events to process')
    parser.add_argument('-j', '--jet_type', dest='jet_type', default='AntiKt4EMPFlowJets', choices=tracking_submitJobs.jet_types,
                        help='Type of jet for which to produce histograms (AntiKt4EMTopoJets or AntiKt4EMPFlowJets)')
    parser.add_argument('-td', '--tree_dir', dest='tree_dir', default='/unix/atlasvhbb2/srettie/tracking/athenaOutputs_full/',
                        help='Directory where the FTAG ntuples are located')
    parser.add_argument('-sj', '--sub_job', dest='sub_job', default='NA',
                        help='Name of sub-directory, used for parallelization')
    parser.add_argument('-rw', '--reweight_quantity', dest='reweight_quantity', default='',
                        help='Quantity used to reweight distributions')
                        
    args = parser.parse_args()

    # Default directories
    tree_dir = args.tree_dir

    if debug:
        print('Arguments:')
        for arg in vars(args):
            print('{}: {}'.format(arg, getattr(args, arg)))

    produce_histograms(args.sample, args.jet_type, args.track_type, args.max_events, args.sub_job, plot_skeletons, args.reweight_quantity)

    if debug: print('Done!')