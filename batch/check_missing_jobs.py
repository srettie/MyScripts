from __future__ import print_function
import os, sys
import subprocess

debug = False

periods = ['a','d']

in_dir = '/unix/atlasvhbb2/srettie/CxAODFramework_branch_tag_r32-24-Reader-Resolved-05/run/tag_05_post_crash/'

def to_delete(samples_to_rerun):
    print('Before merging with re-run files, make sure to delete old partial files...')

    to_delete_merged = ['hist-'+s+'.root' for s in samples_to_rerun]
    to_delete_fetch = ['fetch/hist-'+s+'*.root' for s in samples_to_rerun]
    to_delete = 'ls ' + ' '.join(to_delete_merged+to_delete_fetch)

    print(to_delete)
        
    
def check_missing_outputs(period):
    to_check = in_dir+'Reader_1L_32-15_'+period+'_MVA_H/'
    segments = to_check + '/submit/segments'
    status_cmd = 'ls '+to_check+'/status/'
    statuses = subprocess.check_output(status_cmd, shell=True).split('\n')
    sample_subids = []
    
    # Check for failed jobs:
    for s in statuses:
        if 'fail' in s:
            print('FAILED JOB: '+s)
        subid = s.replace('done-','').replace('completed-','')
        if debug:
            print(s)
            print(subid)

        if subid not in sample_subids:
            sample_subids.append(subid)

    if debug:
        print(sample_subids)

    print('Found total of %s subids'%len(sample_subids))

    to_rerun = []
    n_missing = 0
    
    with open(segments) as f:
        for seg in f:
            seg = seg.strip('\n')
            identifier = seg.split(' ')[0]
            sample_sub = seg.split(' ')[1]
            if debug:
                print(seg)
                print(identifier)
                print(sample_sub)

            # Compare each segment to see if it appears in the status directory
            if identifier not in sample_subids:
                print('MISSING JOB: '+seg)
                n_missing += 1
                sample = seg.split(' ')[1].split('-')[0]
                if debug:
                    print(sample)
                if sample not in to_rerun:
                    to_rerun.append(sample)

    if debug:
        print(to_rerun)

    print('Missing %s jobs for period %s'%(n_missing, period))
    print('Need to re-run %s samples in period %s:'%(len(to_rerun), period))
    
    for r in to_rerun:
        print(r, end = " ")

    print()

    to_delete(to_rerun)

if __name__ == '__main__':
    
    for period in periods:
        check_missing_outputs(period)
