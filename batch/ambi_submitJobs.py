import os
import glob
import batch_utils as bu

do_submit = True
do_test = False

datasets = {
    'nom'             : 'user.srettie.mc16_13TeV.800030.flatpT_Zprime_Extended.e7954_s3582_r12643_20230201_nomi_EXT0',
    'sisp'            : 'user.srettie.mc16_13TeV.800030.flatpT_Zprime_Extended.e7954_s3582_r12643_20230201_nomi_EXT0',
    'pseudo'          : 'user.srettie.mc16_13TeV.800030.flatpT_Zprime_Extended.e7954_s3582_r12643_20230201_nomi_EXT0',
    'skip_ambi'       : 'user.srettie.mc16_13TeV.800030.flatpT_Zprime_Extended.e7954_s3582_r12643_20230202_skip_ambi_EXT0',
    'skip_ambi_roi'   : 'user.srettie.mc16_13TeV.800030.flatpT_Zprime_Extended.e7954_s3582_r12643_20230203_skip_ambi_roi_EXT0',
    'skip_ambi_roi_A' : 'user.srettie.mc16_13TeV.800030.flatpT_Zprime_Extended.e7954_s3582_r12643_20230203_skip_ambi_roi_A_EXT0',
    'skip_ambi_roi_B' : 'user.srettie.mc16_13TeV.800030.flatpT_Zprime_Extended.e7954_s3582_r12643_20230203_skip_ambi_roi_B_EXT0',
    'skip_ambi_roi_C' : 'user.srettie.mc16_13TeV.800030.flatpT_Zprime_Extended.e7954_s3582_r12643_20230203_skip_ambi_roi_C_EXT0',
    'skip_ambi_roi_D' : 'user.srettie.mc16_13TeV.800030.flatpT_Zprime_Extended.e7954_s3582_r12643_20230203_skip_ambi_roi_D_EXT0',
}

if __name__ == '__main__':
    n_jobs = 0
    for d in datasets:
        aods = glob.glob(f'/unix/atlastrackbtag/srettie/CTIDE_ambi_optimization/observer_AOD/CA/{datasets[d]}/*')
        for f in aods:
            out_stem = f.split('/')[-1].replace('user.srettie.', '').replace('.EXT0.', '').replace('.AOD.root', '')
            out_folder = f'/unix/atlastrackbtag/srettie/CTIDE_ambi_optimization/analyzed_ntuples/{d}/{out_stem}'
            setup_cmd = f'mkdir -p {out_folder};cd {out_folder}'
            run_cmd = f'runCTIDEAmbiguitySolverAnalyser.py -i {f} --doEfficiencyPlots'
            if do_test:
                run_cmd += ' -n 10'
            if d == 'nom':
                run_cmd += ' --doReconstructabilityPlots'
            if d == 'sisp':
                run_cmd += ' --doReconstructabilityPlots --doSiSPEfficiency'
            if d == 'pseudo':
                run_cmd += ' --doReconstructabilityPlots --doPTEfficiency'
            cmd = f'{setup_cmd};{run_cmd}'
            bu.job_submit(cmd, f'{d}_{out_stem}', do_submit)
            n_jobs += 1
            if do_test:
                break
    print(f'submitted {n_jobs} jobs')