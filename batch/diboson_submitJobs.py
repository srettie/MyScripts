import os
import sys
import subprocess
import glob
import time


debug = False
do_test = False
do_submit = True

rucio_dir = '/unix/atlasvhbb2/srettie/truthTuples/'
tatsuya_dir = '/unix/atlasvhbb2/srettie/truthTuples_fromTatsuya/TruthTuples_r3.6/'

from_rucio_dl = [
    # Nominal
    'group.phys-higgs.mc15_13TeV.363355.Sherpa_221_NNPDF30NNLO_ZqqZvv.evgen.EVNT.e5525.VHbb_R20.7_r3.6_X_Lep',
    'group.phys-higgs.mc15_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.evgen.EVNT.e5525.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.evgen.EVNT.e5525.VHbb_R20.7_r3.6_X_Lep',
    'group.phys-higgs.mc15_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll.evgen.EVNT.e5525.VHbb_R20.7_r3.6-2_X_Lep',
    'group.phys-higgs.mc15_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.evgen.EVNT.e5583.VHbb_R20.7_r3.6-4_X_Lep',
    'group.phys-higgs.mc15_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq.evgen.EVNT.e5983.VHbb_R20.7_r3.6-3_X_Lep',
    'group.phys-higgs.mc15_13TeV.363489.Sherpa_221_NNPDF30NNLO_WlvZqq.evgen.EVNT.e5525.VHbb_R20.7_r3.6-4_X_Lep',

    # CKKW/QSF Variations
    'group.phys-higgs.mc15_13TeV.366318.Sh_221_NN30NNLO_ZqqZll_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366319.Sh_221_NN30NNLO_ZqqZll_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366320.Sh_221_NN30NNLO_ZqqZll_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366321.Sh_221_NN30NNLO_ZqqZll_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366322.Sh_221_NN30NNLO_ZqqZvv_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366323.Sh_221_NN30NNLO_ZqqZvv_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366324.Sh_221_NN30NNLO_ZqqZvv_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366325.Sh_221_NN30NNLO_ZqqZvv_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366326.Sh_221_NN30NNLO_WqqZvv_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366327.Sh_221_NN30NNLO_WqqZvv_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366328.Sh_221_NN30NNLO_WqqZvv_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366329.Sh_221_NN30NNLO_WqqZvv_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366330.Sh_221_NN30NNLO_WqqZll_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366331.Sh_221_NN30NNLO_WqqZll_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366332.Sh_221_NN30NNLO_WqqZll_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366333.Sh_221_NN30NNLO_WqqZll_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366334.Sh_221_NN30NNLO_WlvZqq_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366335.Sh_221_NN30NNLO_WlvZqq_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366336.Sh_221_NN30NNLO_WlvZqq_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366337.Sh_221_NN30NNLO_WlvZqq_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366338.Sh_221_NN30NNLO_WpqqWmlv_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366339.Sh_221_NN30NNLO_WpqqWmlv_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366340.Sh_221_NN30NNLO_WpqqWmlv_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366341.Sh_221_NN30NNLO_WpqqWmlv_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366342.Sh_221_NN30NNLO_WplvWmqq_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366343.Sh_221_NN30NNLO_WplvWmqq_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366344.Sh_221_NN30NNLO_WplvWmqq_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366345.Sh_221_NN30NNLO_WplvWmqq_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',

    # PP8
    'group.phys-higgs.mc15_13TeV.361606.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvqq.evgen.EVNT.e4711.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.361607.PowhegPy8EG_AZNLOCTEQ6L1_WZqqll_mll20.evgen.EVNT.e4711.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.361608.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqvv.evgen.EVNT.e4711.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.361609.PowhegPy8EG_AZNLOCTEQ6L1_WZlvqq_mqq20.evgen.EVNT.e4711.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.361610.PowhegPy8EG_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.evgen.EVNT.e4711.VHbb_R20.7_r3.6.1_X_Lep',
    'group.phys-higgs.mc15_13TeV.361611.PowhegPy8EG_AZNLOCTEQ6L1_ZZvvqq_mqq20.evgen.EVNT.e4711.VHbb_R20.7_r3.6.2_X_Lep',
]

latest_and_greatest = [
    # Nominal
    'group.phys-higgs.mc15_13TeV.363355.Sherpa_221_NNPDF30NNLO_ZqqZvv.evgen.EVNT.e5525.VHbb_R20.7_r3.6_X_Lep',
    'group.phys-higgs.mc15_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.evgen.EVNT.e5525.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.evgen.EVNT.e5525.VHbb_R20.7_r3.6_X_Lep',
    'group.phys-higgs.mc15_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll.evgen.EVNT.e5525.VHbb_R20.7_r3.6-2_X_Lep',
    'group.phys-higgs.mc15_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.evgen.EVNT.e5583.VHbb_R20.7_r3.6-4_X_Lep',
    'group.phys-higgs.mc15_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq.evgen.EVNT.e5983.VHbb_R20.7_r3.6-3_X_Lep',
    'group.phys-higgs.mc15_13TeV.363489.Sherpa_221_NNPDF30NNLO_WlvZqq.evgen.EVNT.e5525.VHbb_R20.7_r3.6-4_X_Lep',

    # CKKW/QSF Variations
    'group.phys-higgs.mc15_13TeV.366318.Sh_221_NN30NNLO_ZqqZll_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366319.Sh_221_NN30NNLO_ZqqZll_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366320.Sh_221_NN30NNLO_ZqqZll_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366321.Sh_221_NN30NNLO_ZqqZll_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366322.Sh_221_NN30NNLO_ZqqZvv_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366323.Sh_221_NN30NNLO_ZqqZvv_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366324.Sh_221_NN30NNLO_ZqqZvv_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366325.Sh_221_NN30NNLO_ZqqZvv_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366326.Sh_221_NN30NNLO_WqqZvv_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366327.Sh_221_NN30NNLO_WqqZvv_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366328.Sh_221_NN30NNLO_WqqZvv_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366329.Sh_221_NN30NNLO_WqqZvv_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366330.Sh_221_NN30NNLO_WqqZll_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366331.Sh_221_NN30NNLO_WqqZll_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366332.Sh_221_NN30NNLO_WqqZll_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366333.Sh_221_NN30NNLO_WqqZll_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366334.Sh_221_NN30NNLO_WlvZqq_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366335.Sh_221_NN30NNLO_WlvZqq_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366336.Sh_221_NN30NNLO_WlvZqq_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366337.Sh_221_NN30NNLO_WlvZqq_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366338.Sh_221_NN30NNLO_WpqqWmlv_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366339.Sh_221_NN30NNLO_WpqqWmlv_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366340.Sh_221_NN30NNLO_WpqqWmlv_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366341.Sh_221_NN30NNLO_WpqqWmlv_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366342.Sh_221_NN30NNLO_WplvWmqq_CKKW15.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366343.Sh_221_NN30NNLO_WplvWmqq_CKKW30.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366344.Sh_221_NN30NNLO_WplvWmqq_QSF025.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.366345.Sh_221_NN30NNLO_WplvWmqq_QSF4.evgen.EVNT.e7666.VHbb_R20.7_r3.6.2_X_Lep',
    
    # PP8    
    'group.phys-higgs.mc15_13TeV.361606.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvqq.evgen.EVNT.e4711.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.361607.PowhegPy8EG_AZNLOCTEQ6L1_WZqqll_mll20.evgen.EVNT.e4711.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.361608.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqvv.evgen.EVNT.e4711.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.361609.PowhegPy8EG_AZNLOCTEQ6L1_WZlvqq_mqq20.evgen.EVNT.e4711.VHbb_R20.7_r3.6.2_X_Lep',
    'group.phys-higgs.mc15_13TeV.361610.PowhegPy8EG_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.evgen.EVNT.e4711.VHbb_R20.7_r3.6.1_X_Lep',
    'group.phys-higgs.mc15_13TeV.361611.PowhegPy8EG_AZNLOCTEQ6L1_ZZvvqq_mqq20.evgen.EVNT.e4711.VHbb_R20.7_r3.6.2_X_Lep',

    # Herwig
    'group.phys-higgs.mc15_13TeV.361592.PowhegHerwigppEG_AZNLOCTEQ6L1_WWlvqq.evgen.EVNT.e4746.VHbb_R20.7_r3.6.2_Hwpp_X_Lep',
    'group.phys-higgs.mc15_13TeV.361593.PowhegHerwigppEG_WZqqll_mll20.evgen.EVNT.e4769.VHbb_R20.7_r3.6.2_Hwpp_X_Lep',
    'group.phys-higgs.mc15_13TeV.361594.PowhegHerwigppEG_AZNLOCTEQ6L1_WZqqvv.evgen.EVNT.e4769.VHbb_R20.7_r3.6.2_Hwpp_X_Lep',
    'group.phys-higgs.mc15_13TeV.361595.PowhegHerwigppEG_WZlvqq_mqq20.evgen.EVNT.e4769.VHbb_R20.7_r3.6.2_Hwpp_X_Lep',
    'group.phys-higgs.mc15_13TeV.361596.PowhegHerwigppEG_ZZqqll_mqq20mll20.evgen.EVNT.e4769.VHbb_R20.7_r3.6.2_Hwpp_X_Lep',
    'group.phys-higgs.mc15_13TeV.361597.PowhegHerwigppEG_ZZvvqq_mqq20.evgen.EVNT.e4769.VHbb_R20.7_r3.6.2_Hwpp_X_Lep',
]

def compare_tuples():
    for i in range(len(from_rucio_dl)):
        for c in ['0','1','2']:

            from_rucio = from_rucio_dl[i].replace('X_Lep',c+'_Lep')
            from_tatsuya = latest_and_greatest[i].replace('X_Lep',c+'_Lep')

            files_rucio = os.listdir(rucio_dir+from_rucio)
            files_tatsuya = os.listdir(tatsuya_dir+from_tatsuya)

            n_rucio = len(files_rucio)
            n_tatsuya = len(files_tatsuya)

            if from_rucio != from_tatsuya and '363359' not in from_rucio and '363489' not in from_rucio:
                print('Sample names are different!!')
                print(from_rucio+': ')
                print(from_tatsuya+': ')
                print('Rucio files: '+str(len(files_rucio)))
                print('Tatsuya files: '+str(len(files_tatsuya)))

            elif n_rucio != n_tatsuya and '363359' not in from_rucio and '363489' not in from_rucio:
                print('Same sample has different number of files!!')
                print('Sample: '+from_rucio)
                print('Rucio files: '+str(len(files_rucio)))
                print('Tatsuya files: '+str(len(files_tatsuya)))
                if n_rucio > n_tatsuya:
                    print('SOMETHING IS OFF; MORE FILES IN RUCIO THAN TATSUYA...')
                else:
                    print('cp '+tatsuya_dir+from_tatsuya+'/* '+rucio_dir+from_rucio+'/')

            else:
                print('Good matching sample: '+from_rucio)


def get_sample_name(s):
    return(s.split('/')[-1].strip())

def get_dsid(s):
    return(s.split('13TeV.')[1][:6])

def get_job_name(s):
    dsid = get_dsid(s)
    return(dsid+s[-6:])

def get_channel(s):
    if '0_Lep' in s:
        return '0'
    if '1_Lep' in s:
        return '1'
    if '2_Lep' in s:
        return '2'
    
    print('WARNING: CHANNEL NOT FOUND SAMPLE ' + s + ', EXITING')
    sys.exit()
    
def get_do_sherpa(s):
    if 'Sherpa_221' in s:
        return '1'
    if 'Sh_221' in s:
        return '1'
    return '0'

def is_valid_sample(s):
    generic = s.replace('0_Lep','X_Lep')
    generic = generic.replace('1_Lep','X_Lep')
    generic = generic.replace('2_Lep','X_Lep')
    if generic in latest_and_greatest:#if '_13TeV.' in s:
        return True
    return False

def torque_submit(cmd, out_dir, job_name):
    # Use subprocess.run() instead of subprocess.call() when python3 is ready...
    print('Submitting job cmd: ' + cmd)
    os.environ['PBS_MACRO'] = cmd
    os.environ['OUTDIR'] = out_dir
    if debug:
        print('Command is:')
        print(cmd)
        print('Sanity check...PBS_MACRO is now:')
        subprocess.call('echo $PBS_MACRO', shell=True)
        print('Output directory is:')
        print(out_dir)
        print('Sanity check...OUTDIR is now:')
        subprocess.call('echo $OUTDIR', shell=True)

    torque_cmd = 'qsub -V -N ' + job_name + ' /home/srettie/MyScripts/batch/job.sh'

    if not do_submit:
        print(torque_cmd)
    else:
        subprocess.call(torque_cmd, shell=True)
        time.sleep(2)
        

# 0 for resolved, 1 for boosted
analysis_type = '0'

if __name__ == '__main__':
    
    if len(sys.argv) < 3:
        print('Usage: '+sys.argv[0]+' <input_directory> <output_directory>')
        sys.exit()

    in_dir = sys.argv[1]
    out_dir = sys.argv[2]

    # Create output directory if non-existent
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    
    n = 0
    samples = []
    for c in glob.glob(in_dir+'/*'):
        if is_valid_sample(get_sample_name(c)):
            if debug:
                print('Keeping: ' + c)
            samples.append(c)
        else:
            print('Rejecting: ' + c)

    print('Found a total of ' + str(len(samples)) + ' samples in directory ' + in_dir + ':')
    for s in samples:
        s = get_sample_name(s)
        channel = get_channel(s)
        dsid = get_dsid(s)
        do_sherpa = get_do_sherpa(s)

        # Ex: NtupleToHist_VHbb 1 "/unix/atlasvhbb2/srettie/truthTuples/" 366344 "/home/srettie/VHbbTruthFramework-Reader/run/test_out/" 1 0
        cmd = 'NtupleToHist_VHbb ' + channel + ' ' + in_dir + ' ' + dsid + ' . ' + do_sherpa + ' ' + analysis_type

        if do_test and n > 0:
            continue
        if debug:
            print('Running on sample: '+s)
        torque_submit(cmd, out_dir, get_job_name(s))
        n += 1
    print('All done!')
