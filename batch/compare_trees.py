import sys
import math
import datetime as dt
import ROOT
import process_trees
import tracking_submitJobs

multiplier = 1

nbins_diff = 100

diffrange_ip3d_d0sig = 20*multiplier
diffrange_d0 = 1*multiplier
diffrange_d0Err = 0.1*multiplier
diffrange_d0res = 0.3*multiplier
diffrange_d0pull = 5*multiplier
diffrange_ip3d_z0sig = 10*multiplier
diffrange_z0sintheta = 1*multiplier
diffrange_z0Err = 0.1*multiplier
diffrange_z0res = 2*multiplier
diffrange_z0pull = 500*multiplier
diffrange_nPixHits = 5*multiplier
diffrange_nsharedPixHits = 5*multiplier
diffrange_nsplitPixHits = 6*multiplier
diffrange_nPixHoles = 3*multiplier
diffrange_nSCTHits = 15*multiplier
diffrange_nsharedSCTHits = 8*multiplier
diffrange_nSCTHoles = 5*multiplier
diffrange_nPixSCTHits = 20*multiplier
diffrange_chi2 = 100*multiplier
diffrange_chi2OverDOF = 7*multiplier

diffrange_phi = 0.02*multiplier
diffrange_phiErr = 0.001*multiplier
diffrange_theta = 0.01*multiplier
diffrange_thetaErr = 0.0005*multiplier
diffrange_qop = 0.02*multiplier
diffrange_qopErr = 0.01*multiplier

diff_plot_skeletons = {
    # Track histograms
    'jet_trk_ip3d_d0sig_diff'     : ROOT.TH1D('jet_trk_ip3d_d0sig_diff','jet_trk_ip3d_d0sig_diff;Diff Track IP3D d_{0}^{sig};Entries',nbins_diff,-diffrange_ip3d_d0sig,diffrange_ip3d_d0sig),
    'jet_trk_d0_diff'             : ROOT.TH1D('jet_trk_d0_diff','jet_trk_d0_diff;Diff Track d_{0} [mm];Entries',nbins_diff,-diffrange_d0,diffrange_d0),
    'jet_trk_d0Truth_diff'        : ROOT.TH1D('jet_trk_d0Truth_diff','jet_trk_d0Truth_diff;Diff Truth Track d_{0} [mm];Entries',nbins_diff,-diffrange_d0,diffrange_d0),
    'jet_trk_d0Err_diff'          : ROOT.TH1D('jet_trk_d0Err_diff','jet_trk_d0Err_diff;Diff Track #sigma(d_{0}) [mm];Entries',nbins_diff,-diffrange_d0Err,diffrange_d0Err),
    'jet_trk_d0res_diff'          : ROOT.TH1D('jet_trk_d0res_diff','jet_trk_d0res_diff;Diff Track d_{0}^{reco} - d_{0}^{truth} [mm];Entries',nbins_diff,-diffrange_d0res,diffrange_d0res),
    'jet_trk_d0pull_diff'         : ROOT.TH1D('jet_trk_d0pull_diff','jet_trk_d0pull_diff;Diff Track (d_{0}^{reco} - d_{0}^{truth})/#sigma(d_{0});Entries',nbins_diff,-diffrange_d0pull,diffrange_d0pull),
    'jet_trk_ip3d_z0sig_diff'     : ROOT.TH1D('jet_trk_ip3d_z0sig_diff','jet_trk_ip3d_z0sig_diff;Diff Track IP3D z_{0}^{sig};Entries',nbins_diff,-diffrange_ip3d_z0sig,diffrange_ip3d_z0sig),
    'jet_trk_z0_diff'             : ROOT.TH1D('jet_trk_z0_diff','jet_trk_z0_diff;Diff Track z_{0} [mm];Entries',nbins_diff,-diffrange_z0sintheta,diffrange_z0sintheta),
    'jet_trk_z0sintheta_diff'     : ROOT.TH1D('jet_trk_z0sintheta_diff','jet_trk_z0sintheta_diff;Diff Track z_{0}*sin(#theta) [mm];Entries',nbins_diff,-diffrange_z0sintheta,diffrange_z0sintheta),
    'jet_trk_z0Truth_diff'        : ROOT.TH1D('jet_trk_z0Truth_diff','jet_trk_z0Truth_diff;Diff Truth Track z_{0} [mm];Entries',nbins_diff,-diffrange_z0sintheta,diffrange_z0sintheta),
    'jet_trk_z0Err_diff'          : ROOT.TH1D('jet_trk_z0Err_diff','jet_trk_z0Err_diff;Diff Track #sigma(z_{0}) [mm];Entries',nbins_diff,-diffrange_z0Err,diffrange_z0Err),
    'jet_trk_z0res_diff'          : ROOT.TH1D('jet_trk_z0res_diff','jet_trk_z0res_diff;Diff Track z_{0}^{reco} - z_{0}^{truth} [mm];Entries',nbins_diff,-diffrange_z0res,diffrange_z0res),
    'jet_trk_z0pull_diff'         : ROOT.TH1D('jet_trk_z0pull_diff','jet_trk_z0pull_diff;Diff Track (z_{0}^{reco} - z_{0}^{truth})/#sigma(z_{0});Entries',nbins_diff,-diffrange_z0pull,diffrange_z0pull),
    'jet_trk_phi_diff'            : ROOT.TH1D('jet_trk_phi_diff','jet_trk_phi_diff;Diff Track #phi;Entries',nbins_diff,-diffrange_phi,diffrange_phi),
    'jet_trk_phiErr_diff'         : ROOT.TH1D('jet_trk_phiErr_diff','jet_trk_phiErr_diff;Diff Track #sigma(#phi);Entries',nbins_diff,-diffrange_phiErr,diffrange_phiErr),
    'jet_trk_theta_diff'          : ROOT.TH1D('jet_trk_theta_diff','jet_trk_theta_diff;Diff Track #theta;Entries',nbins_diff,-diffrange_theta,diffrange_theta),
    'jet_trk_thetaErr_diff'       : ROOT.TH1D('jet_trk_thetaErr_diff','jet_trk_thetaErr_diff;Diff Track #sigma(#theta);Entries',nbins_diff,-diffrange_thetaErr,diffrange_thetaErr),
    'jet_trk_qop_diff'            : ROOT.TH1D('jet_trk_qop_diff','jet_trk_qop_diff;Diff Track q/p [GeV^{-1}];Entries',nbins_diff,-diffrange_qop,diffrange_qop),
    'jet_trk_qopErr_diff'         : ROOT.TH1D('jet_trk_qopErr_diff','jet_trk_qopErr_diff;Diff Track #sigma(q/p) [GeV^{-1}];Entries',nbins_diff,-diffrange_qopErr,diffrange_qopErr),
    'jet_trk_nIBLHits_diff'       : ROOT.TH1D('jet_trk_nIBLHits_diff','jet_trk_nIBLHits_diff;Diff Number of IBL hits in track;Entries',2*diffrange_nPixHits,-diffrange_nPixHits,diffrange_nPixHits),
    'jet_trk_nPixHits_diff'       : ROOT.TH1D('jet_trk_nPixHits_diff','jet_trk_nPixHits_diff;Diff Number of pixel hits in track;Entries',2*diffrange_nPixHits,-diffrange_nPixHits,diffrange_nPixHits),
    'jet_trk_nsharedPixHits_diff' : ROOT.TH1D('jet_trk_nsharedPixHits_diff','jet_trk_nsharedPixHits_diff;Diff Number of shared pixel hits in track;Entries',2*diffrange_nsharedPixHits,-diffrange_nsharedPixHits,diffrange_nsharedPixHits),
    'jet_trk_nsplitPixHits_diff'  : ROOT.TH1D('jet_trk_nsplitPixHits_diff','jet_trk_nsplitPixHits_diff;Diff Number of split pixel hits in track;Entries',2*diffrange_nsplitPixHits,-diffrange_nsplitPixHits,diffrange_nsplitPixHits),
    'jet_trk_nPixHoles_diff'      : ROOT.TH1D('jet_trk_nPixHoles_diff','jet_trk_nPixHoles_diff;Diff Number of pixel holes in track;Entries',2*diffrange_nPixHoles,-diffrange_nPixHoles,diffrange_nPixHoles),
    'jet_trk_nSCTHits_diff'       : ROOT.TH1D('jet_trk_nSCTHits_diff','jet_trk_nSCTHits_diff;Diff Number of SCT hits in track;Entries',2*diffrange_nSCTHits,-diffrange_nSCTHits,diffrange_nSCTHits),
    'jet_trk_nsharedSCTHits_diff' : ROOT.TH1D('jet_trk_nsharedSCTHits_diff','jet_trk_nsharedSCTHits_diff;Diff Number of shared SCT hits in track;Entries',2*diffrange_nsharedSCTHits,-diffrange_nsharedSCTHits,diffrange_nsharedSCTHits),
    'jet_trk_nSCTHoles_diff'      : ROOT.TH1D('jet_trk_nSCTHoles_diff','jet_trk_nSCTHoles_diff;Diff Number of SCT holes in track;Entries',2*diffrange_nSCTHoles,-diffrange_nSCTHoles,diffrange_nSCTHoles),
    'jet_trk_nPixSCTHits_diff'    : ROOT.TH1D('jet_trk_nPixSCTHits_diff','jet_trk_nPixSCTHits_diff;Diff Number of (pixel + SCT) hits in track;Entries',2*diffrange_nPixSCTHits,-diffrange_nPixSCTHits,diffrange_nPixSCTHits),
    'jet_trk_chi2_diff'           : ROOT.TH1D('jet_trk_chi2_diff','jet_trk_chi2_diff;Diff Track #chi^{2};Entries',nbins_diff,-diffrange_chi2,diffrange_chi2),
    'jet_trk_chi2OverDOF_diff'    : ROOT.TH1D('jet_trk_chi2OverDOF_diff','jet_trk_chi2OverDOF_diff;Diff Track #chi^{2}/DOF;Entries',nbins_diff,-diffrange_chi2OverDOF,diffrange_chi2OverDOF),
}

def book_comparison_histograms(sample_id, skeletons):
    hists = {}

    for t in process_trees.tracks:
        for var, h_skeleton in skeletons.items():
            # Skip 2D plots here for now
            if ('_vs_' in var and var != 'jet_nHFtracks_vs_ntrk') or '_bH_' in var or '_jf_' in var or '_rate_' in var:
                continue
            h_id = sample_id + '_' + t + '_' + var

            hists[h_id] = h_skeleton.Clone()
            hists[h_id].SetDirectory(0)
            hists[h_id+'_bjet'] = h_skeleton.Clone()
            hists[h_id+'_bjet'].SetDirectory(0)
            if 'jet_trk' in var:
                hists[h_id+'_HF'] = h_skeleton.Clone()
                hists[h_id+'_HF'].SetDirectory(0)
                hists[h_id+'_nonHF'] = h_skeleton.Clone()
                hists[h_id+'_nonHF'].SetDirectory(0)
                hists[h_id.replace('_comparison_','_comparison_matchedTracks_')] = h_skeleton.Clone()
                hists[h_id.replace('_comparison_','_comparison_matchedTracks_')].SetDirectory(0)
                hists[h_id.replace('_comparison_','_comparison_matchedTracks_')+'_bjet'] = h_skeleton.Clone()
                hists[h_id.replace('_comparison_','_comparison_matchedTracks_')+'_bjet'].SetDirectory(0)
                hists[h_id.replace('_comparison_','_comparison_matchedTracks_')+'_HF'] = h_skeleton.Clone()
                hists[h_id.replace('_comparison_','_comparison_matchedTracks_')+'_HF'].SetDirectory(0)
                hists[h_id.replace('_comparison_','_comparison_matchedTracks_')+'_nonHF'] = h_skeleton.Clone()
                hists[h_id.replace('_comparison_','_comparison_matchedTracks_')+'_nonHF'].SetDirectory(0)

    # Difference histograms
    for var, h_skeleton in diff_plot_skeletons.items():
        h_id = sample_id + '_matchedTracks_pseudodiffideal_' + var
        hists[h_id] = h_skeleton.Clone()
        hists[h_id].SetDirectory(0)
        hists[h_id+'_bjet'] = h_skeleton.Clone()
        hists[h_id+'_bjet'].SetDirectory(0)
        hists[h_id+'_HF'] = h_skeleton.Clone()
        hists[h_id+'_HF'].SetDirectory(0)
        hists[h_id+'_nonHF'] = h_skeleton.Clone()
        hists[h_id+'_nonHF'].SetDirectory(0)

    return hists

def fill_diff_track_histograms(t1, t2, ijet, jtrk1, jtrk2, hists, file_id):

    is_bjet1,isbjet2          = process_trees.select_jet(t1, ijet, 'bjet'),process_trees.select_jet(t2, ijet, 'bjet')
    is_HF_track1,is_HF_track2 = process_trees.select_track(t1, ijet, jtrk1, 'HF'),process_trees.select_track(t2, ijet, jtrk2, 'HF')

    trk1_theta,trk2_theta                   = t1.jet_trk_theta[ijet][jtrk1],t2.jet_trk_theta[ijet][jtrk2]
    trk1_ip3d_d0sig,trk2_ip3d_d0sig         = t1.jet_trk_ip3d_d0sig[ijet][jtrk1],t2.jet_trk_ip3d_d0sig[ijet][jtrk2]
    trk1_d0,trk2_d0                         = t1.jet_trk_d0[ijet][jtrk1],t2.jet_trk_d0[ijet][jtrk2]
    trk1_d0Truth,trk2_d0Truth               = t1.jet_trk_d0Truth[ijet][jtrk1],t2.jet_trk_d0Truth[ijet][jtrk2]
    trk1_d0Err,trk2_d0Err                   = t1.jet_trk_d0Err[ijet][jtrk1],t2.jet_trk_d0Err[ijet][jtrk2]
    trk1_d0res,trk2_d0res                   = trk1_d0 - trk1_d0Truth,trk2_d0 - trk2_d0Truth
    trk1_d0pull,trk2_d0pull                 = trk1_d0res/trk1_d0Err,trk2_d0res/trk2_d0Err
    trk1_ip3d_z0sig,trk2_ip3d_z0sig         = t1.jet_trk_ip3d_z0sig[ijet][jtrk1],t2.jet_trk_ip3d_z0sig[ijet][jtrk2]
    trk1_z0,trk2_z0                         = t1.jet_trk_z0[ijet][jtrk1],t2.jet_trk_z0[ijet][jtrk2]
    trk1_z0sintheta,trk2_z0sintheta         = trk1_z0*math.sin(trk1_theta),trk2_z0*math.sin(trk2_theta)
    trk1_z0Truth,trk2_z0Truth               = t1.jet_trk_z0Truth[ijet][jtrk1],t2.jet_trk_z0Truth[ijet][jtrk2]
    trk1_z0Err,trk2_z0Err                   = t1.jet_trk_z0Err[ijet][jtrk1],t2.jet_trk_z0Err[ijet][jtrk2]
    trk1_z0res,trk2_z0res                   = trk1_z0 - trk1_z0Truth,trk2_z0 - trk2_z0Truth
    trk1_z0pull,trk2_z0pull                 = trk1_z0res/trk1_z0Err,trk2_z0res/trk2_z0Err
    trk1_phi,trk2_phi                       = t1.jet_trk_phi[ijet][jtrk1],t2.jet_trk_phi[ijet][jtrk2]
    trk1_phiErr,trk2_phiErr                 = math.sqrt(t1.jet_trk_cov_phiphi[ijet][jtrk1]),math.sqrt(t2.jet_trk_cov_phiphi[ijet][jtrk2])
    trk1_theta,trk2_theta                   = t1.jet_trk_theta[ijet][jtrk1],t2.jet_trk_theta[ijet][jtrk2]
    trk1_thetaErr,trk2_thetaErr             = math.sqrt(t1.jet_trk_cov_thetatheta[ijet][jtrk1]),math.sqrt(t2.jet_trk_cov_thetatheta[ijet][jtrk2])
    trk1_qop,trk2_qop                       = t1.jet_trk_qoverp[ijet][jtrk1]*1000.,t2.jet_trk_qoverp[ijet][jtrk2]*1000.
    trk1_qopErr,trk2_qopErr                 = math.sqrt(t1.jet_trk_cov_qoverpqoverp[ijet][jtrk1])*1000.,math.sqrt(t2.jet_trk_cov_qoverpqoverp[ijet][jtrk2])*1000.
    trk1_nIBLHits,trk2_nIBLHits             = t1.jet_trk_nInnHits[ijet][jtrk1],t2.jet_trk_nInnHits[ijet][jtrk2]
    trk1_nPixHits,trk2_nPixHits             = t1.jet_trk_nPixHits[ijet][jtrk1],t2.jet_trk_nPixHits[ijet][jtrk2]
    trk1_nsharedPixHits,trk2_nsharedPixHits = t1.jet_trk_nsharedPixHits[ijet][jtrk1],t2.jet_trk_nsharedPixHits[ijet][jtrk2]
    trk1_nsplitPixHits,trk2_nsplitPixHits   = t1.jet_trk_nsplitPixHits[ijet][jtrk1],t2.jet_trk_nsplitPixHits[ijet][jtrk2]
    trk1_nPixHoles,trk2_nPixHoles           = t1.jet_trk_nPixHoles[ijet][jtrk1],t2.jet_trk_nPixHoles[ijet][jtrk2]
    trk1_nSCTHits,trk2_nSCTHits             = t1.jet_trk_nSCTHits[ijet][jtrk1],t2.jet_trk_nSCTHits[ijet][jtrk2]
    trk1_nsharedSCTHits,trk2_nsharedSCTHits = t1.jet_trk_nsharedSCTHits[ijet][jtrk1],t2.jet_trk_nsharedSCTHits[ijet][jtrk2]
    trk1_nSCTHoles,trk2_nSCTHoles           = t1.jet_trk_nSCTHoles[ijet][jtrk1],t2.jet_trk_nSCTHoles[ijet][jtrk2]
    trk1_nPixSCTHits,trk2_nPixSCTHits       = trk1_nPixHits + trk1_nSCTHits,trk2_nPixHits + trk2_nSCTHits
    trk1_chi2,trk2_chi2                     = t1.jet_trk_chi2[ijet][jtrk1],t2.jet_trk_chi2[ijet][jtrk2]
    trk1_ndf,trk2_ndf                       = t1.jet_trk_ndf[ijet][jtrk1],t2.jet_trk_ndf[ijet][jtrk2]
    try:
        trk1_chi2OverDOF = trk1_chi2/trk1_ndf
    except ZeroDivisionError:
        print('Division by zero, setting chi2/DOF to chi2: chi2 = {}, DOF = {}'.format(trk1_chi2, trk1_ndf))
        trk1_chi2OverDOF = trk1_chi2
    try:
        trk2_chi2OverDOF = trk2_chi2/trk2_ndf
    except ZeroDivisionError:
        print('Division by zero, setting chi2/DOF to chi2: chi2 = {}, DOF = {}'.format(trk2_chi2, trk2_ndf))
        trk2_chi2OverDOF = trk2_chi2

    diff_ip3d_d0sig = trk1_ip3d_d0sig - trk2_ip3d_d0sig
    diff_d0 = trk1_d0 - trk2_d0
    diff_d0Truth = trk1_d0Truth - trk2_d0Truth
    diff_d0Err = trk1_d0Err - trk2_d0Err
    diff_d0res = trk1_d0res - trk2_d0res
    diff_d0pull = trk1_d0pull - trk2_d0pull
    diff_ip3d_z0sig = trk1_ip3d_z0sig - trk2_ip3d_z0sig
    diff_z0 = trk1_z0 - trk2_z0
    diff_z0sintheta = trk1_z0sintheta - trk2_z0sintheta
    diff_z0Truth = trk1_z0Truth - trk2_z0Truth
    diff_z0Err = trk1_z0Err - trk2_z0Err
    diff_z0res = trk1_z0res - trk2_z0res
    diff_z0pull = trk1_z0pull - trk2_z0pull
    diff_phi = trk1_phi - trk2_phi
    diff_phiErr = trk1_phiErr - trk2_phiErr
    diff_theta = trk1_theta - trk2_theta
    diff_thetaErr = trk1_thetaErr - trk2_thetaErr
    diff_qop = trk1_qop - trk2_qop
    diff_qopErr = trk1_qopErr - trk2_qopErr
    diff_nIBLHits = trk1_nIBLHits - trk2_nIBLHits
    diff_nPixHits = trk1_nPixHits - trk2_nPixHits
    diff_nsharedPixHits = trk1_nsharedPixHits - trk2_nsharedPixHits
    diff_nsplitPixHits = trk1_nsplitPixHits - trk2_nsplitPixHits
    diff_nPixHoles = trk1_nPixHoles - trk2_nPixHoles
    diff_nSCTHits = trk1_nSCTHits - trk2_nSCTHits
    diff_nsharedSCTHits = trk1_nsharedSCTHits - trk2_nsharedSCTHits
    diff_nSCTHoles = trk1_nSCTHoles - trk2_nSCTHoles
    diff_nPixSCTHits = trk1_nPixSCTHits - trk2_nPixSCTHits
    diff_chi2 = trk1_chi2 - trk2_chi2
    diff_chi2OverDOF = trk1_chi2OverDOF - trk2_chi2OverDOF

    hists[file_id+'_jet_trk_ip3d_d0sig_diff'].Fill(diff_ip3d_d0sig)
    hists[file_id+'_jet_trk_d0_diff'].Fill(diff_d0)
    hists[file_id+'_jet_trk_d0Truth_diff'].Fill(diff_d0Truth)
    hists[file_id+'_jet_trk_d0Err_diff'].Fill(diff_d0Err)
    hists[file_id+'_jet_trk_d0res_diff'].Fill(diff_d0res)
    hists[file_id+'_jet_trk_d0pull_diff'].Fill(diff_d0pull)
    hists[file_id+'_jet_trk_ip3d_z0sig_diff'].Fill(diff_ip3d_z0sig)
    hists[file_id+'_jet_trk_z0_diff'].Fill(diff_z0)
    hists[file_id+'_jet_trk_z0sintheta_diff'].Fill(diff_z0sintheta)
    hists[file_id+'_jet_trk_z0Truth_diff'].Fill(diff_z0Truth)
    hists[file_id+'_jet_trk_z0Err_diff'].Fill(diff_z0Err)
    hists[file_id+'_jet_trk_z0res_diff'].Fill(diff_z0res)
    hists[file_id+'_jet_trk_z0pull_diff'].Fill(diff_z0pull)
    hists[file_id+'_jet_trk_phi_diff'].Fill(diff_phi)
    hists[file_id+'_jet_trk_phiErr_diff'].Fill(diff_phiErr)
    hists[file_id+'_jet_trk_theta_diff'].Fill(diff_theta)
    hists[file_id+'_jet_trk_thetaErr_diff'].Fill(diff_thetaErr)
    hists[file_id+'_jet_trk_qop_diff'].Fill(diff_qop)
    hists[file_id+'_jet_trk_qopErr_diff'].Fill(diff_qopErr)
    hists[file_id+'_jet_trk_nIBLHits_diff'].Fill(diff_nIBLHits)
    hists[file_id+'_jet_trk_nPixHits_diff'].Fill(diff_nPixHits)
    hists[file_id+'_jet_trk_nsharedPixHits_diff'].Fill(diff_nsharedPixHits)
    hists[file_id+'_jet_trk_nsplitPixHits_diff'].Fill(diff_nsplitPixHits)
    hists[file_id+'_jet_trk_nPixHoles_diff'].Fill(diff_nPixHoles)
    hists[file_id+'_jet_trk_nSCTHits_diff'].Fill(diff_nSCTHits)
    hists[file_id+'_jet_trk_nsharedSCTHits_diff'].Fill(diff_nsharedSCTHits)
    hists[file_id+'_jet_trk_nSCTHoles_diff'].Fill(diff_nSCTHoles)
    hists[file_id+'_jet_trk_nPixSCTHits_diff'].Fill(diff_nPixSCTHits)
    hists[file_id+'_jet_trk_chi2_diff'].Fill(diff_chi2)
    hists[file_id+'_jet_trk_chi2OverDOF_diff'].Fill(diff_chi2OverDOF)

    if is_bjet1 and isbjet2:
        hists[file_id+'_jet_trk_ip3d_d0sig_diff_bjet'].Fill(diff_ip3d_d0sig)
        hists[file_id+'_jet_trk_d0_diff_bjet'].Fill(diff_d0)
        hists[file_id+'_jet_trk_d0Truth_diff_bjet'].Fill(diff_d0Truth)
        hists[file_id+'_jet_trk_d0Err_diff_bjet'].Fill(diff_d0Err)
        hists[file_id+'_jet_trk_d0res_diff_bjet'].Fill(diff_d0res)
        hists[file_id+'_jet_trk_d0pull_diff_bjet'].Fill(diff_d0pull)
        hists[file_id+'_jet_trk_ip3d_z0sig_diff_bjet'].Fill(diff_ip3d_z0sig)
        hists[file_id+'_jet_trk_z0_diff_bjet'].Fill(diff_z0)
        hists[file_id+'_jet_trk_z0sintheta_diff_bjet'].Fill(diff_z0sintheta)
        hists[file_id+'_jet_trk_z0Truth_diff_bjet'].Fill(diff_z0Truth)
        hists[file_id+'_jet_trk_z0Err_diff_bjet'].Fill(diff_z0Err)
        hists[file_id+'_jet_trk_z0res_diff_bjet'].Fill(diff_z0res)
        hists[file_id+'_jet_trk_z0pull_diff_bjet'].Fill(diff_z0pull)
        hists[file_id+'_jet_trk_phi_diff_bjet'].Fill(diff_phi)
        hists[file_id+'_jet_trk_phiErr_diff_bjet'].Fill(diff_phiErr)
        hists[file_id+'_jet_trk_theta_diff_bjet'].Fill(diff_theta)
        hists[file_id+'_jet_trk_thetaErr_diff_bjet'].Fill(diff_thetaErr)
        hists[file_id+'_jet_trk_qop_diff_bjet'].Fill(diff_qop)
        hists[file_id+'_jet_trk_qopErr_diff_bjet'].Fill(diff_qopErr)
        hists[file_id+'_jet_trk_nIBLHits_diff_bjet'].Fill(diff_nIBLHits)
        hists[file_id+'_jet_trk_nPixHits_diff_bjet'].Fill(diff_nPixHits)
        hists[file_id+'_jet_trk_nsharedPixHits_diff_bjet'].Fill(diff_nsharedPixHits)
        hists[file_id+'_jet_trk_nsplitPixHits_diff_bjet'].Fill(diff_nsplitPixHits)
        hists[file_id+'_jet_trk_nPixHoles_diff_bjet'].Fill(diff_nPixHoles)
        hists[file_id+'_jet_trk_nSCTHits_diff_bjet'].Fill(diff_nSCTHits)
        hists[file_id+'_jet_trk_nsharedSCTHits_diff_bjet'].Fill(diff_nsharedSCTHits)
        hists[file_id+'_jet_trk_nSCTHoles_diff_bjet'].Fill(diff_nSCTHoles)
        hists[file_id+'_jet_trk_nPixSCTHits_diff_bjet'].Fill(diff_nPixSCTHits)
        hists[file_id+'_jet_trk_chi2_diff_bjet'].Fill(diff_chi2)
        hists[file_id+'_jet_trk_chi2OverDOF_diff_bjet'].Fill(diff_chi2OverDOF)

    if  is_HF_track1 and is_HF_track2:
        hists[file_id+'_jet_trk_ip3d_d0sig_diff_HF'].Fill(diff_ip3d_d0sig)
        hists[file_id+'_jet_trk_d0_diff_HF'].Fill(diff_d0)
        hists[file_id+'_jet_trk_d0Truth_diff_HF'].Fill(diff_d0Truth)
        hists[file_id+'_jet_trk_d0Err_diff_HF'].Fill(diff_d0Err)
        hists[file_id+'_jet_trk_d0res_diff_HF'].Fill(diff_d0res)
        hists[file_id+'_jet_trk_d0pull_diff_HF'].Fill(diff_d0pull)
        hists[file_id+'_jet_trk_ip3d_z0sig_diff_HF'].Fill(diff_ip3d_z0sig)
        hists[file_id+'_jet_trk_z0_diff_HF'].Fill(diff_z0)
        hists[file_id+'_jet_trk_z0sintheta_diff_HF'].Fill(diff_z0sintheta)
        hists[file_id+'_jet_trk_z0Truth_diff_HF'].Fill(diff_z0Truth)
        hists[file_id+'_jet_trk_z0Err_diff_HF'].Fill(diff_z0Err)
        hists[file_id+'_jet_trk_z0res_diff_HF'].Fill(diff_z0res)
        hists[file_id+'_jet_trk_z0pull_diff_HF'].Fill(diff_z0pull)
        hists[file_id+'_jet_trk_phi_diff_HF'].Fill(diff_phi)
        hists[file_id+'_jet_trk_phiErr_diff_HF'].Fill(diff_phiErr)
        hists[file_id+'_jet_trk_theta_diff_HF'].Fill(diff_theta)
        hists[file_id+'_jet_trk_thetaErr_diff_HF'].Fill(diff_thetaErr)
        hists[file_id+'_jet_trk_qop_diff_HF'].Fill(diff_qop)
        hists[file_id+'_jet_trk_qopErr_diff_HF'].Fill(diff_qopErr)
        hists[file_id+'_jet_trk_nIBLHits_diff_HF'].Fill(diff_nIBLHits)
        hists[file_id+'_jet_trk_nPixHits_diff_HF'].Fill(diff_nPixHits)
        hists[file_id+'_jet_trk_nsharedPixHits_diff_HF'].Fill(diff_nsharedPixHits)
        hists[file_id+'_jet_trk_nsplitPixHits_diff_HF'].Fill(diff_nsplitPixHits)
        hists[file_id+'_jet_trk_nPixHoles_diff_HF'].Fill(diff_nPixHoles)
        hists[file_id+'_jet_trk_nSCTHits_diff_HF'].Fill(diff_nSCTHits)
        hists[file_id+'_jet_trk_nsharedSCTHits_diff_HF'].Fill(diff_nsharedSCTHits)
        hists[file_id+'_jet_trk_nSCTHoles_diff_HF'].Fill(diff_nSCTHoles)
        hists[file_id+'_jet_trk_nPixSCTHits_diff_HF'].Fill(diff_nPixSCTHits)
        hists[file_id+'_jet_trk_chi2_diff_HF'].Fill(diff_chi2)
        hists[file_id+'_jet_trk_chi2OverDOF_diff_HF'].Fill(diff_chi2OverDOF)

def compare_matched_tracks(t_nom, t_pseudo, t_ideal, ijet, hists, file_id):

    jet_ntracks_nom,jet_ntracks_pseudo,jet_ntracks_ideal = t_nom.jet_btag_ntrk[ijet],t_pseudo.jet_btag_ntrk[ijet],t_ideal.jet_btag_ntrk[ijet]
    #is_bjet_nom,is_bjet_pseudo,is_bjet_ideal = process_trees.select_jet(t_nom, ijet, 'bjet'),process_trees.select_jet(t_pseudo, ijet, 'bjet'),process_trees.select_jet(t_ideal, ijet, 'bjet')

    # Was a particle reconstructed; does there exist a reco track with a matching barcode?
    # Should a particle have been reconstructed; does there exist a pseudo or ideal track with a matching barcode?
    # If it was reconstructed, how well did we do; compare reco to ideal track after matching barcodes.

    # Loop over tracks in the jet
    # print('Nominal:')
    # for jtrk_nom in range(jet_ntracks_nom):
    #     if not process_trees.select_track(t_nom, ijet, jtrk_nom):
    #         continue
    #     trk_barcode_nom = t_nom.jet_trk_barcode[ijet][jtrk_nom]
    #     trk_truthMatchProbability_nom = t_nom.jet_trk_truthMatchProbability[ijet][jtrk_nom]
    #     trk_orig_nom = t_nom.jet_trk_orig[ijet][jtrk_nom]
    #     print('Index: {}, Barcode: {}, Probability: {}, Origin: {}'.format(jtrk_nom,trk_barcode_nom,trk_truthMatchProbability_nom,trk_orig_nom))

    # print('Pseudo:')
    # for jtrk_pseudo in range(jet_ntracks_pseudo):
    #     if not process_trees.select_track(t_pseudo, ijet, jtrk_pseudo):
    #         continue
    #     trk_barcode_pseudo = t_pseudo.jet_trk_barcode[ijet][jtrk_pseudo]
    #     trk_truthMatchProbability_pseudo = t_pseudo.jet_trk_truthMatchProbability[ijet][jtrk_pseudo]
    #     trk_orig_pseudo = t_pseudo.jet_trk_orig[ijet][jtrk_pseudo]
    #     print('Index: {}, Barcode: {}, Probability: {}, Origin: {}'.format(jtrk_pseudo,trk_barcode_pseudo,trk_truthMatchProbability_pseudo,trk_orig_pseudo))

    # print('Ideal:')
    # for jtrk_ideal in range(jet_ntracks_ideal):
    #     if not process_trees.select_track(t_ideal, ijet, jtrk_ideal):
    #         continue
    #     trk_barcode_ideal = t_ideal.jet_trk_barcode[ijet][jtrk_ideal]
    #     trk_truthMatchProbability_ideal = t_ideal.jet_trk_truthMatchProbability[ijet][jtrk_ideal]
    #     trk_orig_ideal = t_ideal.jet_trk_orig[ijet][jtrk_ideal]
    #     print('Index: {}, Barcode: {}, Probability: {}, Origin: {}'.format(jtrk_ideal,trk_barcode_ideal,trk_truthMatchProbability_ideal,trk_orig_ideal))


    # Start by simply comparing pseudo tracks to ideal tracks properties
    for jtrk_pseudo in range(jet_ntracks_pseudo):
        if not process_trees.select_track(t_pseudo, ijet, jtrk_pseudo):
            continue
        trk_barcode_pseudo = t_pseudo.jet_trk_barcode[ijet][jtrk_pseudo]
        trk_truthMatchProbability_pseudo = t_pseudo.jet_trk_truthMatchProbability[ijet][jtrk_pseudo]
        trk_orig_pseudo = t_pseudo.jet_trk_orig[ijet][jtrk_pseudo]
        # Only want tracks matched to truth particles...
        if trk_barcode_pseudo < 0:
            print('No truth match: jtrk_pseudo = {}, barcode = {}'.format(jtrk_pseudo,trk_barcode_pseudo))
            continue
        match_found = False
        for jtrk_ideal in range(jet_ntracks_ideal):
            if not process_trees.select_track(t_ideal, ijet, jtrk_ideal):
                continue
            trk_barcode_ideal = t_ideal.jet_trk_barcode[ijet][jtrk_ideal]
            trk_truthMatchProbability_ideal = t_ideal.jet_trk_truthMatchProbability[ijet][jtrk_ideal]
            trk_orig_ideal = t_ideal.jet_trk_orig[ijet][jtrk_ideal]

            if trk_barcode_pseudo == trk_barcode_ideal:
                # Tracks match!
                match_found = True
                process_trees.fill_track_histograms(t_pseudo, ijet, jtrk_pseudo, hists, file_id+'_pseudo')
                process_trees.fill_track_histograms(t_ideal, ijet, jtrk_ideal, hists, file_id+'_ideal')
                # Plot diff of track quantities here instead of just filling histograms as usual!
                fill_diff_track_histograms(t_pseudo, t_ideal, ijet, jtrk_pseudo, jtrk_ideal, hists, file_id+'_pseudodiffideal')
            else:
                continue

        # if not match_found:
        #     print('ERROR!! NO IDEAL MATCH FOUND FOR PSEUDO TRACK {}. BARCODE: {}, ORIGIN: {}.'.format(jtrk_pseudo,trk_barcode_pseudo,trk_orig_pseudo))
    
def fill_comparison_histograms(t_nom, t_pseudo, t_ideal, hists, file_id):
    # Fill debug plots comparing different track types
    njets_nom,njets_pseudo,njets_ideal = t_nom.njets,t_pseudo.njets,t_ideal.njets

    # Sanity check
    if njets_nom != njets_pseudo or njets_nom != njets_ideal:
        print('Different number of jets, skipping event!!')
        print('Nominal: {}, Pseudo: {}, Ideal: {}'.format(njets_nom,njets_pseudo,njets_ideal))
        return True
    
    nbjets_nom,nbjets_pseudo,nbjets_ideal = 0,0,0

    nStrangeJets = 0

    # Loop over jets
    for ijet in range(njets_nom):
        # Apply relevant jet selection
        good_jet_nom    = process_trees.select_jet(t_nom, ijet)
        good_jet_pseudo = process_trees.select_jet(t_pseudo, ijet)
        good_jet_ideal  = process_trees.select_jet(t_ideal, ijet)

        # Sanity check
        if good_jet_nom != good_jet_pseudo or good_jet_nom != good_jet_ideal:
            print('Different results for jet selection, skipping jet!!')
            print('Nominal: {}, Pseudo: {}, Ideal: {}'.format(good_jet_nom,good_jet_pseudo,good_jet_ideal))
            continue

        if not good_jet_nom:
            continue

        # For now, look at events that fail SV1 with ideal tracks, but pass SV1 with pseudo
        jet_sv1_Nvtx_nom,jet_sv1_Nvtx_pseudo,jet_sv1_Nvtx_ideal = t_nom.jet_sv1_Nvtx[ijet],t_pseudo.jet_sv1_Nvtx[ijet],t_ideal.jet_sv1_Nvtx[ijet]

        if not (jet_sv1_Nvtx_pseudo>0 and jet_sv1_Nvtx_ideal==0):
            continue

        nStrangeJets += 1

        # Fill relevant pseudo tracks debug quantities
        jet_pt_nom,jet_pt_pseudo,jet_pt_ideal = t_nom.jet_pt[ijet]/1000.,t_pseudo.jet_pt[ijet]/1000.,t_ideal.jet_pt[ijet]/1000.
        jet_eta_nom,jet_eta_pseudo,jet_eta_ideal = t_nom.jet_eta[ijet],t_pseudo.jet_eta[ijet],t_ideal.jet_eta[ijet]
        jet_phi_nom,jet_phi_pseudo,jet_phi_ideal = t_nom.jet_phi[ijet],t_pseudo.jet_phi[ijet],t_ideal.jet_phi[ijet]
        jet_ntracks_nom,jet_ntracks_pseudo,jet_ntracks_ideal = t_nom.jet_btag_ntrk[ijet],t_pseudo.jet_btag_ntrk[ijet],t_ideal.jet_btag_ntrk[ijet]
        jet_dRiso_nom,jet_dRiso_pseudo,jet_dRiso_ideal = t_nom.jet_dRiso[ijet],t_pseudo.jet_dRiso[ijet],t_ideal.jet_dRiso[ijet]
        jet_sv1_Lxy_nom,jet_sv1_Lxy_pseudo,jet_sv1_Lxy_ideal = t_nom.jet_sv1_Lxy[ijet],t_pseudo.jet_sv1_Lxy[ijet],t_ideal.jet_sv1_Lxy[ijet]
        jet_sv1_Nvtx_nom,jet_sv1_Nvtx_pseudo,jet_sv1_Nvtx_ideal = t_nom.jet_sv1_Nvtx[ijet],t_pseudo.jet_sv1_Nvtx[ijet],t_ideal.jet_sv1_Nvtx[ijet]
        jet_sv1_efc_nom,jet_sv1_efc_pseudo,jet_sv1_efc_ideal = t_nom.jet_sv1_efc[ijet],t_pseudo.jet_sv1_efc[ijet],t_ideal.jet_sv1_efc[ijet]
        jet_sv1_ntrkv_nom,jet_sv1_ntrkv_pseudo,jet_sv1_ntrkv_ideal = t_nom.jet_sv1_ntrkv[ijet],t_pseudo.jet_sv1_ntrkv[ijet],t_ideal.jet_sv1_efc[ijet]

        hists[file_id+'_nom_jet_pt'].Fill(jet_pt_nom)
        hists[file_id+'_nom_jet_eta'].Fill(jet_eta_nom)
        hists[file_id+'_nom_jet_phi'].Fill(jet_phi_nom)
        hists[file_id+'_nom_jet_ntrk'].Fill(jet_ntracks_nom)
        hists[file_id+'_nom_jet_dRiso'].Fill(jet_dRiso_nom)
        hists[file_id+'_nom_jet_sv1_Lxy'].Fill(jet_sv1_Lxy_nom)
        hists[file_id+'_nom_jet_sv1_Nvtx'].Fill(jet_sv1_Nvtx_nom)
        hists[file_id+'_nom_jet_sv1_efc'].Fill(jet_sv1_efc_nom)
        hists[file_id+'_nom_jet_sv1_ntrkv'].Fill(jet_sv1_ntrkv_nom)

        hists[file_id+'_pseudo_jet_pt'].Fill(jet_pt_pseudo)
        hists[file_id+'_pseudo_jet_eta'].Fill(jet_eta_pseudo)
        hists[file_id+'_pseudo_jet_phi'].Fill(jet_phi_pseudo)
        hists[file_id+'_pseudo_jet_ntrk'].Fill(jet_ntracks_pseudo)
        hists[file_id+'_pseudo_jet_dRiso'].Fill(jet_dRiso_pseudo)
        hists[file_id+'_pseudo_jet_sv1_Lxy'].Fill(jet_sv1_Lxy_pseudo)
        hists[file_id+'_pseudo_jet_sv1_Nvtx'].Fill(jet_sv1_Nvtx_pseudo)
        hists[file_id+'_pseudo_jet_sv1_efc'].Fill(jet_sv1_efc_pseudo)
        hists[file_id+'_pseudo_jet_sv1_ntrkv'].Fill(jet_sv1_ntrkv_pseudo)

        hists[file_id+'_ideal_jet_pt'].Fill(jet_pt_ideal)
        hists[file_id+'_ideal_jet_eta'].Fill(jet_eta_ideal)
        hists[file_id+'_ideal_jet_phi'].Fill(jet_phi_ideal)
        hists[file_id+'_ideal_jet_ntrk'].Fill(jet_ntracks_ideal)
        hists[file_id+'_ideal_jet_dRiso'].Fill(jet_dRiso_ideal)
        hists[file_id+'_ideal_jet_sv1_Lxy'].Fill(jet_sv1_Lxy_ideal)
        hists[file_id+'_ideal_jet_sv1_Nvtx'].Fill(jet_sv1_Nvtx_ideal)
        hists[file_id+'_ideal_jet_sv1_efc'].Fill(jet_sv1_efc_ideal)
        hists[file_id+'_ideal_jet_sv1_ntrkv'].Fill(jet_sv1_ntrkv_ideal)

        # Check only bjets for now
        is_bjet_nom,is_bjet_pseudo,is_bjet_ideal = process_trees.select_jet(t_nom, ijet, 'bjet'),process_trees.select_jet(t_pseudo, ijet, 'bjet'),process_trees.select_jet(t_ideal, ijet, 'bjet')

        if is_bjet_nom:
            nbjets_nom += 1
            hists[file_id+'_nom_jet_pt_bjet'].Fill(jet_pt_nom)
            hists[file_id+'_nom_jet_eta_bjet'].Fill(jet_eta_nom)
            hists[file_id+'_nom_jet_phi_bjet'].Fill(jet_phi_nom)
            hists[file_id+'_nom_jet_ntrk_bjet'].Fill(jet_ntracks_nom)
            hists[file_id+'_nom_jet_dRiso_bjet'].Fill(jet_dRiso_nom)
            hists[file_id+'_nom_jet_sv1_Lxy_bjet'].Fill(jet_sv1_Lxy_nom)
            hists[file_id+'_nom_jet_sv1_Nvtx_bjet'].Fill(jet_sv1_Nvtx_nom)
            hists[file_id+'_nom_jet_sv1_efc_bjet'].Fill(jet_sv1_efc_nom)
            hists[file_id+'_nom_jet_sv1_ntrkv_bjet'].Fill(jet_sv1_ntrkv_nom)

        if is_bjet_pseudo:
            nbjets_pseudo += 1
            hists[file_id+'_pseudo_jet_pt_bjet'].Fill(jet_pt_pseudo)
            hists[file_id+'_pseudo_jet_eta_bjet'].Fill(jet_eta_pseudo)
            hists[file_id+'_pseudo_jet_phi_bjet'].Fill(jet_phi_pseudo)
            hists[file_id+'_pseudo_jet_ntrk_bjet'].Fill(jet_ntracks_pseudo)
            hists[file_id+'_pseudo_jet_dRiso_bjet'].Fill(jet_dRiso_pseudo)
            hists[file_id+'_pseudo_jet_sv1_Lxy_bjet'].Fill(jet_sv1_Lxy_pseudo)
            hists[file_id+'_pseudo_jet_sv1_Nvtx_bjet'].Fill(jet_sv1_Nvtx_pseudo)
            hists[file_id+'_pseudo_jet_sv1_efc_bjet'].Fill(jet_sv1_efc_pseudo)
            hists[file_id+'_pseudo_jet_sv1_ntrkv_bjet'].Fill(jet_sv1_ntrkv_pseudo)

        if is_bjet_ideal:
            nbjets_ideal += 1
            hists[file_id+'_ideal_jet_pt_bjet'].Fill(jet_pt_ideal)
            hists[file_id+'_ideal_jet_eta_bjet'].Fill(jet_eta_ideal)
            hists[file_id+'_ideal_jet_phi_bjet'].Fill(jet_phi_ideal)
            hists[file_id+'_ideal_jet_ntrk_bjet'].Fill(jet_ntracks_ideal)
            hists[file_id+'_ideal_jet_dRiso_bjet'].Fill(jet_dRiso_ideal)
            hists[file_id+'_ideal_jet_sv1_Lxy_bjet'].Fill(jet_sv1_Lxy_ideal)
            hists[file_id+'_ideal_jet_sv1_Nvtx_bjet'].Fill(jet_sv1_Nvtx_ideal)
            hists[file_id+'_ideal_jet_sv1_efc_bjet'].Fill(jet_sv1_efc_ideal)
            hists[file_id+'_ideal_jet_sv1_ntrkv_bjet'].Fill(jet_sv1_ntrkv_ideal)

        
        jet_nHFtracks_nom,jet_nHFtracks_pseudo,jet_nHFtracks_ideal = 0,0,0

        # Now loop over tracks
        for jtrk in range(jet_ntracks_nom):
            if not process_trees.select_track(t_nom, ijet, jtrk):
                continue
            process_trees.fill_track_histograms(t_nom, ijet, jtrk, hists, file_id+'_nom')
            if process_trees.select_track(t_nom, ijet, jtrk, 'HF'):
                jet_nHFtracks_nom += 1
        for jtrk in range(jet_ntracks_pseudo):
            if not process_trees.select_track(t_pseudo, ijet, jtrk):
                continue
            process_trees.fill_track_histograms(t_pseudo, ijet, jtrk, hists, file_id+'_pseudo')
            if process_trees.select_track(t_pseudo, ijet, jtrk, 'HF'):
                jet_nHFtracks_pseudo += 1
        for jtrk in range(jet_ntracks_ideal):
            if not process_trees.select_track(t_ideal, ijet, jtrk):
                continue
            process_trees.fill_track_histograms(t_ideal, ijet, jtrk, hists, file_id+'_ideal')
            if process_trees.select_track(t_ideal, ijet, jtrk, 'HF'):
                jet_nHFtracks_ideal += 1

        jet_nOthertracks_nom = jet_ntracks_nom - jet_nHFtracks_nom
        jet_nOthertracks_pseudo = jet_ntracks_pseudo - jet_nHFtracks_pseudo
        jet_nOthertracks_ideal = jet_ntracks_ideal - jet_nHFtracks_ideal

        hists[file_id+'_nom_jet_nHFtracks'].Fill(jet_nHFtracks_nom)
        hists[file_id+'_nom_jet_nOthertracks'].Fill(jet_nOthertracks_nom)
        hists[file_id+'_pseudo_jet_nHFtracks'].Fill(jet_nHFtracks_pseudo)
        hists[file_id+'_pseudo_jet_nOthertracks'].Fill(jet_nOthertracks_pseudo)
        hists[file_id+'_ideal_jet_nHFtracks'].Fill(jet_nHFtracks_ideal)
        hists[file_id+'_ideal_jet_nOthertracks'].Fill(jet_nOthertracks_ideal)
        if is_bjet_nom:
            hists[file_id+'_nom_jet_nHFtracks_bjet'].Fill(jet_nHFtracks_nom)
            hists[file_id+'_nom_jet_nOthertracks_bjet'].Fill(jet_nOthertracks_nom)
        if is_bjet_pseudo:
            hists[file_id+'_pseudo_jet_nHFtracks_bjet'].Fill(jet_nHFtracks_pseudo)
            hists[file_id+'_pseudo_jet_nOthertracks_bjet'].Fill(jet_nOthertracks_pseudo)
        if is_bjet_ideal:
            hists[file_id+'_ideal_jet_nHFtracks_bjet'].Fill(jet_nHFtracks_ideal)
            hists[file_id+'_ideal_jet_nOthertracks_bjet'].Fill(jet_nOthertracks_ideal)

        # Plot things for tracks matched to truth particles
        compare_matched_tracks(t_nom, t_pseudo, t_ideal, ijet, hists, file_id+'_matchedTracks')

    # Only want event-level quantities for events having at least one interesting jet
    if nStrangeJets > 0:
        hists[file_id+'_nom_njets'].Fill(nStrangeJets)
        hists[file_id+'_pseudo_njets'].Fill(nStrangeJets)
        hists[file_id+'_ideal_njets'].Fill(nStrangeJets)
        
        hists[file_id+'_nom_njets_bjet'].Fill(nbjets_nom)
        hists[file_id+'_pseudo_njets_bjet'].Fill(nbjets_pseudo)
        hists[file_id+'_ideal_njets_bjet'].Fill(nbjets_ideal)

        PVx = t_nom.PVx
        PVy = t_nom.PVy
        PVz = t_nom.PVz
        hists[file_id+'_nom_PVx'].Fill(PVx)
        hists[file_id+'_nom_PVy'].Fill(PVy)
        hists[file_id+'_nom_PVz'].Fill(PVz)
        hists[file_id+'_pseudo_PVx'].Fill(PVx)
        hists[file_id+'_pseudo_PVy'].Fill(PVy)
        hists[file_id+'_pseudo_PVz'].Fill(PVz)
        hists[file_id+'_ideal_PVx'].Fill(PVx)
        hists[file_id+'_ideal_PVy'].Fill(PVy)
        hists[file_id+'_ideal_PVz'].Fill(PVz)

        if nbjets_nom > 0:
            hists[file_id+'_nom_PVx_bjet'].Fill(PVx)
            hists[file_id+'_nom_PVy_bjet'].Fill(PVy)
            hists[file_id+'_nom_PVz_bjet'].Fill(PVz)
        if nbjets_pseudo > 0:
            hists[file_id+'_pseudo_PVx_bjet'].Fill(PVx)
            hists[file_id+'_pseudo_PVy_bjet'].Fill(PVy)
            hists[file_id+'_pseudo_PVz_bjet'].Fill(PVz)
        if nbjets_ideal > 0:
            hists[file_id+'_ideal_PVx_bjet'].Fill(PVx)
            hists[file_id+'_ideal_PVy_bjet'].Fill(PVy)
            hists[file_id+'_ideal_PVz_bjet'].Fill(PVz)

def produce_comparison_histograms(sample_path, skeletons):

    sample = sample_path.split('/')[-1]
    simple_name = tracking_submitJobs.get_simple_name(sample)
    print('Loading sample: {}'.format(sample))
    file_id = simple_name + '_comparison'

    hists = book_comparison_histograms(file_id, process_trees.plot_skeletons)

    f_nom    = ROOT.TFile.Open(sample_path+'/nom/all_flav_Akt4EMTo_'+simple_name+'_nom.root')
    f_pseudo = ROOT.TFile.Open(sample_path+'/pseudo/all_flav_Akt4EMTo_'+simple_name+'_pseudo.root')
    f_ideal  = ROOT.TFile.Open(sample_path+'/ideal/all_flav_Akt4EMTo_'+simple_name+'_ideal.root')
    t_nom    = f_nom.bTag_AntiKt4EMTopoJets
    t_pseudo = f_pseudo.bTag_AntiKt4EMTopoJets
    t_ideal  = f_ideal.bTag_AntiKt4EMTopoJets
    nEvents  = t_nom.GetEntries()
    if process_trees.max_evts != -1:
        print('Looping over {} events'.format(process_trees.max_evts))
    else:
        print('Looping over {} events'.format(nEvents))

    start_time = dt.datetime.now()

    # Loop over events
    for i in range(nEvents):
        if (process_trees.max_evts != -1) and (i > process_trees.max_evts):
            break
        if i % 1000 == 0:
            print('Processed {} events in {}'.format(i,str(dt.datetime.now()-start_time)))

        # Get entries of trees
        t_nom.GetEntry(i)
        t_pseudo.GetEntry(i)
        t_ideal.GetEntry(i)

        # Check if nominal event passes selection
        if not process_trees.select_event(t_nom):
           continue

        # Fill histograms
        fill_comparison_histograms(t_nom, t_pseudo, t_ideal, hists, file_id)

    f_nom.Close()
    f_pseudo.Close()
    f_ideal.Close()

    # Check total number of jets passing pseudo but failing ideal SV1
    tot_strange_jets = 0
    for i in range(2,hists[file_id+'_nom_njets'].GetNbinsX()+1):
        tot_strange_jets += hists[file_id+'_nom_njets'].GetBinContent(i)*hists[file_id+'_nom_njets'].GetBinLowEdge(i)
    print('Total number of jets passing pseudo but failing ideal SV1: {}'.format(tot_strange_jets))

    out_name = sample_path+'/hist_'+simple_name+'_comparison.root'
    print('Outputfile: {}'.format(out_name))
    f_out = ROOT.TFile.Open(out_name,'RECREATE')
    f_out.cd()
    for h_name, h in hists.items():
        h.Clone(h_name).Write()
    f_out.Close()
    print('Produced histograms in {}'.format(dt.datetime.now()-start_time))


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: '+sys.argv[0]+' <tree_sample_path>')
        sys.exit()

    sample_path = sys.argv[1].rstrip('/')

    print(dt.datetime.now())
    produce_comparison_histograms(sample_path, process_trees.plot_skeletons)