#!/bin/bash
#SBATCH -p RCIF
# requesting one node
#SBATCH -N1
# requesting 12 cpus
#SBATCH -n12
# request enough memory
#SBATCH --mem=16384
# Email on failures
#SBATCH --mail-user=sebastien.rettie@cern.ch
#SBATCH --mail-type=FAIL
# Change log names; %j gives job id, %x gives job name
#SBATCH --output=/home/srettie/slurm_logs/slurm-%j.%x.out
#SBATCH --error=/home/srettie/slurm_logs/slurm-%j.%x.err

pwd
eval $PBS_MACRO