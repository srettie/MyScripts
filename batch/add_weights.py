import os
import sys
import argparse
import ROOT
import datetime as dt
sys.path.append(os.path.join(os.path.dirname(__file__),'..','plotting'))
import tracking_submitJobs
import process_trees
import reweight_trees

debug = True

int_to_flav = {
    0 : 'ljet',
    4 : 'cjet',
    5 : 'bjet',
}

def add_weights_to_file(input_filename, reweight_quantity, h_weights, jet_type):

    print('Adding weights to file: {}'.format(input_filename))
    f = ROOT.TFile(input_filename)
    t = f.Get('bTag_{}'.format(jet_type))

    # Avoid address not set errors...
    t.SetBranchStatus('jet_ip3d_ntrk',0)
    
    out_filename = input_filename.replace('.root','_with_weights.root')
    new_file = ROOT.TFile(out_filename, 'RECREATE')
    new_tree = t.CloneTree(0)
    
    # Save per-event weight as new branch on tree
    # https://root-forum.cern.ch/t/cloning-tree-and-changing-contents-in-the-new-tree/12204
    leaf_values = ROOT.std.vector('double')()
    leaf_values_bcl = ROOT.std.vector('double')()
    leaf_values_bcl_2d = ROOT.std.vector('double')()
    leaf_values_bcl_2d_bH = ROOT.std.vector('double')()
    leaf_values_bcl_3d = ROOT.std.vector('double')()
    new_tree.Branch('weights_{}'.format(reweight_quantity), leaf_values)
    new_tree.Branch('weights_{}_bcl'.format(reweight_quantity), leaf_values_bcl)
    new_tree.Branch('weights_{}_bcl_2d'.format(reweight_quantity), leaf_values_bcl_2d)
    new_tree.Branch('weights_{}_bcl_2d_bH'.format(reweight_quantity), leaf_values_bcl_2d_bH)
    new_tree.Branch('weights_{}_bcl_3d'.format(reweight_quantity), leaf_values_bcl_3d)

    # Use the ratio as weights distribution
    t0 = dt.datetime.now()
    for i in range(t.GetEntries()):
        if (max_evts != -1) and (i > max_evts):
            break
        if i % 10000 == 0:
            print('Processed {} events in {}'.format(i,str(dt.datetime.now()-t0)))
        # Get tree entry
        t.GetEntry(i,1)
        njets = t.njets
        leaf_values.clear()
        leaf_values_bcl.clear()
        leaf_values_bcl_2d.clear()
        leaf_values_bcl_2d_bH.clear()
        leaf_values_bcl_3d.clear()
        # For each jet, check its pT, and use weight in that pT bin for that jet
        for ijet in range(njets):
            # Get relevant jet quantities
            jet_pt = t.jet_pt[ijet]/1000.
            # Just take first bH for now...
            jet_bH_pt = t.jet_bH_pt[ijet][0]/1000.
            jet_eta = t.jet_eta[ijet]
            jet_flav = t.jet_LabDr_HadF[ijet]
            jet_nBHad = t.jet_nBHadr[ijet]
            if jet_pt > 100. and jet_pt < 5000. and abs(jet_eta) < 2.1 and jet_flav in int_to_flav:
                # Reweight to jet pT spectrum
                weight = h_weights['all'].GetBinContent(h_weights['all'].FindBin(jet_pt))
                # Reweight to b/c/l jet pT spectra individually
                weight_bcl = h_weights[int_to_flav[jet_flav]].GetBinContent(h_weights[int_to_flav[jet_flav]].FindBin(jet_pt))
                # Reweight to b/c/l jet pT spectra and eta distributions in 2D
                weight_bcl_2d = h_weights[int_to_flav[jet_flav]+'_bcl_2d'].GetBinContent(h_weights[int_to_flav[jet_flav]+'_bcl_2d'].FindBin(jet_eta, jet_pt))
                # Reweight to b/c/l jet bH/pT spectra and eta distributions in 2D/3D
                if jet_flav == 5:
                    if jet_nBHad == 1:
                        weight_bcl_2d_bH = h_weights[int_to_flav[jet_flav]+'_bcl_2d_bH'].GetBinContent(h_weights[int_to_flav[jet_flav]+'_bcl_2d_bH'].FindBin(jet_eta, jet_bH_pt))
                        weight_bcl_3d = h_weights[int_to_flav[jet_flav]+'_bcl_3d'].GetBinContent(h_weights[int_to_flav[jet_flav]+'_bcl_3d'].FindBin(jet_eta, jet_pt, jet_bH_pt))
                    else:
                        weight_bcl_2d_bH, weight_bcl_3d = 0., 0.
                else:
                    weight_bcl_2d_bH = h_weights[int_to_flav[jet_flav]+'_bcl_2d_bH'].GetBinContent(h_weights[int_to_flav[jet_flav]+'_bcl_2d_bH'].FindBin(jet_eta, jet_pt))
                    weight_bcl_3d = h_weights[int_to_flav[jet_flav]+'_bcl_3d'].GetBinContent(h_weights[int_to_flav[jet_flav]+'_bcl_3d'].FindBin(jet_eta, jet_pt))
            else:
                weight, weight_bcl, weight_bcl_2d, weight_bcl_2d_bH, weight_bcl_3d = 0., 0., 0., 0., 0.
            leaf_values.push_back(weight)
            leaf_values_bcl.push_back(weight_bcl)
            leaf_values_bcl_2d.push_back(weight_bcl_2d)
            leaf_values_bcl_2d_bH.push_back(weight_bcl_2d_bH)
            leaf_values_bcl_3d.push_back(weight_bcl_3d)
        new_tree.Fill()
    # Save new tree with weight
    print('Added weights in {}'.format(str(dt.datetime.now()-t0)))
    print('Output file: {}'.format(out_filename))
    new_tree.Write()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Add weights to FTAG ntuple')
    parser.add_argument('-f', '--input_file', dest='input_file', default='',
                        help='Input file on which to add weights')
    parser.add_argument('-rw', '--reweight_quantity', dest='reweight_quantity', default='jet_pt',
                        help='Quantity used to reweight distributions')
    parser.add_argument('-j', '--jet_type', dest='jet_type', default='AntiKt4EMPFlowJets', choices=tracking_submitJobs.jet_types,
                        help='Type of jet for which efficiency will be calculated (AntiKt4EMTopoJets or AntiKt4EMPFlowJets)')
    parser.add_argument('-n', '--nevts', dest='max_events', default=-1, type=int,
                        help='Number of events to process')
    args = parser.parse_args()

    if debug:
        print('Arguments:')
        for arg in vars(args):
            print('{}: {}'.format(arg, getattr(args, arg)))

    max_evts = args.max_events
    # Get weights histogram
    h_weights = {}
    h_weights['all'] = reweight_trees.get_weights_hist('', '', args.reweight_quantity, 'all', do_fast = True)
    for jflav in process_trees.jet_flavours:
        h_weights[jflav] = reweight_trees.get_weights_hist('', '', args.reweight_quantity, jflav, do_fast = True)
        h_weights[jflav+'_bcl_2d'] = reweight_trees.get_weights_hist('', '', args.reweight_quantity, jflav+'_bcl_2d', do_fast = True)
        if jflav == 'bjet':
            h_weights[jflav+'_bcl_2d_bH'] = reweight_trees.get_weights_hist('', '', reweight_trees.reweight_quantity_bH, jflav+'_bcl_2d_bH', do_fast = True)
        else:
            h_weights[jflav+'_bcl_2d_bH'] = reweight_trees.get_weights_hist('', '', args.reweight_quantity, jflav+'_bcl_2d_bH', do_fast = True)
        h_weights[jflav+'_bcl_3d'] = reweight_trees.get_weights_hist('', '', args.reweight_quantity, jflav+'_bcl_3d', do_fast = True)

    add_weights_to_file(args.input_file, args.reweight_quantity, h_weights, args.jet_type)
    print('Done adding weights!')