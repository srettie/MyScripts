import os
import glob
import argparse
import batch_utils

do_submit = False
do_test = False

qc_code_dir = '/unix/atlastracking/srettie/quantum_computing/TrackML_data_processing/'

def get_parser():
    p = argparse.ArgumentParser(description='Submit QC jobs')
    p.add_argument('-i', '--in_dir', dest = 'in_dir', required = True, help = 'Raw TrackML input file directory')
    p.add_argument('-o', '--out_dir', dest = 'out_dir', required = True, help = 'Where to save preprocessed data')
    p.add_argument('-n', '--n_files', dest = 'n_files', type = int, default = None, help = 'Number of files to preprocess')
    return p

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    # get input files
    input_files = glob.glob(os.path.join(args.in_dir, '*-hits.csv'))
    # ensure output directory exists
    os.makedirs(args.out_dir, exist_ok=True)
    # submit jobs to batch
    n_jobs = 0
    for f in input_files:
        if do_test and n_jobs > 0:
            continue
        if args.n_files and n_jobs >= args.n_files:
            continue
        print(f'input: {f}')
        # use umami image for simplicity
        cpu_base = 'singularity exec --bind /unix/atlastracking/srettie/ /unix/atlastracking/srettie/FTAG_images/umamibase_cpu.img python '
        prefix = f.replace('-hits.csv', '')
        cmd = cpu_base + qc_code_dir + f'make_labeled_edges_sanity.py -p {prefix} -o {args.out_dir}'
        evt_number = os.path.basename(f).replace('event', '').replace('-hits.csv', '')
        job_name = f'qc_preprocess_{evt_number}'
        batch_utils.job_submit(cmd, job_name, do_submit)
        n_jobs += 1
    print(f'submitted {n_jobs} jobs')