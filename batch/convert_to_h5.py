import os
import sys
import glob
import time
import tracking_submitJobs

do_submit = False
do_test = False

src_dir = '/unix/atlasvhbb2/srettie/tracking/Projects/'
out_dir = '/unix/atlastracking/srettie/training_inputs_custom_ipxd_with_tracks/'
in_dir_zp = '/unix/atlasvhbb2/srettie/tracking/athenaOutputs_full_custom_ipxd/'
in_dir_ttbar = '/unix/atlastracking/srettie/grid_downloads_custom_ipxd/'
conversion_script = '/unix/atlasvhbb2/srettie/tracking/Projects/DL1_framework/tools/convert_fromROOT.py'

files_per_job = 20

n_jobs = 0

samples = [
    'mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210',
    'mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210'
]

for s in samples:
    sample_type = ''
    if '427081' in s:
        sample_type = 'zp_extended'
        in_dir = in_dir_zp
    elif '410470' in s:
        sample_type = 'ttbar'
        in_dir = in_dir_ttbar
    else:
        print('Unknown sample type: {}'.format(sample_type))
        sys.exit()

    if not os.path.exists(out_dir+s): os.mkdir(out_dir+s)

    for t in ['nom','nom_RFMVA_tight','nom_RFMVA_loose']:
        if not os.path.exists(out_dir+s+'/'+t): os.mkdir(out_dir+s+'/'+t)
        # Disable singularity caching to avoid concurrency errors
        # export SINGULARITY_DISABLE_CACHE="true"
        singularity_cmd = 'export XDG_RUNTIME_DIR="";echo "Cache dir";echo $SINGULARITY_CACHEDIR;singularity exec --bind {}/{}/{},{}/{}/{},{} docker://gitlab-registry.cern.ch/mguth/umami:latest '.format(out_dir,s,t,in_dir,s,t,src_dir)
        if sample_type == 'zp_extended': to_convert = glob.glob('{}/{}/{}/trk_*/flav_Akt4EMPf.root'.format(in_dir,s,t))
        elif sample_type == 'ttbar': to_convert = glob.glob('{}/{}/{}/user.*/*.root'.format(in_dir,s,t))
        else:
            print('Unknown sample type: {}'.format(sample_type))
            sys.exit()
        while(to_convert):
            input_files = ''
            for i in range(files_per_job):
                if len(to_convert)<1:
                    continue
                if i == 0:
                    input_files += to_convert.pop()
                else:
                    input_files += ' ' + to_convert.pop()
            if do_test and n_jobs > 0:
                continue
            cmd = singularity_cmd + 'python {} --input {} --output {}/{}/{} --track_type {} --write_tracks'.format(conversion_script, input_files, out_dir, s, t, t)
            qsub_name = 'h5Convert_{}_{}_{}'.format(sample_type,t,n_jobs)
            tracking_submitJobs.torque_submit(cmd, '', qsub_name, do_submit)
            n_jobs += 1

print('Submitted {} jobs!'.format(n_jobs))