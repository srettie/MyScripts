#!/bin/bash
#SBATCH -p GPU
# requesting one node
#SBATCH -N1
# requesting 12 cpus
#SBATCH -n12
# requesting 1 GPU
#SBATCH --gres=gpu:1
# request enough memory
#SBATCH --mem-per-gpu=16384
# Email on failures
#SBATCH --mail-user=sebastien.rettie@cern.ch
#SBATCH --mail-type=FAIL
# Change log names; %j gives job id, %x gives job name
#SBATCH --output=/home/srettie/slurm_logs/slurm-%j.%x.out
#SBATCH --error=/home/srettie/slurm_logs/slurm-%j.%x.err

pwd
echo "nvidia-smi:"
nvidia-smi
echo "CUDA_VISIBLE_DEVICES:"
echo $CUDA_VISIBLE_DEVICES
eval $PBS_MACRO