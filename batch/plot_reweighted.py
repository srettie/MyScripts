import os
import sys
import argparse
import ROOT
import datetime as dt
import produce_roc_curve
sys.path.append(os.path.join(os.path.dirname(__file__),'..','plotting'))
import tracking_submitJobs
import process_trees
import plot_utils
import draw_roc_curves

debug = True
colors = [ROOT.kBlack, ROOT.kRed, ROOT.kBlue, ROOT.kSpring-1]

def get_hist_from_tree(t, var, selection, max_evts = -1):

    if debug: print('Looking at variable: {}, selection: {}'.format(var,selection))

    if var == 'sv1_llr':
        hist = process_trees.plot_skeletons['jet_'+var].Clone('hist')
    else:
        hist = process_trees.plot_skeletons[var].Clone('hist')

    n_to_draw = t.GetEntries() if max_evts == -1 else max_evts
    if var == 'jet_bH_pt_frac':
        nselected = t.Draw('(jet_bH_pt/1000.)/(jet_pt/1000.)>>hist', selection, '', n_to_draw)
    elif 'jet_pt' in var or 'jet_bH_pt' in var:
        nselected = t.Draw('{}/1000.>>hist'.format(var), selection, '', n_to_draw)
    else:
        nselected = t.Draw('{}>>hist'.format(var), selection, '', n_to_draw)

    if debug: print('Selected {} entries'.format(nselected))

    plot_utils.add_overflow(hist)
    plot_utils.add_underflow(hist)
    hist.SetDirectory(0)
    return hist

def compare_histograms(histograms, ids, h_name, ptbin, scale_to_unity = False):

    # Set x-axis range (make sure to do this before any scaling to unity to avoid normalization issues, as integral changes depending on x-axis range)
    xrange_min, xrange_max = histograms[0].GetXaxis().GetXmin(), histograms[0].GetXaxis().GetXmax()
    for h in histograms:
        h.GetXaxis().SetRangeUser(xrange_min,xrange_max)

    # Scale to unity if needed
    if scale_to_unity:
        for h in histograms:
            h.Scale(1./h.Integral())
            h.GetYaxis().SetTitle('Arbitrary Units')

    # Get maxima for y-axis range and set them
    h_min, h_max = plot_utils.get_extrema(histograms, xrange_min, xrange_max)
    for h in histograms:
        h.SetMinimum(0)
        h.SetMaximum(1.7*h_max)

    # Labels
    short_title = 'AntiKt4EMPFlowJets'
    variation_label = ''
    short_title = 'AntiKt4EMPFlowJets'
    if '_bjet' in h_name or '_beff_' in h_name: short_title += ', b-jets'
    if '_cjet' in h_name or '_ceff_' in h_name: short_title += ', c-jets'
    if '_ljet' in h_name or '_leff_' in h_name: short_title += ', l-jets'
    if '_HF' in h_name: short_title += ', HF tracks'
    if '_nonHF' in h_name: short_title += ', non-HF tracks'
    if 'all'  in h_name: short_title += ', all jets'

    if ptbin != '':
        variation_label = draw_roc_curves.pt_bins_titles[ptbin]
        plot_utils.plot_histograms(out_dir, histograms, ids, colors, h_name, short_title, variation_label, debug, ratio_limits = [0.5, 1.5])
    else:
        plot_utils.plot_histograms(out_dir, histograms, ids, colors, h_name, short_title, variation_label, debug, show_entries = True, ratio_limits = [0.5, 1.5])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plot reweighted distribution')
    parser.add_argument('-f1', '--input_file1', dest='input_file1', default='',
                        help='Reference input file')
    parser.add_argument('-f2', '--input_file2', dest='input_file2', default='',
                        help='Input file to reweight')
    parser.add_argument('-v', '--variable', dest='var', default='',
                        help='Variable to plot')
    parser.add_argument('-rw', '--reweight_quantity', dest='reweight_quantity', default='jet_pt',
                        help='Quantity used to reweight distributions')
    parser.add_argument('-j', '--jet_type', dest='jet_type', default='AntiKt4EMPFlowJets', choices=tracking_submitJobs.jet_types,
                        help='Type of jet for which efficiency will be calculated (AntiKt4EMTopoJets or AntiKt4EMPFlowJets)')
    parser.add_argument('-jf', '--jet_flavour', dest='jet_flav', default='', choices=produce_roc_curve.flavour_cuts.keys(), # + ['']
                        help='Flavour of jet to plot (bjet, cjet, ljet)')
    parser.add_argument('-pt', '--pt_bin', dest='pt_bin', default='', choices=process_trees.pt_bins.keys(),# + ['']
                        help='Jet pT bin selection')
    parser.add_argument('-n', '--nevts', dest='max_events', default=-1, type=int,
                        help='Number of events to process')
    parser.add_argument('-o', '--out_dir', dest='out_dir', default='.',
                        help='Where to save the output plots')
    args = parser.parse_args()

    if debug:
        print('Arguments:')
        for arg in vars(args):
            print('{}: {}'.format(arg, getattr(args, arg)))

    fname1 = args.input_file1
    base_dir1 = '/'.join(fname1.split('/')[:-1]) + '/'
    fname2 = args.input_file2
    base_dir2 = '/'.join(fname2.split('/')[:-1]) + '/'
    var = args.var
    rw = args.reweight_quantity
    jet_type = args.jet_type
    jet_flav = args.jet_flav
    pt_bin = args.pt_bin
    max_evts = args.max_events
    out_dir = args.out_dir

    t0 = dt.datetime.now()

    t1 = produce_roc_curve.get_tree(fname1, jet_type, base_dir1)
    t2 = produce_roc_curve.get_tree(fname2, jet_type, base_dir2, with_weights = True)
    trees = [t1,t2]
    if rw == 'NA':
        weights_cut = ROOT.TCut('(1)')
    else:
        weights_cut = ROOT.TCut('(weights_{})'.format(rw))
    if jet_flav != '':
        flav_cut = ROOT.TCut(produce_roc_curve.flavour_cuts[jet_flav])
    else:
        flav_cut = ROOT.TCut('1')
    if jet_flav == 'bjet':
        flav_cut += ROOT.TCut(produce_roc_curve.do_1bH_cut_str)
    if pt_bin != '':
        pt_cut = ROOT.TCut(produce_roc_curve.pt_bins_str[pt_bin])
    else:
        pt_cut = ROOT.TCut('1')

    selections = [produce_roc_curve.baseline_cut + flav_cut + pt_cut, (produce_roc_curve.baseline_cut + flav_cut + pt_cut)*weights_cut]
    hists = [get_hist_from_tree(t, var, sel, max_evts) for t, sel in zip(trees,selections)]
    ids = ['Z\' (no pileup)','Extended Z\' (with pileup), weighted']
 
    h_name = 'h_{}'.format(var)
    if jet_flav != '':
        h_name += '_{}'.format(jet_flav)
    if pt_bin != '':
        h_name += '_{}'.format(pt_bin)
    h_name += '_rw_{}'.format(rw)

    if rw == 'NA':
        # If no reweighting applied, scale to unity
        compare_histograms(hists, ids, h_name, pt_bin, scale_to_unity = True)
    else:
        compare_histograms(hists, ids, h_name, pt_bin)

    print('Done plotting in {}'.format(dt.datetime.now()-t0))