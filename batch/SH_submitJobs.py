import os
import glob
import argparse
import batch_utils

do_submit = False
do_test = False

def get_parser():
    p = argparse.ArgumentParser(description='Submit QC jobs')
    p.add_argument('-i', '--in_dir', dest = 'in_dir', required = True, help = 'Input file directory')
    p.add_argument('-o', '--out_dir', dest = 'out_dir', required = True, help = 'Where to save ntuples')
    p.add_argument('-c', '--run_config', dest = 'run_config', required = True, help = 'Run config file')
    return p

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    n_jobs = 0
    # get input samples
    samples = [os.path.basename(s) for s in glob.glob(os.path.join(args.in_dir, "*"))]
    for sample in samples:
        # input is DAOD_PHYS/LITE (crashes on PHYSLITE at the moment...)
        # if 'DAOD_PHYS' not in sample and 'DAOD_PHYSLITE' not in sample:
        if '.DAOD_PHYS.' not in sample:
            continue
        input_files = glob.glob(os.path.join(args.in_dir, sample, "*"))
        # segfault occurs if we run on this file :(
        if sample == 'mc20_13TeV.801593.Py8EG_A14NNPDF23LO_XHS_X750_S500_4b.deriv.DAOD_PHYS.e8448_a899_r13144_p5631':
            input_files = [f for f in input_files if os.path.basename(f) != 'DAOD_PHYS.33073775._000003.pool.root.1']
        # ensure output directory exists
        os.makedirs(os.path.join(args.out_dir, sample), exist_ok=True)
        # submit jobs to batch
        cmd = f"cd {os.path.join(args.out_dir, sample)};easyjet-ntupler {','.join(input_files)} --runConfig {args.run_config} --outFile {os.path.join(args.out_dir, sample, 'ntuple.root')}"
        if do_test and n_jobs > 0:
            continue
        job_name = f'easyjet_{sample}'
        batch_utils.job_submit(cmd, job_name, do_submit)
        n_jobs += 1
    print(f'submitted {n_jobs} jobs')
