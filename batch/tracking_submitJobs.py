import os
import sys
import subprocess
import glob
import time

debug = False
verbose = False
do_test = False
do_submit = False
do_hadd = False

# To include custom calibration file:
# sed -i 's/######BTaggingFlags.Calib/BTaggingFlags.Calib/' jobOptions_*
# To revert back to original:
# sed -i 's/^BTaggingFlags.Calib/######BTaggingFlags.Calib/' jobOptions_*
# https://indico.cern.ch/event/930273/contributions/3913823/attachments/2059890/3454945/flav_alg_2020_06_18_ip2d_cut_effect.pdf
do_custom_calib = False

JO_dir = '/home/srettie/FlavourTagPerformanceFramework/run/'

# 100 for Z', 5 for Z' extended, 1 for fast QSPI
files_per_job = {
    'group.perf-idtracking.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.e5362_e5984_s3126_r11212_IdealNomV5_EXT0' : 200,
    'mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210'  : 10,
    'mc16_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3640_r10201_p4207'          : 1,
}

r22_JO = '/home/srettie/FTAG_migration/FlavourTagPerformanceFramework/run/jobOptions.py'
do_r22 = False

QSPI_JO = '/home/srettie/QSPI/FlavourTagPerformanceFramework/btagAnalysis/share/jobOptions.py'
do_QSPI = False

zp_extended_sample = 'mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210'
zp_ctide_sample = 'group.perf-idtracking.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.e5362_e5984_s3126_r11212_IdealNomV5_EXT0'

good_samples = [
    'group.perf-idtracking.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.e5362_e5984_s3126_r11212_IdealNomV5_EXT0',
    'mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210',
    #'mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210',
    # 21.0.119
    #'valid1.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.recon.AOD.e6928_s3227_r12207_r12215_r12207',
    # 22.0.18
    # 'valid1.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.recon.AOD.e6928_s3227_r12153_r12215_r12153',
    # 22.0.20
    # 'valid1.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.recon.AOD.e6928_s3227_r12224_r12215_r12224',
    # QSPI
    # 'mc16_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3640_r10201_p4207',
]

# All possible variations
tracks = ['nom','pseudo','ideal','nom_replaceWithTruth','nom_replaceHFWithTruth','nom_replaceFRAGWithTruth','nom_replaceFRAGHFWithTruth','nom_RF75','nom_RF90','nom_RF75_replaceWithTruth','nom_RF75_replaceWithTruth_removeNoMatch','nom_RF75_replaceHFWithTruth','nom_RF75_replaceFRAGWithTruth','nom_RF75_replaceFRAGHFWithTruth','nom_RF75_replaceWithTruthIdeal','selectHF','selectHF_replaceWithTruth','selectHF_emulateFT','selectHF_emulateFT_replaceWithTruth','selectFRAG','selectFRAG_replaceWithTruth','selectFRAGHF','selectFRAGHF_replaceWithTruth','selectFRAGHF_replaceFRAGWithTruth','selectFRAGHF_replaceHFWithTruth','selectFRAGHFGEANT','nom_replaceGEANTWithTruth','nom_replaceFRAGGEANTWithTruth','nom_replaceFRAGHFGEANTWithTruth','nom_replaceHFGEANTWithTruth','pseudoNotReco','nom_RFMVA_loose','nom_RFMVA_tight']

# Available in extended Z' sample
tracks_extended_zp = ['nom','nom_RFMVA_loose','nom_RFMVA_tight']

MVA_JSON = '/unix/atlas1/svanstroud/tracking/MVA/models/neural_net_v1.json'

jet_types = ['AntiKt4EMPFlowJets']

do_resubmit = False
resubmit = [
    'nom_trk_000437_mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210',
]

def get_job_name(s):
    if 'EXT0' in s:
        return 'trk_'+s.split(',')[0].split('/')[-1].strip().replace('.AOD.root','')[-6:]
    elif 'mc16_13TeV' in s or 'valid1' in s:
        return 'trk_'+s.split(',')[0].split('/')[-1].strip().replace('.pool.root.1','').split('_')[-1]
    else:
        print('Sample name not recognized: {}'.format(s))
        sys.exit()

def get_dsid(s):
    return s.split('.')[2]

def get_simple_name(s):
    return s.split('/')[-1].replace('user.gfacini.','').replace('group.perf-idtracking.','').replace('_EXT0','').replace('all_flav_','').replace('.root','')

def get_short_jet_name(j):
    return j.replace("AntiKt","Akt").replace("TopoJets","To").replace("TrackJets","Tr").replace("PFlowJets","Pf")

def torque_submit(cmd, out_dir, job_name, do_submit = False):
    # Use subprocess.run() instead of subprocess.call() when python3 is ready...
    if debug:
        print('Submitting job cmd: ' + cmd)        
    os.environ['PBS_MACRO'] = cmd
    if debug:
        print('Setting output directory to: ' + out_dir)
    os.environ['OUTDIR'] = out_dir
    if debug:
        print('Sanity check...PBS_MACRO is now:')
        subprocess.call('echo $PBS_MACRO', shell=True)
        print('Output directory is:')
        print(out_dir)
        print('Sanity check...OUTDIR is now:')
        subprocess.call('echo $OUTDIR', shell=True)

    if 'roc_' in job_name or 'h5Convert_' in job_name:
        queue = 'longc7'
    elif 'add_weights_' in job_name or 'plot_rw_' in job_name:
        queue = 'shortc7'
    else:
        queue = 'mediumc7'

    if out_dir == '':
        torque_cmd = 'qsub -q ' + queue + ' -V -N ' + job_name + ' /home/srettie/MyScripts/batch/job_noOutDir.sh'
    else:
        torque_cmd = 'qsub -q ' + queue + ' -V -N ' + job_name + ' /home/srettie/MyScripts/batch/job.sh'

    if not do_submit:
        print(torque_cmd)
        print(cmd)
    else:
        subprocess.call(torque_cmd, shell=True)
        time.sleep(0.5)

def hadd_trees(samples, out_dir):
    print('About to hadd {} samples:'.format(len(samples)))
    for sample in samples:
        if sample not in good_samples:
            continue
        for j in jet_types:
            for t in tracks:
                print('Sample: {} ({}, {})'.format(sample,t,j))
                simple_name = get_simple_name(sample)
                out_file = out_dir+'/'+sample+'/'+t+'/all_flav_'+simple_name+'_'+get_short_jet_name(j)+'_'+t+'.root'
                to_add = out_dir+'/'+sample+'/'+t+'/trk_*/flav_'+get_short_jet_name(j)+'.root'
                cmd = 'hadd -f ' + out_file + ' ' + to_add
                if not do_submit:
                    print(cmd)
                else:
                    subprocess.call(cmd, shell=True)
    print('Done hadding!')
                
if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: '+sys.argv[0]+' <input_directory> <output_directory>')
        sys.exit()

    in_dir = sys.argv[1]
    out_dir = sys.argv[2]

    if 'custom_ipxd' in out_dir: do_custom_calib = True

    # Get relevant samples
    samples = [s.split('/')[-1] for s in glob.glob(in_dir+'/*') if s.split('/')[-1].split('_')[-1] == 'EXT0' or s.split('/')[-1].split('.')[0] == 'mc16_13TeV' or s.split('/')[-1].split('.')[0] == 'valid1']

    print('Found {} samples:'.format(len(samples)))
    for s in samples:
        print('\t{}'.format(s))

    if do_hadd:
        print('hadding trees')
        hadd_trees(samples, out_dir)
        sys.exit()
        
    # Create output directory if non-existent
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
        
    # Add proper directory to PYTHONPATH
    if do_r22:
        os.environ['PYTHONPATH'] = '/home/srettie/FTAG_migration/FlavourTagPerformanceFramework/run:' + os.environ['PYTHONPATH']
    elif do_QSPI:
        os.environ['PYTHONPATH'] = '/home/srettie/QSPI/FlavourTagPerformanceFramework/run:' + os.environ['PYTHONPATH']
    else:
        os.environ['PYTHONPATH'] = '/home/srettie/FlavourTagPerformanceFramework/run:' + os.environ['PYTHONPATH']

    nJobs = 0
    
    for sample in samples:
        n = 0
        if sample not in good_samples:
            continue
        files = glob.glob(in_dir+'/'+sample+'/*')
        print('Found a total of {} files for sample {}:'.format(len(files),in_dir+'/'+sample))
        print('Submitting jobs with {} files per job.'.format(files_per_job[sample]))
        if verbose:
            print('Files found:')
            for f in files:
                print('\t{}'.format(f))
        
        while(files):
            jobfiles = ''
            if verbose:
                print('Filling job files:')
            for i in range(files_per_job[sample]):
                if len(files)<1:
                    continue
                if verbose:
                    print(i)
                if i == 0:
                    jobfiles += files.pop()
                else:
                    jobfiles += ',' + files.pop()

            for track_type in tracks:
                # Only certain samples available for extended Z'
                if sample == zp_extended_sample and track_type not in tracks_extended_zp: continue
                cmd = ''
                if 'RFMVA' in track_type:
                    cmd += 'cp {} .;'.format(MVA_JSON)
                if do_custom_calib:
                    calib_file = '/unix/atlasvhbb2/srettie/tracking/reference_histograms/{}/BTagCalibRUN2-08-49_{}.root'.format(sample, track_type)
                    # Use ttbar sample templates for extended Z' sample
                    if '427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended' in sample:
                        calib_file = '/unix/atlasvhbb2/srettie/tracking/reference_histograms/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210/BTagCalibRUN2-08-49_{}.root'.format(track_type)

                    cmd += 'cp {} .;'.format(calib_file)
                    suffix = 'srettie'
                    cmd += 'echo "Calib file:";'
                    cmd += 'echo "{}";'.format(calib_file.split('/')[-1])
                    cmd += 'echo "Suffix:";'
                    cmd += 'echo "{}";'.format(suffix)
                    cmd += 'coolHist_setFileIdentifier.sh {};'.format(calib_file.split('/')[-1])
                    cmd += 'coolHist_extractFileIdentifier.sh {};'.format(calib_file.split('/')[-1])
                    cmd += 'coolHist_insertFileToCatalog.py {};'.format(calib_file.split('/')[-1])
                    cmd += 'coolHist_setReference.py OFLP200 /GLOBAL/BTagCalib/RUN12 1 {} {};'.format(suffix, calib_file.split('/')[-1])
                    cmd += 'echo "setchan /GLOBAL/BTagCalib/RUN12 1 RUN12" | AtlCoolConsole.py "sqlite://;schema=mycool.db;dbname=OFLP200";'

                # Copy relevant JO files to working directory
                cmd += 'cp {}/jobOptions_{}.py .;cp {}/RetagFragment_{}.py .;'.format(JO_dir,track_type,JO_dir,track_type)
                # Run actual athena command
                cmd += 'athena jobOptions_'+track_type+'.py --filesInput='+jobfiles

                if do_r22:
                    # Temporary hack for rel. 22 running
                    cmd = 'athena {} --filesInput={}'.format(r22_JO, jobfiles)                
                if do_QSPI:
                    # Temporary hack for quick QSPI jobs
                    cmd = 'athena {} --filesInput={}'.format(QSPI_JO, jobfiles)
                if do_test:
                    cmd += ' --evtMax=100'
                    if n > 0:
                        continue
                if debug:
                    print('Track type: '+track_type)
                    print('Running on sample: '+sample)
                    print('Using files:')
                    for f in jobfiles.split(','):
                        print('\t{}'.format(f))
                # Create output directory if non-existent
                job_name = get_job_name(jobfiles)
                qsub_name = track_type+'_'+job_name+'_'+get_simple_name(sample)
                job_out_dir = out_dir+'/'+sample+'/'+track_type+'/'+job_name
                if not os.path.exists(job_out_dir):
                    os.makedirs(job_out_dir)
                if do_resubmit and qsub_name not in resubmit:
                    continue
                torque_submit(cmd, job_out_dir, qsub_name, do_submit)
                nJobs += 1
            n += 1
    print('All done! Submitted a total of {} jobs.'.format(nJobs))