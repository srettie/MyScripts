#!/bin/sh

#PBS -r n

### Output files
#PBS -e /unix/atlastrackbtag/srettie/batch_logs/job_${PBS_JOBNAME}_${PBS_JOBID}.err
#PBS -o /unix/atlastrackbtag/srettie/batch_logs/job_${PBS_JOBNAME}_${PBS_JOBID}.log

### Choose queue
#PBS -q long
#PBS -l mem=32gb
#PBS -l pmem=32gb


### Mail to user (remove lines if you don't want emails from jobs)
###PBS -m ae
###PBS -M sebrettie@gmail.com

### Make working directory for the job
#export WDIR="/tmp/srettie_work_${RANDOM}"
#mkdir -p ${WDIR} && cd ${WDIR}

### (optionally) copy input files needed

### rsync -xval /path/to/input.root .

### Execute the macro/command
echo pwd
pwd
echo $PBS_MACRO
eval $PBS_MACRO

### (optionally) clean up and copy outputs

#rsync -xval * $OUTDIR
#cd && rm -rf ${WDIR}
