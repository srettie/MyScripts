import os
import time
import subprocess

debug = False
do_submit = False
do_gnn = True
do_relaxed = False
do_upgrade = False
do_ambiwork = True

# Run options
do_preparation = False
do_preprocessing = False
do_training = False
do_training_plot = False
do_evaluation = False
do_plotting = False
do_plot_input_vars = False

TAG = 'nom'
# TAG = 'reco_pseudo_recoReplPseudo'
# TAG = 'reco_pseudo_recoPlusPseudo'
# TAG = 'reco_pseudo_RF'
# TAG = 'reco_pseudo'
# TAG = 'recoPlusPseudo'
# TAG = 'replWithPseudo'
# TAG = 'RF'

suffix = '' if TAG == 'nom' else '_{}'.format(TAG)
# Configuration files
umami_dir = '/unix/atlastracking/srettie/FTAG_umami/umami/'
PREPROCESS = umami_dir + 'examples/PFlow-Preprocessing-UCL_{}.yaml'
TRAIN = umami_dir + 'examples/Dips-PFlow-Training-config-UCL_{}.yaml'
PLOT = umami_dir + 'examples/plotting_umami_config_dips_UCL{}.yaml'.format(suffix)
PLOT_INPUT = umami_dir + 'examples/plotting_input_vars_compareh5{}.yaml'.format(suffix)

if do_gnn:
    PREPROCESS = PREPROCESS.replace('UCL','GNNonly')
    TRAIN = TRAIN.replace('UCL','GNNonly')
    PLOT = PLOT.replace('UCL','GNNonly')

if do_relaxed:
    PREPROCESS = PREPROCESS.replace('GNNonly','relaxed')

if do_upgrade:
    umami_dir = '/unix/atlastrackbtag/srettie/upgrade/umami/'
    PREPROCESS = umami_dir + 'examples/PFlow-Preprocessing-UCL_{}.yaml'

if do_ambiwork:
    umami_dir = '/unix/atlastrackbtag/srettie/FTAG_preprocessing/umami/'
    PREPROCESS = umami_dir + 'examples/preprocessing/CA_23.0.22/PFlow-Preprocessing-{}.yaml'

GNNonly = [
    'reco',
    'pseudo',
    'recoReplWithPseudo',
    'recoReplWithPseudoFromB',
    'recoReplWithPseudoNotFromB',
    'recoPlusPseudo',
    'recoPlusPseudoFromB',
    'recoPlusPseudoNotFromB',
    'recoNoFakes',
    'recoNoFakesFromB',
    'recoNoFakesNotFromB'
]

# Relaxed trainings
relaxed = [
    'reco',
    'reco_hadROI',
    'pseudo',
    'pseudo2',
    'sisp',
    'sisp2'
]

# Upgrade trainings
upgrade_to_loop = [
    'gnn_itk_mu200',
    'gnn_itk_mu0',
    'gnn_itk_mu200_SEP30',
    'gnn_itk_mu200_SEP30_optfrac'
    'gnn_itk_mu200_SEP30_tight'
    'gnn_itk_mu200_OCT10_PUB'
]

# ambi work trainings
ambiwork = [
    'nom',
    'nom_80',
    'nomnom',
    'nomnom_80',
    'pseudo',
    'pseudo_80',
    'skip_ambi_roi',
    'skip_ambi_roi_80',
]

def job_submit(cmd, job_name, do_submit = False, queue_system = 'torque'):

    if debug:
        print('Submitting job cmd: ' + cmd)        
    os.environ['PBS_MACRO'] = cmd
    if debug:
        print('Sanity check...PBS_MACRO is now:')
        subprocess.call('echo $PBS_MACRO', shell=True)

    slurm_cmd = 'sbatch --spankx11=all -J ' + job_name + ' /home/srettie/MyScripts/batch/job_slurm_gpu.sh'
    torque_cmd = 'qsub -V -N ' + job_name + ' /home/srettie/MyScripts/batch/job_noOutDir.sh'

    submit_cmd = torque_cmd
    if queue_system == 'slurm':
        submit_cmd = slurm_cmd

    if not do_submit:
        print(submit_cmd)
        print(cmd)
    else:
        subprocess.call(submit_cmd, shell=True)
        time.sleep(0.5)


if __name__ == '__main__':

    gpu_base = 'singularity exec --nv --bind /share/rcifdata/srettie/ /share/rcifdata/srettie/FTAG_images/umamibase_gpu.simg python ' + umami_dir
    # To create umami conda environment:
    # source /unix/atlastrackbtag/srettie/conda/etc/profile.d/conda.sh
    # conda create --name umamienv python=3.9.16
    # conda activate umamienv
    # pip install tensorflow
    # pip install -r requirements.txt
    # python -m pip install -e .
    cpu_base = 'source /unix/atlastrackbtag/srettie/conda/etc/profile.d/conda.sh;conda activate /unix/atlastrackbtag/srettie/conda/envs/umamienv;python ' + umami_dir
    gnn_img_base = 'singularity exec --nv --bind /share/rcifdata/srettie/ /share/rcifdata/srettie/FTAG_images/gnn_image.simg bash'
    prepare_base = cpu_base + 'umami/preprocessing.py -c {} --sample {} --prepare;'
    to_prepare = [
        # 'training_ttbar_bjets',
        # 'training_ttbar_cjets',
        # 'training_ttbar_ujets',
        # 'training_ttbar_taujets',
        # 'validation_ttbar',
        # 'testing_ttbar',
        # 'testing_ttbar_r13618',
        'training_zprime_bjets',
        'training_zprime_cjets',
        'training_zprime_ujets',
        # 'training_zprime_taujets',
        'validation_zprime',
        'validation_zprime_bjets',
        'validation_zprime_cjets',
        'validation_zprime_ujets',
        'testing_zprime',
        # 'validation_zprime_highpt',
        # 'testing_zprime_highpt',
    ]

    preprocess_base = cpu_base + 'umami/preprocessing.py -c {} --{};'
    preprocess_steps = [
        'resampling --hybrid_validation',
        'resampling',
        'scaling',
        'write --hybrid_validation',
        'write',
    ]

    training_dict = umami_dir + '../dips_lr_0.001_bs_15000_epoch_200_nTrainJets_Full{}/validation_WP0p77_300000jets_Dict.json'.format(suffix)
    training_base      = gpu_base + 'umami/train_Dips.py -c {};'
    training_plot_base = gpu_base + 'umami/plotting_epoch_performance.py -t dips -d {} -c {};'.format(training_dict, TRAIN)
    evaluation_base    = gpu_base + 'umami/evaluate_model.py --dips -e 199 -c {};'
    plotting_base      = gpu_base + 'umami/plotting_umami.py -o dips_eval_plots -c {};'
    plot_input_base    = cpu_base + 'umami/plot_input_variables.py --jets --tracks -c {}'.format(PLOT_INPUT)

    nJobs = 0

    if do_preparation:
        to_loop = relaxed if do_relaxed else GNNonly
        if do_upgrade:
            to_loop = upgrade_to_loop
        if do_ambiwork:
            to_loop = ambiwork
        for t in to_loop:
            for s in to_prepare:
                cmd = prepare_base.format(PREPROCESS.format(t), s)
                job_submit(cmd, 'prepare_{}_{}{}'.format(s, t, '_gnn' if do_gnn else ''), do_submit)
                nJobs += 1

    if do_preprocessing:
        to_loop = relaxed if do_relaxed else GNNonly
        if do_upgrade:
            to_loop = upgrade_to_loop
        if do_ambiwork:
            to_loop = ambiwork
        for t in to_loop:
            cmd = ''
            for s in preprocess_steps:
                cmd += preprocess_base.format(PREPROCESS.format(t), s)
            job_submit(cmd, 'preprocess_{}{}'.format(t, '_gnn' if do_gnn else ''), do_submit)
            nJobs += 1

    if do_training:
        to_loop = relaxed if do_relaxed else GNNonly
        for t in to_loop:
            cmd = training_base.format(TRAIN.format(t))
            job_submit(cmd, 'train_{}{}'.format(TAG, '_gnn' if do_gnn else ''), do_submit)
            nJobs += 1

    if do_training_plot:
        cmd = training_plot_base
        job_submit(cmd, 'training_plot_{}{}'.format(TAG, '_gnn' if do_gnn else ''), do_submit)
        nJobs += 1
    
    if do_evaluation:
        cmd = evaluation_base.format(TRAIN)
        job_submit(cmd, 'evaluation_{}{}'.format(TAG, '_gnn' if do_gnn else ''), do_submit)
        nJobs += 1

    if do_plotting:
        cmd = plotting_base.format(PLOT)
        job_submit(cmd, 'plot_{}{}'.format(TAG, '_gnn' if do_gnn else ''), do_submit)
        nJobs += 1
    
    if do_plot_input_vars:
        cmd = plot_input_base
        job_submit(cmd, 'plot_input_{}{}'.format(TAG, '_gnn' if do_gnn else ''), do_submit)
        nJobs += 1

    print('Submitted {} jobs'.format(nJobs))