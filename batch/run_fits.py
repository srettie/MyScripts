import os
import sys
import subprocess
import glob
import time

debug = False
do_submit = True

code_dir = '/unix/atlastracking/srettie/VHbbLegacy/medium_pTV_1L_studies/WSMaker_VHbb/'

def get_job_name(s):
    return s

def replace_in_file(f, to_replace, replacement):
    # Use sed to replace lines in file
    replacement_cmd = 'sed -i -e \'s/{}/{}/\' {}'.format(to_replace, replacement, f)
    subprocess.call(replacement_cmd, shell=True)

def create_launcher(period, channel, analysis, fit_type, stxs = 1, options = ''):
    # Start from template
    new_launcher = code_dir + '/scripts/launch_default_jobs_{}_{}_{}_{}.py'.format(period, channel, analysis, fit_type)

    if options != '':
        new_launcher = new_launcher.replace('.py','_{}.py'.format(options))

    cp_cmd = 'cp ' + code_dir + '/scripts/launch_default_jobs.py ' + new_launcher

    subprocess.call(cp_cmd, shell=True)

    # Make necessary modifications to template
    
    # Periods
    replace_in_file(new_launcher, '^MCTypes = \["mc16ade"\]', 'MCTypes = ["mc16{}"]'.format(period))
    # Channel
    replace_in_file(new_launcher, '^channels = \["012"\]', 'channels = ["{}"]'.format(channel))
    # Analysis
    if analysis == 'mbb':
        replace_in_file(new_launcher, '^doCutBase = False', 'doCutBase = True')
        replace_in_file(new_launcher, '^doCutBasePlots = False', 'doCutBasePlots = True')
    # Asimov or observed
    if fit_type == 'obs':
        replace_in_file(new_launcher, '^doExp = "1"', 'doExp = "0"')
    # STXS scheme
    replace_in_file(new_launcher, '^FitSTXS_Scheme = 1', 'FitSTXS_Scheme = {}'.format(stxs))
    # 1L medium pTV
    if 'with1LMedPtV' in options or 'only1LMedPtV' in options:
        replace_in_file(new_launcher, '^Add1LMedium = False', 'Add1LMedium = True')
    
    print('Created new launcher: {}'.format(new_launcher))

    return new_launcher

def torque_submit(cmd, job_name):
    # Use subprocess.run() instead of subprocess.call() when python3 is ready...
    print('Submitting job cmd: ' + cmd)
    os.environ['PBS_MACRO'] = cmd
    if debug:
        print('Command is:')
        print(cmd)
        print('Sanity check...PBS_MACRO is now:')
        subprocess.call('echo $PBS_MACRO', shell=True)

    torque_cmd = 'qsub -V -N ' + job_name + ' /home/srettie/MyScripts/batch/job_noOutDir.sh'

    if not do_submit:
        print(torque_cmd)
    else:
        subprocess.call(torque_cmd, shell=True)
        time.sleep(2)
        
if __name__ == '__main__':
    
    if len(sys.argv) < 2:
        print('Usage: '+sys.argv[0]+' <fit_tag>')
        sys.exit()

    periods = ['ade']#['ade','ad','e']
    channels = ['1']#['0','1','2','012']
    analysis = ['mva','mbb']
    fit_types = ['asimov']#['obs','asimov']
    # used STXS cases : 1=1POI; 2=WH/ZH; 3=5POI; 5=3POI; 9=6POI; 11=stage1.2 for HComb2020
    stxs_schemes = [1]#[1,2,9]
    nfits= 0
    for p in periods:
        for c in channels:
            for a in analysis:
                for f in fit_types:
                    for s in stxs_schemes:

                        fit_tag = sys.argv[1]+'_'+p+'_'+c+'_'+a+'_'+f+'_'+str(s)

                        new_launcher = create_launcher(p, c, a, f, s)
                        cmd = 'cd ' + code_dir + ';python {} {}'.format(new_launcher, fit_tag)
                        torque_submit(cmd, get_job_name(fit_tag))
                        nfits += 1

                        new_launcher = create_launcher(p, c, a, f, s, 'with1LMedPtV_MtopCRShape')
                        cmd = 'cd ' + code_dir + ';python {} {}'.format(new_launcher, fit_tag+'_with1LMedPtV_MtopCRShape')
                        torque_submit(cmd, get_job_name(fit_tag+'_with1LMedPtV_MtopCRShape'))
                        nfits +=1

                        new_launcher = create_launcher(p, c, a, f, s, 'with1LMedPtV_decorrWhfSRCRextrap')
                        cmd = 'cd ' + code_dir + ';python {} {}'.format(new_launcher, fit_tag+'_with1LMedPtV_decorrWhfSRCRextrap')
                        torque_submit(cmd, get_job_name(fit_tag+'_with1LMedPtV_decorrWhfSRCRextrap'))
                        nfits +=1

                        new_launcher = create_launcher(p, c, a, f, s, 'only1LMedPtV_CRShape')
                        cmd = 'cd ' + code_dir + ';python {} {}'.format(new_launcher, fit_tag+'_only1LMedPtV_CRShape')
                        torque_submit(cmd, get_job_name(fit_tag+'_only1LMedPtV_CRShape'))
                        nfits +=1

                        new_launcher = create_launcher(p, c, a, f, s, 'with1LMedPtV_simpleWS_CRShape')
                        cmd = 'cd ' + code_dir + ';python {} {}'.format(new_launcher, fit_tag+'_with1LMedPtV_simpleWS_CRShape')
                        torque_submit(cmd, get_job_name(fit_tag+'_with1LMedPtV_simpleWS_CRShape'))
                        nfits +=1

    print('All done! Submitted {} fits.'.format(nfits))