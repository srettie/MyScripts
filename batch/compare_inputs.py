from __future__ import print_function
import os, sys
import ROOT

debug = False

f1_name = '/unix/atlasvhbb2/yama/InputsProduction/CxAODFramework_master_20191209_allvars/ReaderOutputBatchMC16ad_Inputs_AllVars/inputs/LimitHistograms.VHbb.1Lep.13TeV.mc16d.UCL.32-15.root'
f2_name = '/unix/atlasvhbb2/srettie/CxAODFramework_branch_tag_r32-24-Reader-Resolved-05/run/tag_05_post_crash_merged/Reader_1L_32-15_d_MVA_H/post_crash_d.root'

yanhui_dir = '/unix/atlasvhbb2/yama/Fit/WSMaker_VHbb_March2nd/inputs/SMVHVZ_2019_MVA_mc16ade_NewInputs/'
#seb_dir = '/unix/atlasvhbb2/srettie/postprocess_test/'
seb_dir = '/unix/atlasvhbb2/srettie/WSMaker_VHbb_VHUnblinding_29022020/inputs/postprocessed/'

processes = ['qqZvvH125','ggZvvH125','qqWlvH125','stops','stopt','stopWt','ttbar','Wbb','Wbc','Wbl','Wcc','Wcl','Wl','WW','ZZ','WZ','Zbb','Zbc','Zbl','Zcc','Zcl','Zl','data']
regions = [
    '2tag2jet_150_250ptv_SR',
    '2tag2jet_150_250ptv_CRLow',
    '2tag2jet_150_250ptv_CRHigh',
    '2tag3jet_150_250ptv_SR',
    '2tag3jet_150_250ptv_CRLow',
    '2tag3jet_150_250ptv_CRHigh',
    '2tag2jet_250ptv_SR',
    '2tag2jet_250ptv_CRLow',
    '2tag2jet_250ptv_CRHigh',
    '2tag3jet_250ptv_SR',
    '2tag3jet_250ptv_CRLow',
    '2tag3jet_250ptv_CRHigh',
    '2tag2jet_75_150ptv_SR',
    '2tag2jet_75_150ptv_CRLow',
    '2tag2jet_75_150ptv_CRHigh',
    '2tag3jet_75_150ptv_SR',
    '2tag3jet_75_150ptv_CRLow',
    '2tag3jet_75_150ptv_CRHigh'
]

yanhui_files = [
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_mva.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_pTV.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_mva.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_pTV.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_mva.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_pTV.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_mva.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_pTV.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_mva.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_pTV.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_mva.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_pTV.root'
]
seb_files = [
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_binMV2c10B1.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_binMV2c10B2.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_dPhiLBmin.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_dPhiVBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_dRBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_dYWH.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_mBBJ.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_MET.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_Mtop.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_mTW.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_pTB1.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_pTB2.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_pTJ3.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_binMV2c10B1.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_binMV2c10B2.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_dPhiLBmin.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_dPhiVBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_dRBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_dYWH.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_mBBJ.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_MET.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_Mtop.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_mTW.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_pTB1.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_pTB2.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_pTJ3.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_binMV2c10B1.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_binMV2c10B2.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_dPhiLBmin.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_dPhiVBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_dRBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_dYWH.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_mBBJ.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_MET.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_Mtop.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_mTW.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_mva.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_pTB1.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_pTB2.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_pTJ3.root',
    '13TeV_OneLepton_2tag2jet_150_250ptv_SR_pTV.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_binMV2c10B1.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_binMV2c10B2.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_dPhiLBmin.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_dPhiVBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_dRBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_dYWH.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_mBBJ.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_MET.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_Mtop.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_mTW.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_pTB1.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_pTB2.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_pTJ3.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_binMV2c10B1.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_binMV2c10B2.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_dPhiLBmin.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_dPhiVBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_dRBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_dYWH.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_mBBJ.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_MET.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_Mtop.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_mTW.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_pTB1.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_pTB2.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_pTJ3.root',
    '13TeV_OneLepton_2tag2jet_250ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_binMV2c10B1.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_binMV2c10B2.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_dPhiLBmin.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_dPhiVBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_dRBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_dYWH.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_mBBJ.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_MET.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_Mtop.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_mTW.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_mva.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_pTB1.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_pTB2.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_pTJ3.root',
    '13TeV_OneLepton_2tag2jet_250ptv_SR_pTV.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_binMV2c10B1.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_binMV2c10B2.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_dPhiLBmin.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_dPhiVBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_dRBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_dYWH.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_mBBJ.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_MET.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_Mtop.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_mTW.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_pTB1.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_pTB2.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_pTJ3.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_binMV2c10B1.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_binMV2c10B2.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_dPhiLBmin.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_dPhiVBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_dRBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_dYWH.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_mBBJ.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_MET.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_Mtop.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_mTW.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_pTB1.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_pTB2.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_pTJ3.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_binMV2c10B1.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_binMV2c10B2.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_dPhiLBmin.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_dPhiVBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_dRBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_dYWH.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_mBBJ.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_MET.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_Mtop.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_mTW.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_mva.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_pTB1.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_pTB2.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_pTJ3.root',
    '13TeV_OneLepton_2tag2jet_75_150ptv_SR_pTV.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_binMV2c10B1.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_binMV2c10B2.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_dPhiLBmin.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_dPhiVBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_dRBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_dYWH.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_mBBJ.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_MET.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_Mtop.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_mTW.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_pTB1.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_pTB2.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_pTJ3.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_binMV2c10B1.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_binMV2c10B2.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_dPhiLBmin.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_dPhiVBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_dRBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_dYWH.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_mBBJ.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_MET.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_Mtop.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_mTW.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_pTB1.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_pTB2.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_pTJ3.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_binMV2c10B1.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_binMV2c10B2.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_dPhiLBmin.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_dPhiVBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_dRBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_dYWH.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_mBBJ.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_MET.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_Mtop.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_mTW.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_mva.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_pTB1.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_pTB2.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_pTJ3.root',
    '13TeV_OneLepton_2tag3jet_150_250ptv_SR_pTV.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_binMV2c10B1.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_binMV2c10B2.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_dPhiLBmin.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_dPhiVBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_dRBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_dYWH.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_mBBJ.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_MET.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_Mtop.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_mTW.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_pTB1.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_pTB2.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_pTJ3.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_binMV2c10B1.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_binMV2c10B2.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_dPhiLBmin.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_dPhiVBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_dRBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_dYWH.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_mBBJ.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_MET.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_Mtop.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_mTW.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_pTB1.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_pTB2.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_pTJ3.root',
    '13TeV_OneLepton_2tag3jet_250ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_binMV2c10B1.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_binMV2c10B2.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_dPhiLBmin.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_dPhiVBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_dRBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_dYWH.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_mBBJ.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_MET.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_Mtop.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_mTW.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_mva.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_pTB1.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_pTB2.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_pTJ3.root',
    '13TeV_OneLepton_2tag3jet_250ptv_SR_pTV.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_binMV2c10B1.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_binMV2c10B2.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_dPhiLBmin.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_dPhiVBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_dRBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_dYWH.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_mBBJ.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_mBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_MET.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_Mtop.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_mTW.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_mva.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_pTB1.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_pTB2.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_pTJ3.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRHigh_pTV.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_binMV2c10B1.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_binMV2c10B2.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_dPhiLBmin.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_dPhiVBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_dRBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_dYWH.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_mBBJ.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_mBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_MET.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_Mtop.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_mTW.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_mva.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_pTB1.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_pTB2.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_pTJ3.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_CRLow_pTV.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_binMV2c10B1B2.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_binMV2c10B1.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_binMV2c10B2.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_dPhiLBmin.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_dPhiVBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_dRBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_dYWH.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_mBBJ.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_mBB.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_MET.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_Mtop.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_mTW.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_mvadiboson.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_mva.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_pTB1.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_pTB2.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_pTJ3.root',
    '13TeV_OneLepton_2tag3jet_75_150ptv_SR_pTV.root'
]


def compare_hists(h1, h2, process):
    if not h1:
        if debug:
            print('Missing histogram 1')#%s in file %s!!!'%(h_id, f1_name))
    if not h2:
        if debug:
            print('Missing histogram 2')#%s in file %s!!!'%(h_id, f2_name))

    if not h1 or not h2:
        if debug:
            print('Skipping!')
        return

    # Get numbers
    defaultE = h1.GetEntries()
    defaultI = h1.Integral(-1,1001)
    option1E = h2.GetEntries()
    option1I = h2.Integral(-1,1001)
    if defaultE == 0 or option1E == 0:
        if debug:
            print('No entries, skipping')
        return
    ratioE = option1E / defaultE
    ratioI = option1I / defaultI

    if debug:
        print('%21s \t %21.4f \t %21.4f \t %21.4f \t %21.4f \t %21.4f \t %21.4f'%(process, defaultE, option1E, ratioE, defaultI, option1I, ratioI))

    if ratioE!=1.0:
        print('Non-unity entries ratio for {}!!'.format(process))
    if ratioI!=1.0:
        print('Non-unity integral ratio for {}!!'.format(process))

def compare_inputs(f1_name, f2_name):
    f1 = ROOT.TFile.Open(f1_name)
    f2 = ROOT.TFile.Open(f2_name)
    
    for r in regions:
        print('Yields comparison for %21s'%(r))
        print('%21s \t %21s \t %21s \t %21s \t %21s \t %21s \t %21s'%('SampleName', 'Previous(E)', 'Current(E)', 'Ratio(E)', 'Previous(I)', 'Current(I)', 'Ratio(I)'))
        for p in processes:
            h_id = '%s_%s_mva'%(p, r)
            h1 = f1.Get(h_id)
            h2 = f2.Get(h_id)
            compare_hists(h1, h2, p)

    f1.Close()
    f2.Close()

    
def compare_split_inputs():
    for seb_filename in seb_files:
        if seb_filename not in yanhui_files:
            continue
        print('Comparing {}'.format(seb_filename))
        print('%21s \t %21s \t %21s \t %21s \t %21s \t %21s \t %21s'%('SampleName', 'Previous(E)', 'Current(E)', 'Ratio(E)', 'Previous(I)', 'Current(I)', 'Ratio(I)'))

        f1 = ROOT.TFile.Open(seb_dir+seb_filename)
        f2 = ROOT.TFile.Open(yanhui_dir+seb_filename)

        for key in f1.GetListOfKeys():
            k_name = key.GetName()
            if key.IsFolder():
                # Compare systematics
                for key_sys in f1.Get(k_name).GetListOfKeys():
                    k_sys_name = key_sys.GetName()
                    if 'QQ' in k_sys_name or 'GG' in k_sys_name:
                        continue
                    h1 = f1.Get(k_name+'/'+k_sys_name).Clone()
                    h2 = f2.Get(k_name+'/'+k_sys_name).Clone()
                    compare_hists(h1, h2, k_name+'/'+k_sys_name)
            else:
                # Compare nominal
                if 'QQ' in k_name or 'GG' in k_name:
                    continue
                h1 = f1.Get(k_name).Clone()
                h2 = f2.Get(k_name).Clone()
                compare_hists(h1, h2, k_name)        
        
        f1.Close()
        f2.Close()

if __name__ == '__main__':
    #compare_inputs(f1_name, f2_name)
    compare_split_inputs()
