from __future__ import division
import argparse
import sys
import os
import glob
import ROOT
import datetime as dt
import tracking_submitJobs
import process_trees
sys.path.append(os.path.join(os.path.dirname(__file__),'..','plotting'))
import tracking_plots

debug = True
verbose = False

classifiers = {
    # 'jet_ip2'       : [-20,40],
    # 'jet_ip3'       : [-20,40],
    # 'jet_sv1_sig3d' : [0., 400],
    'sv1_llr'       : [-5, 15],
}

axis_limits = {
    'jet_ip2_ljet'       : [0.,1.],
    'jet_ip3_ljet'       : [0.,1.],
    'jet_sv1_sig3d_ljet' : [0., 0.15],
    'sv1_llr_ljet'       : [0.,0.15],
    'jet_ip2_cjet'       : [0.,1.],
    'jet_ip3_cjet'       : [0.,1.],
    'jet_sv1_sig3d_cjet' : [0., 0.4],
    'sv1_llr_cjet'       : [0.,0.4],
    'jet_ip2_bjet'       : [0.,1.],
    'jet_ip3_bjet'       : [0.,1.],
    'jet_sv1_sig3d_bjet' : [0., 0.8],
    'sv1_llr_bjet'       : [0.,0.8],
}

pt_bins_str = {ptbin : '(jet_pt/1000>={}) && (jet_pt/1000<{})'.format(ptrange[0],ptrange[1]) for (ptbin,ptrange) in process_trees.pt_bins.items()}

# Baseline cuts
baseline_cut = ROOT.TCut('(jet_pt/1000>100) && (jet_pt/1000<5000) && (TMath::Abs(jet_eta)<2.1)')
# bjet, cjet, ljet, isojet
jet_cuts = ROOT.TCut('(jet_LabDr_HadF == 5) || (jet_LabDr_HadF == 4) || (jet_LabDr_HadF == 0) || (jet_dRiso>1.0)')
# Isolation cut
iso_cut_str = '(jet_dRiso>1.0)'
# Single B hadron cut
do_1bH_cut_str = '(jet_nBHadr==1)'
# b/c/l-jet
flavour_cuts = {
    'bjet' : ROOT.TCut('(jet_LabDr_HadF == 5)'),
    'cjet' : ROOT.TCut('(jet_LabDr_HadF == 4)'),
    'ljet' : ROOT.TCut('(jet_LabDr_HadF == 0)'),
}

def get_roc_name(sample, jet_type, track_type, classifier, other_jet, reweight_quantity = '', do_isojet = False, do_1bH = False):
    roc_name = tracking_submitJobs.get_simple_name(sample)
    if reweight_quantity != '':
        roc_name += '_' + tracking_submitJobs.get_short_jet_name(jet_type) + '_roc_' + classifier + '_b{}_'.format(other_jet) + track_type + '_rw_' + reweight_quantity
    else:
        roc_name += '_' + tracking_submitJobs.get_short_jet_name(jet_type) + '_roc_' + classifier + '_b{}_'.format(other_jet) + track_type
    if do_isojet:
        roc_name += '_isojet'
    if do_1bH:
        roc_name += '_1bH'
    return roc_name

def get_N_from_cut(t, cut_str = '', weight_str = ''):
    start_time = dt.datetime.now()
    # Full cut string
    full_cut = baseline_cut + jet_cuts
    # Optional additional cut
    if cut_str != '':
        tcut = ROOT.TCut(cut_str)
        full_cut += tcut
    if weight_str != '':
        weight = ROOT.TCut(weight_str)
    else:
        weight = ROOT.TCut('1')
    # Return number of entries
    if verbose: print('Full cut string: {}'.format(full_cut*weight))
    nentries = t.Draw('>>events', full_cut*weight, 'entrylist')
    if debug: print('Obtained {} entries in {}'.format(nentries, dt.datetime.now()-start_time))
    return nentries

def get_efficiency_hist(t, threshold_cut_str = '', cut_str = '', var = 'jet_pt', weight_str = ''):
    start_time = dt.datetime.now()
    # Full cut string
    full_cut = baseline_cut + jet_cuts
    # Optional additional cut
    if cut_str != '':
        tcut = ROOT.TCut(cut_str)
        full_cut += tcut
    if weight_str != '':
        weight = ROOT.TCut(weight_str)
    else:
        weight = ROOT.TCut('1')
    # Threshold cut
    threshold_cut = ROOT.TCut(threshold_cut_str)

    if verbose: print('Numerator cut string: {}'.format(full_cut+threshold_cut))
    if verbose: print('Denominator cut string: {}'.format(full_cut))

    # Prepare plot skeleton
    h_num   = process_trees.plot_skeletons[var].Clone('h_num')
    h_denom = process_trees.plot_skeletons[var].Clone('h_denom')

    if var == 'jet_pt':
        t.Draw(var+'/1000>>h_num', (full_cut+threshold_cut)*weight)
        t.Draw(var+'/1000>>h_denom', full_cut*weight)
    else:
        t.Draw(var+'>>h_num', (full_cut+threshold_cut)*weight)
        t.Draw(var+'>>h_denom', full_cut*weight)

    # Get efficiency
    h_eff = h_num.Clone()
    h_eff.Divide(h_denom)
    h_eff.GetYaxis().SetTitle('Efficiency')

    h_num.SetDirectory(0)
    h_denom.SetDirectory(0)
    h_eff.SetDirectory(0)

    if debug:
        if weight_str != '':
            print('Plotted {} for {} and {} (reweighted to {}) in {}'.format(var, threshold_cut, cut_str, weight_str, dt.datetime.now()-start_time))
        else:
            print('Plotted {} for {} and {} in {}'.format(var, threshold_cut, cut_str, dt.datetime.now()-start_time))
    return h_num, h_denom, h_eff

def get_tree(filename, jet_type, base_dir, with_weights = False):
    if not os.path.exists(filename):
        if debug: print('File {} does not exists, will TChain subdirectories...'.format(filename))
        filenames = [fn for fn in glob.glob(base_dir + 'trk_*/flav_{}.root'.format(tracking_submitJobs.get_short_jet_name(jet_type)))]
    else:
        if debug: print('Opening file: {}'.format(filename))
        filenames = [filename]
    if with_weights:
        filenames = [fn.replace('.root','_with_weights.root') for fn in filenames]
    t = ROOT.TChain('bTag_' + jet_type)
    for f in filenames:
        t.Add(f)
    return t

def produce_roc_graph(sample, jet_type, track_type, classifier, n, cut, pt_bin = '', other_jet = 'l', reweight_quantity = '', do_isojet = False, do_1bH = False):

    t0 = dt.datetime.now()
    base_dir = tree_dir+'/'+sample+'/'+track_type+'/'
    filename = base_dir + 'all_flav_'+tracking_submitJobs.get_simple_name(sample)+'_'+tracking_submitJobs.get_short_jet_name(jet_type)+'_'+track_type+'.root'

    t = get_tree(filename, jet_type, base_dir, reweight_quantity != '')

    if debug: print('TChain has {} entries'.format(t.GetEntries()))

    eff_hists = {}
    plotted_beff = False
    plotted_oeff = False

    roc_name = get_roc_name(sample, jet_type, track_type, classifier, other_jet, reweight_quantity, do_isojet, do_1bH)

    if pt_bin != '':
        cut += ' && ' + pt_bins_str[pt_bin]
        roc_name += '_' + pt_bin
    if do_isojet:
        cut += ' && ' + iso_cut_str
    if reweight_quantity != '':
        reweight_str = 'weights_' + reweight_quantity
    else:
        reweight_str = ''

    # Setup b-jet/other-jet selections
    bjet_cut = cut + ' && jet_LabDr_HadF == 5'
    if do_1bH:
        bjet_cut += ' && ' + do_1bH_cut_str

    otherjet_cut = cut
    if other_jet == 'c': otherjet_cut += ' && jet_LabDr_HadF == 4'
    elif other_jet == 'l': otherjet_cut += ' && jet_LabDr_HadF == 0'
    else:
        print('ERROR! Unknown other jet type: {}'.format(other_jet))
        sys.exit()

    if debug:
        if reweight_quantity != '':
            print('Getting ROC curve for classifier {}, {} points (reweight to {})'.format(classifier, n, reweight_quantity))
        else:
            print('Getting ROC curve for classifier {}, {} points'.format(classifier, n))
        print('bjetcut = {}'.format(bjet_cut))
        print('otherjetcut = {} '.format(otherjet_cut))
        print('Name: {}'.format(roc_name))

    all_bjets = get_N_from_cut(t, bjet_cut, reweight_str)
    all_otherjets = get_N_from_cut(t, otherjet_cut, reweight_str)

    if debug: print('Denominators: bjets = {}, {}jets = {}'.format(all_bjets, other_jet, all_otherjets))

    if all_bjets == 0 or all_otherjets == 0:
        print('Denominators: bjets = {}, {}jets = {}'.format(all_bjets, other_jet, all_otherjets))
        print('Denominator is zero; exiting!')
        sys.exit()

    roc_graph = ROOT.TGraph(n)
    thresholds_graph = ROOT.TGraph(n)

    step = (classifiers[classifier][1] - classifiers[classifier][0]) / n
    bjet_is_zero = False
    otherjet_is_zero = False
    for i in range(n):
        cutValue = classifiers[classifier][0] + i*step
        cut_str = classifier + ' > ' + str(cutValue)
        threshold_cut_str = classifier + ' > ' + str(classifiers[classifier][0]) + ' && ' + classifier + ' < ' + str(classifiers[classifier][1]) + ' && ' + cut_str
        
        if bjet_is_zero == True: bjet_eff = 0
        else: bjet_eff = get_N_from_cut(t, bjet_cut + ' && ' + threshold_cut_str, reweight_str) / all_bjets
        
        if otherjet_is_zero == True: otherjet_eff = 0
        else: otherjet_eff = get_N_from_cut(t, otherjet_cut + ' && ' + threshold_cut_str, reweight_str) / all_otherjets

        if debug: print('i: {} (step: {}, {}), bjet_eff = {}, otherjet_eff = {}'.format(i,step,cut_str,bjet_eff,otherjet_eff))
        roc_graph.SetPoint(i, bjet_eff, otherjet_eff)
        thresholds_graph.SetPoint(i, bjet_eff, cutValue)
        
        if bjet_eff == 0: bjet_is_zero = True
        if otherjet_eff == 0: otherjet_is_zero = True

        if other_jet == 'l':
            # Only plot efficiency for two points on the ROC curve to save time
            if bjet_eff < 0.5 and not plotted_beff:
                plotted_beff = True
                for var in ['jet_pt','jet_eta','jet_phi']:
                    for eff_type in ['b','l']:
                        h_eff_id = tracking_plots.get_hist_id(sample, jet_type, track_type, var + '_{}eff_{}_beff50'.format(eff_type,classifier))
                        h_num_id = tracking_plots.get_hist_id(sample, jet_type, track_type, var + '_{}num_{}_beff50'.format(eff_type,classifier))
                        h_denom_id = tracking_plots.get_hist_id(sample, jet_type, track_type, var + '_{}denom_{}_beff50'.format(eff_type,classifier))
                        if reweight_quantity != '':
                            h_eff_id, h_num_id, h_denom_id = h_eff_id+'_rw_'+reweight_quantity, h_num_id+'_rw_'+reweight_quantity, h_denom_id+'_rw_'+reweight_quantity
                        if do_isojet:
                            h_eff_id, h_num_id, h_denom_id = h_eff_id+'_isojet', h_num_id+'_isojet', h_denom_id+'_isojet'
                        if do_1bH:
                            h_eff_id, h_num_id, h_denom_id = h_eff_id+'_1bH', h_num_id+'_1bH', h_denom_id+'_1bH'
                        if eff_type == 'b':
                            eff_hists[h_num_id],eff_hists[h_denom_id],eff_hists[h_eff_id] = get_efficiency_hist(t, threshold_cut_str, bjet_cut, var, reweight_str)
                        elif eff_type == 'l':
                            eff_hists[h_num_id],eff_hists[h_denom_id],eff_hists[h_eff_id] = get_efficiency_hist(t, threshold_cut_str, otherjet_cut, var, reweight_str)
                        else:
                            print('ERROR: Unsupported efficiency type: {}'.format(eff_type))
                            sys.exit()
                        eff_hists[h_num_id].SetName(h_num_id)
                        eff_hists[h_denom_id].SetName(h_denom_id)
                        eff_hists[h_eff_id].SetName(h_eff_id)
            if otherjet_eff < 0.04 and not plotted_oeff:
                plotted_oeff = True
                for var in ['jet_pt','jet_eta','jet_phi']:
                    for eff_type in ['b','l']:
                        h_eff_id = tracking_plots.get_hist_id(sample, jet_type, track_type, var + '_{}eff_{}_leff04'.format(eff_type,classifier))
                        h_num_id = tracking_plots.get_hist_id(sample, jet_type, track_type, var + '_{}num_{}_leff04'.format(eff_type,classifier))
                        h_denom_id = tracking_plots.get_hist_id(sample, jet_type, track_type, var + '_{}denom_{}_leff04'.format(eff_type,classifier))
                        if reweight_quantity != '':
                            h_eff_id, h_num_id, h_denom_id = h_eff_id+'_rw_'+reweight_quantity, h_num_id+'_rw_'+reweight_quantity, h_denom_id+'_rw_'+reweight_quantity
                        if do_isojet:
                            h_eff_id, h_num_id, h_denom_id = h_eff_id+'_isojet', h_num_id+'_isojet', h_denom_id+'_isojet'
                        if do_1bH:
                            h_eff_id, h_num_id, h_denom_id = h_eff_id+'_1bH', h_num_id+'_1bH', h_denom_id+'_1bH'
                        if eff_type == 'b':
                            eff_hists[h_num_id],eff_hists[h_denom_id],eff_hists[h_eff_id] = get_efficiency_hist(t, threshold_cut_str, bjet_cut, var, reweight_str)
                        elif eff_type == 'l':
                            eff_hists[h_num_id],eff_hists[h_denom_id],eff_hists[h_eff_id] = get_efficiency_hist(t, threshold_cut_str, otherjet_cut, var, reweight_str)
                        else:
                            print('ERROR: Unsupported efficiency type: {}'.format(eff_type))
                            sys.exit()
                        eff_hists[h_num_id].SetName(h_num_id)
                        eff_hists[h_denom_id].SetName(h_denom_id)
                        eff_hists[h_eff_id].SetName(h_eff_id)

    roc_graph.GetXaxis().SetTitle('b-jet efficiency')
    roc_graph.GetYaxis().SetTitle('{}-jet efficiency'.format(other_jet))
    roc_graph.SetLineColor(tracking_plots.colors[track_type])

    thresholds_graph.GetXaxis().SetTitle('b-jet efficiency')
    thresholds_graph.GetYaxis().SetTitle('{} threshold'.format(classifier))
    thresholds_graph.SetLineColor(tracking_plots.colors[track_type])

    # Save to output file
    f_out_name = filename.rsplit('/', 1)[0] + '/roc_' + roc_name + '.root'
    f_out = ROOT.TFile.Open(f_out_name, 'RECREATE')
    if debug: print('Saving ROC curve graph {} to file {}'.format(roc_name, f_out_name))
    roc_graph.Write(roc_name)
    thresholds_graph.Write(roc_name+'_thresholds')

    for name, eff_hist in eff_hists.items():
        if debug: print('Saving {}'.format(name))
        eff_hist.Write(name)
    
    f_out.Close()
    
    if debug: print('Produced ROC curve with {} points in {}'.format(n, dt.datetime.now()-t0))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Produce ROC curve TGraphs')
    parser.add_argument('-s', '--sample', dest='sample', default='group.perf-idtracking.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.e5362_e5984_s3126_r11212_IdealNomV5_EXT0',
                        help='Sample to be used')
    parser.add_argument('-t', '--track_type', dest='track_type', default='nom',
                        help='Track type to be used')
    parser.add_argument('-c', '--classifier', dest='classifier', default='jet_sv1_sig3d',
                        help='Classifier used to produce the ROC curve')
    parser.add_argument('-n', '--npts', dest='n', default=5, type=int,
                        help='Number of points used to produce ROC curve')
    parser.add_argument('-j', '--jet_type', dest='jet_type', default='AntiKt4EMPFlowJets', choices=tracking_submitJobs.jet_types,
                        help='Type of jet for which efficiency will be calculated (AntiKt4EMTopoJets or AntiKt4EMPFlowJets)')
    parser.add_argument('-o', '--other_jet', dest='other_jet', default='l', choices=['c','l'],
                        help='Other jet efficiency to be calculated (either c or l)')
    parser.add_argument('--cut', dest='cut', default='1',
                        help='Cut applied to all ROC curve selections')
    parser.add_argument('-td', '--tree_dir', dest='tree_dir', default='/unix/atlasvhbb2/srettie/tracking/athenaOutputs_full/',
                        help='Directory where the FTAG ntuples are located')
    parser.add_argument('-pt', '--pt_bin', dest='pt_bin', default='',
                        help='Desired pT bin')
    parser.add_argument('-rw', '--reweight_quantity', dest='reweight_quantity', default='',
                        help='Quantity used to reweight distributions')
    parser.add_argument('--do_isojet', default=False, action='store_true',
                        help='Apply isolation (dRmin > 1.0) to jets')
    parser.add_argument('--do_1bH', default=False, action='store_true',
                        help='Require exactly 1 B hadron for b-jets')
    args = parser.parse_args()

    # Default directories
    tree_dir = args.tree_dir

    if debug:
        print('Arguments:')
        for arg in vars(args):
            print('{}: {}'.format(arg, getattr(args, arg)))

    produce_roc_graph(args.sample, args.jet_type, args.track_type, args.classifier, args.n, args.cut, args.pt_bin, args.other_jet, args.reweight_quantity, args.do_isojet, args.do_1bH)
    if debug: print('Done!')