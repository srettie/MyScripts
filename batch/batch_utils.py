import os
import time
import subprocess

def job_submit(cmd, job_name, do_submit = False, queue_system = 'torque', debug = False):
    if debug:
        print('Submitting job cmd: ' + cmd)        
    os.environ['PBS_MACRO'] = cmd
    if debug:
        print('Sanity check...PBS_MACRO is now:')
        subprocess.call('echo $PBS_MACRO', shell=True)
    slurm_cmd = 'sbatch --spankx11=all -J ' + job_name + ' /home/srettie/MyScripts/batch/job_slurm_gpu.sh'
    torque_cmd = 'qsub -V -N ' + job_name + ' /home/srettie/MyScripts/batch/job_noOutDir.sh'
    submit_cmd = slurm_cmd if queue_system == 'slurm' else torque_cmd
    if not do_submit:
        print(submit_cmd)
        print(cmd)
    else:
        subprocess.call(submit_cmd, shell=True)
        time.sleep(0.5)
