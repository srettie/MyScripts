import sys
sys.path.append('/afs/cern.ch/user/s/srettie/indicomb')

from indicomb import indicomb

indicomb(headerHTML="<center><h2>CTIDE Meetings</h2></center>",
         output="/afs/cern.ch/user/s/srettie/www/indicomb/ctide.html",
         #includeList=["CalRatio","EXOT-2017-20",],
         #excludeList=["CANCELLED","CANCELED"],
         #startDate="2022-10-01",
         categoryNumbers=[5991])

indicomb(headerHTML="<center><h2>hh4b Meetings</h2></center>",
         output="/afs/cern.ch/user/s/srettie/www/indicomb/hh4b.html",
         includeList=["HH-->4b"],
         #excludeList=["CANCELLED","CANCELED"],
         #startDate="2022-10-01",
         categoryNumbers=[10816])
