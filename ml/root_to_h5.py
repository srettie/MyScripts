import os
import glob
import itertools
import argparse
import ROOT
import numpy as np
import h5py
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

N_ROIS = 20_000_000
N_HITS = 400
N_TRACKS = 40
colors = {
    'hit' : '#55C667FF',
    'track'   : '#FDE725FF',
}

def get_parser():
    p = argparse.ArgumentParser(description='root to h5 converter')
    p.add_argument('-i', '--in_dir', dest='in_dir', required=True, help='Input directory containing root files')
    p.add_argument('-o', '--out_dir', dest='out_dir', required=True, help='Where to save output h5 files')
    p.add_argument('-d', '--debug', action='store_true', help='Print debug information')
    p.add_argument('-g', '--plot_graphs', action='store_true', help='Plot and save graphs')
    return p


def flatten_list(l):
    return [item for sublist in l for item in sublist]


def get_clean_edge_list(l):
    # return sorted edge list without duplicates
    l.sort()
    return list(k for k,_ in itertools.groupby(l))


def dump_hypergraph(hg):
    print(f'hg:\n{hg}')
    print(f'hg.e:\n{hg.e}')
    print(f'hg.D_e:\n{hg.D_e}')
    print(f'hg.D_v:\n{hg.D_v}')
    print(f'hg.L_HGNN:\n{hg.L_HGNN}')
    print(f'hg.H.to_dense() (shape {hg.H.to_dense().shape}):\n{hg.H.to_dense()}')


def dump_heterogeneous_data(data):
    print(f'data:\n{data}')
    print(f'data.x_dict:\n{data.x_dict}')
    print(f'data.to_homogeneous():\n{data.to_homogeneous()}')
    print(f'is_undirected:\n{data.is_undirected()}')
    print(f'edge_index_dict:\n{data.edge_index_dict}')
    print(f'num_nodes_dict:\n{data.num_nodes_dict}')
    print(f'num_edges_dict:\n{data.num_edges_dict}')


def dump_np_array(a):
    for n in a.dtype.names:
        print(f'{n}: {a[n]}')


def get_event_data(t, debug = False):
    data = {}
    data['roi_e'] = getattr(t, 'roi_e') / 1000.
    data['roi_eta'] = getattr(t, 'roi_eta')
    data['roi_phi'] = getattr(t, 'roi_phi')
    data['roi_m'] = getattr(t, 'roi_m')
    data['hit_x'] = list(getattr(t, 'cluster_x'))
    data['hit_y'] = list(getattr(t, 'cluster_y'))
    data['hit_z'] = list(getattr(t, 'cluster_z'))
    data['hit_r'] = list(getattr(t, 'cluster_r'))
    data['hit_eta'] = list(getattr(t, 'cluster_eta'))
    data['hit_phi'] = list(getattr(t, 'cluster_phi'))
    data['hit_deta'] = list(getattr(t, 'cluster_deta'))
    data['hit_dphi'] = list(getattr(t, 'cluster_dphi'))
    data['hit_dR'] = list(getattr(t, 'cluster_dR'))
    data['hit_isPixel'] = list(getattr(t, 'cluster_isPixel'))
    data['hit_layer'] = list(getattr(t, 'cluster_layer'))
    data['hit_charge'] = list(getattr(t, 'cluster_charge'))
    data['nhits'] = len(data['hit_x'])
    original_edge_list = [[i for i in v] for v in getattr(t, 'edge_list')]
    data['ntracks_with_duplicates'] = len(original_edge_list)
    edge_list = get_clean_edge_list(original_edge_list)
    data['edge_list'] = edge_list
    data['ntracks'] = len(edge_list)
    data['nduplicates'] = data['ntracks_with_duplicates'] - data['ntracks']
    if debug:
        print(f'roi e = {data["roi_e"]}, eta = {data["roi_eta"]}, phi = {data["roi_phi"]}, m = {data["roi_m"]}')
        print(f'roi has {data["nhits"]} hits and {data["ntracks"]} pseudo tracks ({data["nduplicates"]} duplicates)')
        print(f'hit_x = {data["hit_x"]}')
        print(f'original edge_list:')
        for track in original_edge_list:
            print(f'\t{track}')
        print(f'cleaned edge_list:')
        for track in edge_list:
            print(f'\t{track}')
    return data


def plot_hypergraph(iroi, data, debug = False):
    import dhg
    dhg.random.set_seed(42)
    hg = dhg.Hypergraph(data['nhits'], data['edge_list'])
    if debug:
        dump_hypergraph(hg)
    hg.draw(v_label = [i for i in range(data['nhits'])])
    plt.title(f'ROI {i}, E = {data["roi_e"]:.2f} GeV, {data["ntracks"]} pseudo tracks ({data["nduplicates"]} duplicates), {data["nhits"]} hits')
    plt.savefig(f'roi_{iroi}.pdf', bbox_inches = 'tight')
    plt.clf()


def plot_heterograph(iroi, data, debug = False):
    import torch
    import networkx as nx
    import torch_geometric
    torch_geometric.seed.seed_everything(42)
    # heterogeneous GNN inputs
    graphdata = torch_geometric.data.HeteroData()
    # build node features
    hit_features = torch.tensor(
        [(x,y,z) for x,y,z in zip(data['hit_x'][:N_HITS],data['hit_y'][:N_HITS],data['hit_z'][:N_HITS])] +
        [(np.NAN, np.NAN, np.NAN) for i in range(N_HITS-data['nhits'])]
    )
    # create two node types 'hit' and 'track'
    graphdata['hit'].node_id = torch.arange(N_HITS)
    graphdata['track'].node_id = torch.arange(N_TRACKS)
    # add a feature matrix for hits
    graphdata['hit'].x = hit_features
    # Create an edge type '(track, contains, hit)' and build the graph connectivity
    track_hit_edge_index = torch.tensor(
        [
            flatten_list([[i]*N_HITS for i in range(N_TRACKS)]),
            [i for i in range(N_HITS)] * N_TRACKS
        ]
    )
    graphdata['track', 'contains', 'hit'].edge_index = track_hit_edge_index
    graphdata = torch_geometric.transforms.ToUndirected()(graphdata)
    g = torch_geometric.utils.to_networkx(graphdata.to_homogeneous(), to_undirected = True)
    nx.draw(g, with_labels=True, node_color = [colors['hit']]*N_HITS+[colors['track']]*N_TRACKS)
    plt.title(f'ROI {iroi}, E = {data["roi_e"]:.2f} GeV, {N_TRACKS} pseudo tracks, {N_HITS} hits')
    hit_handle = Line2D([], [], marker = 'o', color = 'w', label = 'hit', markerfacecolor = colors['hit'], markersize = 15)
    track_handle = Line2D([], [], marker = 'o', color = 'w', label = 'track', markerfacecolor = colors['track'], markersize = 15)
    plt.legend(handles=[hit_handle, track_handle])
    plt.savefig(f'heterogeneous_graph_{iroi}.pdf', bbox_inches = 'tight')
    plt.clf()
    # Y labels
    track_idx = []
    hit_idx = []
    for itrack in range(min(N_TRACKS, data['ntracks'])):
        track_idx += [itrack] * len(data['edge_list'][itrack])
        hit_idx += data['edge_list'][itrack]
    track_hit_edge_index_Y = torch.tensor([track_idx, hit_idx])
    graphdata_Y = torch_geometric.data.HeteroData()
    graphdata_Y['hit'].node_id = torch.arange(N_HITS)
    graphdata_Y['track'].node_id = torch.arange(N_TRACKS)
    graphdata_Y['hit'].x = graphdata['hit'].x
    graphdata_Y['track', 'contains', 'hit'].edge_index = track_hit_edge_index_Y
    graphdata_Y = torch_geometric.transforms.ToUndirected()(graphdata_Y)
    g_Y = torch_geometric.utils.to_networkx(graphdata_Y.to_homogeneous(), to_undirected = True)
    g_Y.remove_nodes_from(list(nx.isolates(g_Y)))
    nx.draw(g_Y, with_labels=True, node_color = [colors['hit']]*(g_Y.number_of_nodes()-data['ntracks'])+[colors['track']]*data['ntracks'])
    plt.title(f'ROI {iroi}, E = {data["roi_e"]:.2f} GeV, {data["ntracks"]} pseudo tracks, {g_Y.number_of_nodes()} hits')
    hit_handle = Line2D([], [], marker = 'o', color = 'w', label = 'hit', markerfacecolor = colors['hit'], markersize = 15)
    track_handle = Line2D([], [], marker = 'o', color = 'w', label = 'track', markerfacecolor = colors['track'], markersize = 15)
    plt.legend(handles=[hit_handle, track_handle])
    plt.savefig(f'heterogeneous_graph_Y_{iroi}.pdf', bbox_inches = 'tight')
    if debug:
        dump_heterogeneous_data(graphdata)
        print(f'track_hit_edge_index_Y:\n{track_hit_edge_index_Y}')
        print(f'graphdata_Y:\n{graphdata_Y}')
        print(f'graphdata_Y.to_homogeneous():\n{graphdata_Y.to_homogeneous()}')
        print(f'g_Y:\n{g_Y}')


def get_roi_dtype():
    return np.dtype(
        [
            ('roi_e', '<f4'),
            ('roi_eta', '<f4'),
            ('roi_phi', '<f4'),
            ('roi_m', '<f4'),
            ('edge_list', 'u1', (N_TRACKS, N_HITS,))
        ]
    )


def get_hits_dtype():
    return np.dtype(
        [
            ('hit_x', '<f4', (N_HITS)),
            ('hit_y', '<f4', (N_HITS)),
            ('hit_z', '<f4', (N_HITS)),
            ('hit_r', '<f4', (N_HITS)),
            ('hit_eta', '<f4', (N_HITS)),
            ('hit_phi', '<f4', (N_HITS)),
            ('hit_deta', '<f4', (N_HITS)),
            ('hit_dphi', '<f4', (N_HITS)),
            ('hit_dR', '<f4', (N_HITS)),
            ('hit_isPixel', '?', (N_HITS)),
            ('hit_layer', 'u1', (N_HITS)),
            ('hit_charge', '<f4', (N_HITS)),
        ]
    )


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    # open root ntuple
    debug = args.debug
    fnames = glob.glob(os.path.join(args.in_dir,'*.root'))
    tname = 'hadronic_roi_tree'
    outfile = os.path.join(args.out_dir, 'outfile.h5')
    t = ROOT.TChain(tname)
    for fname in fnames:
        t.Add(fname)
    nrois = t.GetEntries()
    if N_ROIS > 0 and N_ROIS < nrois:
        nrois = N_ROIS
    print(f'running with N_ROIS = {N_ROIS}, N_HITS = {N_HITS}, and N_TRACKS = {N_TRACKS} (Available ROIs = {t.GetEntries()})')
    print(f'nrois = {nrois}')
    # keep track of skipped rois
    nskippedrois = 0
    # book numpy arrays
    all_rois = np.array(np.full(nrois, fill_value = np.nan), dtype=get_roi_dtype())
    all_hits = np.array(np.full(nrois, fill_value = np.nan), dtype=get_hits_dtype())
    roi_idx = 0
    tree_idx = 0
    while roi_idx < nrois:
        if tree_idx % 10000 == 0 or debug:
            print(f'processing tree entry {tree_idx} (roi_idx {roi_idx})')
        t.GetEntry(tree_idx)
        data = get_event_data(t, debug)
        if data['nhits'] > N_HITS:
            nskippedrois += 1
            if debug:
                print(f'skipping event with nhits = {data["nhits"]} > N_HITS = {N_HITS}')
            tree_idx += 1
            continue
        if data['ntracks'] > N_TRACKS:
            raise ValueError(f'ntracks = {data["ntracks"]} > NTRACKS = {N_TRACKS}')
        if args.plot_graphs:
            plot_hypergraph(tree_idx, data, debug = debug)
            plot_heterograph(tree_idx, data, debug = debug)
        # populate ROI features
        roi_vars = [f'roi_{v}' for v in ['e','eta','phi','m']]
        for var in roi_vars:
            all_rois[roi_idx][var] = data[var]
        edges = np.zeros((N_TRACKS, N_HITS))
        for itrack, idxs in enumerate(data['edge_list']):
            edges[itrack][idxs] = 1
        all_rois[roi_idx]['edge_list'] = edges
        # populate hit features
        hit_vars = [f'hit_{v}' for v in ['x','y','z','r','eta','phi','deta','dphi','dR','isPixel','layer','charge']]
        for var in hit_vars:
            all_hits[roi_idx][var][:min(data['nhits'],N_HITS)] = data[var][:N_HITS]
        roi_idx += 1
        tree_idx += 1
    print(f'skipped ROIs with nhits > {N_HITS}: {nskippedrois} ({100*nskippedrois/nrois:.2f}%)')
    if debug:
        print('all_rois:')
        dump_np_array(all_rois)
        print('all_hits:')
        dump_np_array(all_hits)

    # save the arrays to h5 files
    split_idxs = {
        'train' : [0, int(0.9*nrois)],
        'test'  : [int(0.9*nrois), int(0.95*nrois)],
        'val'   : [int(0.95*nrois), nrois],
    }
    for stage in split_idxs:
        start_idx = split_idxs[stage][0]
        stop_idx = split_idxs[stage][1]
        print(f'{stage} idxs: [{start_idx},{stop_idx}]')
        with h5py.File(outfile.replace('.h5', f'_{stage}.h5'), 'w') as f:
            f.create_dataset('rois', data=all_rois[start_idx:stop_idx], compression='gzip')
            f.create_dataset('hits', data=all_hits[start_idx:stop_idx], compression='gzip')

        if debug:
            # read back data as sanity check
            with h5py.File(outfile.replace('.h5', f'_{stage}.h5'), 'r') as f:
                rois = f['rois']
                hits = f['hits']
                print(f'{stage} rois:')
                dump_np_array(rois)
                print(f'{stage} hits:')
                dump_np_array(hits)
