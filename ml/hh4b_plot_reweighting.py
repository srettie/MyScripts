from multiprocessing.sharedctypes import Value
import os
import sys
import math
import glob
import array
import ROOT
import numpy as np
import pandas as pd

sys.path.append('/Users/sebastienrettie/Projects/MyScripts/plotting/')
import plot_utils as pu

do_perJet = True
do_ABCD = False
Xbb_sels = [f'{i}F' for i in range(3)]

training_variables = [
    'log_pt_h1Boosted', 'eta_h1Boosted', 'deta_hhBoosted',
    'log_pt_h2Boosted', 'eta_h2Boosted', 'dpt_hhBoosted'
]
# plot training variable distributions
plotting_variables = [
    'm_hhBoosted', 'm_h1Boosted', 'm_h2Boosted',
    'pt_h1Boosted', 'pt_h2Boosted',
    'pt_VBFj1Boosted', 'eta_VBFj1Boosted',
    'pt_VBFj2Boosted', 'eta_VBFj2Boosted',
    'm_VBFjjBoosted', 'deta_VBFjjBoosted'
]
plotting_variables += training_variables
vars = {
    'm_hhBoosted'         : [15, 300, 3300],
    'm_VBFjjBoosted'      : [25, 0, 5000],
    'm_h1Boosted'         : [7, 90, 160],
    'm_h2Boosted'         : [6, 90, 150],
    'pt_h1Boosted'        : [20, 0, 2000],
    'pt_h2Boosted'        : [20, 0, 2000],
    'eta_VBFj1Boosted'    : [20, -10, 10],
    'eta_VBFj2Boosted'    : [20, -10, 10],
    'deta_VBFjjBoosted'   : [20, -10, 10],
    'pTvecsum_VBFBoosted' : [10, 0, 400],
    'dphi_hhBoosted'      : [20, -2*math.pi, 2*math.pi],
    'dRmin_h1Boosted'     : [25, 1, 6],
    'dRmin_h2Boosted'     : [25, 1, 6],
    'aplanarityBoosted'   : [20, 0, 1],
    'planarityBoosted'    : [20, 0, 1],
    'sphericityBoosted'   : [20, 0, 1],
    'shapeCBoosted'       : [20, 0, 1],
    'shapeDBoosted'       : [20, 0, 1],
}
vars.update({f'FWM{i}Boosted' : [20, 0, 1] for i in range(10)})
vars.update({f'HCM{i}Boosted' : [20, 0, 1] for i in range(10)})
vars.update({
    'eta_h1Boosted' : [40, -4, 4],
    'eta_h2Boosted' : [40, -4, 4],
    'm_h1Boosted' : [50, 50, 200],
    'm_h2Boosted' : [50, 50, 200],
    'eta_VBFj1Boosted' : [50, -5, 5],
    'eta_VBFj2Boosted' : [50, -5, 5],
    'phi_h1Boosted' : [40, -4, 4],
    'phi_h2Boosted' : [40, -4, 4],
    'pt_h1Boosted' : [50, 0, 1000],
    'pt_h2Boosted' : [50, 0, 1000],
    'pt_VBFj1Boosted' : [80, 0, 800],
    'pt_VBFj2Boosted' : [40, 0, 400],
    'deta_hhBoosted' : [40, -4, 4],
    'deta_VBFjjBoosted' : [80, -10, 10],
    'dpt_hhBoosted' : [50, 0, 1000],
    'm_hhBoosted' : [30, 0, 3000],
    'm_VBFjjBoosted' : [40, 0, 4000],
})
CR_VR = '(sqrt( pow((m_h1Boosted-124)/(0.1*log(m_h1Boosted)),2) + pow((m_h2Boosted-117)/(0.1*log(m_h2Boosted)),2) ))'
selections = {
    'SR'             : 'X_hhBoosted < 1.6',
    'VR'             : f'(X_hhBoosted >= 1.6) && ({CR_VR} < 100)',
    'CR'             : f'(X_hhBoosted >= 1.6) && ({CR_VR} >= 100) && ({CR_VR} < 170)',
    'CR_full'        : f'(X_hhBoosted >= 1.6) && ({CR_VR} >= 100)',
    'pass_Xbb_loose' : 'HbbWP_h1Boosted <= 70 && HbbWP_h2Boosted <= 70',
    '0F'             : 'HbbWP_h1Boosted <= 60 && HbbWP_h2Boosted <= 60',
    '1F'             : '(HbbWP_h1Boosted > 60 && HbbWP_h2Boosted <= 60) || (HbbWP_h1Boosted <= 60 && HbbWP_h2Boosted > 60)',
    '2F'             : 'HbbWP_h1Boosted > 60 && HbbWP_h2Boosted > 60',
    'FP'             : 'HbbWP_h1Boosted > 60 && HbbWP_h2Boosted <= 60',
    'PF'             : 'HbbWP_h1Boosted <= 60 && HbbWP_h2Boosted > 60',
    'boostedPrecut'  : 'boostedPrecut',
    'passVBFJets'    : 'passVBFJets',
    'preselection'   : 'boostedPrecut && passVBFJets',
    'resolved_veto'  : '!(resolvedPrecut && kinematic_region==0 && ntag>=4 && X_wt_tag>1.5 && ((pass_vbf_sel && m_hh>400.) || !pass_vbf_sel))',
    'baseline'       : 'boostedPrecut && passVBFJets && m_h1Boosted > 50 && m_h2Boosted > 50 && pt_h1Boosted > 450 && pt_h2Boosted > 250 && m_VBFjjBoosted > 1000 && abs(eta_VBFj1Boosted-eta_VBFj2Boosted) > 3 && pt_VBFj1Boosted > 20 && pt_VBFj2Boosted > 20',
}

def get_hist(t, h_skeleton, var, process, selection, weight, suffix, debug=False):
    h = h_skeleton.Clone(f'h_{var}{suffix}_{process}')
    draw_var = var
    # variables not explicitly in the NNT trees
    if var == 'deta_VBFjjBoosted':
        draw_var = 'eta_VBFj1Boosted-eta_VBFj2Boosted' 
    if var == 'dphi_hhBoosted':
        draw_var = 'phi_h1Boosted-phi_h2Boosted'
    draw_cmd = f'{draw_var} >> h_{var}{suffix}_{process}'
    if debug:
        print(f'drawing: {draw_cmd}')
        print(f'selection: {selection}')
        print(f'weight: {weight}')
    n_evts = t.Draw(draw_cmd, selection*weight)
    if debug:
        print(f'n_evts: {n_evts}')
    pu.add_overflow(h)
    pu.add_underflow(h)
    h.SetTitle(f'h_{var}{suffix}_{process}')
    h.SetLineWidth(2)
    h.SetDirectory(0)
    return h


def get_2D_hist(t, h_skeleton, x, y, process, selection, weight, suffix, debug=False):
    h2 = h_skeleton.Clone(f'h2_{x}_vs_{y}{suffix}_{process}')
    draw_var = f'{y}:{x}'
    draw_cmd = f'{draw_var} >> h2_{x}_vs_{y}{suffix}_{process}'
    if debug:
        print(f'drawing: {draw_cmd}')
        print(f'selection: {selection}')
        print(f'weight: {weight}')
    n_evts = t.Draw(draw_cmd, selection*weight)
    if debug:
        print(f'n_evts: {n_evts}')
    h2.SetTitle(f'h2_{x}_vs_{y}{suffix}_{process}')
    h2.SetDirectory(0)
    return h2


def plot_2D_distributions(t):

    # print numbers
    regions = {
        'SR' : 1.6,
        'VR' : 100,
        'CR' : 170,
    }
    for r in list(regions.keys())+['CR_full']:
        for xbb in Xbb_sels:
            cut = ROOT.TCut(f'({selections["baseline"]})&&({selections[r]})&&({selections[xbb]})')
            # print(cut.GetTitle())
            n = t.Draw('m_hhBoosted',cut)
            print(f'{r}_{xbb}: {n}')

    contours = {
        'SR' : ROOT.TF2('SR', 'sqrt(pow(((x-124)/(1500/x)),2)+pow(((y-117)/(1900/y)),2))', 40, 220, 40, 220),
        'VR' : ROOT.TF2('VR', 'sqrt(pow((x-124)/(0.1*log(x)),2)+pow((y-117)/(0.1*log(y)),2))', 40, 220, 40, 220),
        'CR' : ROOT.TF2('CR', 'sqrt(pow((x-124)/(0.1*log(x)),2)+pow((y-117)/(0.1*log(y)),2))', 40, 220, 40, 220),
    }
    for r, c in regions.items():
        contours[r].SetContour(1, array.array('d', [c]))
    h2_skeleton = ROOT.TH2D('h2', 'h2;m_h1Boosted [GeV];m_h2Boosted [GeV]', 170, 50, 220, 170, 50, 220)
    baseline = ROOT.TCut(selections["baseline"])
    weight = ROOT.TCut('1')
    h2s = {}
    h2s['all'] = get_2D_hist(t, h2_skeleton, 'm_h1Boosted', 'm_h2Boosted', 'all', baseline, weight, '')
    pu.plot_2D(out_dir, h2s['all'], 'all', f'all: {h2s["all"].GetEntries()}', contours = contours.values())
    for xbb in Xbb_sels:
        xbb_cut = ROOT.TCut(selections[xbb])
        h2s[xbb] = get_2D_hist(t, h2_skeleton, 'm_h1Boosted', 'm_h2Boosted', xbb, baseline+xbb_cut, weight, '')
        pu.plot_2D(out_dir, h2s[xbb], xbb, f'{xbb}: {h2s[xbb].GetEntries()}', contours = contours.values())
        for r in regions:
            r_cut = ROOT.TCut(selections[r])
            h2s[f'{xbb}_{r}'] = get_2D_hist(t, h2_skeleton, 'm_h1Boosted', 'm_h2Boosted', f'{xbb}_{r}', baseline+xbb_cut+r_cut, weight, '')
            pu.plot_2D(out_dir, h2s[f'{xbb}_{r}'], f'{xbb}_{r}', f'{xbb}_{r}: {h2s[f"{xbb}_{r}"].GetEntries()}', contours = contours.values())


def plot_1D_distributions(t, rw = 'ABCD'):
    if rw not in ['ABCD', 'perJet']:
        raise ValueError(f'unrecognized reweighting: {rw}')
    # plot 1D distributions
    hists = {}
    h_skeleton_weight = ROOT.TH1D(f'h_skeleton_weight', f'h_skeleton_weight;weight;Events', 100, 0, 1)
    for var in plotting_variables:
        if 'log_' in var:
            var = var.replace('log_', '')
        h_skeleton = ROOT.TH1D(f'h_skeleton_{var}', f'h_skeleton;{var};Events;', vars[var][0], vars[var][1], vars[var][2])
        if rw == 'ABCD':
            # train plots (unweighted histograms should be equivalent to preprocessing plots)
            k_SR_2F, k_CR_2F, k_CR_2F_rw = f'h_{var}_SR_2F', f'h_{var}_CR_2F', f'h_{var}_CR_2F_rw'
            weight = 'NN_SR_2F_weight_bstrap_med'
            h_weight = get_hist(t, h_skeleton_weight, weight, 'CR_2F', ROOT.TCut(f'({selections["baseline"]})&&({selections["CR"]})&&({selections["2F"]})'), ROOT.TCut('1'), '')
            pu.plot_single_hist(out_dir, h_weight, weight, 'CR_2F', show_entries = True)
            hists[k_SR_2F] = get_hist(t, h_skeleton, var, 'SR_2F',       ROOT.TCut(f'({selections["baseline"]})&&({selections["SR"]})&&({selections["2F"]})'), ROOT.TCut('1'), '')
            hists[k_CR_2F] = get_hist(t, h_skeleton, var, 'CR_2F',       ROOT.TCut(f'({selections["baseline"]})&&({selections["CR"]})&&({selections["2F"]})'), ROOT.TCut('1'), '')
            hists[k_CR_2F_rw] = get_hist(t, h_skeleton, var, 'CR_2F_rw', ROOT.TCut(f'({selections["baseline"]})&&({selections["CR"]})&&({selections["2F"]})'), ROOT.TCut(weight), '')
            pu.set_hist_color_style(hists[k_SR_2F], ROOT.kBlack, set_fill = False)
            pu.set_hist_color_style(hists[k_CR_2F], ROOT.kRed, set_fill = False)
            pu.set_hist_color_style(hists[k_CR_2F_rw], ROOT.kBlue, 2, set_fill = False)
            ratio_lims = [0.7, 1.3]
            pu.plot_histograms(
                out_dir,
                [pu.get_unit_normalized_hist(h) for h in [hists[k_SR_2F], hists[k_CR_2F], hists[k_CR_2F_rw]]],
                ['SR', 'CR', 'CR RW'],
                f'h_{var}_SR_CR_2F_comparison_rw_norm',
                short_title = '2F Xbb, baseline selection',
                plot_stat_err = True,
                ratio_limits = ratio_lims
            )
            # now see what things look like in the validation region, i.e. where exactly one large-R jet passes the Xbb WP
            h_skeleton = ROOT.TH1D(f'h_skeleton_{var}_rebin', f'h_skeleton_rebin;{var};Events;', int(vars[var][0]/2), vars[var][1], vars[var][2])
            k_SR_1F, k_CR_1F, k_CR_1F_rw = f'h_{var}_SR_1F', f'h_{var}_CR_1F', f'h_{var}_CR_1F_rw'
            h_weight = get_hist(t, h_skeleton_weight, weight, 'CR_1F', ROOT.TCut(f'({selections["baseline"]})&&({selections["CR"]})&&({selections["1F"]})'), ROOT.TCut('1'), '')
            pu.plot_single_hist(out_dir, h_weight, weight, 'CR_1F', show_entries = True)
            hists[k_SR_1F] = get_hist(t, h_skeleton, var, 'SR',       ROOT.TCut(f'({selections["baseline"]})&&({selections["SR"]})&&({selections["1F"]})'), ROOT.TCut('1'), '')
            hists[k_CR_1F] = get_hist(t, h_skeleton, var, 'CR',       ROOT.TCut(f'({selections["baseline"]})&&({selections["CR"]})&&({selections["1F"]})'), ROOT.TCut('1'), '')
            hists[k_CR_1F_rw] = get_hist(t, h_skeleton, var, 'CR_rw', ROOT.TCut(f'({selections["baseline"]})&&({selections["CR"]})&&({selections["1F"]})'), ROOT.TCut(weight), '')
            pu.set_hist_color_style(hists[k_SR_1F], ROOT.kBlack, set_fill = False)
            pu.set_hist_color_style(hists[k_CR_1F], ROOT.kRed, set_fill = False)
            pu.set_hist_color_style(hists[k_CR_1F_rw], ROOT.kBlue, 2, set_fill = False)
            ratio_lims = [0.7, 1.3]
            pu.plot_histograms(
                out_dir,
                [pu.get_unit_normalized_hist(h) for h in [hists[k_SR_1F], hists[k_CR_1F], hists[k_CR_1F_rw]]],
                ['SR', 'CR', 'CR RW'],
                f'h_{var}_SR_CR_1F_comparison_rw_norm',
                short_title = '1F Xbb, baseline selection',
                plot_stat_err = True,
                ratio_limits = ratio_lims
            )
            if var == 'm_hhBoosted':
                print(f'{var} entries:')
                for xbb in Xbb_sels:
                    if xbb == '0F':
                        continue
                    # sanity check, ensure above are equal to Integral()
                    n_SR = hists[f'h_m_hhBoosted_SR_{xbb}'].GetEntries()
                    n_CR = hists[f'h_m_hhBoosted_CR_{xbb}'].GetEntries()
                    n_CR_rw = hists[f'h_m_hhBoosted_CR_{xbb}_rw'].GetEntries()
                    y_SR = hists[f'h_m_hhBoosted_SR_{xbb}'].Integral()
                    y_CR = hists[f'h_m_hhBoosted_CR_{xbb}'].Integral()
                    y_CR_rw = hists[f'h_m_hhBoosted_CR_{xbb}_rw'].Integral()
                    print(f'\tn_SR_{xbb}: {n_SR:.1f} (yield: {y_SR})')
                    print(f'\tn_CR_{xbb}: {n_CR:.1f} (yield: {y_CR})')
                    print(f'\tn_CR_{xbb}_rw: {n_CR_rw:.1f} (yield: {y_CR_rw})')
                    print(f'\t--> SR/CR {xbb}: {n_SR/n_CR:.3f}')
            else:
                print(var)
        else:
            raise ValueError(f'Unrecognized rw: {rw}')


def plot_pass_vs_fail(t):
    cols = ['pt_h1Boosted', 'pt_h2Boosted',
                        'eta_h1Boosted', 'eta_h2Boosted',
                        'm_h1Boosted', 'm_h2Boosted',
                        'phi_h1Boosted', 'phi_h2Boosted',
                        'm_hhBoosted', 'pt_hhBoosted',
                        'pt_VBFj1Boosted', 'pt_VBFj2Boosted',
                        'eta_VBFj1Boosted', 'eta_VBFj2Boosted',
                        'HbbWP_h1Boosted', 'HbbWP_h2Boosted',
                        'boostedPrecut', 'passVBFJets',
                        'X_hhBoosted', 'm_VBFjjBoosted',
                        'run_number', 'event_number']
    df = pu.rdf_to_df(ROOT.RDataFrame(t), f'({selections["1F"]})&&({selections["baseline"]})', cols)
    df.insert(0, 'CR_VR', np.sqrt( np.power((df['m_h1Boosted']-124)/(0.1*np.log(df['m_h1Boosted'])),2) +
                                        np.power((df['m_h2Boosted']-117)/(0.1*np.log(df['m_h2Boosted'])),2) ), False)
    dfs = {}
    hists = {}
    dfs['SR'] = df[(df['X_hhBoosted'] < 1.6)].copy()
    dfs['CR'] = df[(df['X_hhBoosted'] >= 1.6) & (df['CR_VR'] >= 100) & (df['CR_VR'] < 170)].copy()
    for r in ['SR', 'CR']:
        mask_1F_leading = (dfs[r]['HbbWP_h1Boosted'] > 60) & (dfs[r]['HbbWP_h2Boosted'] <= 60)
        mask_1F_subleading = (dfs[r]['HbbWP_h1Boosted'] <= 60) & (dfs[r]['HbbWP_h2Boosted'] > 60)
        for v in ['pt', 'eta', 'phi', 'm']:
            dfs[r][f'{v}_pass_jet'] = -99
            dfs[r].loc[mask_1F_leading, f'{v}_pass_jet'] = dfs[r].loc[mask_1F_leading, f'{v}_h2Boosted']
            dfs[r].loc[mask_1F_subleading, f'{v}_pass_jet'] = dfs[r].loc[mask_1F_subleading, f'{v}_h1Boosted']
            dfs[r][f'{v}_fail_jet'] = -99
            dfs[r].loc[mask_1F_leading, f'{v}_fail_jet'] = dfs[r].loc[mask_1F_leading, f'{v}_h1Boosted']
            dfs[r].loc[mask_1F_subleading, f'{v}_fail_jet'] = dfs[r].loc[mask_1F_subleading, f'{v}_h2Boosted']
            print(f'{v} pass -99: {np.count_nonzero(dfs[r][f"{v}_pass_jet"] == -99)}')
            print(f'{v} fail -99: {np.count_nonzero(dfs[r][f"{v}_fail_jet"] == -99)}')
        # pass/fail plots
        for v in ['pt', 'eta', 'phi', 'm']:
            h_skeleton = ROOT.TH1D(f'h_skeleton_{v}', f'h_skeleton;Jet {v}{" [GeV]" if v in ["pt", "m"] else ""};Events', vars[f'{v}_h1Boosted'][0], vars[f'{v}_h1Boosted'][1], vars[f'{v}_h1Boosted'][2])
            hists[f'h_pass_{v}'] = h_skeleton.Clone(f'h_pass_{v}')
            pu.set_hist_color_style(hists[f'h_pass_{v}'], ROOT.kBlack, set_fill = False)
            hists[f'h_pass_{v}'] = pu.fill_h(hists[f'h_pass_{v}'], dfs[r][f'{v}_pass_jet'])
            hists[f'h_fail_{v}'] = h_skeleton.Clone(f'h_fail_{v}')
            pu.set_hist_color_style(hists[f'h_fail_{v}'], ROOT.kRed, set_fill = False)
            hists[f'h_fail_{v}'] = pu.fill_h(hists[f'h_fail_{v}'], dfs[r][f'{v}_fail_jet'])
            # now look at lead/sublead
            hists[f'h_pass_lead_{v}'] = h_skeleton.Clone(f'h_pass_lead_{v}')
            pu.set_hist_color_style(hists[f'h_pass_lead_{v}'], ROOT.kBlack, set_fill = False)
            hists[f'h_pass_lead_{v}'] = pu.fill_h(hists[f'h_pass_lead_{v}'], dfs[r][mask_1F_subleading][f'{v}_h1Boosted'])
            hists[f'h_fail_lead_{v}'] = h_skeleton.Clone(f'h_fail_lead_{v}')
            pu.set_hist_color_style(hists[f'h_fail_lead_{v}'], ROOT.kRed, set_fill = False)
            hists[f'h_fail_lead_{v}'] = pu.fill_h(hists[f'h_fail_lead_{v}'], dfs[r][mask_1F_leading][f'{v}_h1Boosted'])

            hists[f'h_pass_sublead_{v}'] = h_skeleton.Clone(f'h_pass_sublead_{v}')
            pu.set_hist_color_style(hists[f'h_pass_sublead_{v}'], ROOT.kBlack, set_fill = False)
            hists[f'h_pass_sublead_{v}'] = pu.fill_h(hists[f'h_pass_sublead_{v}'], dfs[r][mask_1F_leading][f'{v}_h2Boosted'])
            hists[f'h_fail_sublead_{v}'] = h_skeleton.Clone(f'h_fail_sublead_{v}')
            pu.set_hist_color_style(hists[f'h_fail_sublead_{v}'], ROOT.kRed, set_fill = False)
            hists[f'h_fail_sublead_{v}'] = pu.fill_h(hists[f'h_fail_sublead_{v}'], dfs[r][mask_1F_subleading][f'{v}_h2Boosted'])

        for v in ['pt', 'eta', 'phi', 'm']:
            pu.plot_histograms(
                out_dir,
                [pu.get_unit_normalized_hist(h) for h in [hists[f'h_pass_{v}'], hists[f'h_fail_{v}']]],
                ['Pass Xbb', 'Fail Xbb'],
                f'h_{v}_pass_fail_comparison_{r}',
                short_title = f'1F {r}, baseline selection',
                plot_stat_err = True
            )
            pu.plot_histograms(
                out_dir,
                [pu.get_unit_normalized_hist(h) for h in [hists[f'h_pass_lead_{v}'], hists[f'h_fail_lead_{v}']]],
                ['h1 pass Xbb', 'h1 fail Xbb'],
                f'h_{v}_pass_fail_lead_comparison_{r}',
                short_title = f'1F {r}, baseline selection',
                plot_stat_err = True
            )
            pu.plot_histograms(
                out_dir,
                [pu.get_unit_normalized_hist(h) for h in [hists[f'h_pass_sublead_{v}'], hists[f'h_fail_sublead_{v}']]],
                ['h2 pass Xbb', 'h2 fail Xbb'],
                f'h_{v}_pass_fail_sublead_comparison_{r}',
                short_title = f'1F {r}, baseline selection',
                plot_stat_err = True
            )

if __name__ == '__main__':
    if do_perJet and do_ABCD:
        raise ValueError('Can\'t run perJet and ABCD simultaneously!')

    rw = 'perJet' if do_perJet else 'ABCD'

    # open data files
    t_name = 'fullmassplane'
    in_dir = f'/Users/sebastienrettie/HH/reweighting/{rw}_SEP22/output_dir/'
    out_dir = f'/Users/sebastienrettie/HH/reweighting/{rw}_SEP22/plots/'
    input_files = glob.glob(os.path.join(in_dir, 'dat_NN*.root'))
    data_samples = [f'dat_NN_{"CR_perJet" if do_perJet else "SR_2F"}_{p}_bootstrap_NN_5_bootstraps.root' for p in range(15, 19)]
    t = pu.get_tree(input_files, data_samples, t_name)
    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    ROOT.gROOT.SetBatch(1)
    pu.set_ATLAS_style()
    # make plots
    if rw == 'ABCD':
        plot_2D_distributions(t)
        plot_1D_distributions(t, rw)
    else:
        plot_pass_vs_fail(t)


# To run:
# python hh4b-background-estimation/Resolved/NN_RW.py -d ~/HH/nano_ntuples/merged/Data/data15.root ~/HH/nano_ntuples/merged/Data/data16.root ~/HH/nano_ntuples/merged/Data/data17.root ~/HH/nano_ntuples/merged/Data/data18.root -y all --bootstrap 100 -o weight_dir --config VBF_Boosted --vbfboosted --target_reg SR_2F --not-all-tree

# then, for XX in [15,16,17,18]

# python hh4b-background-estimation/Resolved/combine_weights.py -d ~/HH/reweighting/weight_dir/dat_NN_SR_2F_XX_bootstrap.root -w weight_dir/ -y 20XX --bootstrap 100 -o output_dir/ --comb-label NN_100_bootstraps --target_reg SR_2F --nom-only --vbfboosted
