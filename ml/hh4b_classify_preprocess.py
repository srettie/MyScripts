import os
import sys
import ROOT
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.model_selection import train_test_split
sys.path.append('/Users/sebastienrettie/Projects/MyScripts/plotting/')
from signal_optimization_hh4b import get_rdfs, get_df_from_rdfs, fix_relative_variables, set_hist_color_style, baseline_cuts, data_cuts, vars, color_map
import plot_utils as pu

out_stem = 'classify_preprocessed'
in_dir = '/Users/sebastienrettie/HH/nano_ntuples/merged/'
out_dir = '/Users/sebastienrettie/HH/ml/classify/preprocessing/'
# get rdfs from NNTs
rdfs = get_rdfs(in_dir)
# get df from rdfs
samples = ['k2v0', 'dijet_bfilt', 'ttbar_HT']
selection = f'{baseline_cuts["boostedPrecut"]} && \
              {baseline_cuts["passVBFJets"]} && \
              {data_cuts["pass_Xbb_loose"]} && \
              {baseline_cuts["m_h1Boosted"]} && \
              {baseline_cuts["m_h2Boosted"]} && \
              {baseline_cuts["pt_h1Boosted"]} && \
              {baseline_cuts["pt_h2Boosted"]} && \
              {data_cuts["resolved_veto"]}'
training_variables = [
    'rel_pt_h1Boosted', 'rel_pt_h2Boosted',
    'eta_h1Boosted', 'eta_h2Boosted',
    'phi_h1Boosted', 'phi_h2Boosted',
    'rel_m_h1Boosted', 'rel_m_h2Boosted',
    'eta_hhBoosted', 'phi_hhBoosted',
    'rel_pt_VBFj1Boosted', 'pt_VBFj2Boosted',
    'eta_VBFj1Boosted', 'eta_VBFj2Boosted',
    'phi_VBFj1Boosted', 'phi_VBFj2Boosted',
    'E_VBFj1Boosted', 'E_VBFj2Boosted'
]
df_variables = [v.replace('rel_', '') for v in training_variables] + ['pt_hhBoosted', 'totalWeightBoosted', 'lumi', 'label', 'sample']
df, full_weights = get_df_from_rdfs(rdfs, samples, selection, df_variables)
full_weights = pd.Series(full_weights)
df = fix_relative_variables(df, ['pt_h1Boosted', 'pt_h2Boosted', 'm_h1Boosted', 'm_h2Boosted', 'pt_VBFj1Boosted'])

# split train/test
X_train, X_val, Y_train, Y_val, w_train, w_val = train_test_split(df[training_variables], df[['label']], full_weights, test_size=0.2, random_state=42)
print(f'X_train ({type(X_train)}):\n{X_train}')
print(f'Y_train ({type(Y_train)}):\n{Y_train}')
print(f'w_train ({type(w_train)}):\n{w_train}')

# scale inputs
std_scale = StandardScaler().fit(X_train.values)
X_train_std = pd.DataFrame(std_scale.transform(X_train.values), index=X_train.index, columns=X_train.columns)
X_val_std = pd.DataFrame(std_scale.transform(X_val.values), index=X_val.index, columns=X_val.columns)
print(f'X_train_std ({type(X_train_std)}):\n{X_train_std}')
print(f'X_val_std ({type(X_val_std)}):\n{X_val_std}')

minmax_scale = MinMaxScaler().fit(X_train.values)
X_train_minmax = pd.DataFrame(minmax_scale.transform(X_train.values), index=X_train.index, columns=X_train.columns)
X_val_minmax = pd.DataFrame(minmax_scale.transform(X_val.values), index=X_val.index, columns=X_val.columns)
print(f'X_train_minmax ({type(X_train_minmax)}):\n{X_train_minmax}')
print(f'X_val_minmax ({type(X_val_minmax)}):\n{X_val_minmax}')

# save to .h5 files
out_file = os.path.join(out_dir, out_stem)
X_train.to_hdf(f'{out_file}.h5', key = 'X_train', mode = 'w')
Y_train.to_hdf(f'{out_file}.h5', key = 'Y_train', mode = 'a')
w_train.to_hdf(f'{out_file}.h5', key = 'w_train', mode = 'a')
X_val.to_hdf(f'{out_file}.h5', key = 'X_val', mode = 'a')
Y_val.to_hdf(f'{out_file}.h5', key = 'Y_val', mode = 'a')
w_val.to_hdf(f'{out_file}.h5', key = 'w_val', mode = 'a')

X_train_std.to_hdf(f'{out_file}_std.h5', key = 'X_train', mode = 'w')
Y_train.to_hdf(f'{out_file}_std.h5', key = 'Y_train', mode = 'a')
w_train.to_hdf(f'{out_file}_std.h5', key = 'w_train', mode = 'a')
X_val_std.to_hdf(f'{out_file}_std.h5', key = 'X_val', mode = 'a')
Y_val.to_hdf(f'{out_file}_std.h5', key = 'Y_val', mode = 'a')
w_val.to_hdf(f'{out_file}_std.h5', key = 'w_val', mode = 'a')

X_train_minmax.to_hdf(f'{out_file}_minmax.h5', key = 'X_train', mode = 'w')
Y_train.to_hdf(f'{out_file}_minmax.h5', key = 'Y_train', mode = 'a')
w_train.to_hdf(f'{out_file}_minmax.h5', key = 'w_train', mode = 'a')
X_val_minmax.to_hdf(f'{out_file}_std.h5', key = 'X_val', mode = 'a')
Y_val.to_hdf(f'{out_file}_std.h5', key = 'Y_val', mode = 'a')
w_val.to_hdf(f'{out_file}_std.h5', key = 'w_val', mode = 'a')

# plot training variables
def fill_h(h, arr):
    for v in arr:
        h.Fill(v)
    return h

sample_map = {
    'signal'      : 1,
    'dijet_bfilt' : 2,
    'ttbar_HT'    : 3,
}

vars.update({
    'rel_pt_h1Boosted' : [100, 0, 10],
    'rel_pt_h2Boosted' : [100, 0, 10],
    'rel_m_h1Boosted' : [30, 0, 3],
    'rel_m_h2Boosted' : [30, 0, 3],
    'rel_pt_VBFj1Boosted' : [30, 0, 3],
    'pt_VBFj2Boosted' : [30, 0, 300],
    'eta_hhBoosted' : [40, -4, 4],
    'eta_h1Boosted' : [40, -4, 4],
    'eta_h2Boosted' : [40, -4, 4],
    'phi_hhBoosted' : [40, -4, 4],
    'phi_h1Boosted' : [40, -4, 4],
    'phi_h2Boosted' : [40, -4, 4],
    'phi_VBFj1Boosted' : [40, -4, 4],
    'phi_VBFj2Boosted' : [40, -4, 4],
    'E_VBFj1Boosted' : [200, 0, 2000],
    'E_VBFj2Boosted' : [200, 0, 2000],
})

pu.set_ATLAS_style()

for train_var in training_variables:
    h_skeleton = ROOT.TH1D(f'h_skeleton_{train_var}', f'h_skeleton;{train_var};Events;', vars[train_var][0], vars[train_var][1], vars[train_var][2])
    h_k2v0 = h_skeleton.Clone(f'h_{train_var}_k2v0')
    h_k2v0.SetDirectory(0)
    set_hist_color_style(h_k2v0, color_map['k2v0'], set_fill = False)
    h_dijet_bfilt = h_skeleton.Clone(f'h_{train_var}_dijet_bfilt')
    h_dijet_bfilt.SetDirectory(0)
    set_hist_color_style(h_dijet_bfilt, color_map['dijet_bfilt'])
    h_ttbar_HT = h_skeleton.Clone(f'h_{train_var}_ttbar_HT')
    h_ttbar_HT.SetDirectory(0)
    set_hist_color_style(h_ttbar_HT, color_map['ttbar_HT'])
    h_k2v0 = fill_h(h_k2v0, df[df['sample']==sample_map['signal']][train_var])
    h_dijet_bfilt = fill_h(h_dijet_bfilt, df[df['sample']==sample_map['dijet_bfilt']][train_var])
    h_ttbar_HT = fill_h(h_ttbar_HT, df[df['sample']==sample_map['ttbar_HT']][train_var])
    h_bkg_stack = ROOT.THStack('bkg_stack', f'bkg_stack;{train_var};Events;')
    h_bkg_stack.Add(h_ttbar_HT)
    h_bkg_stack.Add(h_dijet_bfilt)
    pu.plot_stack(
        out_dir, h_bkg_stack,
        f'h_{train_var}_stack',
        [h_k2v0], ['k2v0'],
        plot_stat_err = True
    )