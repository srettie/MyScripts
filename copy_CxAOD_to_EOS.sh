export COPY_SCRIPT="/afs/cern.ch/work/s/srettie/private/VHbbLegacy/r33_01_production/CxAODMakerCore/VHbb/CxAODOperations_VHbb/scripts_postProduction/copy_CxAODs_to_eos.py"
export SAMPLE_INFO="/afs/cern.ch/work/s/srettie/private/VHbbLegacy/r33_01_production/CxAODMakerCore/VHbb/CxAODOperations_VHbb/data/DxAOD/info/sample_info.txt"
export NOT_SORTED_BASE="/eos/user/s/srettie/VHbbLegacy/r33_01_production/not_sorted/"
export TO_COPY_BASE="/eos/user/s/srettie/VHbbLegacy/r33_01_production/CxAOD_r33-01_"

for jtype in em18 pf18 pf19
do
    for period in a d e
    do
        cd $NOT_SORTED_BASE/${jtype}/${period}/
        python $COPY_SCRIPT HIGG5D2 33-01_${period} $TO_COPY_BASE${jtype}/ -s $SAMPLE_INFO -v -d
    done
done

cd $NOT_SORTED_BASE/../

echo "Copied all files!"