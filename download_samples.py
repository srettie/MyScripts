import argparse
import subprocess

debug = False
do_test = False

def get_sample_name(name):
    return(name.split(':')[1].strip())

def get_parser():
    p = argparse.ArgumentParser(description='Optimize hh4b signal', allow_abbrev=False)
    p.add_argument('-d', '--debug', action='store_true', help='Print debug information')
    p.add_argument('--input_file', dest='input_file', default=None, required=True, help='File containing datasets to download')
    p.add_argument('--dl_dir', dest='dl_dir', default=None, required=True, help='Directory where downloads will be stored')
    p.add_argument('--do_test', action='store_true', help='Only download small portion of files as a test')
    return p

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    # required arguments
    input_file, dl_dir = args.input_file, args.dl_dir
    if args.debug:
        debug = args.debug
    if args.do_test:
        do_test = args.do_test
    n = 0
    with open(input_file) as f:
        for line in f:
            if do_test and n >= 2:
                continue
            line = line.strip()
            if not line or line[0] == '#' or line == '':
                continue
            cmd = ['rucio', 'download', '--ndownloader', '9']
            if do_test:
                cmd += ['--nrandom', '2']
            cmd += [line]
            subprocess.run(cmd, cwd = dl_dir)
            n += 1
    print(f'All done! Downloaded {n} samples.')