export SAMPLE_LIST_DIR="/afs/cern.ch/work/s/srettie/private/VHbbLegacy/r33_01_production/CxAODMakerCore/VHbb/CxAODOperations_VHbb/data/DxAOD/VHbb/"
export YIELDS_SCRIPT="/afs/cern.ch/work/s/srettie/private/VHbbLegacy/r33_01_production/CxAODMakerCore/VHbb/CxAODOperations_VHbb/scripts_postProduction/count_Nentry_SumOfWeight.py"
export AMI_SCRIPT="/afs/cern.ch/work/s/srettie/private/VHbbLegacy/r33_01_production/CxAODMakerCore/VHbb/CxAODOperations_VHbb/scripts_postProduction/getAMIInfo.py"
export CHECK_SCRIPT="/afs/cern.ch/work/s/srettie/private/VHbbLegacy/r33_01_production/CxAODMakerCore/VHbb/CxAODOperations_VHbb/scripts_postProduction/checkYields.py"
export CXAOD_BASE="/eos/user/s/srettie/VHbbLegacy/r33_01_production/CxAOD_r33-01_"

lsetup pyami

for jtype in em18 pf18 pf19
do
    for period in a d e
    do
        cd ${CXAOD_BASE}${jtype}/HIGG5D2_13TeV/CxAOD_33-01_${period}/
        python $YIELDS_SCRIPT
        mv yields.13TeV_DxAOD_sortedNEW.txt yields.13TeV_DxAOD_sorted.txt
        mv yields.13TeV_sortedNEW.txt yields.13TeV_sorted.txt
        cp $SAMPLE_LIST_DIR/list_sample_grid.13TeV_25ns.test_${period}.HIGG5D2.txt .
        python $AMI_SCRIPT list_sample_grid.13TeV_25ns.test_${period}.HIGG5D2.txt
        python $CHECK_SCRIPT
    done
done

echo "Produced all yield files!"