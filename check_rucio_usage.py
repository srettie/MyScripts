import argparse

account = 'perf-idtracking'
check_account_usage = 'rucio list-account-usage {} | tee usage_{}.txt'
rse = 'CERN-PROD_PERF-IDTRACKING'
check_rse_data = 'rucio list-datasets-rse --long {} | tee rse_data_{}.txt'

def get_parser():
    p = argparse.ArgumentParser(description='Debug AOD files')
    p.add_argument('-r', '--rse', dest='rse', default=None, help='RSE to check')
    p.add_argument('-a', '--account', dest='account', default=None, help='Account to check')
    return p

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    if args.rse:
        rse = args.rse
    if args.account:
        account = args.account

    # check account usage
    cmd = check_account_usage.format(account, account)
    print(cmd)
    
    # check RSE data
    cmd = check_rse_data.format(rse, rse)
    print(cmd)

    # get sorted list of biggest culprits
    datasets = {}
    with open(f'rse_data_{rse}.txt') as f:
        for l in f.readlines():
            if not l.startswith('|'):
                continue
            ds = l.split('|')[1]
            if ':' not in ds:
                continue
            size = int(l.split('|')[-2].split('/')[0])
            datasets[ds] = size
    total_size = 0
    sorted_datasets = {k: v for k, v in sorted(datasets.items(), key=lambda item: -item[1])}
    with open(f'{rse}.txt', 'w') as f:
        for k, v in sorted_datasets.items():
            line = f'{k}\t'
            if v / 1e12 > 1:
                line += f'{v/1e12} TB'
            elif v / 1e9 > 1:
                line += f'{v/1e9} GB'
            elif v / 1e6 > 1:
                line += f'{v/1e6} MB'
            else:
                line += f'{v/1e3} kB'
            line += '\n'
            f.write(line)
            total_size += v
    print(f'Total size: {total_size/1e12} TB')