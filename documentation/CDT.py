# To connect to remote jupyter notebook:

# On remote
jupyter notebook --no-browser --port=7777 --ip 127.0.0.1

# On local machine
ssh -Y -A -L 7777:localhost:7777 srettie@plus1.hep.ucl.ac.uk  ssh -N -L 7777:localhost:7777 pc213

# In local browser
localhost:7777