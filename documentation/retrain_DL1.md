# DL1(r) Training

## Setup
Start by downloading the `umami` framework

```
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/algorithms/umami.git
cd umami
```

The documentation in the [repository](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami) itself is very comprehensive and complete (make sure to also take a look in the `umami/docs` directory).
The following should be used in addition to the main `umami` documentation, in particular to get things running on the UCL cluster.
The easiest way to get started is to run within the provided containers.
Choose either the CPU or the GPU image, depending on what kind of machine you're running on.
You can bind the relevant paths you need to be available within the container by passing a comma-separated list of directories.

* If running on a CPU:

    ```singularity exec --home $PWD --bind /unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/hybrid --contain docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/algorithms/umami:latest bash```

* If running on a GPU:

    ```singularity exec --home $PWD --bind /unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/hybrid --contain --nv docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/algorithms/umami:latest-gpu bash```


## Prepare DL1 Inputs
Only use this section if you want to create your own preprocessed files.
If you already have a set of preprocessed files and all you want to do is retrain DL1(r), you can skip this step.
These instructions assume that you have produced `.h5` files using either the [`training-dataset-dumper`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper) or the [athena ntuple converter](https://gitlab.cern.ch/atlas-flavor-tagging-tools/DL1_framework/-/blob/master/tools/convert_fromROOT.py) and are ready to prepare the inputs to the DL1(r) training.

### Prepare paths to inputs
Start by setting the relevant paths to point to the files you want to use.

* Using FTAG samples
    ```
    export TRACK=""
    export FPATH="/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/hybrid"
    export ZPRIME="/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/user.mguth.427081.btagTraining.e6928_e5984_s3126_r10201_r10210_p3985.EMPFlow.2020-02-15-T225316-R8334_output.h5/*.h5"
    export TTBAR="/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/user.mguth.410470.btagTraining.e6337_s3126_r10201_p3985.EMPFlow.2020-02-14-T232210-R26303_output.h5/user.mguth.20591838._00*"
    export PREPROCESS="examples/PFlow-Preprocessing.yaml"
    ```

* Using `.h5` files converted from athena ntuples

    * Sanity check
        ```
        export TRACK="_nom"
        export FPATH="/unix/atlastracking/srettie/training_inputs/hybrid/nom"
        export ZPRIME="/unix/atlastracking/srettie/training_inputs/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210/nom/*.h5"
        export TTBAR="/unix/atlastracking/srettie/training_inputs/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210/nom/*.h5"
        export PREPROCESS="examples/PFlow-Preprocessing_nom.yaml"
        ```
    * Nominal with custom IPXD
        ```
        export TRACK="_nom_custom_ipxd"
        export FPATH="/unix/atlastracking/srettie/training_inputs_custom_ipxd/hybrid/nom"
        export ZPRIME="/unix/atlastracking/srettie/training_inputs_custom_ipxd/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210/nom/*.h5"
        export TTBAR="/unix/atlastracking/srettie/training_inputs_custom_ipxd/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210/nom/*.h5"
        export PREPROCESS="examples/PFlow-Preprocessing_nom_custom_ipxd.yaml"
        ```
    * RFMVA_loose
        ```
        export TRACK="_nom_RFMVA_loose_custom_ipxd"
        export FPATH="/unix/atlastracking/srettie/training_inputs_custom_ipxd/hybrid/nom_RFMVA_loose"
        export ZPRIME="/unix/atlastracking/srettie/training_inputs_custom_ipxd/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210/nom_RFMVA_loose/*.h5"
        export TTBAR="/unix/atlastracking/srettie/training_inputs_custom_ipxd/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210/nom_RFMVA_loose/*.h5"
        export PREPROCESS="examples/PFlow-Preprocessing_nom_RFMVA_loose_custom_ipxd.yaml"
        ```
    * RFMVA_tight
        ```
        export TRACK="_nom_RFMVA_tight_custom_ipxd"
        export FPATH="/unix/atlastracking/srettie/training_inputs_custom_ipxd/hybrid/nom_RFMVA_tight"
        export ZPRIME="/unix/atlastracking/srettie/training_inputs_custom_ipxd/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210/nom_RFMVA_tight/*.h5"
        export TTBAR="/unix/atlastracking/srettie/training_inputs_custom_ipxd/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210/nom_RFMVA_tight/*.h5"
        export PREPROCESS="examples/PFlow-Preprocessing_nom_RFMVA_tight_custom_ipxd.yaml"
        ```

### Create hybrid files
Training jets have even event numbers (`--even`) and validation/testing jets have odd event numbers (`--odd`) in order to not mix validation/testing and training jets.
For more information, see [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami/-/blob/master/docs/DL1r-instructions.md).
Hybrid files are created using the [`training-dataset-dumper`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper).
To produce a complete set of training/testing/validation files, you need to produce:

* Training files using the ttbar sample (using `-c 1.0`, one for each jet flavour): 
    ```
    python create_hybrid-large_files.py --n_split 4 --even --bjets -Z ${ZPRIME} -t ${TTBAR} -n 10000000 -c 1.0 -o ${FPATH}/MC16d_hybrid-bjets_even_1_PFlow-merged.h5
    python create_hybrid-large_files.py --n_split 4 --even --cjets -Z ${ZPRIME} -t ${TTBAR} -n 12745953 -c 1.0 -o ${FPATH}/MC16d_hybrid-cjets_even_1_PFlow-merged.h5
    python create_hybrid-large_files.py --n_split 5 --even --ujets -Z ${ZPRIME} -t ${TTBAR} -n 20000000 -c 1.0 -o ${FPATH}/MC16d_hybrid-ujets_even_1_PFlow-merged.h5
    ```
* Training files using the extended Z' sample (using `-c 0.0`, only one command for all jet flavours):
    ```
    python create_hybrid-large_files.py --even -Z ${ZPRIME} -t ${TTBAR} -n 9593092 -c 0.0 -o ${FPATH}/MC16d_hybrid-ext_even_0_PFlow-merged.h5
    ```
* Validation/testing files using the ttbar sample
    ```
    python create_hybrid-large_files.py --n_split 2 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 4000000 -c 1.0 -o ${FPATH}/MC16d_hybrid_odd_100_PFlow-no_pTcuts.h5
    ```
* Validation/testing files using the extended Z' sample:
    ```
    python create_hybrid-large_files.py --n_split 2 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 4000000 -c 0.0 -o ${FPATH}/MC16d_hybrid-ext_odd_0_PFlow-no_pTcuts.h5
    ```

### Merge `.h5` training hybrid files
Merge the files created above using the [`hdf5_manipulator`](https://gitlab.cern.ch/mguth/hdf5_manipulator):
```
python /unix/atlasvhbb2/srettie/tracking/Projects/hdf5_manipulator/merge_big.py --input ${FPATH}/MC16d_hybrid-bjets_even_1_PFlow-merged-file_* --output ${FPATH}/MC16d_hybrid-bjets_even_1_PFlow-merged.h5
python /unix/atlasvhbb2/srettie/tracking/Projects/hdf5_manipulator/merge_big.py --input ${FPATH}/MC16d_hybrid-cjets_even_1_PFlow-merged-file_* --output ${FPATH}/MC16d_hybrid-cjets_even_1_PFlow-merged.h5
python /unix/atlasvhbb2/srettie/tracking/Projects/hdf5_manipulator/merge_big.py --input ${FPATH}/MC16d_hybrid-ujets_even_1_PFlow-merged-file_* --output ${FPATH}/MC16d_hybrid-ujets_even_1_PFlow-merged.h5
python /unix/atlasvhbb2/srettie/tracking/Projects/hdf5_manipulator/merge_big.py --input ${FPATH}/MC16d_hybrid-ext_even_0_PFlow-merged-file_* --output ${FPATH}/MC16d_hybrid-ext_even_0_PFlow-merged.h5
```
### Preprocess the merged `.h5` files to get preprocessed/scaled inputs:
The four steps to preprocessing the inputs for the DL1(r) training use the [`umami framework`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami).
You need to set the `PREPROCESS` variable to a configuration file that points to the training files you want to use, e.g. [this one](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami/-/blob/master/examples/PFlow-Preprocessing.yaml).
First, run undersample to ensure the same number of b/c/light jets in each pT/eta bin.
```
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1_Variables.yaml --undersampling
```
Next, create the scaling dictionaries and apply the relevant scaling:
```
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1_Variables.yaml --scaling
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1_Variables.yaml --apply_scales
```
Finally, write the preprocessed information to a single output file for training:
```
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1_Variables.yaml --write
```
The Final output of the preprocessing consists of one `.h5` file containing the preprocessed variables (including weights and labels) and one json parameter dict file containing all shift, scale and default values.


## To run the training
You'll need a training configuration file, e.g. [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami/-/blob/master/examples/DL1r-PFlow-Training-config.yaml).
To run the training, from the `umami` directory, simly specify the number of epochs you want to train for (300 epochs should be enough for DL1(r)) and the training configuration file you want to use:
```
python train_DL1.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml -e 300
```
To check performance the training performance (can be done while training):
```
python train_DL1.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml -p
```
Once the training is done, evaluate the performance of the re-trained network using:
```
python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml -e 280 --dl1
```
And use the files produced in the previous line to make performance plots:
```
python umami/plotting-DL1.py -c examples/comparison_dl1${TRACK}.yaml
```
It's also possible to evaluate the performance of the training using the Z' sample for testing instead of the default ttbar sample (note this is not in the `master` branch of the `umami` framework yet...):
```
python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml -e 280 --dl1 --zp
python umami/plotting-DL1.py -c examples/comparison_dl1${TRACK}.yaml
```

## To retrain DIPS
A similar procedure can be used to retrain the DIPS network.
The commands are mostly the same (copied below for completeness), but include the track information

* Using FTAG samples
    ```
    export TRACK=""
    export FPATH="/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/hybrid_with_tracks"
    export ZPRIME="/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/user.mguth.427081.btagTraining.e6928_e5984_s3126_r10201_r10210_p3985.EMPFlow.2020-02-15-T225316-R8334_output.h5/*.h5"
    export TTBAR="/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/user.mguth.410470.btagTraining.e6337_s3126_r10201_p3985.EMPFlow.2020-02-14-T232210-R26303_output.h5/user.mguth.20591838._00*"
    export PREPROCESS="examples/PFlow-Preprocessing_with_tracks.yaml"
    ```

* Using `.h5` files converted from athena ntuples

    * Sanity check
    ```
    export TRACK="_nom"
    export FPATH="/unix/atlastracking/srettie/training_inputs_with_tracks/hybrid/nom"
    export ZPRIME="/unix/atlastracking/srettie/training_inputs_with_tracks/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210/nom/*.h5"
    export TTBAR="/unix/atlastracking/srettie/training_inputs_with_tracks/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210/nom/*.h5"
    export PREPROCESS="examples/PFlow-Preprocessing_nom_with_tracks.yaml"
    ```
    * Nominal with custom IPXD
    ```
    export TRACK="_nom_custom_ipxd"
    export FPATH="/unix/atlastracking/srettie/training_inputs_custom_ipxd_with_tracks/hybrid/nom"
    export ZPRIME="/unix/atlastracking/srettie/training_inputs_custom_ipxd_with_tracks/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210/nom/*.h5"
    export TTBAR="/unix/atlastracking/srettie/training_inputs_custom_ipxd_with_tracks/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210/nom/*.h5"
    export PREPROCESS="examples/PFlow-Preprocessing_nom_custom_ipxd_with_tracks.yaml"
    ```


### Create hybrid files
```
python create_hybrid-large_files.py --n_split 4 --even --bjets -Z ${ZPRIME} -t ${TTBAR} -n 10000000 -c 1.0 -o ${FPATH}/MC16d_hybrid-bjets_even_1_PFlow-merged.h5 --write_tracks
python create_hybrid-large_files.py --n_split 4 --even --cjets -Z ${ZPRIME} -t ${TTBAR} -n 12745953 -c 1.0 -o ${FPATH}/MC16d_hybrid-cjets_even_1_PFlow-merged.h5 --write_tracks
python create_hybrid-large_files.py --n_split 5 --even --ujets -Z ${ZPRIME} -t ${TTBAR} -n 20000000 -c 1.0 -o ${FPATH}/MC16d_hybrid-ujets_even_1_PFlow-merged.h5 --write_tracks
python create_hybrid-large_files.py --even -Z ${ZPRIME} -t ${TTBAR} -n 9593092 -c 0.0 -o ${FPATH}/MC16d_hybrid-ext_even_0_PFlow-merged.h5 --write_tracks
python create_hybrid-large_files.py --n_split 2 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 4000000 -c 1.0 -o ${FPATH}/MC16d_hybrid_odd_100_PFlow-no_pTcuts.h5 --write_tracks
python create_hybrid-large_files.py --n_split 2 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 4000000 -c 0.0 -o ${FPATH}/MC16d_hybrid-ext_odd_0_PFlow-no_pTcuts.h5 --write_tracks
```

### Merge `.h5` training hybrid files
```
python /unix/atlasvhbb2/srettie/tracking/Projects/hdf5_manipulator/merge_big.py --input ${FPATH}/MC16d_hybrid-bjets_even_1_PFlow-merged-file_* --output ${FPATH}/MC16d_hybrid-bjets_even_1_PFlow-merged.h5
python /unix/atlasvhbb2/srettie/tracking/Projects/hdf5_manipulator/merge_big.py --input ${FPATH}/MC16d_hybrid-cjets_even_1_PFlow-merged-file_* --output ${FPATH}/MC16d_hybrid-cjets_even_1_PFlow-merged.h5
python /unix/atlasvhbb2/srettie/tracking/Projects/hdf5_manipulator/merge_big.py --input ${FPATH}/MC16d_hybrid-ujets_even_1_PFlow-merged-file_* --output ${FPATH}/MC16d_hybrid-ujets_even_1_PFlow-merged.h5
python /unix/atlasvhbb2/srettie/tracking/Projects/hdf5_manipulator/merge_big.py --input ${FPATH}/MC16d_hybrid-ext_even_0_PFlow-merged-file_* --output ${FPATH}/MC16d_hybrid-ext_even_0_PFlow-merged.h5
```

### Preprocessing
```
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1_Variables.yaml --undersampling -t
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1_Variables.yaml --scaling -t
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1_Variables.yaml --apply_scales -t
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1_Variables.yaml --write -t
```

### Train DIPS
```
python umami/train_Dips.py -c examples/Dips-PFlow-Training-config${TRACK}.yaml -e 300
python umami/train_Dips.py -c examples/Dips-PFlow-Training-config${TRACK}.yaml -p
```
### Evaluate performance:
Note: Have to remove umami prediction when only re-training DIPS.
```
python umami/evaluate_model.py -c examples/Dips-PFlow-Training-config${TRACK}.yaml -e 280
python umami/plotting-DL1.py -c examples/comparison_dips${TRACK}.yaml
```

### For pT bins and Z' sample evaluation
Running the code below will give validation samples with 2M jets in each sample and evaluate the model using these newly created samples.
```
ptbins_tt = [0,150,250]
ptbins_zp = [0,150,400,1000,1750,2750]
base_cmd_zp = 'python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${{ZPRIME}} -t ${{TTBAR}} -n 2000000 -c 0.0 {} -o ${{FPATH}}/MC16d_hybrid-ext_odd_0_PFlow-{}.h5 --write_tracks'
base_cmd_tt = 'python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${{ZPRIME}} -t ${{TTBAR}} -n 2000000 -c 1.0 {} -o ${{FPATH}}/MC16d_hybrid_odd_100_PFlow-{}.h5 --write_tracks'
base_cmd_eval_tt = 'python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${{TRACK}}.yaml --dl1 -e 280'
base_cmd_eval_zp = 'python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${{TRACK}}.yaml --dl1 -e 280 --zp'

def produce_validation_samples(ptbins, base_cmd):
    for i, ptstart in enumerate(ptbins):
        if i == 0:
            cmd = base_cmd.format('-ptmax '+str(1000*ptbins[i+1]), str(ptbins[i])+'pT'+str(ptbins[i+1]))
        elif i == len(ptbins) - 1:
            cmd = base_cmd.format('-ptmin '+str(1000*ptbins[i]), str(ptbins[i])+'pTInf')
        else:
            cmd = base_cmd.format('-ptmin '+str(1000*ptbins[i])+' -ptmax '+str(1000*ptbins[i+1]), str(ptbins[i])+'pT'+str(ptbins[i+1]))
        print(cmd)

def evaluate_model(ptbins, base_cmd):
    for i, ptstart in enumerate(ptbins):
        if i == len(ptbins) - 1:
            cmd = base_cmd + ' --ptrange {}'.format(str(ptbins[i]) + 'pTInf')
        else:
            cmd = base_cmd + ' --ptrange {}'.format(str(ptbins[i]) + 'pT' + str(ptbins[i+1]))
        print(cmd)

produce_validation_samples(ptbins_zp, base_cmd_zp)
produce_validation_samples(ptbins_tt, base_cmd_tt)
evaluate_model(ptbins_zp, base_cmd_eval_zp)
evaluate_model(ptbins_tt, base_cmd_eval_tt)
```
More explicitely:
```
python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 2000000 -c 0.0 -ptmax 150000 -o ${FPATH}/MC16d_hybrid-ext_odd_0_PFlow-0pT150.h5 --write_tracks
python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 2000000 -c 0.0 -ptmin 150000 -ptmax 400000 -o ${FPATH}/MC16d_hybrid-ext_odd_0_PFlow-150pT400.h5 --write_tracks
python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 2000000 -c 0.0 -ptmin 400000 -ptmax 1000000 -o ${FPATH}/MC16d_hybrid-ext_odd_0_PFlow-400pT1000.h5 --write_tracks
python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 2000000 -c 0.0 -ptmin 1000000 -ptmax 1750000 -o ${FPATH}/MC16d_hybrid-ext_odd_0_PFlow-1000pT1750.h5 --write_tracks
python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 2000000 -c 0.0 -ptmin 1750000 -ptmax 2750000 -o ${FPATH}/MC16d_hybrid-ext_odd_0_PFlow-1750pT2750.h5 --write_tracks
python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 2000000 -c 0.0 -ptmin 2750000 -o ${FPATH}/MC16d_hybrid-ext_odd_0_PFlow-2750pTInf.h5 --write_tracks

python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 2000000 -c 1.0 -ptmax 150000 -o ${FPATH}/MC16d_hybrid_odd_100_PFlow-0pT150.h5 --write_tracks
python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 2000000 -c 1.0 -ptmin 150000 -ptmax 250000 -o ${FPATH}/MC16d_hybrid_odd_100_PFlow-150pT250.h5 --write_tracks
python create_hybrid-large_files.py --n_split 1 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 2000000 -c 1.0 -ptmin 250000 -o ${FPATH}/MC16d_hybrid_odd_100_PFlow-250pTInf.h5 --write_tracks

python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml --dl1 -e 280 --zp --ptrange 0pT150
python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml --dl1 -e 280 --zp --ptrange 150pT400
python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml --dl1 -e 280 --zp --ptrange 400pT1000
python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml --dl1 -e 280 --zp --ptrange 1000pT1750
python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml --dl1 -e 280 --zp --ptrange 1750pT2750
python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml --dl1 -e 280 --zp --ptrange 2750pTInf

python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml --dl1 -e 280 --ptrange 0pT150
python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml --dl1 -e 280 --ptrange 150pT250
python umami/evaluate_model.py -c examples/DL1-PFlow-Training-config${TRACK}.yaml --dl1 -e 280 --ptrange 250pTInf
```


## Train dedicated high-pT network
Start with basic selection of only extended Z' sample events with pT > 400 GeV
* Using FTAG samples
    ```
    export TRACK=""
    export FPATH="/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/highpt"
    export ZPRIME="/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/user.mguth.427081.btagTraining.e6928_e5984_s3126_r10201_r10210_p3985.EMPFlow.2020-02-15-T225316-R8334_output.h5/*.h5"
    export TTBAR="/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/user.mguth.410470.btagTraining.e6337_s3126_r10201_p3985.EMPFlow.2020-02-14-T232210-R26303_output.h5/user.mguth.20591838._00*"
    export PREPROCESS="examples/PFlow-Preprocessing_highpt.yaml"
    ```

```
python create_hybrid-large_files.py --even -Z ${ZPRIME} -t ${TTBAR} -n 8792457 -c 0.0 -o ${FPATH}/MC16d_hybrid-ext_even_0_PFlow_highpt-merged.h5 --write_tracks -p 400000
python create_hybrid-large_files.py --n_split 4 --odd --no_cut -Z ${ZPRIME} -t ${TTBAR} -n 8000000 -ptmin 400000 -c 0.0 -o ${FPATH}/MC16d_hybrid-ext_odd_0_PFlow_highpt-merged.h5 --write_tracks

python /unix/atlasvhbb2/srettie/tracking/Projects/hdf5_manipulator/merge_big.py --input ${FPATH}/MC16d_hybrid-ext_even_0_PFlow_highpt-merged-file_* --output ${FPATH}/MC16d_hybrid-ext_even_0_PFlow_highpt-merged.h5



python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1r_Variables.yaml --undersampling -t
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1r_Variables.yaml --scaling -t
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1r_Variables.yaml --apply_scales -t
python umami/preprocessing.py -c ${PREPROCESS} -v umami/configs/DL1r_Variables.yaml --write -t

python train_DL1.py -c examples/DL1r-PFlow-Training-config_highpt.yaml -e 300
python train_DL1.py -c examples/DL1r-PFlow-Training-config_highpt.yaml -p


python umami/evaluate_model.py -c examples/DL1r-PFlow-Training-config_highpt.yaml --dl1 -e 280 --zp

python umami/evaluate_model.py -c examples/DL1r-PFlow-Training-config_highpt.yaml --dl1 -e 280 --zp --ptrange 400pT1000
python umami/evaluate_model.py -c examples/DL1r-PFlow-Training-config_highpt.yaml --dl1 -e 280 --zp --ptrange 1000pT1750
python umami/evaluate_model.py -c examples/DL1r-PFlow-Training-config_highpt.yaml --dl1 -e 280 --zp --ptrange 1750pT2750
python umami/evaluate_model.py -c examples/DL1r-PFlow-Training-config_highpt.yaml --dl1 -e 280 --zp --ptrange 2750pTInf

python umami/plotting-DL1.py -c examples/comparison_dl1r_highpt.yaml
```