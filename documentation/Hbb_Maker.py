# Used for DAOD --> CAOD

# To build initially:
export my_dir="/unix/atlasvhbb2/srettie/VHbbLegacy/"
cd ${my_dir}
git clone --recursive ssh://git@gitlab.cern.ch:7999/CxAODFramework/CxAODMakerCore.git
export source_dir="${my_dir}/CxAODMakerCore"
cd ${source_dir}
source ./copydatafromafs.sh
# Check which AnalysisBase we need in ${my_dir}/CMakeLists.txt
cd ${my_dir}
mkdir build
cd build
setupATLAS
asetup AnalysisBase,21.2.XX,here # (as found above)
cmake ../CxAODMakerCore/
cmake —build .
source x86_64-centos7-gcc8-opt/setup.sh

# Optionally, get r32-15 branch:
git fetch --all
git checkout -b r32-15-branch origin/r32-15-branch
git submodule update
# Note that the branch does not have the timestamp or TruthZWJetContainer rename fix, so either change this manually, or use the master for the latest p-tags…

# Or avoid all this, and just run
source install.sh

# After initial build:
cd ${my_dir}
cd build/
setupATLAS
asetup
source x86_64-centos7-gcc8-opt/setup.sh

# To run stuff:
cd ../run/
source ../CxAODMakerCore/VHbb/CxAODOperations_VHbb/scripts_CxAODMaker/runTestFiles.sh
# Example
python ../../CxAODMakerCore/VHbb/CxAODOperations_VHbb/scripts_CxAODMaker/submitMaker.py -c 1L -m e -n 100 -l /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/DxAOD/VHbb_p4308/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_HIGG2D4.e6337_s3126_r10724_p4308 -d -x

# To run grid production
source ../CxAODMakerCore/VHbb/CxAODOperations_VHbb/scripts_CxAODMaker/runGridProd.sh

# Sample info:
a -> r9364
d -> r10201
e -> r10724
0L —> HIGG5D1
1L —> HIGG5D2
2L —> HIGG2D4

# For production:
# Download files from grid for em18/pf18/pf19 separately
# Move into separate folders for a/d/e:
mv *r9364* a/; mv *r10201* d/; mv *r10724* e/;

# Change relevant files/directories and run the copying steering script,
# which runs the script to copy to eos (make sure isEOS is false if starting from EOS!!)
source ~/private/Projects/MyScripts/copy_CxAOD_to_EOS.sh

# Check CxAOD yields; change relevant files/directories and run the check steering script
source ~/private/Projects/MyScripts/check_CxAOD_yields.sh

# Copy the CxAODs and their yield files to the Higgs eos
cd /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/
cp -v -r /eos/user/s/srettie/VHbbLegacy/r33_04_production/CxAOD_r33-04_* .

# Packages for which .gitlab-ci.yml have been updated:
CxAODMaker_VHbb
CxAODMaker
CxAODTools_VHbb
CxAODTools

