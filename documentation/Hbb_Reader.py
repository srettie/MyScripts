# Used for CAOD --> Fit Inputs

# On lxplus, from /afs/cern.ch/work/s/srettie/private/VHbbLegacy/split_studies/run/

# Initial setup:
git clone --recursive ssh://git@gitlab.cern.ch:7999/CxAODFramework/CxAODReaderCore.git
checkout relevant branches
cd CxAODReaderCore/
source ./install.sh

# After initial setup:
setupATLAS
# Optional re-compilation:
cd CxAODReaderCore/
source ./install.sh
source ../build/x86_64-centos7-gcc8-opt/setup.sh

# For testing:
../CxAODReaderCore/VHbb/CxAODOperations_VHbb/scripts/submitReader.sh /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r32-15/ /afs/cern.ch/work/s/srettie/private/VHbbLegacy/split_studies/run/ 1L a VHbb MVA H 32-15 "qqZvvHbbJ_PwPy8MINLO qqZllHbbJ_PwPy8MINLO" "" 1
# Only signal:
../CxAODReaderCore/VHbb/CxAODOperations_VHbb/scripts/submitReader.sh /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r32-15/ /afs/cern.ch/work/s/srettie/private/VHbbLegacy/split_studies/run/ 1L a,d,e VHbb MVA H 32-15 "qqZvvHbbJ_PwPy8MINLO qqZllHbbJ_PwPy8MINLO qqWlvHbbJ_PwPy8MINLO ggZvvHbb_PwPy8 ggZllHbb_PwPy8" "" 1
# Only Z+jets
../CxAODReaderCore/VHbb/CxAODOperations_VHbb/scripts/submitReader.sh /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r32-15/ /afs/cern.ch/work/s/srettie/private/VHbbLegacy/split_studies/run/ 1L a,d,e VHbb MVA H 32-15 "ZeeB_Sh221 Zee_Sh221 ZmumuB_Sh221 Zmumu_Sh221 ZtautauB_Sh221 Ztautau_Sh221 ZeeC_Sh221 ZeeL_Sh221 ZmumuC_Sh221 ZmumuL_Sh221 ZtautauC_Sh221 ZtautauL_Sh221" "" 1
# Full production:
../CxAODReaderCore/VHbb/CxAODOperations_VHbb/scripts/submitReader.sh /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r32-15/ /afs/cern.ch/work/s/srettie/private/VHbbLegacy/split_studies/run/ 1L a,d,e VHbb MVA H 32-15 none none 1



# For v33-01 validation (use setup_submodule_branches.sh to checkout origin/master-updatesCxAOD33 and manually checkout master of CxAODOperations_VHbb)
../CxAODReaderCore/VHbb/CxAODOperations_VHbb/scripts_CxAODReader/submitReader.sh /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r33-01_em18/ /afs/cern.ch/work/s/srettie/private/VHbbLegacy/r33_01_production/Reader/run/ 1L a,d,e VHbb MVA D 33-01 qqWlvHbbJ_PwPy8MINLO 1

# MAKE SURE TO SET USE_PF="true"
../CxAODReaderCore/VHbb/CxAODOperations_VHbb/scripts_CxAODReader/submitReader.sh /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r33-01_pf18/ /afs/cern.ch/work/s/srettie/private/VHbbLegacy/r33_01_production/Reader/run/ 1L a,d,e VHbb MVA D 33-01 qqWlvHbbJ_PwPy8MINLO 1

# MAKE SURE TO SET USE_PF="true" AND BTAGTIMESTAMP="_BTagging201903" AND BTAGALGO="DL1r"
../CxAODReaderCore/VHbb/CxAODOperations_VHbb/scripts_CxAODReader/submitReader.sh /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r33-01_pf19/ /afs/cern.ch/work/s/srettie/private/VHbbLegacy/r33_01_production/Reader/run/ 1L a,d,e VHbb MVA D 33-01 qqWlvHbbJ_PwPy8MINLO 1

# For v32-15 comparison checks (checkout r32-24-Reader-Resolved-05-CSFix tag of CxAODReaderCore)
../CxAODReaderCore/VHbb/CxAODOperations_VHbb/scripts/submitReader.sh /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r32-15/ /afs/cern.ch/work/s/srettie/private/VHbbLegacy/r32_15_production/Reader/run/ 1L a,d,e VHbb MVA D 32-15 "qqWlvHbbJ_PwPy8MINLO" "" 1

# After running the reader with option “0”, the config file will be in output folder and also in:
build/x*/data/CxAODReader_VHbb/


########################
# To check failed jobs:
########################
# Make sure all histograms/trees are appropriately named and linked into proper Reader_* directory, e.g.:
# Add "hist-" to beginning of all histogram files:
for f in * ; do mv -- "$f" "hist-$f" ; done
# Link from afs to eos for large files:
ln -s /eos/user/s/srettie/VHbbLegacy/split_studies/run/1L_32-15_a_MVA_H/histo/*.root .
# Actually check failures:
python /afs/cern.ch/work/s/srettie/private/VHbbLegacy/split_studies/CxAODReaderCore/VHbb/CxAODOperations_VHbb/scripts/checkReaderFails.py Reader_1L_32-15_a_MVA_H/

# To hadd histograms:
python /afs/cern.ch/work/s/srettie/private/VHbbLegacy/split_studies/CxAODReaderCore/VHbb/CxAODOperations_VHbb/scripts/haddReaderHists.py --dir ./


##################
# Reader options
##################

# DO_MVA_TRAINING_TREES --> Save trees for MVA training
# NUMBEROFEVENTS --> Set number of events to process
# DRIVER --> where to run; condor, direct, LSF
# DOONLYINPUTS -->  Stores only histograms of mBB and MVA to have small files for final fit if true
# NOMINALONLY --> Stores only nominal if true, includes systematics if false
# ANASTRATEGY --> "Resolved" by defaul, set to "Merged" for boosted analysis