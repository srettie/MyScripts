# Start on laptop due to latest keras/tensorflow versions
git clone ssh://git@gitlab.cern.ch:7999/Atlas-Inner-Tracking/ctide-nn-config-testing.git
cd ~/Projects/NumberNetwork/ctide-nn-config-testing/configurations/
# Setup lwtnn, just need to do once
git clone https://github.com/lwtnn/lwtnn.git
mkdir lwtnn/build
cd lwtnn/build
cmake -DBUILTIN_BOOST=true -DBUILTIN_EIGEN=true ..
make -j 4

# Put architecnture and weights file in relevant directory under configurations/
cd ~/Projects/NumberNetwork/ctide-nn-config-testing/configurations/
mkdir NN_000
cd NN_000
cp ~/Downloads/relwtnnforpixelnn/architecture_num.json ~/Downloads/relwtnnforpixelnn/weights.h5 .

# Create variables file configuration files from
../lwtnn/converters/kerasfunc2json.py architecture_num.json weights.h5 > variables_num.json

# Set relevant variable names (set do_NN = True in script)
python ../set_variable_names.py

# Set relevant offsets and scales in order to normalize input variables
python set_weights_offsets_NN.py

# Create configuration file
../lwtnn/converters/kerasfunc2json.py architecture_num.json weights.h5 variables_num.json > config_num.json

# Test configuration file
../lwtnn/build/bin/lwtnn-test-lightweight-graph config_num.json
../lwtnn/scripts/lwtnn-test-keras-functional.py architecture_num.json variables_num.json weights.h5 

# Upload files to git and commit

# Now on lxplus for athena and COOL
# After checking out code
cd /afs/cern.ch/work/s/srettie/private/NumberNetwork/ctide-nn-config-testing/
setupATLAS
asetup
cd /afs/cern.ch/work/s/srettie/private/NumberNetwork/ctide-nn-config-testing/configurations/
# Copy configuration files to the TEST directory
cp NN_000/config_num.json MDN_001/config_pos* TEST/
cd /afs/cern.ch/work/s/srettie/private/NumberNetwork/ctide-nn-config-testing/convert_to_cool/
# Convert json file to db file (if you get TableAlreadyExistingException, delete the .db files in the convert_to_cool/ directory)
python convert_jsons_to_cool.py

# Current database names are:
PixelNNCalibJSON-SIM-RUN2-000-02
PixelNNCalibJSON-DATA-RUN2-000-02

# Test database files
python read_local_db.py

# Output of read_local_db.py:
# Checking OFLP200 db instance.
# sqlite://;schema=/afs/cern.ch/work/s/srettie/private/NumberNetwork/new_db_10112020/localdb_OFLP200.db;dbname=OFLP200
# Got folder /PIXEL/PixelClustering/PixelNNCalibJSON
# Tags in folder:
# PixelNNCalibJSON-SIM-RUN2-000-02
# Checking contents...
# NumberNetwork object size 5
# PositionNetwork_N1 object size 5
# PositionNetwork_N2 object size 5
# PositionNetwork_N3 object size 5
# Closing database
# Checking CONDBR2 db instance.
# sqlite://;schema=/afs/cern.ch/work/s/srettie/private/NumberNetwork/new_db_10112020/localdb_CONDBR2.db;dbname=CONDBR2
# Got folder /PIXEL/PixelClustering/PixelNNCalibJSON
# Tags in folder:
# PixelNNCalibJSON-DATA-RUN2-000-02
# Checking contents...
# NumberNetwork object size 5
# PositionNetwork_N1 object size 5
# PositionNetwork_N2 object size 5
# PositionNetwork_N3 object size 5
# Closing database

# Check what exists in COOL, use this to see before/after you add your db to COOL
python check_cool_folders.py

# Output of check_cool_folders.py before adding new databases:
# Checking tables in COOLOFL_PIXEL OFLP200
# Opened database COOLOFL_PIXEL/OFLP200
# Obtained folder <cppyy.gbl.cool.IFolder object at 0x884a620 held by std::shared_ptr<cool::IFolder> at 0x85c3e60>
# Available tags are:
# PixelNNCalibJSON-SIM-RUN2-000-00
# PixelNNCalibJSON-SIM-RUN2-000-01
# Checking tables in COOLOFL_PIXEL CONDBR2
# Opened database COOLOFL_PIXEL/CONDBR2
# Obtained folder <cppyy.gbl.cool.IFolder object at 0x86b1e00 held by std::shared_ptr<cool::IFolder> at 0x48dc370>
# Available tags are:
# PixelNNCalibJSON-DATA-RUN2-000-00
# PixelNNCalibJSON-DATA-RUN2-000-01
# Finished.

# When really ready, publish the databases to COOL
python publish_to_cool.py

# Output of publish_to_cool.py:
# ('Beginning update of DB instance', 'OFLP200')
# Default DB file to upload is localdb_OFLP200.db. Is this what you wish to use? (y/n)
# ('Will upload file', 'localdb_OFLP200.db', 'to DB instance', 'OFLP200', 'using account ATLAS_COOLOFL_PIXEL_W.')
# Do you confirm? (y/n)
# Please enter COOL password for account ATLAS_COOLOFL_PIXEL_W:
# Consider folder /PIXEL/PixelClustering/PixelNNCalibJSON
# Taking IOVs [0,0] to [2147483647,4294967295] tag PixelNNCalibJSON-SIM-RUN2-000-02 from source
# Summary for folder /PIXEL/PixelClustering/PixelNNCalibJSON tag PixelNNCalibJSON-SIM-RUN2-000-02
# Found data with IOV range [0,0] to [2147483647,4294967295] in source
# Total of 2 objects in 1 channels
# Destination tag PixelNNCalibJSON-SIM-RUN2-000-02 does not exist yet
# INFO: you are writing new tag, nothing to BackUp
# Notification will be sent to []
# Checking for AtlCoolCopy.exe availability ...
# /cvmfs/atlas.cern.ch/repo/sw/software/21.0/Athena/21.0.117/InstallArea/x86_64-slc6-gcc62-opt/bin/AtlCoolCopy.exe
# Please enter a comment to describe the update ('Enter' to finish)
# Fix IOV for mc15
# Data will be written to Oracle server ATLAS_COOLWRITE schema ATLAS_COOLOFL_PIXEL account ATLAS_COOLOFL_PIXEL_W instance OFLP200

# Copy from /PIXEL/PixelClustering/PixelNNCalibJSON PixelNNCalibJSON-SIM-RUN2-000-02 to /PIXEL/PixelClustering/PixelNNCalibJSON PixelNNCalibJSON-SIM-RUN2-000-02 taking IOVs from [0,0] to [2147483647,4294967295]
# Confirm execution of copy operations? (y/n)y
# running command AtlCoolCopy.exe "sqlite://;schema=localdb_OFLP200.db;dbname=OFLP200" "oracle://ATLAS_COOLWRITE;schema=ATLAS_COOLOFL_PIXEL;dbname=OFLP200" -folder /PIXEL/PixelClustering/PixelNNCalibJSON -tag PixelNNCalibJSON-SIM-RUN2-000-02 -outtag PixelNNCalibJSON-SIM-RUN2-000-02  comm = -folder /PIXEL/PixelClustering/PixelNNCalibJSON -tag PixelNNCalibJSON-SIM-RUN2-000-02 -outtag PixelNNCalibJSON-SIM-RUN2-000-02
# WARNING Please stop using AtlCoolCopy.exe, and use AtlCoolCopy instead

# Open source database: sqlite://;schema=localdb_OFLP200.db;dbname=OFLP200
# Open destination database: oracle://ATLAS_COOLWRITE;schema=ATLAS_COOLOFL_PIXEL;dbname=OFLP200
# Add folders in path:/PIXEL/PixelClustering/PixelNNCalibJSON [ /PIXEL/PixelClustering/PixelNNCalibJSON ]
# Start to process folder: /PIXEL/PixelClustering/PixelNNCalibJSON (run/lumi)
# Multi version folder: consider tags [ PixelNNCalibJSON-SIM-RUN2-000-02 ]
# Copying tag PixelNNCalibJSON-SIM-RUN2-000-02 of folder /PIXEL/PixelClustering/PixelNNCalibJSON to destination tag PixelNNCalibJSON-SIM-RUN2-000-02
# Folder copied with 2 objects

# Copying finished after 1 AtlCoolCopy runs and 0 failures
# ('Beginning update of DB instance', 'CONDBR2')
# Default DB file to upload is localdb_CONDBR2.db. Is this what you wish to use? (y/n)
# ('Will upload file', 'localdb_CONDBR2.db', 'to DB instance', 'CONDBR2', 'using account ATLAS_COOLOFL_PIXEL_W.')
# Do you confirm? (y/n)
# Please enter COOL password for account ATLAS_COOLOFL_PIXEL_W:
# Consider folder /PIXEL/PixelClustering/PixelNNCalibJSON
# Taking IOVs [0,0] to [2147483647,4294967295] tag PixelNNCalibJSON-DATA-RUN2-000-02 from source
# Summary for folder /PIXEL/PixelClustering/PixelNNCalibJSON tag PixelNNCalibJSON-DATA-RUN2-000-02
# Found data with IOV range [0,0] to [2147483647,4294967295] in source
# Total of 2 objects in 1 channels
# Destination tag PixelNNCalibJSON-DATA-RUN2-000-02 does not exist yet
# INFO: you are writing new tag, nothing to BackUp
# Notification will be sent to []
# Checking for AtlCoolCopy.exe availability ...
# /cvmfs/atlas.cern.ch/repo/sw/software/21.0/Athena/21.0.117/InstallArea/x86_64-slc6-gcc62-opt/bin/AtlCoolCopy.exe
# Please enter a comment to describe the update ('Enter' to finish)
# Fix IPV for mc15
# Data will be written to Oracle server ATLAS_COOLWRITE schema ATLAS_COOLOFL_PIXEL account ATLAS_COOLOFL_PIXEL_W instance CONDBR2

# Copy from /PIXEL/PixelClustering/PixelNNCalibJSON PixelNNCalibJSON-DATA-RUN2-000-02 to /PIXEL/PixelClustering/PixelNNCalibJSON PixelNNCalibJSON-DATA-RUN2-000-02 taking IOVs from [0,0] to [2147483647,4294967295]
# Confirm execution of copy operations? (y/n)y
# running command AtlCoolCopy.exe "sqlite://;schema=localdb_CONDBR2.db;dbname=CONDBR2" "oracle://ATLAS_COOLWRITE;schema=ATLAS_COOLOFL_PIXEL;dbname=CONDBR2" -folder /PIXEL/PixelClustering/PixelNNCalibJSON -tag PixelNNCalibJSON-DATA-RUN2-000-02 -outtag PixelNNCalibJSON-DATA-RUN2-000-02  comm = -folder /PIXEL/PixelClustering/PixelNNCalibJSON -tag PixelNNCalibJSON-DATA-RUN2-000-02 -outtag PixelNNCalibJSON-DATA-RUN2-000-02
# WARNING Please stop using AtlCoolCopy.exe, and use AtlCoolCopy instead

# Open source database: sqlite://;schema=localdb_CONDBR2.db;dbname=CONDBR2
# Open destination database: oracle://ATLAS_COOLWRITE;schema=ATLAS_COOLOFL_PIXEL;dbname=CONDBR2
# Add folders in path:/PIXEL/PixelClustering/PixelNNCalibJSON [ /PIXEL/PixelClustering/PixelNNCalibJSON ]
# Start to process folder: /PIXEL/PixelClustering/PixelNNCalibJSON (run/lumi)
# Multi version folder: consider tags [ PixelNNCalibJSON-DATA-RUN2-000-02 ]
# Copying tag PixelNNCalibJSON-DATA-RUN2-000-02 of folder /PIXEL/PixelClustering/PixelNNCalibJSON to destination tag PixelNNCalibJSON-DATA-RUN2-000-02
# Folder copied with 2 objects

# Copying finished after 1 AtlCoolCopy runs and 0 failures
# Done. Use check_cool_folders.py to confirm that your new tags have appeared.

# Now get athena running (https://atlassoftwaredocs.web.cern.ch/gittutorial/)
cd /afs/cern.ch/work/s/srettie/private/NumberNetwork/
setupATLAS
lsetup git
git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git
# Checkout the packages we want to change
cd athena
# Add package from sparse checkout (full path is athena/InnerDetector/InDetRecTools/SiClusterizationTool)
git atlas addpkg SiClusterizationTool
# To see what packages are checked out: git atlas listpkg
# To remove packate from sparse checkout: git atlas rmpkg SiClusterizationTool

# Create your own branch to track your changes
git fetch upstream
git checkout -b 21.0-srettie_NNdev upstream/21.0 --no-track
#git checkout -b master-srettie_NNdev upstream/master --no-track

# Setup athena (to check available releases: ls /cvmfs/atlas.cern.ch/repo/sw/software/21.0/Athena/)
cd /afs/cern.ch/work/s/srettie/private/NumberNetwork/build/
asetup Athena,21.0.117
#asetup master,latest,Athena

# Make sure the packages you want to compile are in the package_filters.txt
cp ../athena/Projects/WorkDir/package_filters_example.txt ../package_filters_example.txt
# Edit package_filters.txt
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir
make -j

# Go to run directory
mkdir /afs/cern.ch/work/s/srettie/private/NumberNetwork/run/
cd /afs/cern.ch/work/s/srettie/private/NumberNetwork/run/

# Make sure to source setup
source /afs/cern.ch/work/s/srettie/private/NumberNetwork/build/x86_64-centos7-gcc62-opt/setup.sh

# Now test some JOs

# Baseline to start from
athena /afs/cern.ch/work/s/srettie/private/NumberNetwork/ctide-nn-config-testing/ctide-testing-jos/jobOptions_baseline.py | tee myLog.log

# Edit jobOptions_lwtnn.py to point to our database files
athena /afs/cern.ch/work/s/srettie/private/NumberNetwork/ctide-nn-config-testing/ctide-testing-jos/jobOptions_lwtnn.py | tee myLog.log

# Run on HITS




# Now to modify the code: start looking at Rel21(https://gitlab.cern.ch/atlas/athena/-/blob/21.0/InnerDetector/InDetRecTools/SiClusterizationTool/src/NnClusterizationFactory.cxx), where Kate implemented positions network evaluation on L624, and where the current number network implementation is on line 512