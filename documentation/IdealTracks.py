
# Clone this project, follow the instructions in the readme and run the setup.

git clone https://gitlab.cern.ch/atlas-flavor-tagging-tools/FlavourTagPerformanceFramework

# Go into the run directory and open jobOptions.py

# Activate the retag flag which by default is off:
doRetag = True

# Set this flag to false:
alg.retriveTruthJets = False

# Set the info for the taggers and tracks to true:
alg.SVInfo;alg.JetFitterInfo = True
alg.ImpactParameterInfo = True
alg.TrackInfo = True

# Uncomment these lines from the jobOptions.py

algSeq += CfgMgr.BTagTrackAugmenter(
    "BTagTrackAugmenter_" + JetCollection,
    OutputLevel=INFO,
    JetCollectionName = JetCollection,
    TrackToVertexIPEstimator = ToolSvc.trkIPEstimator,
    SaveTrackVectors = True,
    )

# Change the name of the file you want to run over, save and close the file. I have a few samples downloaded on the UCL cluster here: /unix/atlasvhbb2/srettie/tracking/

# Open RetagFragment.py and change this line, which specifies the Track collection you want to run b-tagging on:
tracksKey = "InDetTrackParticles"

# You can set this e.g. to:
tracksKey = "InDetPseudoTrackParticles"
tracksKey = "InDetIdealTrackParticles"

# Close the file and then run the following command to produce the n-tuples:
athena jobOptions.py
