# To submit Z' extended sample at UCL
# For Z', we have 5000 files, each with 100 events, so 100 files per job means 10k events per job
# For Z' extended, we have 854 files, each with 10k events, so 5 file per job means 50k events per job
# Will have a total of 171 jobs, x3, so 513
# For each of athena_full and athena_full_custom_ipxd
# 18 jobs for ROC curves and 171 x 3 = 513 jobs for process_trees of Z' extented
# To submit ttbar samples on the grid:

# From home/srettie/FlavourTagPerformanceFramework/run
pathena jobOptions_nom.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nFiles 1 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_testprod
# Test production, note tight/loose have been switched in output...
pathena jobOptions_nom_RFMVA_loose.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nFiles 1 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_RFMVA_tight_testprod --extFile neural_net_v1.json
pathena jobOptions_nom_RFMVA_tight.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nFiles 1 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_RFMVA_loose_testprod --extFile neural_net_v1.json

# Now run the full production
pathena jobOptions_nom.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_prod1
pathena jobOptions_nom_RFMVA_loose.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_RFMVA_loose_prod1 --extFile neural_net_v1.json
pathena jobOptions_nom_RFMVA_tight.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_RFMVA_tight_prod1 --extFile neural_net_v1.json

# Now try to get custom IPxD templates working...
# Just copying the calibration file... no
pathena jobOptions_nom.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nFiles 1 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_custom_ipxd_testprod3 --extFile BTagCalibRUN2-08-49_nom.root

# Add the PoolFC option
pathena jobOptions_nom.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nFiles 1 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_custom_ipxd_testprod4 --extFile BTagCalibRUN2-08-49_nom.root --addPoolFC BTagCalibRUN2-08-49_nom.root

# Add also the mycool.db file
pathena jobOptions_nom.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nFiles 1 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_custom_ipxd_testprod5 --extFile mycool.db,BTagCalibRUN2-08-49_nom.root --addPoolFC BTagCalibRUN2-08-49_nom.root

# Now run the full production with custom ipxd templates
cp /unix/atlasvhbb2/srettie/tracking/reference_histograms/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210/BTagCalibRUN2-08-49_nom.root .;echo "Calib file:";echo "BTagCalibRUN2-08-49_nom.root";echo "Suffix:";echo "srettie";coolHist_setFileIdentifier.sh BTagCalibRUN2-08-49_nom.root;coolHist_extractFileIdentifier.sh BTagCalibRUN2-08-49_nom.root;coolHist_insertFileToCatalog.py BTagCalibRUN2-08-49_nom.root;coolHist_setReference.py OFLP200 /GLOBAL/BTagCalib/RUN12 1 srettie BTagCalibRUN2-08-49_nom.root;echo "setchan /GLOBAL/BTagCalib/RUN12 1 RUN12" | AtlCoolConsole.py "sqlite://;schema=mycool.db;dbname=OFLP200";

pathena jobOptions_nom.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_custom_ipxd_prod1 --extFile mycool.db,BTagCalibRUN2-08-49_nom.root --addPoolFC BTagCalibRUN2-08-49_nom.root

cp /unix/atlasvhbb2/srettie/tracking/reference_histograms/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210/BTagCalibRUN2-08-49_nom_RFMVA_loose.root .;echo "Calib file:";echo "BTagCalibRUN2-08-49_nom_RFMVA_loose.root";echo "Suffix:";echo "srettie";coolHist_setFileIdentifier.sh BTagCalibRUN2-08-49_nom_RFMVA_loose.root;coolHist_extractFileIdentifier.sh BTagCalibRUN2-08-49_nom_RFMVA_loose.root;coolHist_insertFileToCatalog.py BTagCalibRUN2-08-49_nom_RFMVA_loose.root;coolHist_setReference.py OFLP200 /GLOBAL/BTagCalib/RUN12 1 srettie BTagCalibRUN2-08-49_nom_RFMVA_loose.root;echo "setchan /GLOBAL/BTagCalib/RUN12 1 RUN12" | AtlCoolConsole.py "sqlite://;schema=mycool.db;dbname=OFLP200";

pathena jobOptions_nom_RFMVA_loose.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_RFMVA_loose_custom_ipxd_prod1 --extFile mycool.db,BTagCalibRUN2-08-49_nom_RFMVA_loose.root,neural_net_v1.json --addPoolFC BTagCalibRUN2-08-49_nom_RFMVA_loose.root

cp /unix/atlasvhbb2/srettie/tracking/reference_histograms/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210/BTagCalibRUN2-08-49_nom_RFMVA_tight.root .;echo "Calib file:";echo "BTagCalibRUN2-08-49_nom_RFMVA_tight.root";echo "Suffix:";echo "srettie";coolHist_setFileIdentifier.sh BTagCalibRUN2-08-49_nom_RFMVA_tight.root;coolHist_extractFileIdentifier.sh BTagCalibRUN2-08-49_nom_RFMVA_tight.root;coolHist_insertFileToCatalog.py BTagCalibRUN2-08-49_nom_RFMVA_tight.root;coolHist_setReference.py OFLP200 /GLOBAL/BTagCalib/RUN12 1 srettie BTagCalibRUN2-08-49_nom_RFMVA_tight.root;echo "setchan /GLOBAL/BTagCalib/RUN12 1 RUN12" | AtlCoolConsole.py "sqlite://;schema=mycool.db;dbname=OFLP200";

pathena jobOptions_nom_RFMVA_tight.py --inDS mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r10201_r10210 --nGBPerJob=MAX --nFilesPerJob 5 --outDS user.srettie.410470.PhPy8EG.AOD.srettie_nom_RFMVA_tight_custom_ipxd_prod1 --extFile mycool.db,BTagCalibRUN2-08-49_nom_RFMVA_tight.root,neural_net_v1.json --addPoolFC BTagCalibRUN2-08-49_nom_RFMVA_tight.root