# Validate changes on master by comparing to reference
import sys
import glob
import ROOT
import plot_utils

plot_dir = '/Users/sebastien/VHLegacy/Plots/'
tree_name = 'CollectionTree'
tree_file = 'data-CxAOD/CxAOD.root'
cutflow_filename = 'hist-CxAOD.root'
cutflows = ['CutFlow/PreselectionCutFlow','CutFlow_Nominal/muon_Nominal','CutFlow_Nominal/electron_Nominal','CutFlow_Nominal/jet_Nominal']

def get_short_name(s):
    return s.replace('Maker_','').replace('mc16_13TeV.','').replace('.PowhegPythia8EvtGen_NNPDF3_AZNLO','').replace('MINLO_','').split('_VpT.deriv.DAOD_HIGG')[0]
        
def compare_cutflows(f1,f2,channel,compare_EMTopo_PFlow):
    
    print('\n\nComparing channel: {}'.format(channel))
    # Setup pads and canvas
    ratio_frac = 0.3

    cnv = ROOT.TCanvas()
    cnv.cd()
    
    p2 = ROOT.TPad('p2','p2',0.,0.,1.,ratio_frac)
    p2.Draw()
    p2.SetTopMargin(0.03)
    p2.SetBottomMargin(ratio_frac)
    
    p1 = ROOT.TPad('p1','p1',0.,ratio_frac,1.,1.)
    p1.Draw()
    p1.SetBottomMargin(0.03)
    p1.cd()

    # Font style
    x_label_font = 43
    y_label_font = 43
    x_title_font = 43
    y_title_font = 43    
    # Sizes in pixels
    x_title_size = 14
    y_title_size = 14
    x_label_size = 0.1
    y_label_size = 0.1
    # Offsets relative to axis
    x_title_offset = 4.0
    y_title_offset = 1.6
    
    short_name = get_short_name(channel)
    
    for cf_name in cutflows:
        print('\nCutflow name: {}'.format(cf_name))
        cf1 = f1.Get(cf_name).Clone()
        cf1.SetDirectory(0)
        cf2 = f2.Get(cf_name).Clone()
        cf2.SetDirectory(0)
        
        diff = cf1.Clone()
        diff.SetDirectory(0)
        
        # Check we have the same number of bins
        ncuts1, ncuts2 = cf1.GetNbinsX(), cf2.GetNbinsX()
        if ncuts2 != ncuts2:
            print('ERROR: CF1 has {} bins, CF2 has {} bins'.format(ncuts1,ncuts2))
            
        for i in range(1,ncuts1+1):            
            # Check name of cuts
            b1label = cf1.GetXaxis().GetBinLabel(i)
            b2label = cf2.GetXaxis().GetBinLabel(i)
            if b1label != b2label:
                print('ERROR: Bin {} has difference labels: {} (EMTopo) vs. {} (PFlow)'.format(i,b1label,b2label))
            
            # Check number of events for this cut
            b1 = cf1.GetBinContent(i)
            b2 = cf2.GetBinContent(i)
            difference = b2-b1
            r = b1/b2
            reldiff = difference/b1
            diff.SetBinContent(i,r)
            
            # # Alert if large discrepancies
            # if 100*reldiff > 10:
            #     print('Comparing: {} for {}'.format(cf_name, short_name))
            #     print('\t{}: {} vs. {} events ({:.2%} difference)'.format(b1label,b1,b2,reldiff))        
        
        # Style
        cf1.SetLineColor(ROOT.kRed)
        cf1.SetMinimum(0)
        cf1.SetMaximum(1.5*cf1.GetMaximum())
        cf1.GetXaxis().SetLabelSize(0)
        cf2.SetLineColor(ROOT.kBlack)
        
        # Draw
        p1.cd()
        cf1.Draw('hist')
        cf2.Draw('hist,same')
        
        # Legend
        lx1,ly1,lx2,ly2 = 0.6,0.7,0.9,0.9
        leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
        if compare_EMTopo_PFlow:
            leg.AddEntry(cf1, 'EMTopo', 'l')
            leg.AddEntry(cf2, 'PFlow', 'l')
        else:
            leg.AddEntry(cf1, 'master', 'l')
            leg.AddEntry(cf2, 'r32-15', 'l')

        leg.Draw('same')
        
        plot_utils.draw_labels(cf_name)

        p2.cd()
        #diff.GetXaxis().SetLabelFont(x_label_font)
        #diff.GetYaxis().SetLabelFont(y_label_font)
        diff.GetXaxis().SetLabelSize(x_label_size)
        diff.GetYaxis().SetLabelSize(y_label_size)
        # Titles
        if compare_EMTopo_PFlow:
            diff.GetYaxis().SetTitle('EMTopo / PFlow')
        else:
            diff.GetYaxis().SetTitle('master / r32-15')
            
        diff.GetXaxis().SetTitleFont(x_title_font)
        diff.GetYaxis().SetTitleFont(y_title_font)
        diff.GetXaxis().SetTitleSize(x_title_size)
        diff.GetYaxis().SetTitleSize(y_title_size)
        diff.GetYaxis().SetTitleOffset(y_title_offset)
        diff.GetXaxis().SetTitleOffset(x_title_offset)
        diff.Draw('hist')
        
        file_title = short_name
        if compare_EMTopo_PFlow:
            file_title = 'EMTopo_vs_PFlow_'+short_name.replace('PF_0','')
            
        if cf_name == cutflows[-1]:
            cnv.SaveAs(plot_dir+file_title+'.pdf)')                
        else:
            cnv.SaveAs(plot_dir+file_title+'.pdf(')
            
    cnv.Close()
    
if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: '+sys.argv[0]+' <master_path> [<reference_path>]')
        sys.exit()
    
    # Compare EMTopo to PFlow jets here...
    compare_EMTopo_PFlow = (len(sys.argv) == 2)

    if compare_EMTopo_PFlow:
        p1 = sys.argv[1]
        c1 = [c.split('/')[-1] for c in glob.glob(p1+'/EMTopo/*') if 'Maker_' in c]
        c2 = [c.split('/')[-1] for c in glob.glob(p1+'/PFlow/*') if 'Maker_' in c]
        channels = [c for c in c1 if c.replace('EMTopo','PFlow').replace('PF_0','PF_1') in c2]
        print('Comparing EMTopo to PFlow jets for {}'.format(p1))
    else:
        p1 = sys.argv[1]
        c1 = [c.split('/')[-1] for c in glob.glob(p1+'/*') if 'Maker_' in c]
        p2 = sys.argv[2]
        c2 = [c.split('/')[-1] for c in glob.glob(p2+'/*') if 'Maker_' in c]    
        channels = sorted(list(set(c1).intersection(set(c2))))
        print('Comparing {} to {}'.format(p1,p2))
        

    for c in channels:
        print('\t{}'.format(c))
        
    for c in channels:
        if compare_EMTopo_PFlow:
            f1 = ROOT.TFile.Open(p1+'/EMTopo/'+c+'/'+cutflow_filename)
            f2 = ROOT.TFile.Open(p1+'/PFlow/'+c.replace('PF_0','PF_1')+'/'+cutflow_filename)            
        else:
            f1 = ROOT.TFile.Open(p1+c+'/'+cutflow_filename)
            f2 = ROOT.TFile.Open(p2+c+'/'+cutflow_filename)
        compare_cutflows(f1,f2,c,compare_EMTopo_PFlow)
        f1.Close()
        f2.Close()