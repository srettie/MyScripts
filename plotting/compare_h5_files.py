# Compare FTAG .h5 files to locally produced .h5 files
import sys
import os
import glob
import datetime as dt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import h5py

debug = False
verbose = False

nbins = 400
njets = -1
# Only use every nth element in arrays to save time
nskip = 1

out_dir = '/share/rcifdata/srettie/FTAG_preprocessing/debug_plots/compare_test_samples/'

# DIPS retraining path
# dips_path = '/unix/atlastracking/srettie/retrain_DIPS/nominal/'
# dips_path = '/unix/atlastracking/srettie/retrain_DIPS/loose/'
# dips_path = '/share/rcifdata/srettie/FTAG_preprocessing/full_dips_loose_highpt/'
dips_path = '/share/rcifdata/srettie/FTAG_preprocessing/full_dips_loose/'
# dips_path = '/unix/atlastracking/srettie/retrain_DIPS/loose_highpt_odd_training/'
gnn_path = '/share/rcifdata/srettie/FTAG_preprocessing/'


files_info = {
    # Files on hypatia
    'gnn_test_zp'                    : [gnn_path+'gnn_highptMS99/hybrids/MC16d-inclusive_testing_zprime_highpt_PFlow.h5', 'jets'],
    'gnn_test_zp_pseudo'             : [gnn_path+'gnn_pseudoMS99/hybrids/MC16d-inclusive_testing_zprime_highpt_PFlow.h5', 'jets'],
    'gnn_test_zp_sanity'             : [gnn_path+'gnn_highptMS99_sanity/hybrids/MC16d-inclusive_testing_zprime_highpt_PFlow.h5', 'jets'],
    'gnn_test_zp_pseudo_sanity'      : [gnn_path+'gnn_pseudoMS99_sanity/hybrids/MC16d-inclusive_testing_zprime_highpt_PFlow.h5', 'jets'],
    'gnn_train_zp'                   : [gnn_path+'gnn_highptMS99/preprocessed/PFlow-hybrid-resampled.h5', 'jets'],
    'gnn_train_zp_pseudo'            : [gnn_path+'gnn_pseudoMS99/preprocessed/PFlow-hybrid-resampled.h5', 'jets'],
    'gnn_train_zp_sanity'            : [gnn_path+'gnn_highptMS99_sanity/preprocessed/PFlow-hybrid-resampled.h5', 'jets'],
    'gnn_train_zp_pseudo_sanity'     : [gnn_path+'gnn_pseudoMS99_sanity/preprocessed/PFlow-hybrid-resampled.h5', 'jets'],
    # Before merging/preprocessing
    'dips_train_tt_bjets'            : [dips_path+'hybrids/MC16d_hybrid-bjets_even_1_PFlow-merged-file_*.h5', 'jets'],
    'dips_train_tt_cjets'            : [dips_path+'hybrids/MC16d_hybrid-cjets_even_1_PFlow-merged-file_*.h5', 'jets'],
    'dips_train_tt_ujets'            : [dips_path+'hybrids/MC16d_hybrid-ujets_even_1_PFlow-merged-file_*.h5', 'jets'],
    'dips_train_zp'                  : [dips_path+'hybrids/MC16d_hybrid-ext_even_0_PFlow-merged-file_*.h5', 'jets'],
    # After merging (should be the same as sum of relevant files above)
    'dips_train_tt_bjets_merged'     : [dips_path+'preprocessed/MC16d_hybrid-bjets_even_1_PFlow-merged.h5', 'jets'],
    'dips_train_tt_cjets_merged'     : [dips_path+'preprocessed/MC16d_hybrid-cjets_even_1_PFlow-merged.h5', 'jets'],
    'dips_train_tt_ujets_merged'     : [dips_path+'preprocessed/MC16d_hybrid-ujets_even_1_PFlow-merged.h5', 'jets'],
    'dips_train_zp_merged'           : [dips_path+'preprocessed/MC16d_hybrid-ext_even_0_PFlow-merged.h5', 'jets'],
    # After downsampling
    'dips_train_hybrid_downsampled'  : [dips_path+'preprocessed/PFlow-hybrid-downsampled-file-*.h5', 'bcujets'],
    # After applying scaling
    'dips_train_hybrid_preprocessed' : [dips_path+'preprocessed/PFlow-hybrid-preprocessed-file-*.h5', 'jets'],
    # Final file used for training
    'dips_final_training_file'       : [dips_path+'preprocessed/PFlow-hybrid-preprocessed_shuffled.h5', 'X_train'],
    # Validation and testing samples
    'dips_val_tt'                    : [dips_path+'hybrids/MC16d_hybrid_odd_100_PFlow-no_pTcuts-file_0.h5', 'jets'],
    'dips_test_tt'                   : [dips_path+'hybrids/MC16d_hybrid_odd_100_PFlow-no_pTcuts-file_1.h5', 'jets'],
    'dips_val_zp'                    : [dips_path+'hybrids/MC16d_hybrid-ext_odd_0_PFlow-no_pTcuts-file_0.h5', 'jets'],
    'dips_test_zp'                   : [dips_path+'hybrids/MC16d_hybrid-ext_odd_0_PFlow-no_pTcuts-file_1.h5', 'jets'],
    'dips_val_zp_highpt'             : [dips_path+'hybrids/MC16d_hybrid-ext_odd_0_PFlow-highpt-file_0.h5', 'jets'],
    'dips_test_zp_highpt'            : [dips_path+'hybrids/MC16d_hybrid-ext_odd_0_PFlow-highpt-file_1.h5', 'jets'],
    # Downsampled testing samples (in folder loose_highpt_odd_training)
    # 'dips_test_zp_preprocessed_full'      : [dips_path+'preprocessed/MC16d_hybrid-ext_odd_0_PFlow-preprocessed_full.h5', 'jets'],
    # 'dips_test_zp_preprocessed_noshuffle' : [dips_path+'preprocessed/MC16d_hybrid-ext_odd_0_PFlow-preprocessed_noshuffle.h5', 'jets'],
    # 'dips_test_zp_preprocessed'           : [dips_path+'preprocessed/MC16d_hybrid-ext_odd_0_PFlow-preprocessed.h5', 'jets'],
}



# Just over 4.5M jets in these files:
fname_ftag = '/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/hybrid_with_tracks/preprocessed/PFlow-hybrid_70-test-preprocessed-file-1_5.h5'
fname_nom = '/unix/atlastracking/srettie/training_inputs_with_tracks/hybrid/nom/preprocessed/PFlow-hybrid_70-test-preprocessed-file-1_5.h5'
fname_nom_custom_ipxd = '/unix/atlastracking/srettie/training_inputs_custom_ipxd/hybrid/nom/preprocessed/PFlow-hybrid_70-nom_custom_ipxd-preprocessed-file-1_5.h5'
fname_nom_RFMVA_loose_custom_ipxd = '/unix/atlastracking/srettie/training_inputs_custom_ipxd/hybrid/nom_RFMVA_loose/preprocessed/PFlow-hybrid_70-nom_custom_ipxd-preprocessed-file-1_5.h5'
fname_nom_RFMVA_tight_custom_ipxd = '/unix/atlastracking/srettie/training_inputs_custom_ipxd/hybrid/nom_RFMVA_tight/preprocessed/PFlow-hybrid_70-nom_custom_ipxd-preprocessed-file-1_5.h5'

fname_ftag_raw = '/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/user.mguth.427081.btagTraining.e6928_e5984_s3126_r10201_r10210_p3985.EMPFlow.2020-02-15-T225316-R8334_output.h5/user.mguth.20595808._000002.output.h5'
fname_nom_raw = '/unix/atlastracking/srettie/training_inputs_with_tracks_test/zp_extended_nom-99_sub0.h5'

# ~2M jets in these files:
fname_ftag_validation_tt = '/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/hybrid_with_tracks/MC16d_hybrid_odd_100_PFlow-no_pTcuts-file_0.h5'
fname_ftag_validation_zp = '/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/hybrid_with_tracks/MC16d_hybrid-ext_odd_0_PFlow-no_pTcuts-file_0.h5'

# Validation in pT bins
ptbins_zp = ['0pT150','150pT400','400pT1000','1000pT1750','1750pT2750','2750pTInf']
ptbins_titles_zp = ['pT < 150 GeV','150 GeV < pT < 400 GeV','400 GeV < pT < 1 TeV', '1 TeV < pT < 1.75 TeV','1.75 TeV < pT < 2.75 TeV', 'pT > 2.75 TeV']
base_name_zp = '/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/hybrid_with_tracks/MC16d_hybrid-ext_odd_0_PFlow-{}-file_0.h5'
sample_id_zp = 'Z\''

ptbins_tt = ['0pT150','150pT250','250pTInf']
ptbins_titles_tt = ['pT < 150 GeV','150 GeV < pT < 250 GeV','pT > 250 GeV']
base_name_tt = '/unix/atlasvhbb2/srettie/tracking/FTAG_training_samples/hybrid_with_tracks/MC16d_hybrid_odd_100_PFlow-{}-file_0.h5'
sample_id_tt = 'ttbar'

ptbins = ptbins_zp
ptbins_titles = ptbins_titles_zp
base_name = base_name_zp
sample_id = sample_id_zp

f_names = [base_name.format(ptbin) for ptbin in ptbins]
    #fname_ftag_validation_tt,
    #fname_ftag_validation_zp,
    #fname_ftag,
    #fname_nom,
    #fname_nom_custom_ipxd,
    #fname_nom_RFMVA_loose_custom_ipxd,
    #fname_nom_RFMVA_tight_custom_ipxd,
#]
identifiers = [f'Official FTAG Validation ({sample_id}, {ptbin_title})' for ptbin_title in ptbins_titles]
    # 'Official FTAG Validation (ttbar)',
    # 'Official FTAG Validation (Z\')',
    # 'Official FTAG',
    # 'Nominal',
    # 'Nominal (custom IPxD)',
    # 'RFMVA (loose)',
    # 'RFMVA (tight)',
#]
colors = [
    '#1f77b4',
    '#ff7f0e',
    '#2ca02c',
    '#d62728',
    '#9467bd',
    '#8c564b',
    '#e377c2',
    '#7f7f7f',
    '#bcbd22',
    '#17becf',
    '#bcdd22',
    '#17decf'
]
variables = [
    'pt',
    'eta',
    'absEta_btagJes',
    # 'DL1r_pb',
    # 'DL1r_pc',
    # 'DL1r_pu',
    # 'DL1_pb',
    # 'DL1_pc',
    # 'DL1_pu',
    'pt_btagJes',
    'JetFitter_mass',
    'JetFitter_isDefaults',
    'JetFitter_energyFraction',
    'JetFitter_significance3d',
    'JetFitter_nVTX',
    'JetFitter_nSingleTracks',
    'JetFitter_nTracksAtVtx',
    'JetFitter_N2Tpair',
    'JetFitter_deltaR',
    'SV1_NGTinSvx',
    'SV1_masssvx',
    'SV1_isDefaults',
    'SV1_N2Tpair',
    'SV1_efracsvx',
    'SV1_deltaR',
    'SV1_Lxy',
    'SV1_L3d',
    'SV1_significance3d',
    'IP2D_bu',
    'IP2D_isDefaults',
    'IP2D_bc',
    'IP2D_cu',
    'IP3D_bu',
    'IP3D_isDefaults',
    'IP3D_bc',
    'IP3D_cu',
    'JetFitterSecondaryVertex_nTracks',
    'JetFitterSecondaryVertex_isDefaults',
    'JetFitterSecondaryVertex_mass',
    'JetFitterSecondaryVertex_energy',
    'JetFitterSecondaryVertex_energyFraction',
    'JetFitterSecondaryVertex_displacement3d',
    'JetFitterSecondaryVertex_displacement2d',
    'JetFitterSecondaryVertex_maximumTrackRelativeEta',
    'JetFitterSecondaryVertex_minimumTrackRelativeEta',
    'JetFitterSecondaryVertex_averageTrackRelativeEta',
    'maximumTrackRelativeEta',
    'minimumTrackRelativeEta',
    'averageTrackRelativeEta',
    # 'rnnip_pb',
    # 'rnnip_pc',
    # 'rnnip_pu',
    'HadronConeExclTruthLabelID',
    # 'category',
]
vars_tracks = [
    'chiSquared',
    'numberDoF',
    'IP3D_signed_d0',
    'IP2D_signed_d0',
    'IP3D_signed_z0',
    'IP3D_signed_d0_significance',
    'IP3D_signed_z0_significance',
    'btagIp_d0',
    'btagIp_z0SinTheta',
    'phi',
    'theta',
    'qOverP',
    'IP2D_grade',
    'IP3D_grade',
    'numberOfInnermostPixelLayerHits',
    'numberOfNextToInnermostPixelLayerHits',
    'numberOfInnermostPixelLayerSharedHits',
    'numberOfInnermostPixelLayerSplitHits',
    'numberOfPixelHits',
    'numberOfPixelHoles',
    'numberOfPixelSharedHits',
    'numberOfPixelSplitHits',
    'numberOfSCTHits',
    'numberOfSCTHoles',
    'numberOfSCTSharedHits',
    'pt',
    'eta',
    'deta',
    'dphi',
    'dr',
    'ptfrac'
]
# Get position of variable in numpy ndarray --> Ask Manuel for actual numbers!!
track_var_to_index = {
    'IP3D_signed_d0_significance'           : 0,
    'IP3D_signed_z0_significance'           : 1,
    'numberOfInnermostPixelLayerHits'       : 2,
    'numberOfNextToInnermostPixelLayerHits' : 3,
    'numberOfInnermostPixelLayerSharedHits' : 4,
    'numberOfInnermostPixelLayerSplitHits'  : 5,
    'numberOfPixelSharedHits'               : 6,
    'numberOfPixelSplitHits'                : 7,
    'numberOfSCTSharedHits'                 : 8,
    'ptfrac'                                : 9,
    'dr'                                    : 10,
    'numberOfPixelHits'                     : 11,
    'numberOfSCTHits'                       : 12,
}

selections = {
    'all'    : 'HadronConeExclTruthLabelID == 5 | HadronConeExclTruthLabelID != 5',
    'bjets'  : 'HadronConeExclTruthLabelID == 5',
    'cjets'  : 'HadronConeExclTruthLabelID == 4',
    'ujets'  : 'HadronConeExclTruthLabelID == 0',
    'highpt' : 'pt_btagJes > 400000',
}

binning = {
    'pt': 200,#np.linspace(10000, 2000000, 200),
    'absEta_btagJes': 200,#np.linspace(0, 2.5, 26),
    'HadronConeExclTruthLabelID': 20,#np.linspace(0,20,20)
}

def get_n_jets(f, dsname = 'jets', selection = ''):
    if dsname == 'jets':
        jets = pd.DataFrame(f['jets'][:])
        bjets = jets.query(selections['bjets'])
        cjets = jets.query(selections['cjets'])
        ujets = jets.query(selections['ujets'])
        if selection != '':
            bjets = bjets.query(selections[selection])
            cjets = cjets.query(selections[selection])
            ujets = ujets.query(selections[selection])
        nbjets,ncjets,nujets = len(bjets),len(cjets),len(ujets)
        njets = nbjets+ncjets+nujets
    if dsname == 'bcujets':
        bjets = pd.DataFrame(f['bjets'][:])
        cjets = pd.DataFrame(f['cjets'][:])
        ujets = pd.DataFrame(f['ujets'][:])
        if selection != '':
            bjets = bjets.query(selections[selection])
            cjets = cjets.query(selections[selection])
            ujets = ujets.query(selections[selection])
        nbjets,ncjets,nujets = len(bjets),len(cjets),len(ujets)
        njets = nbjets+ncjets+nujets
    if dsname == 'final':
        jets = f['X_train']
        njets,nbjets,ncjets,nujets = len(jets),-1,-1,-1
    return njets,nbjets,ncjets,nujets


def print_file_info(filename, f_id, selection = ''):
    n_jets,n_bjets,n_cjets,n_ujets = 0,0,0,0
    subfiles = glob.glob(filename)
    if verbose:
        print('All subfile names:')
        for fn in subfiles:
            print('\t{}'.format(fn))

    if len(subfiles) == 1:
        f = h5py.File(subfiles[0], 'r')
        if 'downsampled' in f_id:
            tmp_n_jets,tmp_n_bjets,tmp_n_cjets,tmp_n_ujets = get_n_jets(f, 'bcujets', selection)
        elif 'final' in f_id:
            tmp_n_jets,tmp_n_bjets,tmp_n_cjets,tmp_n_ujets = get_n_jets(f, 'final', selection)
        else:
            tmp_n_jets,tmp_n_bjets,tmp_n_cjets,tmp_n_ujets = get_n_jets(f, 'jets', selection)
        n_jets += tmp_n_jets
        n_bjets += tmp_n_bjets
        n_cjets += tmp_n_cjets
        n_ujets += tmp_n_ujets
    else:
        for fn in subfiles:
            f = h5py.File(fn, 'r')
            if 'downsampled' in f_id:
                tmp_n_jets,tmp_n_bjets,tmp_n_cjets,tmp_n_ujets = get_n_jets(f, 'bcujets', selection)
            elif 'final' in f_id:
                tmp_n_jets,tmp_n_bjets,tmp_n_cjets,tmp_n_ujets = get_n_jets(f, 'final', selection)
            else:
                tmp_n_jets,tmp_n_bjets,tmp_n_cjets,tmp_n_ujets = get_n_jets(f, 'jets', selection)
            n_jets += tmp_n_jets
            n_bjets += tmp_n_bjets
            n_cjets += tmp_n_cjets
            n_ujets += tmp_n_ujets
            if verbose:
                print('\tLooking at subfile: {}'.format(fn))
                print('\t\tNumber of jets: {}'.format(tmp_n_jets))
                print('\t\tNumber of bjets: {}'.format(tmp_n_bjets))
                print('\t\tNumber of cjets: {}'.format(tmp_n_cjets))
                print('\t\tNumber of ujets: {}'.format(tmp_n_ujets))

    print('Looking at file: {} {}'.format(f_id, selection))
    print('\tNumber of jets: {}'.format(n_jets))
    print('\tNumber of bjets: {}'.format(n_bjets))
    print('\tNumber of cjets: {}'.format(n_cjets))
    print('\tNumber of ujets: {}'.format(n_ujets))

def plot_arrays(arrays, nbins, labels, plot_name, x_title, title = ''):
    plt.hist(arrays,
             nbins,
             color=colors[:len(arrays)],
             label=labels, histtype='step',
             stacked=False, fill=False, density=True)
    plt.title(title)
    plt.xlabel(x_title)
    plt.ylabel('A.U.')
    min_initial, max_initial = plt.gca().get_ylim()[0],plt.gca().get_ylim()[1]
    # Start with log scale
    plt.yscale('log')
    plt.gca().set_ylim(top=1000*max_initial)
    plt.legend()
    print('Saving file: {}'.format(plot_name.replace('.pdf','_log.pdf')))
    plt.savefig(plot_name.replace('.pdf','_log.pdf'), transparent=True)
    # Save zoomed-in around ~1 TeV (i.e. set max to 2 TeV)
    if x_title == 'pt_btagJes':
        plt.gca().set_xlim(right=2000000)
        print('Saving file: {}'.format(plot_name.replace('.pdf','_xzoom.pdf')))
        plt.savefig(plot_name.replace('.pdf','_xzoom.pdf'), transparent=True)
    plt.close()


def compare_files(files, ids, sel, var, dataset = 'jets'):
    dfs = []
    for f in files:
        df = pd.DataFrame(f['jets'][:njets:nskip])
        df.index.name = 'jet_num'
        # Add tracks information to df
        if dataset in ['tracks','trks']:
            # Rename jet pt/eta columns to avoid pandas errors
            df.rename(columns={'pt': 'jet_pt', 'eta': 'jet_eta'}, inplace=True)
            if dataset == 'trks':
                tracks_ndarray = f['trks'][:njets:nskip][:,:,track_var_to_index[var]]
            elif dataset == 'tracks':
                tracks_ndarray = f['tracks'][:njets:nskip][var]
            else:
                print('Unrecognized dataset: {}'.format(dataset))
                sys.exit()
            # Convert numpy ndarray to df column
            # https://stackoverflow.com/questions/35525028/how-to-transform-a-3d-arrays-into-a-dataframe-in-python
            indices = pd.MultiIndex.from_product([range(d) for d in tracks_ndarray.shape], names=['jet_num', 'track_num'])
            df_tracks = pd.DataFrame(tracks_ndarray.flatten(), index=indices, columns=[var])
            df = df.join(df_tracks)
            
        if sel != 'all':
            df = df.query(selections[sel])
        dfs.append(df)

    # Plot stuff
    if debug:
        for df, identifier in zip(dfs, ids):
            print('\nAvailable columns for {} ({}):'.format(identifier,len(df.columns)))
            for c in df.columns: print(c)
            if verbose:
                # more options can be specified also
                with pd.option_context('display.max_rows', None, 'display.max_columns', None):
                    print('\n{}:\n'.format(identifier))
                    print(df[var])

    if dataset in ['tracks','trks']:
        plot_name = out_dir + '{}_{}_{}.pdf'.format(dataset,var,sel)
    else:
        plot_name = out_dir + '{}_{}.pdf'.format(var,sel)

    plt.hist([df[np.isfinite(df[var])][var] for df in dfs],
#    plt.hist([df[np.greater_equal(df[var],0)][var] for df in dfs],
             nbins,
             color=colors[:len(dfs)],
             label=ids, histtype='step',
             stacked=False, fill=False, density=True)
    plt.title(sel)
    plt.xlabel(var)
    plt.ylabel('A.U.')
    min_initial, max_initial = plt.gca().get_ylim()[0],plt.gca().get_ylim()[1]
    # Start with log scale
    plt.yscale('log')
    plt.gca().set_ylim(top=1000*max_initial)
    plt.legend()
    print('Saving file: {}'.format(plot_name.replace('.pdf','_log.pdf')))
    plt.savefig(plot_name.replace('.pdf','_log.pdf'), transparent=True)
    # Now save linear scale
    plt.yscale('linear')
    plt.gca().set_ylim([min_initial,1.5*max_initial])
    print('Saving file: {}'.format(plot_name))
    plt.savefig(plot_name, transparent=True)
    plt.close()


def get_bcu_arrays(filename, dsname, var):
    subfiles = glob.glob(filename)
    if len(subfiles) == 1:
        print('Filename: {}'.format(subfiles[0]))
    else:
        print('Subfiles:')
        for fn in subfiles:
            print('\t{}'.format(fn))
    for i, fn in enumerate(subfiles):
        print('i == {}: {}'.format(i,fn))
        f = h5py.File(fn, 'r')
        if i == 0:
            if dsname == 'bcujets':
                dfb = pd.DataFrame(f['bjets'][:njets:nskip])
                dfc = pd.DataFrame(f['cjets'][:njets:nskip])
                dfu = pd.DataFrame(f['ujets'][:njets:nskip])
            else:
                dfb = pd.DataFrame(f[dsname][:njets:nskip]).query(selections['bjets'])
                dfc = pd.DataFrame(f[dsname][:njets:nskip]).query(selections['cjets'])
                dfu = pd.DataFrame(f[dsname][:njets:nskip]).query(selections['ujets'])
        else:
            if dsname == 'bcujets':
                dfb = pd.concat([dfb, pd.DataFrame(f['bjets'][:njets:nskip])])
                dfc = pd.concat([dfc, pd.DataFrame(f['cjets'][:njets:nskip])])
                dfu = pd.concat([dfu, pd.DataFrame(f['ujets'][:njets:nskip])])
            else:
                dfb = pd.concat([dfb, pd.DataFrame(f[dsname][:njets:nskip]).query(selections['bjets'])])
                dfc = pd.concat([dfc, pd.DataFrame(f[dsname][:njets:nskip]).query(selections['cjets'])])
                dfu = pd.concat([dfu, pd.DataFrame(f[dsname][:njets:nskip]).query(selections['ujets'])])
    return dfb[var],dfc[var],dfu[var]

if __name__ == '__main__':
    if not os.path.exists(out_dir): os.mkdir(out_dir)
    t0 = dt.datetime.now()

    print('Analyzing files below:')
    for f_id, f_info in files_info.items():
        print('\t{} file: {} ({})'.format(f_id, f_info[0], f_info[1]))

    print('Printing file info:')
    for f_id, f_info in files_info.items():
        print('{} file: {} ({})'.format(f_id, f_info[0], f_info[1]))
        print_file_info(f_info[0], f_id)
        print_file_info(f_info[0], f_id, 'highpt')

    var = 'pt_btagJes'

    # Plot initial distributions
    arrays, labels = [], []
    for f_id, f_info in files_info.items():
        if f_id not in ['dips_train_tt_bjets_merged','dips_train_tt_cjets_merged','dips_train_tt_ujets_merged','dips_train_zp_merged']:
            continue
        print('Plotting: {}'.format(f_id))
        # Get relevant arrays
        fname, dsname = f_info[0], f_info[1]
        if '_tt_' in f_id:
            f = h5py.File(fname, 'r')
            df = pd.DataFrame(f[dsname][:njets:nskip])
            jet_type = f_id.split('jets')[0][-1]
            dfjets = df.query(selections['{}jets'.format(jet_type)])
            arr = dfjets[var]
            arrays.append(arr)
            labels.append('{}jets, '.format(jet_type)+r'$t\overline{t}$'+' ({})'.format(len(arr)))
        elif '_zp_' in f_id:
            arrb,arrc,arru = get_bcu_arrays(fname, dsname, var)
            arrays.append(arrb)
            arrays.append(arrc)
            arrays.append(arru)
            labels.append('bjets, Z\' ({})'.format(len(arrb)))
            labels.append('cjets, Z\' ({})'.format(len(arrc)))
            labels.append('ujets, Z\' ({})'.format(len(arru)))
    if len(arrays) > 0:
        plot_arrays(arrays, nbins, labels, out_dir+'initial_{}.pdf'.format(var), var, 'Initial distributions')

    # Plot downsampled distributions
    arrays, labels = [], []
    for f_id, f_info in files_info.items():
        if f_id not in ['dips_train_hybrid_downsampled']:
            continue
        print('Plotting: {}'.format(f_id))
        # Get relevant arrays
        fname, dsname = f_info[0], f_info[1]
        arrb,arrc,arru = get_bcu_arrays(fname, dsname, var)
        arrays.append(arrb)
        arrays.append(arrc)
        arrays.append(arru)
        labels.append('bjets, hybrid ({})'.format(len(arrb)))
        labels.append('cjets, hybrid ({})'.format(len(arrc)))
        labels.append('ujets, hybrid ({})'.format(len(arru)))
    if len(arrays) > 0:
        plot_arrays(arrays, nbins, labels, out_dir+'downsampled_{}.pdf'.format(var), var, 'Downsampled distributions')

    # Plot preprocessed distributions
    arrays, labels = [], []
    for f_id, f_info in files_info.items():
        if f_id not in ['dips_train_hybrid_preprocessed']:
            continue
        print('Plotting: {}'.format(f_id))
        # Get relevant arrays
        fname, dsname = f_info[0], f_info[1]
        arrb,arrc,arru = get_bcu_arrays(fname, dsname, var)
        arrays.append(arrb)
        arrays.append(arrc)
        arrays.append(arru)
        labels.append('bjets, hybrid ({})'.format(len(arrb)))
        labels.append('cjets, hybrid ({})'.format(len(arrc)))
        labels.append('ujets, hybrid ({})'.format(len(arru)))
    if len(arrays) > 0:
        plot_arrays(arrays, nbins, labels, out_dir+'preprocessed_{}.pdf'.format(var), var, 'Preprocessed distributions')

    # Plot validation distributions
    arrays, labels = [], []
    for f_id, f_info in files_info.items():
        if f_id not in ['dips_val_tt','dips_val_zp','dips_val_zp_highpt']:
            continue
        print('Plotting: {}'.format(f_id))
        # Get relevant arrays
        fname, dsname = f_info[0], f_info[1]
        arrb,arrc,arru = get_bcu_arrays(fname, dsname, var)
        arrays.append(arrb)
        arrays.append(arrc)
        arrays.append(arru)
        sample_type = ''
        if '_tt' in f_id:
            sample_type = r'$t\overline{t}$'
        if '_zp' in f_id:
            sample_type = 'Z\''
            if '_highpt' in f_id:
                sample_type = 'high-pT Z\''
        labels.append('bjets, {} ({})'.format(sample_type, len(arrb)))
        labels.append('cjets, {} ({})'.format(sample_type, len(arrc)))
        labels.append('ujets, {} ({})'.format(sample_type, len(arru)))
    if len(arrays) > 0:
        plot_arrays(arrays, nbins, labels, out_dir+'validation_{}.pdf'.format(var), var, 'Validation distributions')

    # Plot test distributions
    arrays, labels = [], []
    for f_id, f_info in files_info.items():
        if f_id not in ['dips_test_tt','dips_test_zp','dips_test_zp_highpt','dips_test_zp_preprocessed_full','dips_test_zp_preprocessed_noshuffle','dips_test_zp_preprocessed', 'gnn_test_zp', 'gnn_test_zp_pseudo', 'gnn_test_zp_sanity', 'gnn_test_zp_pseudo_sanity']:
            continue
        print('Plotting: {}'.format(f_id))
        # Get relevant arrays
        fname, dsname = f_info[0], f_info[1]
        arrb,arrc,arru = get_bcu_arrays(fname, dsname, var)
        arrays.append(arrb)
        arrays.append(arrc)
        arrays.append(arru)
        sample_type = ''
        if '_tt' in f_id:
            sample_type = r'$t\overline{t}$'
        if '_zp' in f_id:
            sample_type = 'Z\''
            if '_highpt' in f_id:
                sample_type = 'high-pT Z\''
            if 'pseudo' in f_id:
                sample_type = 'Z\' pseudo'
            if 'sanity' in f_id:
                sample_type = 'Z\' sanity'
            if 'pseudo_sanity' in f_id:
                sample_type = 'Z\' pseudo sanity'
            if f_id == 'gnn_test_zp':
                sample_type = 'zp'
            if f_id == 'gnn_test_zp_pseudo':
                sample_type = 'zp_pseudo'
            if f_id == 'gnn_test_zp_sanity':
                sample_type = 'zp_sanity'
            if f_id == 'gnn_test_zp_pseudo_sanity':
                sample_type = 'zp_pseudo_sanity'
        labels.append('bjets, {} ({})'.format(sample_type, len(arrb)))
        labels.append('cjets, {} ({})'.format(sample_type, len(arrc)))
        labels.append('ujets, {} ({})'.format(sample_type, len(arru)))
    if len(arrays) > 0:
        plot_arrays(arrays, nbins, labels, out_dir+'test_{}.pdf'.format(var), var, 'Test distributions')

    # for v in variables:
    #     for s in selections:
    #         print('Comparing: {}, {}'.format(s,v))
    #         ti = dt.datetime.now()
    #         compare_files(files, identifiers, s, v)
    #         print('\t-->{}'.format(dt.datetime.now()-ti))
    #         # Flush to log file to check things while no-hupped
    #         sys.stdout.flush()

    # for v in track_var_to_index.keys():
    #     for s in selections:
    #         print('Comparing trks: {}, {}'.format(s,v))
    #         ti = dt.datetime.now()
    #         compare_files(files, identifiers, s, v, dataset = 'trks')
    #         print('\t-->{}'.format(dt.datetime.now()-ti))
    #         # Flush to log file to check things while no-hupped
    #         sys.stdout.flush()

    # # Only use every 10 events for track quantities to avoid memory issues
    # nskip = 1000
    # # Look at original .h5 (no preprocessing) here
    # for v in vars_tracks:
    #     for s in selections:
    #         print('Comparing tracks: {}, {}'.format(s,v))
    #         ti = dt.datetime.now()
    #         compare_files(files, identifiers, s, v, dataset = 'tracks')
    #         print('\t-->{}'.format(dt.datetime.now()-ti))
    #         # Flush to log file to check things while no-hupped
    #         sys.stdout.flush()

    print('Done comparing {} .h5 files in {}!'.format(len(files_info), dt.datetime.now()-t0))
