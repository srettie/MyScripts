import os
import sys
import ROOT
import math

debug = False
verbose = False

do_fast = True
do_tables = False

in_dir = '/unix/atlasvhbb2/srettie/truthHists_resolved_newMaps/'
out_dir = '/home/srettie/truthPlots/'
all_hists = in_dir+'all_hists.root'
# For the pTV variable, need to add all the pTV binned plots together...
variables = ['mBB','pTV']
channels = [
    '0Lep',# ZbbZvv, ZvvWqq
    '1Lep',# ZbbWlv, WlvWqq
    '2Lep',# ZbbZll, ZllWqq
]
regions = ['SR','CRHigh','CRLow']
jets = ['2jet', '3jet']
pt_ranges = ['75_150', '150_250','250']#['250']#['150_250']#['75_150']#['250']#
systematics = ['scale','qsf','pp8','pdf','ckkw','ueps']
reference_systs = ['VVMbbME','VVPTVME','VVMbbPSUE','VVPTVPSUE']
processes = ['WW','WZ','ZZ']
levels = ['all']#, 'split_jets', 'split_SRCR']

colors = [
    ROOT.kBlack,
    ROOT.kBlue,
    ROOT.kRed,
    ROOT.kGreen+1,
    ROOT.kYellow+1,
    ROOT.kViolet,
    ROOT.kOrange,
    ROOT.kGray,
    ROOT.kMagenta,
]

ranges = {
    'mBB' : [0,500],
    'pTV' : [0,500],
    '0Lep_SR_mBB' : [0, 250],
    '0Lep_CRLow_mBB' : [0, 150],
    '0Lep_CRHigh_mBB' : [0, 500],
    '0Lep_SR_MET' : [100, 600],
    '0Lep_CRLow_MET' : [100, 600],
    '0Lep_CRHigh_MET' : [100, 600],
    
    '1Lep_SR_mBB' : [0, 250],
    '1Lep_CRLow_mBB' : [0, 150],
    '1Lep_CRHigh_mBB' : [0, 600],
    '1Lep_SR_pTV' : [0, 500],
    '1Lep_CRLow_pTV' : [0, 500],
    '1Lep_CRHigh_pTV' : [0, 500],
    
    '2Lep_SR_mBB' : [0, 250],
    '2Lep_CRLow_mBB' : [0, 150],
    '2Lep_CRHigh_mBB' : [0, 500],
    '2Lep_SR_pTV' : [0, 500],
    '2Lep_CRLow_pTV' : [0, 500],
    '2Lep_CRHigh_pTV' : [0, 500],
}

rebins = {
    'mBB' : 20,
    'pTV' : 10,
    '0Lep_SR_mBB' : 2,
    '0Lep_CRLow_mBB' : 2,
    '0Lep_CRHigh_mBB' : 5,
    '0Lep_SR_MET' : 5,
    '0Lep_CRLow_MET' : 5,
    '0Lep_CRHigh_MET' : 5,
    
    '1Lep_SR_mBB' : 10,
    '1Lep_CRLow_mBB' : 10,
    '1Lep_CRHigh_mBB' : 20,
    '1Lep_SR_pTV' : 20,
    '1Lep_CRLow_pTV' : 20,
    '1Lep_CRHigh_pTV' : 20,
    
    '2Lep_SR_mBB' : 2,
    '2Lep_CRLow_mBB' : 2,
    '2Lep_CRHigh_mBB' : 10,
    '2Lep_SR_pTV' : 4,
    '2Lep_CRLow_pTV' : 4,
    '2Lep_CRHigh_pTV' : 4,
}

# Use ATLAS Style
ROOT.gROOT.SetBatch(1)
ROOT.gROOT.LoadMacro('/home/srettie/atlasrootstyle/AtlasStyle.C')
ROOT.gROOT.LoadMacro('/home/srettie/atlasrootstyle/AtlasLabels.C')
ROOT.gROOT.LoadMacro('/home/srettie/atlasrootstyle/AtlasUtils.C')
ROOT.SetAtlasStyle()

# ######
# # WW
# ######
# -->363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv
# -->363360.Sherpa_221_NNPDF30NNLO_WplvWmqq

# # Alternative PowhegPythia8
# 361606.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvqq

# # Alternative Sherpa
# 366338.Sh_221_NN30NNLO_WpqqWmlv_CKKW15
# 366339.Sh_221_NN30NNLO_WpqqWmlv_CKKW30
# 366340.Sh_221_NN30NNLO_WpqqWmlv_QSF025
# 366341.Sh_221_NN30NNLO_WpqqWmlv_QSF4

# 366342.Sh_221_NN30NNLO_WplvWmqq_CKKW15
# 366343.Sh_221_NN30NNLO_WplvWmqq_CKKW30
# 366344.Sh_221_NN30NNLO_WplvWmqq_QSF025
# 366345.Sh_221_NN30NNLO_WplvWmqq_QSF4

# ######
# # WZ
# ######
# -->363357.Sherpa_221_NNPDF30NNLO_WqqZvv
# -->363358.Sherpa_221_NNPDF30NNLO_WqqZll
# -->363489.Sherpa_221_NNPDF30NNLO_WlvZqq

# # Alternative PowhegPythia8
# 361607.PowhegPy8EG_AZNLOCTEQ6L1_WZqqll_mll20
# 361608.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqvv
# 361609.PowhegPy8EG_AZNLOCTEQ6L1_WZlvqq_mqq20

# # Alternative Sherpa
# 366326.Sh_221_NN30NNLO_WqqZvv_CKKW15
# 366327.Sh_221_NN30NNLO_WqqZvv_CKKW30
# 366328.Sh_221_NN30NNLO_WqqZvv_QSF025
# 366329.Sh_221_NN30NNLO_WqqZvv_QSF4

# 366330.Sh_221_NN30NNLO_WqqZll_CKKW15
# 366331.Sh_221_NN30NNLO_WqqZll_CKKW30
# 366332.Sh_221_NN30NNLO_WqqZll_QSF025
# 366333.Sh_221_NN30NNLO_WqqZll_QSF4

# 366334.Sh_221_NN30NNLO_WlvZqq_CKKW15
# 366335.Sh_221_NN30NNLO_WlvZqq_CKKW30
# 366336.Sh_221_NN30NNLO_WlvZqq_QSF025
# 366337.Sh_221_NN30NNLO_WlvZqq_QSF4

# ######
# # ZZ
# ######
# -->363355.Sherpa_221_NNPDF30NNLO_ZqqZvv
# -->363356.Sherpa_221_NNPDF30NNLO_ZqqZll

# # Alternative PowhegPythia8
# 361610.PowhegPy8EG_AZNLOCTEQ6L1_ZZqqll_mqq20mll20
# 361611.PowhegPy8EG_AZNLOCTEQ6L1_ZZvvqq_mqq20

# # Alternative Sherpa
# 366318.Sh_221_NN30NNLO_ZqqZll_CKKW15
# 366319.Sh_221_NN30NNLO_ZqqZll_CKKW30
# 366320.Sh_221_NN30NNLO_ZqqZll_QSF025
# 366321.Sh_221_NN30NNLO_ZqqZll_QSF4

# 366322.Sh_221_NN30NNLO_ZqqZvv_CKKW15
# 366323.Sh_221_NN30NNLO_ZqqZvv_CKKW30
# 366324.Sh_221_NN30NNLO_ZqqZvv_QSF025
# 366325.Sh_221_NN30NNLO_ZqqZvv_QSF4

nominal_samples = {
    # Sherpa 2.2.1 with NNPDF3.0
    # 0L
    # '363355.Sherpa_221_NNPDF30NNLO_ZqqZvv'   : in_dir + 'output_XLep_363355.root',
    # '363357.Sherpa_221_NNPDF30NNLO_WqqZvv'   : in_dir + 'output_XLep_363357.root',
    # # 1L
    # '363489.Sherpa_221_NNPDF30NNLO_WlvZqq'   : in_dir + 'output_XLep_363489.root',
    # '363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv' : in_dir + 'output_XLep_363359.root',
    # '363360.Sherpa_221_NNPDF30NNLO_WplvWmqq' : in_dir + 'output_XLep_363360.root',
    # # 2L
    # '363356.Sherpa_221_NNPDF30NNLO_ZqqZll'   : in_dir + 'output_XLep_363356.root',
    # '363358.Sherpa_221_NNPDF30NNLO_WqqZll'   : in_dir + 'output_XLep_363358.root',

    '363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv' : in_dir + 'output_XLep_363359.root',
    '363360.Sherpa_221_NNPDF30NNLO_WplvWmqq' : in_dir + 'output_XLep_363360.root',
    
    '363358.Sherpa_221_NNPDF30NNLO_WqqZll'   : in_dir + 'output_XLep_363358.root',
    '363357.Sherpa_221_NNPDF30NNLO_WqqZvv'   : in_dir + 'output_XLep_363357.root',
    '363489.Sherpa_221_NNPDF30NNLO_WlvZqq'   : in_dir + 'output_XLep_363489.root',

    '363356.Sherpa_221_NNPDF30NNLO_ZqqZll'   : in_dir + 'output_XLep_363356.root',
    '363355.Sherpa_221_NNPDF30NNLO_ZqqZvv'   : in_dir + 'output_XLep_363355.root',

   
}

variation_samples_ckkw = {
    '366338.Sh_221_NN30NNLO_WpqqWmlv_CKKW15' : in_dir + 'output_XLep_366338.root',
    '366339.Sh_221_NN30NNLO_WpqqWmlv_CKKW30' : in_dir + 'output_XLep_366339.root',
    '366342.Sh_221_NN30NNLO_WplvWmqq_CKKW15' : in_dir + 'output_XLep_366342.root',
    '366343.Sh_221_NN30NNLO_WplvWmqq_CKKW30' : in_dir + 'output_XLep_366343.root',
    
    '366326.Sh_221_NN30NNLO_WqqZvv_CKKW15'   : in_dir + 'output_XLep_366326.root',
    '366327.Sh_221_NN30NNLO_WqqZvv_CKKW30'   : in_dir + 'output_XLep_366327.root',
    '366330.Sh_221_NN30NNLO_WqqZll_CKKW15'   : in_dir + 'output_XLep_366330.root',
    '366331.Sh_221_NN30NNLO_WqqZll_CKKW30'   : in_dir + 'output_XLep_366331.root',
    '366334.Sh_221_NN30NNLO_WlvZqq_CKKW15'   : in_dir + 'output_XLep_366334.root',
    '366335.Sh_221_NN30NNLO_WlvZqq_CKKW30'   : in_dir + 'output_XLep_366335.root',
    
    '366318.Sh_221_NN30NNLO_ZqqZll_CKKW15'   : in_dir + 'output_XLep_366318.root',
    '366319.Sh_221_NN30NNLO_ZqqZll_CKKW30'   : in_dir + 'output_XLep_366319.root',
    '366322.Sh_221_NN30NNLO_ZqqZvv_CKKW15'   : in_dir + 'output_XLep_366322.root',
    '366323.Sh_221_NN30NNLO_ZqqZvv_CKKW30'   : in_dir + 'output_XLep_366323.root',
}

variation_samples_qsf = {
    '366340.Sh_221_NN30NNLO_WpqqWmlv_QSF025' : in_dir + 'output_XLep_366340.root',
    '366341.Sh_221_NN30NNLO_WpqqWmlv_QSF4'   : in_dir + 'output_XLep_366341.root',
    '366344.Sh_221_NN30NNLO_WplvWmqq_QSF025' : in_dir + 'output_XLep_366344.root',
    '366345.Sh_221_NN30NNLO_WplvWmqq_QSF4'   : in_dir + 'output_XLep_366345.root',

    '366328.Sh_221_NN30NNLO_WqqZvv_QSF025'   : in_dir + 'output_XLep_366328.root',
    '366329.Sh_221_NN30NNLO_WqqZvv_QSF4'     : in_dir + 'output_XLep_366329.root',
    '366332.Sh_221_NN30NNLO_WqqZll_QSF025'   : in_dir + 'output_XLep_366332.root',
    '366333.Sh_221_NN30NNLO_WqqZll_QSF4'     : in_dir + 'output_XLep_366333.root',
    '366336.Sh_221_NN30NNLO_WlvZqq_QSF025'   : in_dir + 'output_XLep_366336.root',
    '366337.Sh_221_NN30NNLO_WlvZqq_QSF4'     : in_dir + 'output_XLep_366337.root',

    '366320.Sh_221_NN30NNLO_ZqqZll_QSF025'   : in_dir + 'output_XLep_366320.root',
    '366321.Sh_221_NN30NNLO_ZqqZll_QSF4'     : in_dir + 'output_XLep_366321.root',
    '366324.Sh_221_NN30NNLO_ZqqZvv_QSF025'   : in_dir + 'output_XLep_366324.root',
    '366325.Sh_221_NN30NNLO_ZqqZvv_QSF4'     : in_dir + 'output_XLep_366325.root',
}

variation_samples_pp8 = {
    '361606.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvqq'  : in_dir + 'output_XLep_361606.root',
    
    '361607.PowhegPy8EG_AZNLOCTEQ6L1_WZqqll_mll20'      : in_dir + 'output_XLep_361607.root',
    '361608.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqvv'  : in_dir + 'output_XLep_361608.root',
    '361609.PowhegPy8EG_AZNLOCTEQ6L1_WZlvqq_mqq20'      : in_dir + 'output_XLep_361609.root',
    
    '361610.PowhegPy8EG_AZNLOCTEQ6L1_ZZqqll_mqq20mll20' : in_dir + 'output_XLep_361610.root',
    '361611.PowhegPy8EG_AZNLOCTEQ6L1_ZZvvqq_mqq20'      : in_dir + 'output_XLep_361611.root',
}

variation_samples_herwig = {
    '361592.PowhegHerwigppEG_AZNLOCTEQ6L1_WWlvqq'  : in_dir + 'output_XLep_361592.root',

    '361593.PowhegHerwigppEG_WZqqll_mll20'         : in_dir + 'output_XLep_361593.root',
    '361594.PowhegHerwigppEG_AZNLOCTEQ6L1_WZqqvv'  : in_dir + 'output_XLep_361594.root',
    '361595.PowhegHerwigppEG_WZlvqq_mqq20'         : in_dir + 'output_XLep_361595.root',
    
    '361596.PowhegHerwigppEG_ZZqqll_mqq20mll20'    : in_dir + 'output_XLep_361596.root',
    '361597.PowhegHerwigppEG_ZZvvqq_mqq20'         : in_dir + 'output_XLep_361597.root',
}

all_samples = {}
all_samples.update(nominal_samples)
all_samples.update(variation_samples_ckkw)
all_samples.update(variation_samples_qsf)
all_samples.update(variation_samples_pp8)
all_samples.update(variation_samples_herwig)
    
dsid_to_process = {
    # Nominal
    '363355' : 'ZZ',
    '363357' : 'WZ',
    '363489' : 'WZ',
    '363359' : 'WW',
    '363360' : 'WW',
    '363356' : 'ZZ',
    '363358' : 'WZ',
    # Variations
    '366338' : 'WW',
    '366339' : 'WW',
    '366342' : 'WW',
    '366343' : 'WW',
    '366326' : 'WZ',
    '366327' : 'WZ',
    '366330' : 'WZ',
    '366331' : 'WZ',
    '366334' : 'WZ',
    '366335' : 'WZ',
    '366318' : 'ZZ',
    '366319' : 'ZZ',
    '366322' : 'ZZ',
    '366323' : 'ZZ',
    '366340' : 'WW',
    '366341' : 'WW',
    '366344' : 'WW',
    '366345' : 'WW',
    '366328' : 'WZ',
    '366329' : 'WZ',
    '366332' : 'WZ',
    '366333' : 'WZ',
    '366336' : 'WZ',
    '366337' : 'WZ',
    '366320' : 'ZZ',
    '366321' : 'ZZ',
    '366324' : 'ZZ',
    '366325' : 'ZZ',
    '361606' : 'WW',
    '361607' : 'WZ',
    '361608' : 'WZ',
    '361609' : 'WZ',
    '361610' : 'ZZ',
    '361611' : 'ZZ',
    '361592' : 'WW',
    '361593' : 'WZ',
    '361594' : 'WZ',
    '361595' : 'WZ',
    '361596' : 'ZZ',
    '361597' : 'ZZ',
}

dsid_to_name = {
    # Nominal
    '363355' : 'Sherpa_221_NNPDF30NNLO_ZqqZvv',
    '363357' : 'Sherpa_221_NNPDF30NNLO_WqqZvv',
    '363489' : 'Sherpa_221_NNPDF30NNLO_WlvZqq',
    '363359' : 'Sherpa_221_NNPDF30NNLO_WpqqWmlv',
    '363360' : 'Sherpa_221_NNPDF30NNLO_WplvWmqq',
    '363356' : 'Sherpa_221_NNPDF30NNLO_ZqqZll',
    '363358' : 'Sherpa_221_NNPDF30NNLO_WqqZll',
    # Variations
    '366338' : 'Sh_221_NN30NNLO_WpqqWmlv_CKKW15',
    '366339' : 'Sh_221_NN30NNLO_WpqqWmlv_CKKW30',
    '366342' : 'Sh_221_NN30NNLO_WplvWmqq_CKKW15',
    '366343' : 'Sh_221_NN30NNLO_WplvWmqq_CKKW30',
    '366326' : 'Sh_221_NN30NNLO_WqqZvv_CKKW15',
    '366327' : 'Sh_221_NN30NNLO_WqqZvv_CKKW30',
    '366330' : 'Sh_221_NN30NNLO_WqqZll_CKKW15',
    '366331' : 'Sh_221_NN30NNLO_WqqZll_CKKW30',
    '366334' : 'Sh_221_NN30NNLO_WlvZqq_CKKW15',
    '366335' : 'Sh_221_NN30NNLO_WlvZqq_CKKW30',
    '366318' : 'Sh_221_NN30NNLO_ZqqZll_CKKW15',
    '366319' : 'Sh_221_NN30NNLO_ZqqZll_CKKW30',
    '366322' : 'Sh_221_NN30NNLO_ZqqZvv_CKKW15',
    '366323' : 'Sh_221_NN30NNLO_ZqqZvv_CKKW30',
    '366340' : 'Sh_221_NN30NNLO_WpqqWmlv_QSF025',
    '366341' : 'Sh_221_NN30NNLO_WpqqWmlv_QSF4',
    '366344' : 'Sh_221_NN30NNLO_WplvWmqq_QSF025',
    '366345' : 'Sh_221_NN30NNLO_WplvWmqq_QSF4',
    '366328' : 'Sh_221_NN30NNLO_WqqZvv_QSF025',
    '366329' : 'Sh_221_NN30NNLO_WqqZvv_QSF4',
    '366332' : 'Sh_221_NN30NNLO_WqqZll_QSF025',
    '366333' : 'Sh_221_NN30NNLO_WqqZll_QSF4',
    '366336' : 'Sh_221_NN30NNLO_WlvZqq_QSF025',
    '366337' : 'Sh_221_NN30NNLO_WlvZqq_QSF4',
    '366320' : 'Sh_221_NN30NNLO_ZqqZll_QSF025',
    '366321' : 'Sh_221_NN30NNLO_ZqqZll_QSF4',
    '366324' : 'Sh_221_NN30NNLO_ZqqZvv_QSF025',
    '366325' : 'Sh_221_NN30NNLO_ZqqZvv_QSF4',
    '361606' : 'PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvqq',
    '361607' : 'PowhegPy8EG_AZNLOCTEQ6L1_WZqqll_mll20',
    '361608' : 'PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqvv',
    '361609' : 'PowhegPy8EG_AZNLOCTEQ6L1_WZlvqq_mqq20',
    '361610' : 'PowhegPy8EG_AZNLOCTEQ6L1_ZZqqll_mqq20mll20',
    '361611' : 'PowhegPy8EG_AZNLOCTEQ6L1_ZZvvqq_mqq20',
    '361592' : 'PowhegHerwigppEG_AZNLOCTEQ6L1_WWlvqq',
    '361593' : 'PowhegHerwigppEG_WZqqll_mll20',
    '361594' : 'PowhegHerwigppEG_AZNLOCTEQ6L1_WZqqvv',
    '361595' : 'PowhegHerwigppEG_WZlvqq_mqq20',
    '361596' : 'PowhegHerwigppEG_ZZqqll_mqq20mll20',
    '361597' : 'PowhegHerwigppEG_ZZvvqq_mqq20',
}

#######################################################################################
# plot_key: hist_[X]Lep_[WW/WZ/ZZ/VV]_2tag[Y]jet_[pT_region]_[SR/CRLow/CRHigh]_[pTV/mBB/BDT]
# h_name  : [DSID]_[plot_key]_[sys_or_nom]
#######################################################################################

# Master list of all histogram names we need
needed_hists = []

def get_all(d, basepath = ''):
    for key in d.GetListOfKeys():
        k_name = key.GetName()
        if key.IsFolder():
            for i in get_all(d.Get(k_name), basepath + k_name + '/'):
                yield i
        else:
            yield basepath + k_name, d.Get(k_name)

def get_file_dsid(filename):
    return filename.split('/')[-1].split('Lep_')[-1].strip('.root')

def get_hist_dsid(h_name):
    return h_name[0:6]

def get_process(plot_key):
    if '_WW_' in plot_key:
        return 'WW'
    if '_WZ_' in plot_key:
        return 'WZ'
    if '_ZZ_' in plot_key:
        return 'ZZ'

def is_used_region(region, channel):
    if debug:
        print('Checking region: '+region)
    if '75_150ptv' in region and channel == '0Lep':
        return False
    return True

def is_WW_sample(dsid):
    if dsid_to_process[dsid] == 'WW':
        return True
    return False
def is_WZ_sample(dsid):
    if dsid_to_process[dsid] == 'WZ':
        return True
    return False
def is_ZZ_sample(dsid):
    if dsid_to_process[dsid] == 'ZZ':
        return True
    return False

def is_bad_hist(h_name):
    # Some histograms have no events, so skip requiring them
    dsid = get_hist_dsid(h_name)

    no_events_1L = ['363357','366326','366327','366329','366328','363355','366322','366323','366324','366325','361594','361597']
    no_events_2L = ['363355','361611','366325','366324','366323','366322','363357','361608','366326','366327','366329','366328','361597','361594']

    missing_0L = {
        '361593' : ['2tag3jet_250ptv_CRLow'],
    }
    missing_1L = {
        '361608' : ['2tag2jet_150_250ptv','2tag2jet_250ptv_SR','2tag2jet_250ptv_CRLow','2tag3jet_150_250ptv','2tag3jet_250ptv','2tag3jet_75_150ptv_SR','2tag3jet_75_150ptv_CRLow'],
        '361611' : ['2tag2jet_150_250ptv','2tag2jet_250ptv','2tag3jet_150_250ptv_SR','2tag3jet_150_250ptv_CRLow','2tag3jet_250ptv_SR','2tag3jet_250ptv_CRLow','2tag3jet_75_150ptv_CR'],
    }
    missing_2L = {
        '361606' : ['2tag2jet_250ptv','2tag3jet_150_250ptv_CRLow','2tag3jet_250ptv_CRLow'],
        '361609' : ['2tag2jet_250ptv','2tag3jet_250ptv_SR'],
        '363359' : ['2tag2jet_150_250ptv_SR','2tag2jet_150_250ptv_CRLow','2tag2jet_250ptv_SR','2tag2jet_250ptv_CRLow','2tag3jet_250ptv_CRLow'],
        '363360' : ['2tag2jet_250ptv','2tag2jet_150_250ptv_SR','2tag2jet_150_250ptv_CRLow','2tag3jet_250ptv_CR'],
        '363489' : ['2tag2jet_250ptv_SR','2tag2jet_250ptv_CRLow'],    
        '366334' : ['2tag2jet_250ptv_CR'],
        '366336' : ['2tag2jet_250ptv_CRLow','2tag2jet_250ptv_SR'],
        '366337' : ['2tag3jet_250ptv_CRLow','2tag3jet_250ptv_SR','2tag2jet_250ptv_CRLow','2tag2jet_250ptv_SR','2tag3jet_150_250ptv_CRLow'],
        '366338' : ['2tag3jet_250ptv_CRLow','2tag2jet_250ptv_CR','2tag2jet_75_150ptv_CRHigh','2tag2jet_150_250ptv_SR','2tag2jet_150_250ptv_CRHigh'],
        '366339' : ['2tag3jet_250ptv_CRLow','2tag2jet_250ptv_CRLow'],
        '366340' : ['2tag3jet_250ptv_CRLow','2tag3jet_250ptv_SR','2tag2jet_250ptv_CRLow','2tag2jet_250ptv_SR','2tag2jet_150_250ptv_CRHigh','2tag2jet_150_250ptv_SR'],
        '366341' : ['2tag2jet_250ptv','2tag2jet_150_250ptv_SR','2tag3jet_250ptv_SR','2tag3jet_250ptv_CRLow'],
        '366342' : ['2tag2jet_150_250ptv','2tag2jet_250ptv_CR','2tag2jet_75_150ptv_CR','2tag3jet_150_250ptv_CR','2tag3jet_250ptv_CRLow'],
        '366343' : ['2tag2jet_250ptv','2tag2jet_150_250ptv_CRLow','2tag3jet_250ptv_CRLow'],
        '366344' : ['2tag2jet_250ptv','2tag2jet_150_250ptv_CRLow','2tag3jet_250ptv','2tag3jet_150_250ptv_CR'],
        '366345' : ['2tag2jet_250ptv','2tag2jet_150_250ptv_SR','2tag3jet_150_250ptv_CRLow','2tag3jet_250ptv_SR','2tag3jet_250ptv_CRLow'],
        '361592' : ['2tag2jet','2tag3jet_75_150ptv_SR','2tag3jet_75_150ptv_CRLow','2tag3jet_150_250ptv_CR','2tag3jet_250ptv'],
        '361595' : ['2tag2jet_250ptv','2tag2jet_150_250ptv','2tag2jet_75_150ptv_CRLow','2tag3jet_250ptv_CR','2tag3jet_75_150ptv_CRLow'],
    }

    # No events in samples
    if '1Lep' in h_name and dsid in no_events_1L:
        return True
    if '2Lep' in h_name and dsid in no_events_2L:
        return True

    # Note: there is a 150 GeV MET cut in the 0L channel
    if '0Lep' in h_name and '75_150ptv' in h_name:
        return True

    # Missing histograms in files
    if '0Lep' in h_name and dsid in missing_0L:
        for missing in missing_0L[dsid]:
            if missing in h_name:
                return True

    if '1Lep' in h_name and dsid in missing_1L:
        for missing in missing_1L[dsid]:
            if missing in h_name:
                return True
        
    if '2Lep' in h_name and dsid in missing_2L:
        for missing in missing_2L[dsid]:
            if missing in h_name:
                return True

    # For some reason these don't have the ICHEP variations...
    if '_CS' in h_name:
        if dsid == '363359' and '0Lep' in h_name:
            return True
        if dsid == '363360' and '0Lep' in h_name:
            return True
        if dsid == '363356' and '1Lep' in h_name:
            return True
        if dsid == '363359' and '1Lep' in h_name:
            return True
        if dsid == '363360' and '1Lep' in h_name:
            return True
        if dsid == '363359' and '2Lep' in h_name:
            return True
        if dsid == '363360' and '2Lep' in h_name:
            return True

    
    return False


def get_histograms(filenames, do_fast = False):
    if debug:
        print('Getting histograms...')
    
    hists = {}
    if do_fast:
        f = ROOT.TFile.Open(all_hists)
        for k, o in get_all(f):
            h_name = o.GetName()
            h_class = o.ClassName()
            if debug and verbose:
                print('GOOD ' + h_class + ': ' + h_name + ' (' + k + ')')
            hists[h_name] = f.Get(k).Clone()
            hists[h_name].SetDirectory(0)

    else:
        # Save all relevant histograms to convenient file
        f_all = ROOT.TFile.Open(all_hists,'RECREATE')
        for f_name in filenames:
            if debug:
                print('Opening file: ' + f_name)

            # Get file
            f = ROOT.TFile.Open(f_name)
            dsid = get_file_dsid(f_name)

            # Get histograms
            for k, o in get_all(f):

                h_name = o.GetName()
                h_class = o.ClassName()

                if debug and verbose:
                    print('Found ' + h_class + ': ' + h_name)

                # Only keep relevant histograms
                if (dsid + '_' + h_name) in needed_hists:
                    if debug:
                        print('GOOD ' + h_class + ': ' + dsid + '_' + h_name + ' (' + k + ')')
                    hists[dsid + '_' + h_name] = f.Get(k).Clone()
                    hists[dsid + '_' + h_name].SetDirectory(0)

                    # Save to master file for fast use
                    f_all.cd()
                    hists[dsid + '_' + h_name].Clone(dsid + '_' + h_name).Write()
                    f.cd()

            # Close file
            f.Close()
        # Close master file for fast use
        f_all.Close()
    # Check that all required histograms are found
    hists_missing = False
    for h_needed in needed_hists:
        if h_needed not in hists and not is_bad_hist(h_needed):
            print('ERROR!! HISTOGRAM NOT FOUND: ' + h_needed)
            hists_missing = True
    if hists_missing:
        return None
    
    # Return all relevant histograms
    return hists

def get_plot_key(channel, process, njets, ptrange, variable, region):
    return 'hist_' + channel + '_' + process + '_2tag' + njets + '_' + ptrange + 'ptv_' + region + '_' + variable

def get_hist_name(plot_key, dsid, sys_or_nom):
    return dsid + '_' + plot_key + '_' + sys_or_nom

def draw_labels(plot_key, variation):
    # Draw ATLAS label and luminosity label
    labelx = 0.13
    labely = 0.85
    offset = 0.06
    ROOT.ATLASLabel(labelx, labely, 'Internal')
    ROOT.myText(labelx, labely-offset, 1, '#sqrt{s} = 13 TeV')

    # Extra labels for selection
    extraLabel = ''
    if 'pTV' in plot_key:
        extraLabel += plot_key.replace('hist_','').replace('_pTV','').replace('_75_150ptv','')
    elif 'MET' in plot_key:
        extraLabel += plot_key.replace('hist_','').replace('_MET','').replace('_150_250ptv','')
    else:
        extraLabel += plot_key.replace('hist_','').replace('_mBB','').replace('_pTV','')
    #if inclusive_ptv:
    #    extraLabel = extraLabel.replace('_75_150ptv','_inc_ptv').replace('_150_250ptv','_inc_ptv')
    if extraLabel != '':
        ROOT.myText(labelx, labely-2*offset, 1, extraLabel)

    variationLabel = ''
    if variation == 'scale':
        variationLabel += 'Scale'
    else:
        variationLabel += variation.upper()
    if variation == 'pp8':
        variationLabel += ' Variation'
    else:
        variationLabel += ' Variations'
        
    if variationLabel != '':
        ROOT.myText(labelx, labely-3*offset, 1, variationLabel)
        
def get_legend_title(h, plot_key):
    h_name = h.GetName()
    # alpha_s = 0.117
    if 'PDF269000' in h_name:
        return '#alpha_{S} = 0.117'
    # alpha_s = 0.119
    if 'PDF270000' in h_name:
        return '#alpha_{S} = 0.119'
    # CT14nnlo
    if 'PDF13000' in h_name:
        return 'CT14nnlo'
    # MMHT2014nnlo68cl
    if 'PDF25300' in h_name:
        return 'MMHT2014nnlo68cl'
    # Scale variations
    if 'PDF261000' in h_name:
        if 'MUR0.5_MUF0.5' in h_name:
            return '0.5#mu_{R}, 0.5#mu_{F}'
        if 'MUR0.5_MUF1' in h_name:
            return '0.5#mu_{R}, #mu_{F}'
        if 'MUR1_MUF0.5' in h_name:
            return '#mu_{R}, 0.5#mu_{F}'
        if 'MUR1_MUF2' in h_name:
            return '#mu_{R}, 2#mu_{F}'
        if 'MUR2_MUF1' in h_name:
            return '2#mu_{R}, #mu_{F}'                
        if 'MUR2_MUF2' in h_name:
            return '2#mu_{R}, 2#mu_{F}'
    # CKKW
    if 'CKKW15' in h_name:
        return 'Q_{cut} = 15 GeV'
    if 'CKKW30' in h_name:
        return 'Q_{cut} = 30 GeV'
    # PP8
    if 'PP8' in h_name:
        return 'PowhegPy8'
    # QSF
    if 'QSF025' in h_name:
        return '0.25QSF'
    if 'QSF4' in h_name:
        return '4QSF'

    # ICHEP
    if 'VVMbbME' in h_name:
        return 'ICHEP VVMbbME'
    if 'VVPTVME' in h_name:
        return 'ICHEP VVPTVME'
    if 'VVMbbPSUE' in h_name:
        return 'ICHEP VVMbbPSUE'
    if 'VVPTVPSUE' in h_name:
        return 'ICHEP VVPTVPSUE'
    
    return h_name.replace(plot_key+'_','')

def get_variable_name(plot_key):
    return plot_key.split('_')[-1][0:3]

def get_region_name(plot_key):
    if 'SR' in plot_key:
        return 'SR'
    elif 'CRLow' in plot_key:
        return 'CRLow'
    elif 'CRHigh' in plot_key:
        return 'CRHigh'
    else:
        print('REGION NOT FOUND! RETURNING NONE...')
        return None

def get_ref_name(selection_name, variable, variation):
    # 4 possibilities, each with up/down; only return up for now...
    # VVMbbME, VVPTVME, VVMbbPSUE, VVPTVPSUE

    if '_WW_' in selection_name:
        # No ICHEP shape for WW...
        return None
    if '_ZZ_' in selection_name and '1Lep' in selection_name:
        # No ICHEP shape for 1Lep ZZ
        return None
    
    ref_var = ''
    if variable == 'mBB':
        ref_var = 'Mbb'
    if variable == 'pTV':
        ref_var = 'PTV'

    effect = ''
    if variation == 'pp8':
        effect = 'ME'
    if variation == 'ueps':
        effect = 'PSUE'

    if variable != '' and effect != '':
        if debug:
            print('Reference is '+ selection_name + '_VV' + ref_var + effect + '__1up_CS')
        return selection_name + '_VV' + ref_var + effect + '__1up_CS'
    else:
        return None

def get_reference_variations(plot_key, generic = False):
    variations = []

    if '_VV_' in plot_key or generic:
        for sys in reference_systs:
            variations.append(plot_key+'_'+sys+'__1down_CS')
            variations.append(plot_key+'_'+sys+'__1up_CS')
    else:
        for s in nominal_samples:
            dsid = s[0:6]
            do_append = False
            if 'WW' in plot_key and dsid_to_process[dsid] == 'WW':
                do_append = True
            if 'WZ' in plot_key and dsid_to_process[dsid] == 'WZ':
                do_append = True
            if 'ZZ' in plot_key and dsid_to_process[dsid] == 'ZZ':
                do_append = True
            if do_append:
                for sys in reference_systs:
                    variations.append(dsid + '_' + plot_key+'_'+sys+'__1down_CS')
                    variations.append(dsid + '_' + plot_key+'_'+sys+'__1up_CS')
    
    return variations
    
def get_scale_permutations(plot_key, generic = False):
    permutations = []
    scales = [0.5,1,2]

    if '_VV_' in plot_key or generic:
        for s1 in scales:
            for s2 in scales:
                if s1 == 1 and s2 == 1:
                    continue
                if abs(s1-s2) <= 1:
                    permutations.append(plot_key+'_MUR'+str(s1)+'_MUF'+str(s2)+'_PDF261000')
    else:
        for s in nominal_samples:
            dsid = s[0:6]
            do_append = False
            if 'WW' in plot_key and dsid_to_process[dsid] == 'WW':
                do_append = True
            if 'WZ' in plot_key and dsid_to_process[dsid] == 'WZ':
                do_append = True
            if 'ZZ' in plot_key and dsid_to_process[dsid] == 'ZZ':
                do_append = True
            if do_append:
                for s1 in scales:
                    for s2 in scales:
                        if s1 == 1 and s2 == 1:
                            continue
                        if abs(s1-s2) <= 1:
                            permutations.append(dsid + '_' + plot_key+'_MUR'+str(s1)+'_MUF'+str(s2)+'_PDF261000')
                            
    return permutations

def get_pdf_variations(plot_key, generic = False):
    variations = []
    scales = '_MUR1_MUF1_'

    if '_VV_' in plot_key or generic:    
        # alpha_s = 0.117
        variations.append(plot_key+scales+'PDF269000')
        # alpha_s = 0.119
        variations.append(plot_key+scales+'PDF270000')
        # CT14nnlo
        variations.append(plot_key+scales+'PDF13000')
        # MMHT2014nnlo68cl
        variations.append(plot_key+scales+'PDF25300')
    else:
        for s in nominal_samples:
            dsid = s[0:6]
            do_append = False
            if 'WW' in plot_key and dsid_to_process[dsid] == 'WW':
                do_append = True
            if 'WZ' in plot_key and dsid_to_process[dsid] == 'WZ':
                do_append = True
            if 'ZZ' in plot_key and dsid_to_process[dsid] == 'ZZ':
                do_append = True
            if do_append:
                variations.append(dsid + '_' + plot_key + scales + 'PDF269000')
                variations.append(dsid + '_' + plot_key + scales + 'PDF270000')
                variations.append(dsid + '_' + plot_key + scales + 'PDF13000')
                variations.append(dsid + '_' + plot_key + scales + 'PDF25300')
    # Full set
    #for i in range(261000,261101):
    #    variations.append(plot_key+scales+'PDF'+str(i))

    return variations

def get_pp8_variations(plot_key, generic = False):
    variations = []
    # One variation: 'PP8'
    if '_VV_' in plot_key or generic:
        variations.append(plot_key + '_PP8')
    else:
        # Only use relevant processes; WW/WZ/ZZ
        for s in variation_samples_pp8:
            dsid = s[0:6]
            if 'WW' in plot_key and dsid_to_process[dsid] == 'WW':
                variations.append(dsid + '_' + plot_key + '_Nominal')
            if 'WZ' in plot_key and dsid_to_process[dsid] == 'WZ':
                variations.append(dsid + '_' + plot_key + '_Nominal')
            if 'ZZ' in plot_key and dsid_to_process[dsid] == 'ZZ':
                variations.append(dsid + '_' + plot_key + '_Nominal')        
    return variations

def get_herwig_variations(plot_key, generic = False):
    variations = []
    # One variation: 'HERWIG'
    if '_VV_' in plot_key or generic:
        variations.append(plot_key + '_HERWIG')
    else:
        # Only use relevant processes; WW/WZ/ZZ
        for s in variation_samples_herwig:
            dsid = s[0:6]
            if 'WW' in plot_key and dsid_to_process[dsid] == 'WW':
                variations.append(dsid + '_' + plot_key + '_Nominal')
            if 'WZ' in plot_key and dsid_to_process[dsid] == 'WZ':
                variations.append(dsid + '_' + plot_key + '_Nominal')
            if 'ZZ' in plot_key and dsid_to_process[dsid] == 'ZZ':
                variations.append(dsid + '_' + plot_key + '_Nominal')        
    return variations

def get_ckkw_variations(plot_key, generic = False):
    variations = []
    # Two variations: 'CKKW15' and 'CKKW30'
    if '_VV_' in plot_key or generic:
        variations.append(plot_key + '_CKKW15')
        variations.append(plot_key + '_CKKW30')
    else:
        # Only use relevant processes; WW/WZ/ZZ
        for s in variation_samples_ckkw:
            dsid = s[0:6]
            if 'WW' in plot_key and dsid_to_process[dsid] == 'WW':
                variations.append(dsid + '_' + plot_key + '_Nominal')
            if 'WZ' in plot_key and dsid_to_process[dsid] == 'WZ':
                variations.append(dsid + '_' + plot_key + '_Nominal')
            if 'ZZ' in plot_key and dsid_to_process[dsid] == 'ZZ':
                variations.append(dsid + '_' + plot_key + '_Nominal')        
    return variations

def get_qsf_variations(plot_key, generic = False):
    variations = []
    # Two variations: 'QSF025' and 'QSF4'
    if '_VV_' in plot_key or generic:
        variations.append(plot_key + '_QSF025')
        variations.append(plot_key + '_QSF4')
    else:
        # Only use relevant processes; WW/WZ/ZZ
        for s in variation_samples_qsf:
            dsid = s[0:6]
            if 'WW' in plot_key and dsid_to_process[dsid] == 'WW':
                variations.append(dsid + '_' + plot_key + '_Nominal')
            if 'WZ' in plot_key and dsid_to_process[dsid] == 'WZ':
                variations.append(dsid + '_' + plot_key + '_Nominal')
            if 'ZZ' in plot_key and dsid_to_process[dsid] == 'ZZ':
                variations.append(dsid + '_' + plot_key + '_Nominal')
    return variations

def get_variation_names(plot_key, variation, generic = False):
    v_names = []
    if variation == 'scale':
        v_names = get_scale_permutations(plot_key, generic)
    if variation == 'pp8':
        v_names = get_pp8_variations(plot_key, generic)
    if variation == 'ueps':
        v_names = get_herwig_variations(plot_key, generic)
    if variation == 'ckkw':
        v_names = get_ckkw_variations(plot_key, generic)
    if variation == 'qsf':
        v_names = get_qsf_variations(plot_key, generic)
    if variation == 'pdf':
        v_names = get_pdf_variations(plot_key, generic)
    if variation == 'ref':
        v_names = get_reference_variations(plot_key, generic)
        
    # if debug:
    #     print('Variation ' + variation + ' for plot ' + plot_key + ' has ' + str(len(v_names)) + ' variations:')
    #     for vn in v_names:
    #         print('\t'+vn)
    return v_names

def get_envelope(h_nominal, ratios):
    npts = ratios[0].GetNbinsX()
    env = ROOT.TGraphAsymmErrors(npts)
    env.GetXaxis().SetTitle(ratios[0].GetXaxis().GetTitle())
    
    # TGraph starts at point 0, and TH1 starts at bin 1 (bin 0 is underflow, nBins+1 is overflow)
    # ipt + 1 == ibin

    # Set envelope x and y values and dummy errors
    for ipt in range(npts):
        x = ratios[0].GetBinCenter(ipt+1)
        y = 1.0
        ex = x - ratios[0].GetBinLowEdge(ipt+1)
        exl, exh, eyl, eyh = ex, ex, 0., 0.
        env.SetPoint(ipt, x, y)
        env.SetPointError(ipt, exl, exh, eyl, eyh)        
        # Update envelope errors from ratios
        for i,r in enumerate(ratios):
            if r.GetBinContent(ipt+1) == 0.0:
                # If ratio has content 0, then variation had no entries in bin when dividing nominal
                continue
            r_up = r.GetBinContent(ipt+1) - 1.0
            r_down = 1.0 - r.GetBinContent(ipt+1)
            if r_up > env.GetErrorYhigh(ipt):
                env.SetPointEYhigh(ipt, r_up)
            if r_down > env.GetErrorYlow(ipt):
                env.SetPointEYlow(ipt, r_down)
    # Aesthetics
    env.SetMarkerColor(ROOT.kBlue)
    env.SetFillColor(ROOT.kBlue)
    env.SetFillStyle(3144)

    return env

def get_selections(level):
    # TODO: MAKE THIS INCLUSIVE?
    selections = []
    if  level == 'all':
        # Combine SR+CRLow+CRHigh+2j+3j + pTV bins
        selections.append('inclusive')

    if level == 'split_jets' or level == 'all':
        # Combine SR+CRLow+CRHigh + pTV bins
        for j in jets:
            selections.append('2tag'+j+'_sumptv_sumregions')

    if level == 'split_SRCR' or level == 'split_jets' or level == 'all':
        # Combine pTV bins
        for j in jets:
            for r in regions:
                selections.append('2tag'+j+'_sumptv_'+r)

    return selections

def combine_histograms(histograms, process, channel, level, variable, variation):

    # plot_key: hist_[X]Lep_[WW/WZ/ZZ/VV]_2tag[Y]jet_[pT_region]_[SR/CRLow/CRHigh]_[pTV/mBB/BDT]
    # h_name  : [DSID]_[plot_key]_[sys_or_nom]

    cb_hists = {}

    selections = get_selections(level)

    
    if debug:
        print('Combining ' + variable + ' histograms for process ' + process + ' in channel ' + channel + ' and variation ' + variation + ' at level ' + level)
        print('Selections:')
        for s in selections:
            print('\t'+s)

    to_add_map = {}
    # Choose which histograms to later combine
    for sel in selections:
        selection_name = 'hist_' + process + '_' + channel + '_' + sel + '_' + variable
        if debug:
            print('Combining nominal: '+selection_name+'_Nominal')
        # Nominal
        to_add_map[selection_name+'_Nominal'] = []
        for j in jets:
            for pt in pt_ranges:
                for r in regions:
                    # 150 GeV MET cut in 0Lep channel
                    if pt == '75_150' and channel == '0Lep':
                        continue
                    for sample in nominal_samples:
                        dsid = sample[0:6]
                        if dsid_to_process[dsid] != process:
                            continue
                        if channel == '0Lep' and variable == 'pTV':
                            plot_key = 'hist_' + channel + '_' + process + '_2tag' + j + '_' + pt + 'ptv_' + r + '_MET'
                        else:
                            plot_key = 'hist_' + channel + '_' + process + '_2tag' + j + '_' + pt + 'ptv_' + r + '_' + variable
                        h_name = dsid + '_' + plot_key + '_Nominal'

                        # Depending on sel, include histogram to add here or not...
                        if sel == 'inclusive':
                            if verbose:
                                print('inclusive')

                        if sel == '2tag2jet_sumptv_sumregions':
                            if '2tag3jet' in h_name:
                                continue
                            
	                if sel == '2tag2jet_sumptv_SR':
                            if '2tag3jet' in h_name:
                                continue
                            if '_SR' not in h_name:
                                continue
                            
	                if sel == '2tag2jet_sumptv_CRHigh':
                            if '2tag3jet' in h_name:
                                continue
                            if '_CRHigh' not in h_name:
                                continue

	                if sel == '2tag2jet_sumptv_CRLow':
                            if '2tag3jet' in h_name:
                                continue
                            if '_CRLow' not in h_name:
                                continue

                        if sel == '2tag3jet_sumptv_sumregions':
                            if '2tag2jet' in h_name:
                                continue

	                if sel == '2tag3jet_sumptv_SR':
                            if '2tag2jet' in h_name:
                                continue
                            if '_SR' not in h_name:
                                continue

	                if sel == '2tag3jet_sumptv_CRHigh':
                            if '2tag2jet' in h_name:
                                continue
                            if '_CRHigh' not in h_name:
                                continue

	                if sel == '2tag3jet_sumptv_CRLow':
                            if '2tag2jet' in h_name:
                                continue
                            if '_CRLow' not in h_name:
                                continue

                        # Add if we make it up to here
                        if verbose:
                            print('\tAdding: '+h_name)
                        to_add_map[selection_name+'_Nominal'].append(h_name)

                        
                                
        # Variations
        maps_to_include = get_variation_names(selection_name, variation, True)
        # Initialize maps to empty lists
        for mti in maps_to_include:
            to_add_map[mti] = []

        
        # Fill appropriate lists
        for mti in maps_to_include:
            if debug:
                print('Combining variation: '+mti)
            for j in jets:
                for pt in pt_ranges:
                    for r in regions:
                        # 150 GeV MET cut in 0Lep channel
                        if pt == '75_150' and channel == '0Lep':
                            continue
                        if channel == '0Lep' and variable == 'pTV':
                            plot_key = 'hist_' + channel + '_' + process + '_2tag' + j + '_' + pt + 'ptv_' + r + '_MET'
                        else:
                            plot_key = 'hist_' + channel + '_' + process + '_2tag' + j + '_' + pt + 'ptv_' + r + '_' + variable
                        v_names = get_variation_names(plot_key, variation)
                        for vn in v_names:
                            dsid = vn[0:6]
                            # Depending on sel, include histogram to add here or not...
                            # TODO: ADD OTHER SELECTIONS FOR THE OTHER LEVELS
                            if sel == 'inclusive':
                                if verbose:
                                    print('inclusive')
                                    
                            if sel == '2tag2jet_sumptv_sumregions':
                                if '2tag3jet' in vn:
                                    continue

	                    if sel == '2tag2jet_sumptv_SR':
                                if '2tag3jet' in vn:
                                    continue
                                if '_SR' not in vn:
                                    continue

	                    if sel == '2tag2jet_sumptv_CRHigh':
                                if '2tag3jet' in vn:
                                    continue
                                if '_CRHigh' not in vn:
                                    continue


	                    if sel == '2tag2jet_sumptv_CRLow':
                                if '2tag3jet' in vn:
                                    continue
                                if '_CRLow' not in vn:
                                    continue

            	            if sel == '2tag3jet_sumptv_sumregions':
                                if '2tag2jet' in vn:
                                    continue

	                    if sel == '2tag3jet_sumptv_SR':
                                if '2tag2jet' in vn:
                                    continue
                                if '_SR' not in vn:
                                    continue

	                    if sel == '2tag3jet_sumptv_CRHigh':
                                if '2tag2jet' in vn:
                                    continue
                                if '_CRHigh' not in vn:
                                    continue

	                    if sel == '2tag3jet_sumptv_CRLow':
                                if '2tag2jet' in vn:
                                    continue
                                if '_CRLow' not in vn:
                                    continue

                            # Add if we make it up to here
                            # E.g. here, if 'CKKW30' in mti and 'CKKW30' in vn, then append(vn) to to_add_map[mti]
                            if variation == 'pp8' or variation == 'ueps':
                                if verbose:
                                    print('\tAdding: '+vn)
                                to_add_map[mti].append(vn)

                            if variation == 'ckkw':
                                if 'CKKW15' in mti and 'CKKW15' in dsid_to_name[dsid]:
                                    if verbose:
                                        print('\tAdding: '+vn)
                                    to_add_map[mti].append(vn)
                                if 'CKKW30' in mti and 'CKKW30' in dsid_to_name[dsid]:
                                    if verbose:
                                        print('\tAdding: '+vn)
                                    to_add_map[mti].append(vn)

                            if variation == 'qsf':
                                if 'QSF025' in mti and 'QSF025' in dsid_to_name[dsid]:
                                    if verbose:
                                        print('\tAdding: '+vn)
                                    to_add_map[mti].append(vn)
                                if 'QSF4' in mti and 'QSF4' in dsid_to_name[dsid]:
                                    if verbose:
                                        print('\tAdding: '+vn)
                                    to_add_map[mti].append(vn)

                            if variation == 'scale' or variation == 'pdf' or variation == 'ref':
                                if mti.replace(selection_name,'') in vn:
                                    if verbose:
                                        print('\tAdding: '+vn)
                                    to_add_map[mti].append(vn)


    # Actually combine the plots
    for sel, to_add in to_add_map.items():
        if debug:
            print('About to add '+str(len(to_add))+' histograms for: '+sel)
        if verbose:
            for hn in to_add:
                print('\t\t'+hn)
        if verbose:
            print('Actually adding:')
        first = True
        for hn in to_add:
            if is_bad_hist(hn):
                continue
            if verbose:
                print('\t'+str(histograms[hn]) + ': '+str(histograms[hn].GetEntries()))
            if first:
                cb_hists[sel] = histograms[hn].Clone(sel)
                cb_hists[sel].SetTitle(sel)
                first = False
            else:
                cb_hists[sel].Add(histograms[hn].Clone())

    return cb_hists


def combine_processes(histograms, plot_key, variation):
    # Combine WW/WZ/ZZ to VV histograms
    if debug:
        print('Combining histograms for plot_key: ' + plot_key + ' and variation: ' + variation)
    
    cb_hists = {}

    # Combine nominal
    if debug:
        print('Combining: ' + plot_key + '_Nominal')
    first = True
    for s in nominal_samples:
        dsid = s[0:6]
        if is_WW_sample(dsid):
            continue
        base_name = plot_key.replace('VV', dsid_to_process[dsid])
        check_name = dsid + '_' + base_name + '_Nominal'
        if debug:
            print('\t' + check_name)
        if is_bad_hist(check_name):
            continue

        if debug:
            #if '2tag2jet_150_250ptv' in plot_key:
            err = ROOT.Double(0)
            integral = histograms[check_name].IntegralAndError(0, -1, err)    
            print('\t--> Adding plot '+check_name+' with '+str(integral)+' entries with error '+str(err))
            
        if first:
            cb_hists[plot_key+'_Nominal'] = histograms[check_name].Clone(plot_key)
            first = False
        else:
            cb_hists[plot_key+'_Nominal'].Add(histograms[check_name])

    if debug:
        #if '2tag2jet_150_250ptv' in plot_key:
        err = ROOT.Double(0)
        integral = cb_hists[plot_key+'_Nominal'].IntegralAndError(0, -1, err)
        print('\t\t--> Final plot '+plot_key+'_Nominal'+' has '+str(integral)+' entries with error '+str(err))

    # Combine variations
    WW_WZ_ZZ = nominal_samples
    if variation == 'pp8':
        WW_WZ_ZZ = variation_samples_pp8
    if variation == 'ckkw':
        WW_WZ_ZZ = variation_samples_ckkw
    if variation == 'qsf':
        WW_WZ_ZZ = variation_samples_qsf

    v_names = get_variation_names(plot_key, variation)
    for vn in v_names:
        if debug:
            print('Combining: ' + vn)
        h_to_add = []
        for s in WW_WZ_ZZ:
            dsid = s[0:6]
            if is_WW_sample(dsid):
                continue
            check_name = dsid + '_' + vn.replace('VV', dsid_to_process[dsid], 1)

            if variation == 'ckkw':
                # Get all the CKK15 (or CKK30) plots and combine
                if 'CKKW15' in vn and 'CKKW15' in s:
                    check_name = dsid + '_' + vn.replace('VV', dsid_to_process[dsid]).replace('_CKKW15','_Nominal')
                elif 'CKKW30' in vn and 'CKKW30' in s:
                    check_name = dsid + '_' + vn.replace('VV', dsid_to_process[dsid]).replace('_CKKW30','_Nominal')
                else:
                    continue
                
            if variation == 'qsf':
                # Get all the QSF025 (or QSF4) plots and combine
                if 'QSF025' in vn and 'QSF025' in s:
                    check_name = dsid + '_' + vn.replace('VV', dsid_to_process[dsid]).replace('_QSF025','_Nominal')
                elif 'QSF4' in vn and 'QSF4' in s:
                    check_name = dsid + '_' + vn.replace('VV', dsid_to_process[dsid]).replace('_QSF4','_Nominal')
                else:
                    continue
            if variation == 'pp8':
                check_name = dsid + '_' + vn.replace('VV', dsid_to_process[dsid]).replace('_PP8','_Nominal')
            h_to_add.append(check_name)

        first = True
        for hn in h_to_add:
            if debug:
                print('\t' + hn)
            if is_bad_hist(hn):
                continue

            if debug:
                #if '2tag2jet_150_250ptv' in plot_key:
                err = ROOT.Double(0)
                integral = histograms[hn].IntegralAndError(0, -1, err)
                print('\t--> Adding plot '+hn+' with '+str(integral)+' entries with error '+str(err))
                
            if first:
                cb_hists[vn] = histograms[hn].Clone(vn)
                first = False
            else:
                cb_hists[vn].Add(histograms[hn])

        if debug:
            #if '2tag2jet_150_250ptv' in plot_key:
            err = ROOT.Double(0)
            integral = cb_hists[vn].IntegralAndError(0, -1, err)
            print('\t\t--> Final plot '+vn+' has '+str(integral)+' entries with error '+str(err))

    # Return all combined histograms
    return cb_hists



def plot_variations(hists, process, channel, level, variable, variation, problematic = False):#plot_key, variation, inclusive_ptv = False):

    print('Plotting ' + variable + ' for ' + process + ' process in channel ' + channel + ' at level ' + level + ' for systematic: ' + variation)

    selections = get_selections(level)

    for sel in selections:
        selection_name = 'hist_' + process + '_' + channel + '_' + sel + '_' + variable
        
        # Get relevant histograms    
        h_nom = hists[selection_name + '_Nominal'].Clone()
        if variation == 'ueps':
            # Nominal is PP8 for UEPS
            h_nom = hists[selection_name + '_PP8'].Clone()
        h_variations_name = get_variation_names(selection_name, variation, True)
        h_variations = []
        for h_var_name in h_variations_name:
            try:
                h_variations.append(hists[h_var_name].Clone())
            except KeyError:
                # Some histograms (mostly WW), have no events in particular phase space regions
                print('WARNING!! NO HISTOGRAM FOR: '+h_var_name)

        h_ref_name = get_ref_name(selection_name, variable, variation)
        if h_ref_name != None:
            h_ref = hists[h_ref_name].Clone()
        else:
            if debug:
                print('No reference histogram will be plotted')
            h_ref = None

        cnv = ROOT.TCanvas('cnv','cnv',600,600)
        cnv.cd()

        ratio_frac = 0.3
        left_margin = 0.08
        need_envelope = variation == 'scale' or variation == 'ckkw' or variation == 'qsf' or variation == 'pdf'
        if need_envelope:
            p3 = ROOT.TPad('p3','p3',0.,0.,1.,ratio_frac/2)
            p3.Draw()
            p3.SetTopMargin(0.03)
            p3.SetBottomMargin(1.2*ratio_frac)
            p3.SetLeftMargin(left_margin)

            p2 = ROOT.TPad('p2','p2',0.,ratio_frac/2,1.,ratio_frac)
            p2.Draw()
            p2.SetTopMargin(0.03)
            p2.SetBottomMargin(0.03)
            p2.SetLeftMargin(left_margin)

            p1 = ROOT.TPad('p1','p1',0.,ratio_frac,1.,1.)
            p1.Draw()
            p1.SetBottomMargin(0.03)
            p1.SetLeftMargin(left_margin)
            p1.cd()

        else:
            p2 = ROOT.TPad('p2','p2',0.,0.,1.,ratio_frac)
            p2.Draw()
            p2.SetTopMargin(0.03)
            p2.SetBottomMargin(ratio_frac)
            p2.SetLeftMargin(left_margin)        

            p1 = ROOT.TPad('p1','p1',0.,ratio_frac,1.,1.)
            p1.Draw()
            p1.SetBottomMargin(0.03)
            p1.SetLeftMargin(left_margin)
            p1.cd()


        # Rebin and set appropriate ranges
        rebin = rebins[variable]
        # Need integer number of bins in range for envelope TGraphAsymmErrors...
        x_range_nom = h_nom.GetBinLowEdge(h_nom.GetNbinsX()+1) - h_nom.GetBinLowEdge(1)
        if ((h_nom.GetNbinsX()/rebin)/(x_range_nom/(ranges[variable][1] - ranges[variable][0])))%1 > 0:
            print('NON-INTEGER NUMBER OF BINS IN RANGE - AXES WILL NOT ALIGN IN REQUESTED RANGE!!')
            print('Histogram: ' + selection_name)
            print('N bins: ' + str(h_nom.GetNbinsX()) +
                  ', rebin: ' + str(rebin) +
                  ', range: ' + str(h_nom.GetBinLowEdge(1)) + ' - ' + str(h_nom.GetBinLowEdge(h_nom.GetNbinsX()+1)) +
                  ' --> ' + str(ranges[variable][0]) + ' - ' + str(ranges[variable][1]))
            sys.exit()


        # Normalize to unit area and rebin/scale (only once even for multiple variations)
        h_nom.Rebin(rebin)
        h_nom.Scale(1.0/h_nom.Integral())
        h_nom.SetMaximum(2.0*h_nom.GetMaximum())
        h_nom.GetXaxis().SetRangeUser(ranges[variable][0], ranges[variable][1])
        for h in h_variations:
            h.Rebin(rebin)
            h.Scale(1.0/h.Integral())
            h.GetXaxis().SetRangeUser(ranges[variable][0], ranges[variable][1])

        if h_ref != None:
            h_ref.Rebin(rebin)
            h_ref.Scale(1.0/h_ref.Integral())
            h_ref.GetXaxis().SetRangeUser(ranges[variable][0], ranges[variable][1])



        # Histogram styles and aesthetics
        ratio_min, ratio_max = 0.5, 1.5
        # Font style
        x_label_font = 43
        y_label_font = 43
        x_title_font = 43
        y_title_font = 43    
        # Sizes in pixels
        x_title_size = 14
        y_title_size = 14
        x_label_size = 12
        y_label_size = 12
        # Offsets relative to axis
        x_title_offset = 5.0
        y_title_offset = 1.2

        # Colors
        h_nom.SetLineColor(colors[0])
        h_nom.SetMarkerColor(colors[0])
        for i, h in enumerate(h_variations):
            h.SetLineColor(colors[i+1])
            h.SetMarkerColor(colors[i+1])

        # Labels
        h_nom.GetXaxis().SetLabelFont(x_label_font)
        h_nom.GetYaxis().SetLabelFont(y_label_font)
        h_nom.GetXaxis().SetLabelSize(0)
        h_nom.GetYaxis().SetLabelSize(y_label_size)

        # Titles
        h_nom.GetXaxis().SetTitleSize(0)
        h_nom.GetYaxis().SetTitleFont(y_title_font)
        h_nom.GetYaxis().SetTitleSize(y_title_size)
        h_nom.GetYaxis().SetTitle('Events')

        # Draw histograms
        h_nom.Draw('hist,e')
        for h in h_variations:
            h.Draw('same,e')

        # Legend
        lx1,ly1,lx2,ly2 = 0.65,0.6,0.9,0.9
        leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
        if variation == 'ueps':
            leg.AddEntry(h_nom, 'PP8', 'l')
        else:
            leg.AddEntry(h_nom, 'Nominal', 'l')
        for h in h_variations:
            leg.AddEntry(h, get_legend_title(h, selection_name), 'l')
        leg.Draw('same')


        # Labels
        draw_labels(selection_name, variation)

        
        # Get ratio plots
        ratios = []
        stat_err = h_nom.Clone()
        stat_err.Divide(h_nom)
        stat_err.GetYaxis().SetRangeUser(ratio_min, ratio_max)
        stat_err.GetYaxis().SetNdivisions(505)
        stat_err.GetXaxis().SetLabelFont(x_label_font)
        stat_err.GetYaxis().SetLabelFont(y_label_font)
        if need_envelope:
            stat_err.GetXaxis().SetLabelSize(0)
        else:
            stat_err.GetXaxis().SetLabelSize(x_label_size)
        stat_err.GetYaxis().SetLabelSize(y_label_size)
        # Titles
        stat_err.GetYaxis().SetTitle('Nom/Var')
        stat_err.GetXaxis().SetTitle(stat_err.GetXaxis().GetTitle() + ' [GeV]')
        stat_err.GetXaxis().SetTitleFont(x_title_font)
        stat_err.GetYaxis().SetTitleFont(y_title_font)
        if need_envelope:
            stat_err.GetXaxis().SetTitleSize(0)
        else:
            stat_err.GetXaxis().SetTitleSize(x_title_size)
        stat_err.GetYaxis().SetTitleSize(y_title_size)
        stat_err.GetYaxis().SetTitleOffset(y_title_offset)
        stat_err.GetXaxis().SetTitleOffset(x_title_offset)

        ratios.append(stat_err)

        for i, h in enumerate(h_variations):
            r = h_nom.Clone()
            r.Divide(h)
            # Ranges
            r.GetYaxis().SetRangeUser(ratio_min, ratio_max)
            r.GetYaxis().SetNdivisions(505)
            # Colors
            r.SetLineColor(colors[i+1])
            r.SetMarkerColor(colors[i+1])
            # Labels
            r.GetXaxis().SetLabelFont(x_label_font)
            r.GetYaxis().SetLabelFont(y_label_font)
            if need_envelope:
                r.GetXaxis().SetLabelSize(0)
            else:
                r.GetXaxis().SetLabelSize(x_label_size)
            r.GetYaxis().SetLabelSize(y_label_size)
            # Titles
            r.GetYaxis().SetTitle('Nom/Var')
            r.GetXaxis().SetTitleFont(x_title_font)
            r.GetYaxis().SetTitleFont(y_title_font)
            if need_envelope:
                r.GetXaxis().SetTitleSize(0)
            else:
                r.GetXaxis().SetTitleSize(x_title_size)
            r.GetYaxis().SetTitleSize(y_title_size)
            r.GetYaxis().SetTitleOffset(y_title_offset)
            r.GetXaxis().SetTitleOffset(x_title_offset)

            ratios.append(r)


        # Draw ratios and envelope if needed
        if need_envelope:
            p3.cd()
            env = get_envelope(h_nom, ratios)
            # Ranges
            env.GetXaxis().SetLimits(ranges[variable][0], ranges[variable][1])
            env.GetYaxis().SetRangeUser(ratio_min, ratio_max)
            env.GetYaxis().SetNdivisions(505)
            # Labels
            env.GetXaxis().SetLabelFont(x_label_font)
            env.GetYaxis().SetLabelFont(y_label_font)
            env.GetXaxis().SetLabelSize(x_label_size)
            env.GetYaxis().SetLabelSize(y_label_size)
            # Titles
            env.GetYaxis().SetTitle('Envelope')
            env.GetXaxis().SetTitleFont(x_title_font)
            env.GetYaxis().SetTitleFont(y_title_font)
            env.GetXaxis().SetTitleSize(x_title_size)
            env.GetYaxis().SetTitleSize(y_title_size)
            env.GetXaxis().SetTitleOffset(x_title_offset)
            env.GetYaxis().SetTitleOffset(y_title_offset)

            env.Draw('A2')

            unityLine = ROOT.TLine()
            unityLine.SetLineColor(ROOT.kBlack)
            unityLine.DrawLine(ranges[variable][0],1.0,ranges[variable][1],1.0)


        # Back to p2
        p2.cd()

        for i, h_ratio in enumerate(ratios):
            if i ==0:
                h_ratio.Draw('e')
            else:
                h_ratio.Draw('same,e')

        unityLine = ROOT.TLine()
        unityLine.SetLineColor(ROOT.kBlack)
        unityLine.DrawLine(ranges[variable][0],1.0,ranges[variable][1]-1,1.0)

        # Plot reference if necessary
        if h_ref != None:
            if debug:
                print(h_ref)
            h_ref.SetLineColor(colors[-1])
            h_ref.SetMarkerColor(colors[-1])
            leg.AddEntry(h_ref, get_legend_title(h_ref, selection_name), 'l')

            p1.cd()
            h_ref.Draw('same')

            p2.cd()
            r_ref = h_nom.Clone()
            r_ref.Divide(h_ref)
            r_ref.SetLineColor(colors[-1])
            r_ref.SetMarkerColor(colors[-1])
            r_ref.Draw('same')

        if problematic:
            cnv.SaveAs(out_dir + 'debug_hist_' + process + '_' + channel + '_' + sel + '_' + variable + '_' + variation + '.pdf')
        else:
            cnv.SaveAs(out_dir + 'hist_' + process + '_' + channel + '_' + sel + '_' + variable + '_' + variation + '.pdf')

        cnv.Close()


def get_stat_uncertainty(hists, plot_key, var):
    unc_up, unc_down = 0.0, 0.0
    err_up, err_down = ROOT.Double(0), ROOT.Double(0)
    integral_up, integral_down = 0.0, 0.0
    
    if var == 'Nominal':
        integral_up = hists[plot_key+'_Nominal'].IntegralAndError(0, -1, err_up)
        unc_up = 100*(err_up/integral_up)
        unc_down = unc_up
    if var == 'pp8':
        integral_up = hists[plot_key+'_PP8'].IntegralAndError(0, -1, err_up)
        unc_up = 100*(err_up/integral_up)
        unc_down = unc_up
    if var == 'ckkw':
        integral_up = hists[plot_key+'_CKKW30'].IntegralAndError(0, -1, err_up)
        integral_down = hists[plot_key+'_CKKW15'].IntegralAndError(0, -1, err_down)
        unc_up = 100*(err_up/integral_up)
        unc_down = 100*(err_down/integral_down)
    if var == 'qsf':
        integral_up = hists[plot_key+'_QSF4'].IntegralAndError(0, -1, err_up)
        integral_down = hists[plot_key+'_QSF025'].IntegralAndError(0, -1, err_down)
        unc_up = 100*(err_up/integral_up)
        unc_down = 100*(err_down/integral_down)
    if var == 'scale' or var == 'pdf':
        # Just take the max of the variations
        var_names = get_variation_names(plot_key, var)
        for h_name in var_names:
            err = ROOT.Double(0)
            integral = hists[h_name].IntegralAndError(0, -1, err)
            unc = 100*(err/integral)
            if unc > unc_up:
                integral_up = integral
                err_up = err
                unc_up = unc
        unc_down = unc_up

    if debug:#'2tag2jet_250ptv' in plot_key:
        print('Plot: '+plot_key+' ('+var+'): integral = '+str(integral_up)+', error: '+str(err_up))
        
    return unc_up, unc_down

def get_yield_variation(hists, plot_key, var):
    var_up, var_down = 0.0, 0.0
    
    integral_nom = hists[plot_key+'_Nominal'].Integral(0, -1)
    if debug:
        print(plot_key, integral_nom)
    if var == 'pp8':
        integral_up = hists[plot_key+'_PP8'].Integral(0, -1)
        if debug:
            print('pp8var: '+str(integral_up))
        var_up = 100*(integral_up/integral_nom-1)
        var_down = var_up
    if var == 'ckkw':
        integral_up = hists[plot_key+'_CKKW30'].Integral(0, -1)
        integral_down = hists[plot_key+'_CKKW15'].Integral(0, -1)
        var_up = 100*(integral_up/integral_nom-1)
        var_down = 100*(integral_down/integral_nom-1)
    if var == 'qsf':
        integral_up = hists[plot_key+'_QSF4'].Integral(0, -1)
        integral_down = hists[plot_key+'_QSF025'].Integral(0, -1)
        var_up = 100*(integral_up/integral_nom-1)
        var_down = 100*(integral_down/integral_nom-1)
    if var == 'scale' or var == 'pdf':
        # Just take the max of the variations
        var_names = get_variation_names(plot_key, var, True)
        for h_name in var_names:
            integral = hists[h_name].Integral(0, -1)
            var_tmp = 100*(integral/integral_nom-1)
            if var_tmp > var_up:
                var_up = var_tmp
        var_down = var_up
    if var == 'overall_norm':
        
        # # Sum over channels
        h_sum_nom = hists[plot_key+'_Nominal'].Clone()
        for c in channels:
            if c == '0Lep':
                continue
            h_sum_nom.Add(hists[plot_key.replace('0Lep',c)+'_Nominal'].Clone())

        integral_nom = h_sum_nom.Integral(0, -1)

        if debug:
            print('nomsum',plot_key, integral_nom)

        
        # PP8
        h_sum_pp8 = hists[plot_key+'_PP8'].Clone()
        for c in channels:
            if c == '0Lep':
                continue
            h_sum_pp8.Add(hists[plot_key.replace('0Lep',c)+'_PP8'].Clone())
            
        integral_pp8 = h_sum_pp8.Integral(0, -1)

        if debug:
            print('normvar_pp8: '+str(integral_pp8))

        var_pp8 = 100*(integral_pp8/integral_nom-1)


        # Scale
        var_scale = 0
        var_names = get_variation_names(plot_key, 'scale', True)
        for h_name in var_names:
            h_sum_var = hists[h_name].Clone()
            for c in channels:
                if c == '0Lep':
                    continue
                h_sum_var.Add(hists[h_name.replace('0Lep',c)].Clone())

            integral_scale = h_sum_var.Integral(0, -1)
            var_tmp = 100*(integral_scale/integral_nom-1)

            var_scale += (var_tmp*var_tmp)
            #if abs(var_tmp) > var_scale:
            #    var_scale = abs(var_tmp)

            if debug:
                print('normvar scale: '+str(integral_scale))


        # var_scale is already sum of squares...
        var_up = math.sqrt(var_scale + var_pp8*var_pp8)
        var_down = var_up


    if var == 'ueps':
        # Nominal is Powheg
        integral_nom = hists[plot_key+'_PP8'].Integral(0, -1)
        if debug:
            print('pp8 as nom: '+str(integral_nom))

        # Variation is Herwig++
        integral_up = hists[plot_key+'_HERWIG'].Integral(0, -1)
        if debug:
            print('herwigvar: '+str(integral_up))
        var_up = 100*(integral_up/integral_nom-1)
        var_down = var_up

                
        if abs(var_up) > 50:
            print('Large discrepancy for plot_key: '+plot_key)
            if 'ZZ_0Lep_2tag2jet' in plot_key:
                plot_variations(hists, 'ZZ', '0Lep', 'all', 'mBB', 'ueps', True)

            

    return var_up, var_down

def get_double_ratios(hists, plot_key, var, extrap):

    # Double ratios: (yield_var^A/yield_var_B) / (yield_nom^A/yield_nom_B)
    # SRCR: (yield_var^SR/yield_var_CR) / (yield_nom^SR/yield_nom_CR)
    # 1L0L or 2L0L: (yield_var^0L/yield_var_[1L/2L]) / (yield_nom^0L/yield_nom_[1L/2L])        
    # 2j3j: (yield_var^2j/yield_var_3j) / (yield_nom^2j/yield_nom_3j)

    var_up, var_down = 0.0, 0.0

    if debug:
        print('Double ratio plot_key: '+plot_key+' ('+var+')')
        
    isSRCR = 'SRCR' in extrap
    is2j3j = '2j3j' in extrap
    isXL0L = extrap == '1L0L' or extrap == '2L0L'

    # Setup keys for plot names
    key_A = ''
    key_B = ''

    if isSRCR:
        key_A = 'SR'
        if 'CRHigh' in plot_key:
            key_B = 'CRHigh'
        elif 'CRLow' in plot_key:
            key_B = 'CRLow'
        else:
            print('ERROR: UNKNOWN CONTROL REGION: '+plot_key)

    if isXL0L:
        key_A = '0L'
        key_B = extrap.replace('0L','')

    if is2j3j:
        key_A = '2jet'
        key_B = '3jet'

    # Generic up/down variations
    up_key   = ''
    down_key = ''
    if var == 'pp8':
        up_key   = plot_key+'_PP8'
        down_key = plot_key+'_PP8'
    if var == 'ueps':
        up_key   = plot_key+'_HERWIG'
        down_key = plot_key+'_HERWIG'
    if var == 'ckkw':
        up_key   = plot_key+'_CKKW30'
        down_key = plot_key+'_CKKW15'
    if var == 'qsf':
        up_key   = plot_key+'_QSF4'
        down_key = plot_key+'_QSF025'
    if var == 'scale' or var == 'pdf':
        # Just take the max of the variations
        var_names = get_variation_names(plot_key, var, True)
        max_ratio = 0.0
        for h_name in var_names:
            tmp_name_A = h_name.replace(key_B,key_A)
            tmp_name_B = h_name.replace(key_A,key_B)
            if '0L' in key_A:
                tmp_name_A  = tmp_name_A.replace(pt_ranges[0],pt_ranges[1])
            if '0L' in key_B:
                tmp_name_B  = tmp_name_B.replace(pt_ranges[0],pt_ranges[1])
            tmp_integral_A = hists[tmp_name_A].Integral(0, -1)
            tmp_integral_B = hists[tmp_name_B].Integral(0, -1)
            tmp_ratio      = tmp_integral_A/tmp_integral_B
            if  tmp_ratio > max_ratio:
                max_ratio = tmp_ratio
                up_key    = h_name
                down_key  = h_name
                
    # Setup which plots to get depending on extrapolation desired
    key_nom_A      = plot_key.replace(key_B,key_A)+'_Nominal'
    key_nom_B      = plot_key.replace(key_A,key_B)+'_Nominal'
    key_var_A_up   = up_key.replace(key_B,key_A)
    key_var_A_down = down_key.replace(key_B,key_A)
    key_var_B_up   = up_key.replace(key_A,key_B)
    key_var_B_down = down_key.replace(key_A,key_B)

    # Nominal is Powheg for UEPS
    if var == 'ueps':
        key_nom_A = plot_key.replace(key_B,key_A)+'_PP8'
        key_nom_B = plot_key.replace(key_A,key_B)+'_PP8'
    
    # Generic double ratio code
    h_nom_A      = hists[key_nom_A].Clone()
    h_nom_B      = hists[key_nom_B].Clone()
    h_var_A_up   = hists[key_var_A_up].Clone()
    h_var_A_down = hists[key_var_A_down].Clone()
    h_var_B_up   = hists[key_var_B_up].Clone()
    h_var_B_down = hists[key_var_B_down].Clone()

    if debug:
        print('Initial plots have:')
        print(key_nom_A)
        print(key_nom_B)
        print(key_var_A_up)
        print(key_var_A_down)
        print(key_var_B_up)
        print(key_var_B_down)
        print('\th_nom_A '+str(h_nom_A.Integral(0, -1)))
        print('\th_nom_B '+str(h_nom_B.Integral(0, -1)))
        print('\th_var_A_up '+str(h_var_A_up.Integral(0, -1)))
        print('\th_var_A_down '+str(h_var_A_down.Integral(0, -1)))
        print('\th_var_B_up '+str(h_var_B_up.Integral(0, -1)))
        print('\th_var_B_down '+str(h_var_B_down.Integral(0, -1)))

    # Integrals
    integral_nom_A      = h_nom_A.Integral(0, -1)
    integral_nom_B      = h_nom_B.Integral(0, -1)
    integral_var_A_up   = h_var_A_up.Integral(0, -1)
    integral_var_A_down = h_var_A_down.Integral(0, -1)
    integral_var_B_up   = h_var_B_up.Integral(0, -1)
    integral_var_B_down = h_var_B_down.Integral(0, -1)

    # Variations
    var_up   = (((integral_var_A_up/integral_var_B_up) / (integral_nom_A/integral_nom_B))-1)*100
    var_down = (((integral_var_A_down/integral_var_B_down) / (integral_nom_A/integral_nom_B))-1)*100

    if debug and (abs(var_up)>14 or abs(var_down)>14):
        print('plot_key: '+plot_key)
        print('variation: '+var)
        print('A: '+key_A+', B: '+key_B)
        print('integral_nom_A      = '+str(integral_nom_A))
        print('integral_nom_B      = '+str(integral_nom_B))
        print('integral_var_A_up   = '+str(integral_var_A_up))
        print('integral_var_A_down = '+str(integral_var_A_down))
        print('integral_var_B_up   = '+str(integral_var_B_up))
        print('integral_var_B_down = '+str(integral_var_B_down))
        print('num_ratio_up        = '+str((integral_var_A_up/integral_var_B_up)))
        print('denom_ratio_up      = '+str((integral_nom_A/integral_nom_B)))
        print('double_ratio        = '+str(((integral_var_A_up/integral_var_B_up) / (integral_nom_A/integral_nom_B))))
        
    return var_up, var_down
    
def get_region_title(r, inclusive_pt = False):
    # Pretty names for tables
    if debug:
        print('Region: '+r)

    title = ''
    if '_SR' in r:
        title += 'SR: '
    elif '_CRHigh' in r:
        title += 'CRHigh: '
    elif '_CRLow' in r:
        title += 'CRLow: '

    if '2tag2jet' in r:
        title += '2j'
    elif '2tag3jet' in r:
        title += '3j'

    if inclusive_pt:
        title += ''
    elif 'jet_75_150ptv' in r:
        title += ', $75~\\text{GeV} < p_\\text{T}^V < 150~\\text{GeV}$'
    elif 'jet_150_250ptv' in r:
        title += ', $150~\\text{GeV} < p_\\text{T}^V < 250~\\text{GeV}$'
    elif 'jet_250ptv' in r:
        title += ', $p_\\text{T}^V > 250~\\text{GeV}$'

    if title != '':
        return title
    else:
        return r

def get_sys_title(sys):
    # Pretty names for tables
    if sys == 'Nominal':
        return 'Nominal'
    if sys == 'scale':
        return 'Scale'
    if sys == 'ckkw':
        return 'CKKW'
    if sys == 'qsf':
        return 'QSF'
    if sys == 'pp8':
        return 'PowhegPy8'
    if sys == 'ueps':
        return 'UEPS'
    if sys == 'pdf':
        return 'PDF'
    return sys

def get_table_start(regions, process_header = False):
    table_start = ''
    table_start += '\\begin{table}[!htpb] \n'
    table_start += '\\footnotesize \n'
    table_start += '\\centering \n'
    # tmp
    table_start += '\\resizebox{\\textwidth}{!}{ \n'
    table_start += '\\def\\arraystretch{1.1} \n'
    table_start += '\\begin{tabular}{ c |'
    for r in regions:
        table_start += '| c '
    table_start += ' } \n'

    # Column headers
    table_start += '\\hline \n'
    table_start += '\\hline \n'

    if process_header:
        ncol = str(len(regions)/3)
        # Add first line for channels
        table_start += ' & \multicolumn{'+ncol+'}{c}{0L: $ZZ \\rightarrow \\nu\\bar{\\nu} b\\bar{b}$} & \multicolumn{'+ncol+'}{c}{1L: $WZ \\rightarrow l\\nu b\\bar{b}$} & \multicolumn{'+ncol+'}{c}{2L: $ZZ \\rightarrow l^{+}l^{-} b\\bar{b}$} \\\\ \n'

    
    table_start += 'Systematic'
    for r in regions:
        table_start += ' & ' + get_region_title(r)
    table_start += '\\\\ \n'
    table_start += '\\hline \n'

    return table_start

def get_table_end(caption):
    table_end = ''
    table_end += '\\hline \n'
    table_end += '\\hline \n'
    table_end += '\\end{tabular} \n'
    table_end += '} \n'

    chan = 'XLep'
    if '0Lep' in caption:
        chan = '0Lep'
    if '1Lep' in caption:
        chan = '1Lep'
    if '2Lep' in caption:
        chan = '2Lep'
    if '1-to-0' in caption:
        chan = '1L0L'
    if '2-to-0' in caption:
        chan = '2L0L'
    if 'signal region to control region' in caption:
        chan += 'SRCR'
    if '2-jet to 3-jet' in caption:
        chan += '2j3j'

    region = ''
    if 'all systematic uncertainties' in caption:
        region = 'OverallNormalization'
    if 'parton shower and underlying event' in caption:
        region = 'PSUE'
    if 'region extrapolation' in caption:
        region = '0LXL'
    if 'SR to CR' in caption:
        region = 'SRCR'
        
    table_end += '\\caption{ \\label{tab:diboson' + chan + region + '} ' + caption + ' } \n'
    table_end += '\\end{table} \n'
    return table_end


def unc_to_tex(the_line, unc_up, unc_down):
    
    if not isinstance(unc_up, float):
        the_line += ' & N/A '
    elif unc_down == unc_up:
        the_line += ' & $\\pm ' + str('%2.2f'%abs(unc_up)) + ' $ \\% '
    elif unc_up>0 and unc_down<0:
        the_line += ' & $+ ' + str('%2.2f'%unc_up) + ' /\\text{-} ' + str('%2.2f'%abs(unc_down)) + '$ \\% '
    elif unc_up>0 and unc_down>0:
        the_line += ' & $+ ' + str('%2.2f'%unc_up) + ' /+ ' + str('%2.2f'%unc_down) + '$ \\% '
    elif unc_up<0 and unc_down<0:
        the_line += ' & $\\text{-} ' + str('%2.2f'%abs(unc_up)) + ' /\\text{-} ' + str('%2.2f'%abs(unc_down)) + '$ \\% '
    elif unc_up<0 and unc_down>0:
        the_line += ' & $\\text{-} ' + str('%2.2f'%abs(unc_up)) + ' /+ ' + str('%2.2f'%unc_down) + '$ \\% '

    return the_line

def ratio_variations_table(combined_hists, systematics, regions, extrap):
    
    isSRCR = 'SRCR' in extrap
    is2j3j = '2j3j' in extrap
    isXL0L = '1L0L' in extrap or '2L0L' in extrap

    if not isSRCR and not is2j3j and not isXL0L:
        print('INVALID EXTRAPOLATION: ' + extrap)
        return

    ch = ''
    if '0Lep' in extrap:
        ch = '0Lep'
    if '1Lep' in extrap:
        ch = '1Lep'
    if '2Lep' in extrap:
        ch = '2Lep'

    reg = ''
    if 'SR' in regions[0]:
        reg += 'SR'
    if 'CRHigh' in regions[0]:
        reg += 'CRHigh'
    if 'CRLow' in regions[0]:
        reg += 'CRLow'
    
    caption = ''
    caption += 'Systematic uncertainties on '
    if extrap == '1L0L':
        caption += '1-to-0 lepton channel extrapolation in '+reg+'; double ratio [yield$_{var}^{0L}$/yield$_{var}^{1L}$]/[yield$_{nom}^{0L}$/yield$_{nom}^{1L}$].'
    if extrap == '2L0L':
        caption += '2-to-0 lepton channel extrapolation in '+reg+'; double ratio [yield$_{var}^{0L}$/yield$_{var}^{2L}$]/[yield$_{nom}^{0L}$/yield$_{nom}^{2L}$].'
    if isSRCR:
        caption += 'signal region to control region extrapolation in the '+ch+' channel; double ratio [yield$_{var}^{SR}$/yield$_{var}^{CR}$]/[yield$_{nom}^{SR}$/yield$_{nom}^{CR}$].'
    if is2j3j:
        caption += '2-jet to 3-jet selection extrapolation in the '+ch+' channel in '+reg+'; double ratio [yield$_{var}^{2j}$/yield$_{var}^{3j}$]/[yield$_{nom}^{2j}$/yield$_{nom}^{3j}$].'

    the_table = get_table_start(regions, True)
    
    for s in systematics:
        the_line = 'Double Ratio ' + extrap + ' ' + get_sys_title(s)
        for r in regions:
            if isSRCR or is2j3j:
                plot_key = 'hist_'+ch+'_VV_'+r+'_mBB'
            else:
                plot_key = 'hist_0Lep_VV_'+r+'_mBB'
            if debug:
                print(plot_key)
            val_up, val_down = get_double_ratios(combined_hists, plot_key, s, extrap)
            if val_down == val_up:
                the_line += ' & $\\pm ' + str('%2.2f'%abs(val_up)) + ' $ \\% '
            elif val_up>0 and val_down<0:
                the_line += ' & $+ ' + str('%2.2f'%val_up) + ' /\\text{-} ' + str('%2.2f'%abs(val_down)) + '$ \\% '
            elif val_up>0 and val_down>0:
                the_line += ' & $+ ' + str('%2.2f'%val_up) + ' /+ ' + str('%2.2f'%val_down) + '$ \\% '
            elif val_up<0 and val_down<0:
                the_line += ' & $\\text{-} ' + str('%2.2f'%abs(val_up)) + ' /\\text{-} ' + str('%2.2f'%abs(val_down)) + '$ \\% '
            elif val_up<0 and val_down>0:
                the_line += ' & $\\text{-} ' + str('%2.2f'%abs(val_up)) + ' /+ ' + str('%2.2f'%val_down) + '$ \\% '

        the_line+='\\\\ \n'
        the_table += the_line

    the_table += get_table_end(caption)

    if debug:
        print(the_table)
    return the_table

def overall_norm_table(combined_hists):

    caption = 'Summary of all systematic uncertainties on the diboson cross-section.'

    the_table = get_table_start(['Norm. Effect'])
    
    for p in processes:
        the_line = 'Sys'+p+'Norm'
        #for c in channels:
        plot_key = 'hist_' + p + '_0Lep_inclusive_mBB'
        if debug:
            print(plot_key)
        unc_up, unc_down = get_yield_variation(combined_hists, plot_key, 'overall_norm')

        the_line = unc_to_tex(the_line, unc_up, unc_down)

        if unc_up > 50:
            for c in channels:
                plot_variations(combined_hists, p, c, 'all', 'mBB', 'pp8', True)
                plot_variations(combined_hists, p, c, 'all', 'mBB', 'scale', True)
                plot_variations(combined_hists, p, c, 'all', 'pTV', 'pp8', True)
                plot_variations(combined_hists, p, c, 'all', 'pTV', 'scale', True)
                
        the_line += '\\\\ \n'
        the_table += the_line

    the_table += get_table_end(caption)

    return the_table

def SRCR_table(combine_hists):
    caption = 'Summary of SR to CR region extrapolation uncertainties; double ratio [yield$_{var}^{SR}$/yield$_{var}^{CR}$]/[yield$_{nom}^{SR}$/yield$_{nom}^{CR}$].'

    selection_permutations = []
    for c in ['ZZ_0Lep', 'WZ_1Lep', 'ZZ_2Lep']:
        for j in jets:
            for r in ['CRLow','CRHigh']:
                selection_permutations.append(c+'_2tag'+j+'_sumptv_'+r)

    if debug:
        print('Selections to bin included in SRCR table:')
        for s in selection_permutations:
            print('\t'+s)
        
    the_table = get_table_start(selection_permutations, True)

    for s in systematics:
        the_line = get_sys_title(s)
        for selection in selection_permutations:
            plot_key = 'hist_'+selection+'_mBB'
            unc_up, unc_down = get_double_ratios(combined_hists, plot_key, s, 'SRCR')

            the_line = unc_to_tex(the_line, unc_up, unc_down)

        the_line += '\\\\ \n'
        the_table += the_line


    the_table += get_table_end(caption)

    return the_table    

def UEPS_table(combined_hists):
    caption = 'Summary of the effects of parton shower and underlying event variations.'

    selection_permutations = []
    for c in ['ZZ_0Lep', 'WZ_1Lep', 'ZZ_2Lep']:
        for j in jets:
            selection_permutations.append(c+'_2tag'+j+'_sumptv_sumregions')
    if debug:
        print('Selections to be included in UEPS table:')
        for s in selection_permutations:
            print('\t'+s)

    the_table = get_table_start(selection_permutations, True)
    
    # First line is acceptance uncertainties
    the_line = 'SysVZ\_UEPS\_Acc'
    
    for selection in selection_permutations:
        plot_key = 'hist_'+selection+'_mBB'
        if debug:
            print('About to get yield variation for: '+plot_key)
        unc_up, unc_down = get_yield_variation(combined_hists, plot_key, 'ueps')

        the_line = unc_to_tex(the_line, unc_up, unc_down)

    the_line += '\\\\ \n'
    the_table += the_line
    
    # Second line is 2j-3j double ratio
    the_line = 'SysVZ\_UEPS\_32JR'

    for selection in selection_permutations:
        if '2j' in selection:
            the_line += ' & - '
        else:
            plot_key = 'hist_'+selection+'_mBB'
            unc_up, unc_down = get_double_ratios(combined_hists, plot_key, 'ueps', '2j3j')

            the_line = unc_to_tex(the_line, unc_up, unc_down)

    the_line += '\\\\ \n'
    the_table += the_line

    the_table += get_table_end(caption)

    return the_table

def XL0L_table(combined_hists):
    caption = 'Summary of region extrapolation uncertainties; double ratio [yield$_{var}^{0L}$/yield$_{var}^{XL}$]/[yield$_{nom}^{0L}$/yield$_{nom}^{XL}$].'

    the_table = get_table_start(['0Lep to 1Lep', '0Lep to 2Lep'])

    for s in systematics:
        the_line = get_sys_title(s)
        for c in ['1Lep','2Lep']:
            if c == '1Lep':
                plot_key = 'hist_WZ_0Lep_inclusive_mBB'
            elif c == '2Lep':
                plot_key = 'hist_ZZ_0Lep_inclusive_mBB'
            # TODO: Take UEPS variation for now, but quadrature sum all of the systematics in the end
            unc_up, unc_down = get_double_ratios(combined_hists, plot_key, s, c.replace('Lep','L')+'0L')

            the_line = unc_to_tex(the_line, unc_up, unc_down)

        the_line += '\\\\ \n'
        the_table += the_line

    the_table += get_table_end(caption)
    
    return the_table    


def yield_variations_table(combined_hists, systematics, regions, channel):
    # For now...
    systematics = ['Nominal']+systematics

    region_of_interest = ''
    if 'SR' in regions[0]:
        region_of_interest = 'SR'
    if 'CRHigh' in regions[0]:
        region_of_interest = 'CRHigh'
    if 'CRLow' in regions[0]:
        region_of_interest = 'CRLow'
        
    caption = 'Statistical uncertainty and relative yield variation ($\\frac{var}{nom}-1$) between the nominal sample and systematically varied samples in the '+channel+' channel ' + region_of_interest + '.'
    the_table = get_table_start(regions)

    # Statistical uncertainties
    for s in systematics:
        the_line = 'MCstat. ' + get_sys_title(s)
        for r in regions:
            plot_key = 'hist_'+channel+'_VV_'+r+'_mBB'
            if debug:
                print(plot_key)
            if not is_used_region(r, channel):
                unc_up, unc_down = 'N/A', 'N/A'
            else:
                unc_up, unc_down = get_stat_uncertainty(combined_hists, plot_key, s)

            if not isinstance(unc_up, float):
                the_line += ' & N/A '
            elif unc_down == unc_up:
                the_line += ' & $\\pm ' + str('%2.2f'%unc_up) + ' $ \\% '
            else:
                the_line += ' & $+ ' + str('%2.2f'%unc_up) + ' /\\text{-} ' + str('%2.2f'%unc_down) + '$ \\% '
        the_line += '\\\\ \n'
        the_table += the_line

        
    the_table += '\\hline \n'
    the_table += '\\hline \n'

    # Yield Variations
    for s in systematics:
        the_line = get_sys_title(s) + ' Yield'
        if s == 'Nominal':
            continue
        for r in regions:
            plot_key = 'hist_'+channel+'_VV_'+r+'_mBB'
            if debug:
                print(plot_key)
            if not is_used_region(r, channel):
                unc_up, unc_down = 'N/A', 'N/A'
            else:
                unc_up, unc_down = get_yield_variation(combined_hists, plot_key, s)

            if not isinstance(unc_up, float):
                the_line += ' & N/A '
            elif unc_down == unc_up:
                the_line += ' & $\\pm ' + str('%2.2f'%abs(unc_up)) + ' $ \\% '
            elif unc_up>0 and unc_down<0:
                the_line += ' & $+ ' + str('%2.2f'%unc_up) + ' /\\text{-} ' + str('%2.2f'%abs(unc_down)) + '$ \\% '
            elif unc_up>0 and unc_down>0:
                the_line += ' & $+ ' + str('%2.2f'%unc_up) + ' /+ ' + str('%2.2f'%unc_down) + '$ \\% '
            elif unc_up<0 and unc_down<0:
                the_line += ' & $\\text{-} ' + str('%2.2f'%abs(unc_up)) + ' /\\text{-} ' + str('%2.2f'%abs(unc_down)) + '$ \\% '
            elif unc_up<0 and unc_down>0:
                the_line += ' & $\\text{-} ' + str('%2.2f'%abs(unc_up)) + ' /+ ' + str('%2.2f'%unc_down) + '$ \\% '
        the_line+='\\\\ \n'
        the_table += the_line
    
    the_table += get_table_end(caption)

    if debug:
        print(the_table)
    return the_table
    
if __name__ == '__main__':
                    
    if len(sys.argv) > 1 and not do_fast:
        # If manual channel is input, only run this one
        all_hists = all_hists.replace('all_hists.root','all_hists_'+sys.argv[1]+'Lep.root')
        channels = [str(sys.argv[1])+'Lep']
    
    # Get histograms from relevant files
    filenames = []
    
    for fn in all_samples.values():
        for c in channels:
            filenames.append(fn.replace('XLep',c))
        
    dsids = []
    print('Loading ' + str(len(filenames)) + ' files:')
    for fn in filenames:
        print(fn)
        if get_file_dsid(fn) not in dsids:
            dsids.append(get_file_dsid(fn))

    if debug:
        print('Including the following DSIDs:')
        for d in dsids:
            print(d)

    # Create output directory if non-existent
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)


    # Populate list of needed histograms
    for sn, fn in nominal_samples.items():
        dsid = sn[0:6]
        process = dsid_to_process[dsid]
        for c in channels:
            for j in jets:
                for pt in pt_ranges:
                    for r in regions:
                        for v in variables:
                            # 150 GeV MET cut in 0Lep channel
                            if pt == '75_150' and c == '0Lep':
                                continue
                            if c == '0Lep' and v == 'pTV':
                                v  = 'MET'
                            # Nominal histograms
                            plot_key = get_plot_key(c, process, j, pt, v, r)
                            h_name = get_hist_name(plot_key, dsid, 'Nominal')
                            needed_hists.append(h_name)

                            # Systematic variations
                            for s in systematics:
                                h_name_variations = get_variation_names(plot_key, s)
                                for hn in h_name_variations:
                                    if hn not in needed_hists:
                                        needed_hists.append(hn)

                            # Reference results
                            h_name_variations = get_variation_names(plot_key, 'ref')
                            for hn in h_name_variations:
                                if hn not in needed_hists:
                                    needed_hists.append(hn)
    
    if debug and verbose:
        print('Needed histograms:')
        for h in needed_hists:
            print('\t' + h)
        
    # Get relevant histograms
    hists = get_histograms(filenames, do_fast)

    if hists == None:
        print('ERROR!! HISTOGRAM CONTAINER EMPTY, EXITING...')
        sys.exit()
        
    if verbose:
        print('Histograms:')
        for hn, h in hists.items():
            print(hn + ' : ' + str(h))

    if not do_fast:
        print('Saved all relevant histogram files to single file; exiting.')
        sys.exit()


    # Combine relevant histograms
    combined_hists = {}
    for c in channels:
        for p in processes:
            for l in levels:
                for v in variables:
                    for s in systematics:
                        if debug:
                            print('Combining:')
                            print(p,c,l,v,s)
                        combined_hists.update(combine_histograms(hists, p, c, l, v, s))
                        if verbose:
                            print('After: '+s)
                            for key, h in combined_hists.items():
                                print('\t'+key+': '+str(h))
                    if debug:
                        print('Combining:')
                        print(p,c,l,v,'ref')
                    combined_hists.update(combine_histograms(hists, p, c, l, v, 'ref'))
                    if verbose:
                        print('After sys loop: '+v)
                        for key, h in combined_hists.items():
                            print('\t'+key+': '+str(h))

    if verbose:
        print('Plots obtained:')
        for key, h in combined_hists.items():
            print('\t'+key+': '+str(h))

    # Combine relevant processes for diboson (VV) histograms
    # combined_hists = {}
    # for c in channels:
    #     for j in jets:
    #         for pt in pt_ranges:
    #             for r in regions:
    #                 for v in variables:
    #                     # 150 GeV MET cut in 0Lep channel
    #                     if pt == '75_150' and c == '0Lep':
    #                         continue
    #                     if c == '0Lep' and v == 'pTV':
    #                         v  = 'MET'
    #                     for s in systematics:
    #                         combined_hists.update(combine_processes(hists, get_plot_key(c,'VV',j,pt,v,r), s))
    #                     # Combine reference plots
    #                     combined_hists.update(combine_processes(hists, get_plot_key(c,'VV',j,pt,v,r), 'ref'))
    # if debug:
    #     for c in combined_hists:
    #         print(c)

    # Produce tables --> run before plotting shapes to avoid scaling to unity
    if do_tables:

        # Overall normalizations
        print(overall_norm_table(combined_hists))
        
        # UEPS
        print(UEPS_table(combined_hists))

        # Double ratio tables
        print(XL0L_table(combined_hists))

        # SR <--> CR
        print(SRCR_table(combined_hists))
        
        sys.exit()
    
    
    # Produce plots
    print('Plotting!')

    for c in channels:
        for p in processes:
            for l in levels:
                for v in variables:
                    for s in systematics:
                        print('Plotting:')
                        print(p,c,l,v,s)
                        plot_variations(combined_hists, p, c, l, v, s)
                            
    print('Done!')
