import os
import sys
import glob
import argparse
import ROOT
import numpy as np
import pandas as pd
import plot_utils

in_dir = '/unix/atlastracking/srettie/quantum_computing/'
out_dir = 'plots'
# recalculate efficiencies/purities (this is slow)
force_calculation = False
# Selections; |eta| < 5, |delta_phi/delta_rho| < 0.0006, |z0| < 100 mm
# no eta selection has no effect because eta cut is at 5 and we're only selecting barrel hits by construction
selections = [
    'full',
    'no_edge_eta_selection',
    # 'no_edge_ratio_selection',
    # 'no_edge_z0_selection',
    'no_edge_eta_ratio_selection',
    'no_edge_eta_z0_selection',
    'no_edge_selection',
    # 'no_edge_ratio_z0_selection',
]
labels = {
    'full'                        : '#left|#Delta#phi / #Deltar#right| < 6#times10^{-4}, #left|z_{0}#right| < 100 mm, #left|#eta#right| < 5',
    'no_edge_selection'           : 'No edge selection',
    'no_edge_eta_selection'       : '#left|#Delta#phi / #Deltar#right| < 6#times10^{-4}, #left|z_{0}#right| < 100 mm',
    'no_edge_ratio_selection'     : '#left|z_{0}#right| < 100 mm, #left|#eta#right| < 5',
    'no_edge_z0_selection'        : '#left|#Delta#phi / #Deltar#right| < 6#times10^{-4}, #left|#eta#right| < 5',
    'no_edge_eta_ratio_selection' : '#left|z_{0}#right| < 100 mm',
    'no_edge_eta_z0_selection'    : '#left|#Delta#phi / #Deltar#right| < 6#times10^{-4}',
    'no_edge_ratio_z0_selection'  : '#left|#eta#right| < 5',
}
colors = {
    'full'                        : ROOT.kBlack,
    'no_edge_selection'           : ROOT.kBlue,
    'no_edge_eta_selection'       : ROOT.kRed,
    'no_edge_ratio_selection'     : ROOT.kGreen,
    'no_edge_z0_selection'        : ROOT.kCyan,
    'no_edge_eta_ratio_selection' : ROOT.kGreen+1,
    'no_edge_eta_z0_selection'    : ROOT.kViolet,
    'no_edge_ratio_z0_selection'  : ROOT.kGray,
}
pd_columns = [
    'edge',
    'label',
    'pseudorapidity',
    'phis',
    'pts',
    'layer',
    'track_length',
    'num_particles_in_event'
]

def get_parser():
    p = argparse.ArgumentParser(description='Make QC plots')
    p.add_argument('-i', '--in_dir', dest='in_dir', default=None, help='Input directory where preprocessed numpy files are located')
    p.add_argument('-o', '--out_dir', dest='out_dir', default=None, help='Directory where plots are saved')
    return p

def get_num_true_edges(labels):
    return np.count_nonzero(labels == 1)

def get_efficiency(labels_sel, labels_all):
    return 100 * get_num_true_edges(labels_sel) / get_num_true_edges(labels_all)

def get_purity(labels):
    return 100 * get_num_true_edges(labels) / len(labels)

def generate_efficiency(f_name, selection, out_name):
    # get pandas df from numpy array
    labels = pd.DataFrame(np.load(f_name, allow_pickle = True), columns = pd_columns)['label']
    labels_all = pd.DataFrame(np.load(f_name.replace(selection, 'no_edge_selection'), allow_pickle = True), columns = pd_columns)['label']
    eff = get_efficiency(labels, labels_all)
    with open(out_name, 'w') as f:
        f.write(str(eff))
    return eff

def generate_purity(f_name, selection, out_name):
    # get pandas df from numpy array
    labels = pd.DataFrame(np.load(f_name, allow_pickle = True), columns = pd_columns)['label']
    pur = get_purity(labels)
    with open(out_name, 'w') as f:
        f.write(str(pur))
    return pur

def get_eff_pur(selection):
    # return efficiencies and purities for given selection
    efficiencies, purities = [], []
    files_path = os.path.join(in_dir, f'preprocessed_{selection}', '*.npy')
    print(f'files_path: {files_path}')
    files = glob.glob(files_path)
    for f in files:
        print(f'file: {f}')
        # check if efficiencies/purities file exists already (much faster)
        eff_file = f.replace('edges_preprocessed_', 'eff_').replace('.npy', '.txt')
        pur_file = f.replace('edges_preprocessed_', 'pur_').replace('.npy', '.txt')
        if force_calculation or not os.path.exists(pur_file):
            generate_purity(f, selection, pur_file)
        if force_calculation or not os.path.exists(eff_file):
            generate_efficiency(f, selection, eff_file)
        with open(eff_file) as f:
            for l in f:
                print(f'efficiency: {l}')
                efficiencies.append(float(l))
        with open(pur_file) as f:
            for l in f:
                print(f'purity: {l}')
                purities.append(float(l))
        sys.stdout.flush()
    # sanity check
    if len(efficiencies) != len(purities):
        raise RuntimeError('Non-matching list lengths!')
    return efficiencies, purities

def print_array_information(p):
    print(f'Minimum: {min(p)}')
    print(f'Maximum: {max(p)}')
    print(f'Average: {np.average(p)}')
    print(f'Standard deviation: {np.std(p)}')
    print()

def compare_histograms(selections, histograms, out_name):
    # canvas
    cnv = ROOT.TCanvas()
    cnv.cd()
    # legend
    # lx1,ly1,lx2,ly2 = 0.35,0.6,0.9,0.9
    lx1,ly1,lx2,ly2 = 0.2,0.6,0.7,0.9
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    for s in selections:
        leg.AddEntry(histograms[s], f'{labels[s]}', 'l')
    # aesthetics
    y_max = max([h.GetMaximum() for h in histograms.values()])
    for s in selections:
        histograms[s].SetMaximum(1.05 * y_max)
        histograms[s].SetLineColor(colors[s])
        if s in ['no_edge_eta_selection']:
            histograms[s].SetLineStyle(2)
    # draw and save
    for s in selections:
        histograms[s].Draw('hist, same')
    leg.Draw('same')
    cnv.SaveAs(os.path.join(out_dir, f'{out_name}.pdf'))

def plot_efficiencies_purities(selections):
    efficiencies, purities = {}, {}
    h_eff, h_pur = {}, {}
    nbins_eff, min_eff, max_eff = 20, 98.5, 100.5
    nbins_pur, min_pur, max_pur = 65, 0, 65
    for s in selections:
        # get efficiencies and purities
        efficiencies[s], purities[s] = get_eff_pur(s)
        print(f'{s} efficiency:')
        print_array_information(efficiencies[s])
        print(f'{s} purity:')
        print_array_information(purities[s])
        # initialize histograms
        h_eff[s] = ROOT.TH1D(f'h_eff_{s}', f'h_eff_{s};Edge Efficiency [%];Count', nbins_eff, min_eff, max_eff)
        h_pur[s] = ROOT.TH1D(f'h_pur_{s}', f'h_pur_{s};Edge Purity [%];Count', nbins_pur, min_pur, max_pur)
        # fill histograms
        for i in range(len(purities[s])):
            h_eff[s].Fill(efficiencies[s][i])
            h_pur[s].Fill(purities[s][i])
    compare_histograms(selections, h_eff, 'edge_efficiency')
    compare_histograms(selections, h_pur, 'edge_purity')


if __name__ == '__main__':
    # purities obtained from the log files
    parser = get_parser()
    args = parser.parse_args()
    if args.out_dir:
        out_dir = args.out_dir
    os.makedirs(out_dir, exist_ok = True)
    if args.in_dir:
        in_dir = args.in_dir
    # compare efficiencies and purities for edges obtained with and without selections
    plot_efficiencies_purities(selections)