import os
import sys
import ROOT
import glob
import datetime as dt
import subprocess
import plot_utils
import tracking_plots
import reweight_trees
sys.path.append(os.path.join(os.path.dirname(__file__),'..','batch'))
import process_trees
import tracking_submitJobs
import produce_roc_curve

debug = True
verbose = False
do_fast = True
do_submit = False
do_hadd = False
do_reweight = True

good_samples = tracking_submitJobs.good_samples

pt_bins_titles = {
    '0pT150'     : ' (p_{T} < 150 GeV)',
    '150pT400'   : ' (150 GeV < p_{T} < 400 GeV)',
    '400pT1000'  : ' (400 GeV < p_{T} < 1.0 TeV)',
    '1000pT1750' : ' (1.0 TeV < p_{T} < 1.75 TeV)',
    '1750pT2750' : ' (1.75 TeV < p_{T} < 2.75 TeV)',
    '2750pTInf'  : ' (p_{T} > 2.75 TeV)',
}

def hadd_roc_curves(samples, tracks):
    # Save all relevant ROC curves to convenient file   
    print('About to hadd ROC curves for {} samples:'.format(len(samples)))
    cmd = 'hadd -f '+tree_dir+'/all_roc_curves.root '
    for sample in samples:
        if sample not in good_samples:
                continue
        cmd += tree_dir+'/'+sample+'/*/roc_*.root '
    if not do_submit:
        print(cmd)
    else:
        subprocess.call(cmd,shell=True)

def produce_roc_curves(samples, classifiers, tracks, n, cut):
    print('Generating ROC curves!')
    nJobs = 0
    for sample in samples:
        print(sample)
        if sample not in good_samples:
            continue
        for j in tracking_submitJobs.jet_types:
            for classifier in classifiers:
                for other_jet in ['l','c']:
                    for track_type in tracks:
                        cmd = 'python /home/srettie/MyScripts/batch/produce_roc_curve.py -s {} -t {} -c {} -n {} -j {} -o {} --cut {} -td {}'.format(sample, track_type, classifier, n, j, other_jet, cut, tree_dir)
                        j_name = 'roc_{}_{}_{}_{}_{}'.format(other_jet, track_type, classifier, j, sample)
                        tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
                        tracking_submitJobs.torque_submit(cmd+' --do_isojet','',j_name+'_isojet',do_submit)
                        tracking_submitJobs.torque_submit(cmd+' --do_1bH','',j_name+'_1bH',do_submit)
                        tracking_submitJobs.torque_submit(cmd+' --do_isojet --do_1bH','',j_name+'_isojet_1bH',do_submit)
                        nJobs += 4
                        if do_reweight and 'mc16_13TeV.427081' in sample:
                            for rw in ['','_bcl','_bcl_2d','_bcl_2d_bH','_bcl_3d']:
                                cmd_rw = cmd + ' -rw {}'.format(reweight_trees.reweight_quantity+rw)
                                j_name_rw = j_name + '_rw_{}'.format(reweight_trees.reweight_quantity+rw)
                                tracking_submitJobs.torque_submit(cmd_rw,'',j_name_rw,do_submit)
                                tracking_submitJobs.torque_submit(cmd_rw+' --do_isojet','',j_name_rw+'_isojet',do_submit)
                                tracking_submitJobs.torque_submit(cmd_rw+' --do_1bH','',j_name_rw+'_1bH',do_submit)
                                tracking_submitJobs.torque_submit(cmd_rw+' --do_isojet --do_1bH','',j_name_rw+'_isojet_1bH',do_submit)
                                nJobs += 4
                        for ptbin in process_trees.pt_bins:
                            cmd = 'python /home/srettie/MyScripts/batch/produce_roc_curve.py -s {} -t {} -c {} -n {} -j {} -o {} --cut {} -td {} -pt {}'.format(sample, track_type, classifier, n, j, other_jet, cut, tree_dir, ptbin)
                            j_name = 'roc_{}_{}_{}_{}_{}_{}'.format(other_jet, track_type, classifier, j, sample, ptbin)
                            tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
                            tracking_submitJobs.torque_submit(cmd+' --do_isojet','',j_name+'_isojet',do_submit)
                            tracking_submitJobs.torque_submit(cmd+' --do_1bH','',j_name+'_1bH',do_submit)
                            tracking_submitJobs.torque_submit(cmd+' --do_isojet --do_1bH','',j_name+'_isojet_1bH',do_submit)
                            nJobs += 4
                            if do_reweight and 'mc16_13TeV.427081' in sample:
                                for rw in ['','_bcl','_bcl_2d','_bcl_2d_bH','_bcl_3d']:
                                    cmd_rw = cmd + ' -rw {}'.format(reweight_trees.reweight_quantity+rw)
                                    j_name_rw = j_name + '_rw_{}'.format(reweight_trees.reweight_quantity+rw)
                                    tracking_submitJobs.torque_submit(cmd_rw,'',j_name_rw,do_submit)
                                    tracking_submitJobs.torque_submit(cmd_rw+' --do_isojet','',j_name_rw+'_isojet',do_submit)
                                    tracking_submitJobs.torque_submit(cmd_rw+' --do_1bH','',j_name_rw+'_1bH',do_submit)
                                    tracking_submitJobs.torque_submit(cmd_rw+' --do_isojet --do_1bH','',j_name_rw+'_isojet_1bH',do_submit)
                                    nJobs += 4
    print('All done! Submitted a total of {} jobs.'.format(nJobs))

def draw_roc_curves(sample, jet_type, classifier, tracks, n, ptbin='', reweight_quantity='', do_isojet = False, do_1bH = False):
    simple_name = tracking_submitJobs.get_simple_name(sample)
    base_name = simple_name + '_' + jet_type + '_roc_' + classifier + '_'

    roc_curves = {}
    # Get ROC curves
    f_all_roc_curves = ROOT.TFile.Open(all_roc_curves)
    if not f_all_roc_curves:
        print('ERROR! File not found: {}'.format(all_roc_curves))
        sys.exit()

    for other_jet in ['l','c']:
        for track_type in tracks:
            g_name = produce_roc_curve.get_roc_name(sample, jet_type, track_type, classifier, other_jet, '', do_isojet, do_1bH)
            if track_type == 'extendedZp':
                g_name = produce_roc_curve.get_roc_name(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', classifier, other_jet, reweight_quantity, do_isojet, do_1bH)
            if ptbin != '':
                g_name += '_' + ptbin
            roc_curves[g_name] = f_all_roc_curves.Get(g_name)
    f_all_roc_curves.Close()

    if debug:
        print('Obtained {} roc curves:'.format(len(roc_curves)))
        for rc in roc_curves:
            print(rc)

    for other_jet in ['l','c']:
        cnv = ROOT.TCanvas()
        cnv.cd()
        # Legend
        lx1,ly1,lx2,ly2 = 0.21,0.45,0.6,0.65
        #lx1,ly1,lx2,ly2 = 0.51,0.25,0.9,0.45
        leg = ROOT.TLegend(lx1, ly1, lx2, ly2)

        # Get maxima for drawing
        xmax, ymax = 0, 0
        for track_type in tracks:
            g_name = produce_roc_curve.get_roc_name(sample, jet_type, track_type, classifier, other_jet, '', do_isojet, do_1bH)
            if track_type == 'extendedZp':
                g_name = produce_roc_curve.get_roc_name(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', classifier, other_jet, reweight_quantity, do_isojet, do_1bH)
            if ptbin != '':
                g_name += '_' + ptbin
            if verbose: print('Checking graph: {}'.format(g_name))
            if not roc_curves[g_name] or roc_curves[g_name].ClassName() != 'TGraph':
                continue
            if roc_curves[g_name].GetPointX(0) > xmax:
                xmax = roc_curves[g_name].GetPointX(0)
            if roc_curves[g_name].GetPointY(0) > ymax:
                ymax = roc_curves[g_name].GetPointY(0)

        first_drawn = False
        for track_type in tracks:
            g_name = produce_roc_curve.get_roc_name(sample, jet_type, track_type, classifier, other_jet, '', do_isojet, do_1bH)
            if track_type == 'extendedZp':
                g_name = produce_roc_curve.get_roc_name(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', classifier, other_jet, reweight_quantity, do_isojet, do_1bH)
            if ptbin != '':
                g_name += '_' + ptbin
            if verbose: print('Drawing graph: {}'.format(g_name))
            if not roc_curves[g_name] or roc_curves[g_name].ClassName() != 'TGraph':
                print(g_name)
                print('\t--> Not actually an existing TGraph!!')
                leg.AddEntry(roc_curves[g_name], tracking_plots.legend_titles[track_type] + ' (N/A)', 'l')
                continue
            
            roc_curves[g_name].SetLineColor(tracking_plots.colors[track_type])
            if not first_drawn:
                roc_curves[g_name].Draw('AL')
                roc_curves[g_name].GetXaxis().SetLimits(0,xmax)
                roc_curves[g_name].GetYaxis().SetRangeUser(0,ymax)                
                roc_curves[g_name].Draw('AL')
                first_drawn = True
            else:
                roc_curves[g_name].Draw('SAME')
            leg.AddEntry(roc_curves[g_name], tracking_plots.legend_titles[track_type], 'l')
        leg.SetFillColorAlpha(ROOT.kWhite,0.)
        leg.Draw('same')

        # Labels
        sample_shortName = tracking_plots.get_short_name(sample)
        sample_shortTitle = tracking_plots.get_short_title(sample, jet_type)
        variation_label = ''
        if do_isojet: variation_label += ', dR_{min} > 1.0'
        if do_1bH:    variation_label += ', N_{bH} = 1'

        # Save plots
        if ptbin != '':
            plot_utils.draw_labels(sample_shortTitle, classifier + pt_bins_titles[ptbin] + variation_label)
            plot_name = 'h_' + sample_shortName + '_' + jet_type + '_ROC_b' + other_jet + '_' + classifier + '_' + ptbin + '.pdf'
        else:
            plot_utils.draw_labels(sample_shortTitle, classifier + variation_label)
            plot_name = 'h_' + sample_shortName + '_' + jet_type + '_ROC_b' + other_jet + '_' + classifier + '.pdf'
        if 'extendedZp' in tracks:  plot_name = plot_name.replace('.pdf','_withExtended.pdf')
        if reweight_quantity != '': plot_name = plot_name.replace('.pdf','_rw_{}.pdf'.format(reweight_quantity))
        if do_isojet: plot_name = plot_name.replace('.pdf','_isojet.pdf')
        if do_1bH: plot_name = plot_name.replace('.pdf','_1bH.pdf')
        cnv.SaveAs(plot_dir + plot_name)
        cnv.Close()

if __name__ == "__main__":
    if len(sys.argv) == 2:
        tree_dir = sys.argv[1]
        out_dir  = '/unix/atlasvhbb2/srettie/tracking/plots/'
    elif len(sys.argv) >= 3:
        tree_dir = sys.argv[1]
        out_dir  = sys.argv[2]
    else:
        # Default directories
        tree_dir = '/unix/atlasvhbb2/srettie/tracking/athenaOutputs_full/'
        out_dir  = '/unix/atlasvhbb2/srettie/tracking/plots/'

    t_0 = dt.datetime.now()

    # Create output directory if non-existent
    if not os.path.exists(out_dir): os.mkdir(out_dir)

    n = 300
    my_cut = '1'#'jet_sv1_Nvtx > 0'

    samples = [s.split('/')[-1] for s in glob.glob(tree_dir+'/*') if s.split('/')[-1].split('_')[-1] == 'EXT0' or s.split('/')[-1].split('.')[0] == 'mc16_13TeV']

    print('Found {} samples:'.format(len(samples)))
    for s in samples:
        print('\t{}'.format(s))

    all_roc_curves = tree_dir+'/all_roc_curves.root'

    tracks = tracking_submitJobs.tracks

    if do_hadd:
        print('hadding histogram files')
        hadd_roc_curves(samples, tracks)
        sys.exit()

    if not do_fast:
        produce_roc_curves(samples, produce_roc_curve.classifiers, tracks, n, my_cut)
        sys.exit()

    print('Plotting ROC curves!')
    if debug: print('All ROC curves: {}'.format(all_roc_curves))
    # Produce plots
    for sample in samples:
        if sample not in good_samples:
            print('Skipping: {}'.format(sample))
            continue
        print('Plotting ROC curves for: {}'.format(sample))
        for j in tracking_submitJobs.jet_types:
            base_path = out_dir + tracking_plots.get_short_name(sample)
            if 'custom_ipxd' in tree_dir: base_path += '_custom_ipxd'
            if not os.path.exists(base_path): os.mkdir(base_path)
            base_path += '/'+j+'/'
            if not os.path.exists(base_path): os.mkdir(base_path)
            print('Saving plots in: {}'.format(base_path))
            for classifier in produce_roc_curve.classifiers:
                for ptbin in list(process_trees.pt_bins) + ['']:
                    to_enum = [
                        ['nom','pseudo','ideal','extendedZp'],
                        ['nom','nom_RF75','nom_RF75_replaceWithTruth','nom_RF90'],
                        ['selectHF','selectHF_replaceWithTruth'],
                        ['nom','nom_replaceHFWithTruth','nom_replaceFRAGWithTruth','nom_replaceFRAGHFWithTruth','nom_replaceFRAGHFGEANTWithTruth','nom_replaceWithTruth','pseudo','ideal'],
                        ['nom','pseudo','ideal','nom_RF75','selectFRAG','selectFRAGHF','selectFRAGHFGEANT'],
                        ['nom','pseudo','ideal','nom_RFMVA_loose','nom_RFMVA_tight','nom_RF75']
                    ]
                    # Extended Z' sample only has nom and RFMVA
                    if 'mc16_13TeV' in sample:
                        to_enum = [['nom','nom_RFMVA_loose','nom_RFMVA_tight']]
                    for i, tracks in enumerate(to_enum):
                        if i > 0: continue#
                        if i == 0 : plot_dir = base_path + '/nom_pseudo_ideal/'
                        if i == 1 : plot_dir = base_path + '/nom_vs_RF75/'
                        if i == 2 : plot_dir = base_path + '/selectHF/'
                        if i == 3 : plot_dir = base_path + '/selectFRAGHF/'
                        if i == 4 : plot_dir = base_path + '/selectFRAGHF_vs_RF75/'
                        if i == 5 or 'mc16_13TeV' in sample: plot_dir = base_path + '/RFMVA/'
                        if not os.path.exists(plot_dir): os.mkdir(plot_dir)
                        if debug: print('Plotting ROC curve for sample {} ({}), classifier {}, using {} points for tracks {}, with cut {} and pT bin {}'.format(sample, j, classifier, n, tracks, my_cut, ptbin))
                        draw_roc_curves(sample, j, classifier, tracks, n, ptbin)
                        if 'extendedZp' in tracks:
                            # draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt')
                            # draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl')
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_2d')
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_2d', do_isojet = True,  do_1bH = False)
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_2d', do_isojet = False, do_1bH = True)
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_2d', do_isojet = True,  do_1bH = True)
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_2d_bH')
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_2d_bH', do_isojet = True,  do_1bH = False)
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_2d_bH', do_isojet = False, do_1bH = True)
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_2d_bH', do_isojet = True,  do_1bH = True)
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_3d')
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_3d', do_isojet = True,  do_1bH = False)
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_3d', do_isojet = False, do_1bH = True)
                            draw_roc_curves(sample, j, classifier, tracks, n, ptbin, reweight_quantity='jet_pt_bcl_3d', do_isojet = True,  do_1bH = True)
    if debug: print('Entire ROC curve plotting took {}'.format(dt.datetime.now()-t_0))

    print('Done!')