import os
import ROOT
import pandas as pd
from math import log as ln
from math import sqrt

# Detector info
pixel_layers = {
    'IBL'  : 33.25,
    'BL'   : 50.5,
    'Pix2' : 88.5,
    'Pix3' : 122.5,
}
legend_names = {
    'SM'          : 'SM',
    'k2v0'        : '#kappa_{2V} = 0',
    'k2v0p5'      : '#kappa_{2V} = 0.5',
    'dijet_bfilt' : 'Dijet (b-filt)',
    'ttbar_HT'    : 't#bar{t} (HT)',
    'bkg'         : 'MC background',
    'data_bkg'    : 'DD background',
}

# Use ATLAS Style
def set_ATLAS_style():
    ROOT.gROOT.SetBatch(1)
    ROOT.gROOT.LoadMacro("~/atlasrootstyle/AtlasStyle.C")
    ROOT.gROOT.LoadMacro("~/atlasrootstyle/AtlasLabels.C")
    ROOT.gROOT.LoadMacro("~/atlasrootstyle/AtlasUtils.C")
    ROOT.SetAtlasStyle()

def get_all(d, basepath = ''):
    if not d.InheritsFrom(ROOT.TDirectory.Class()):
        return
    for key in d.GetListOfKeys():
        k_name = key.GetName()
        if key.IsFolder():
            for i in get_all(d.Get(k_name), basepath + k_name + '/'):
                yield i
        else:
            yield basepath + k_name, d.Get(k_name)

def load_from_file(filename, object_class = '', debug = False):
    to_load = {}
    f = ROOT.TFile.Open(filename)
    for k, o in get_all(f):
        h_name = o.GetName()
        h_class = o.ClassName()
        if debug:
            print('Found ' + h_class + ': ' + h_name + ' (' + k + ')')
        if object_class in h_class:
            to_load[h_name] = f.Get(k).Clone()
            if 'TH1' in h_class:
                to_load[h_name].SetDirectory(0)
    f.Close()
    return to_load

# bin = 0; underflow bin
# bin = 1; first bin with low-edge xlow INCLUDED
# bin = nbins; last bin with upper-edge xup EXCLUDED
# bin = nbins+1; overflow bin
def add_overflow(h):
    nentries = h.GetEntries()
    nbinsx = h.GetNbinsX()
    h.SetBinContent(nbinsx, h.GetBinContent(nbinsx)+h.GetBinContent(nbinsx+1))
    # don't want this to change the number of entries
    h.SetEntries(nentries)

def add_underflow(h):
    nentries = h.GetEntries()
    h.SetBinContent(1, h.GetBinContent(1)+h.GetBinContent(0))
    # don't want this to change the number of entries
    h.SetEntries(nentries)

def get_hist_min(h, xrange = None):
    h_min = 9999999.
    for ibin in range(1, h.GetNbinsX()+1):
        if xrange and (h.GetXaxis().GetBinLowEdge(ibin) < xrange[0] or h.GetXaxis().GetBinUpEdge(ibin) > xrange[1]):
            continue
        value = h.GetBinContent(ibin)
        if value < h_min: h_min = value
    return h_min

def get_hist_max(h, xrange = None):
    h_max = 0.
    for ibin in range(1, h.GetNbinsX()+1):
        if xrange and (h.GetXaxis().GetBinLowEdge(ibin) < xrange[0] or h.GetXaxis().GetBinUpEdge(ibin) > xrange[1]):
            continue
        value = h.GetBinContent(ibin)
        if value > h_max: h_max = value
    return h_max

def get_max_bin_height(histograms, xrange = None):
    return max([get_hist_max(h, xrange) for h in histograms])

def get_min_bin_height(histograms, xrange = None):
    return min([get_hist_min(h, xrange) for h in histograms])

def get_extrema(histograms, xrange_min, xrange_max):
    h_min, h_max = 9999999., 0.
    for h in histograms:
        start_bin, end_bin = h.GetXaxis().FindBin(xrange_min), h.GetXaxis().FindBin(xrange_max)
        for ibin in range(start_bin, end_bin):
            value = h.GetBinContent(ibin)
            if value < h_min: h_min = value
            if value > h_max: h_max = value
    return h_min, h_max

def get_stat_err_hist(h, color = ROOT.kBlack):
    h_err = h.Clone(f'{h.GetName()}_err')
    h_err.SetFillColorAlpha(color, 1)
    h_err.SetFillStyle(3004)
    h_err.SetMarkerSize(0)
    h_err.SetDirectory(0)
    return h_err

def get_unit_normalized_hist(h):
    h_normalized = h.Clone(f'{h.GetName()}_scaled')
    if h.Integral() > 0:
        h_normalized.Scale(1./h.Integral())
    h_normalized.GetYaxis().SetTitle('a.u.')
    h_normalized.SetFillStyle(0)
    h_normalized.SetDirectory(0)
    return h_normalized

def draw_labels(plot_key, variation):
    # Draw ATLAS label and luminosity label
    labelx = 0.21
    labely = 0.87
    offset = 0.06
    ROOT.ATLASLabel(labelx, labely, "Internal")
    ROOT.myText(labelx, labely-offset, 1, '#sqrt{s} = 13 TeV')

    # Extra labels for selection
    extraLabel = ''
    extraLabel += plot_key.split('/')[-1].replace('hist_','')
    if extraLabel != '':
        ROOT.myText(labelx, labely-2*offset, 1, extraLabel)

    variationLabel = ''
    variationLabel += variation
    if variationLabel != '':
        ROOT.myText(labelx, labely-3*offset, 1, variationLabel)

def plot_2D(out_dir, histogram, h_name, short_title = '', variation_label = '', debug = False, contours = None):
    # Generic method to plot 2D histogram
    cnv = ROOT.TCanvas()
    cnv.cd()
    cnv.SetRightMargin(0.16)
    # Draw plot
    histogram.Draw('colz')
    draw_labels(short_title, variation_label)
    # Draw contours
    if contours:
        for c in contours:
            c.Draw('cont3,same')
    # Save plot
    out_name = f'{h_name}.pdf'
    cnv.SaveAs(os.path.join(out_dir, out_name))
    cnv.Close()

def plot_tgraphs(out_dir, tgs, descriptors, tg_name, short_title = '', variation_label = '', hline = None, debug = False):
    # Generic method to plot TGraphs
    if debug:
        print(f'Plotting: {tg_name}')

    cnv = ROOT.TCanvas()
    cnv.cd()

    # Font style
    x_label_font,y_label_font = 43,43
    x_title_font,y_title_font = 43,43
    # Sizes in pixels
    x_title_size,y_title_size = 14,14
    x_label_size,y_label_size = 12,12
    # Offsets relative to axis
    x_title_offset = 4.0
    y_title_offset = 1.6

    # Get plot limits
    x_min = min([ROOT.TMath.MinElement(len(tg.GetX()), tg.GetX()) for tg in tgs])
    x_max = max([ROOT.TMath.MaxElement(len(tg.GetX()), tg.GetX()) for tg in tgs])
    y_min = min([ROOT.TMath.MinElement(len(tg.GetY()), tg.GetY()) for tg in tgs])
    y_max = max([ROOT.TMath.MaxElement(len(tg.GetY()), tg.GetY()) for tg in tgs])

    # Legend
    lx1,ly1,lx2,ly2 = 0.7,0.6,0.9,0.9
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)

    # Draw tgs
    for i, (tg, tg_desc) in enumerate(zip(tgs, descriptors)):
        if i == 0:
            # Labels
            tg.GetXaxis().SetLabelFont(x_label_font)
            tg.GetYaxis().SetLabelFont(y_label_font)
            tg.GetXaxis().SetLabelSize(x_label_size)
            tg.GetYaxis().SetLabelSize(y_label_size)
            # Titles
            tg.GetXaxis().SetTitleFont(x_title_font)
            tg.GetXaxis().SetTitleSize(x_title_size)
            tg.GetYaxis().SetTitleFont(y_title_font)
            tg.GetYaxis().SetTitleSize(y_title_size)
            # Limits
            tg.GetXaxis().SetLimits(x_min, x_max)
            tg.GetYaxis().SetRangeUser(0.9 * y_min, 1.5 * y_max)
            tg.Draw('ALP')
        else:
            tg.Draw('LP,same')
        leg.AddEntry(tg, legend_names[tg_desc])
    leg.Draw('same')

    # Labels
    draw_labels(short_title, variation_label)

    # Horizontal line
    if hline:
        myLine = ROOT.TLine()
        myLine.SetLineColor(ROOT.kRed)
        myLine.DrawLine(x_min,hline,x_max,hline)
    
    # Save plots
    out_name = f'{tg_name}.pdf'
    cnv.SaveAs(os.path.join(out_dir, out_name))
    cnv.Close()

def plot_single_hist(out_dir, histogram, h_name, short_title = '', variation_label = '', debug = False, show_entries = False, plot_stat_err = False, x_range = None, y_range = None):
    # Generic method to plot single histograms
    # Scaling and axis range setting should already be done
    if debug:
        print(f'Plotting: {h_name}')

    cnv = ROOT.TCanvas()
    cnv.cd()

    # Font style
    x_label_font,y_label_font = 43,43
    x_title_font,y_title_font = 43,43
    # Sizes in pixels
    x_title_size,y_title_size = 14,14
    x_label_size,y_label_size = 12,12
    # Offsets relative to axis
    x_title_offset = 4.0
    y_title_offset = 1.6

    # Labels
    histogram.GetXaxis().SetLabelFont(x_label_font)
    histogram.GetYaxis().SetLabelFont(y_label_font)
    histogram.GetXaxis().SetLabelSize(x_label_size)
    histogram.GetYaxis().SetLabelSize(y_label_size)
    # Titles
    histogram.GetXaxis().SetTitleFont(x_title_font)
    histogram.GetXaxis().SetTitleSize(x_title_size)
    histogram.GetYaxis().SetTitleFont(y_title_font)
    histogram.GetYaxis().SetTitleSize(y_title_size)

    # Draw histogram
    histogram.SetMaximum(1.5 * histogram.GetMaximum())
    if x_range:
        histogram.GetXaxis().SetRangeUser(x_range[0], x_range[1])
    if y_range:
        histogram.SetMinimum(y_range[0])
        histogram.SetMaximum(y_range[1])
    histogram.Draw('hist')

    # Draw statistical error
    if plot_stat_err:
        h_err = get_stat_err_hist(histogram, histogram.GetLineColor())
        h_err.Draw('e2,same')

    # Draw pixel layer lines for Lxy plots
    if 'jet_sv1_Lxy' in h_name:
        plot_ymax = histogram.GetMaximum()
        myLine = ROOT.TLine()
        myLine.SetLineColor(ROOT.kBlack)
        myLine.SetLineStyle(2)
        for layer, position in pixel_layers.items():
            myLine.DrawLine(position,0.,position,plot_ymax)
            
    # Labels
    draw_labels(short_title, variation_label)

    # Add number of entries for debugging purposes
    if show_entries:
        nentries = histogram.GetEntries()
        integral = histogram.Integral(0,histogram.GetNbinsX()+1)
        ltx = ROOT.TLatex()
        xpos, ypos = 0.65,0.8
        ltx.DrawLatexNDC(xpos,ypos,f'Entries: {nentries:.0f}')
        ltx.DrawLatexNDC(xpos,ypos-0.1,f'Integral: {integral:.2f}')
    
    # Save plots
    out_name = f'{h_name}.pdf'
    cnv.SaveAs(os.path.join(out_dir, out_name))
    cnv.Close()

def plot_stack(out_dir, stack, h_name, signals = None, signal_keys = None, short_title = '', variation_label = '', debug = False, plot_stat_err = False):
    # Generic method to plot stacked histograms (and optional signal histograms)
    # Scaling and axis range setting should already be done
    if debug:
        print(f'Plotting: {h_name}')

    # Setup pads and canvas
    ratio_frac = 0.3

    cnv = ROOT.TCanvas()
    cnv.cd()
    
    p2 = ROOT.TPad('p2','p2',0.,0.,1.,ratio_frac)
    p2.Draw()
    p2.SetTopMargin(0.03)
    p2.SetBottomMargin(ratio_frac)
    
    p1 = ROOT.TPad('p1','p1',0.,ratio_frac,1.,1.)
    p1.Draw()
    p1.SetBottomMargin(0.03)
    p1.cd()

    # Font style
    x_label_font,y_label_font = 43,43
    x_title_font,y_title_font = 43,43
    # Sizes in pixels
    x_title_size,y_title_size = 14,14
    x_label_size,y_label_size = 12,12
    # Offsets relative to axis
    x_title_offset = 1.4
    y_title_offset = 1.6

    # Draw stack
    p1.cd()
    stack.SetMaximum(1.5 * stack.GetMaximum())
    sig_max = get_max_bin_height(signals)
    if signals and sig_max > stack.GetMaximum():
        stack.SetMaximum(1.5 * sig_max)
    stack.Draw('hist')
    if plot_stat_err:
        stat_errs = {}
        stat_errs['stack'] = get_stat_err_hist(stack.GetStack().Last(), ROOT.kGray)
        stat_errs['stack'].Draw('e2,same')
    # Labels
    stack.GetXaxis().SetLabelFont(x_label_font)
    stack.GetYaxis().SetLabelFont(y_label_font)
    stack.GetXaxis().SetLabelSize(0)
    stack.GetYaxis().SetLabelSize(y_label_size)
    # Titles
    stack.GetXaxis().SetTitleSize(0)
    stack.GetYaxis().SetTitleFont(y_title_font)
    stack.GetYaxis().SetTitleSize(y_title_size)

    if signals:
        for signal, signal_key in zip(signals, signal_keys):
            signal.Draw('hist,same')
            if plot_stat_err:
                stat_errs[signal_key] = get_stat_err_hist(signal, signal.GetLineColor())
                stat_errs[signal_key].Draw('e2,same')

    # Legend
    lx1,ly1,lx2,ly2 = 0.7,0.6,0.9,0.9
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    for h in reversed(stack.GetHists()):
        take_two = 'HT' in h.GetName() or 'bfilt' in h.GetName()
        h_leg_name = h.GetName().split('_')[-1] if not take_two else '_'.join(h.GetName().split("_")[-2:])
        leg.AddEntry(h, legend_names[h_leg_name], 'l')
    if signals:
        if signal_keys:
            for signal, signal_key in zip(signals, signal_keys):
                leg.AddEntry(signal, legend_names[signal_key], 'l')
        else:
            for signal in signals:
                leg.AddEntry(signal, signal.GetName().split('_')[-1], 'l')
    leg.Draw('same')

    # Labels
    draw_labels(short_title, variation_label)

    # Ratio
    p2.cd()
    if signals:
        ratios = []
        for signal in signals:
            r = signal.Clone()
            r.SetDirectory(0)
            r.Divide(stack.GetStack().Last())
            r.GetXaxis().SetLabelFont(x_label_font)
            r.GetYaxis().SetLabelFont(y_label_font)
            r.GetXaxis().SetLabelSize(x_label_size)
            r.GetYaxis().SetLabelSize(y_label_size)
            r.GetYaxis().SetNdivisions(505)
            r.GetYaxis().SetTitle('Ratio to stack')
            r.GetXaxis().SetTitleFont(x_title_font)
            r.GetYaxis().SetTitleFont(y_title_font)
            r.GetXaxis().SetTitleSize(x_title_size)
            r.GetYaxis().SetTitleSize(y_title_size)
            r.GetYaxis().SetTitleOffset(y_title_offset)
            r.GetXaxis().SetTitleOffset(x_title_offset)
            r.SetMarkerSize(0.)
            ratios.append(r)        
        ratio_min, ratio_max = get_min_bin_height(ratios), get_max_bin_height(ratios)
        for i, r in enumerate(ratios):
            if i == 0:
                r.SetMinimum(0.9 * ratio_min)
                r.SetMaximum(1.1 * ratio_max)
                r.Draw('hist')
            else:
                r.Draw('hist,same')
            if plot_stat_err:
                stat_errs[f'ratio_{i}'] = get_stat_err_hist(r, r.GetLineColor())
                stat_errs[f'ratio_{i}'].Draw('e2,same')
        unityLine = ROOT.TLine()
        unityLine.SetLineColor(ROOT.kBlack)
        xrange_min = r.GetXaxis().GetBinLowEdge(r.GetXaxis().GetFirst())
        xrange_max = r.GetXaxis().GetBinLowEdge(r.GetXaxis().GetLast()+1)
        unityLine.DrawLine(xrange_min,1.0,xrange_max,1.0)

    # Save plots
    out_name = f'{h_name}.pdf'
    cnv.SaveAs(os.path.join(out_dir, out_name))
    cnv.Close()

def get_th1_from_tefficiency(teff):
    th1 = teff.GetCopyPassedHisto()
    th1.Reset()
    for ibin in range(th1.GetNbinsX()+1):
        th1.SetBinContent(ibin, teff.GetEfficiency(ibin))
        th1.SetBinError(ibin, max([teff.GetEfficiencyErrorLow(ibin), teff.GetEfficiencyErrorUp(ibin)]))
    th1.SetDirectory(0)
    th1.SetLineColor(teff.GetLineColor())
    th1.SetLineStyle(teff.GetLineStyle())
    return th1

def plot_efficiencies(out_dir, histograms, descriptors, h_name, short_title = '', variation_label = '', x_range = None, y_range = None, debug = False, ratio_limits = None):
    # Generic method to take efficiency histograms and plot them
    if debug:
        print(f'Plotting: {h_name}')
    # Setup pads and canvas
    ratio_frac = 0.3
    cnv = ROOT.TCanvas()
    cnv.cd()
    p2 = ROOT.TPad('p2','p2',0.,0.,1.,ratio_frac)
    p2.Draw()
    p2.SetTopMargin(0.03)
    p2.SetBottomMargin(ratio_frac)
    p1 = ROOT.TPad('p1','p1',0.,ratio_frac,1.,1.)
    p1.Draw()
    p1.SetBottomMargin(0.03)
    p1.cd()
    # get relevant maxima
    ymin = 0
    # ymax = 1.
    ymax = 1.5 * get_max_bin_height([get_th1_from_tefficiency(h) for h in histograms])
    if y_range:
        ymin = y_range[0]
        ymax = y_range[1]
    xmin = histograms[0].GetTotalHistogram().GetBinLowEdge(histograms[0].GetTotalHistogram().FindFirstBinAbove())
    xmax = histograms[0].GetTotalHistogram().GetBinLowEdge(histograms[0].GetTotalHistogram().FindLastBinAbove()+1)
    if x_range:
        xmin = x_range[0]
        xmax = x_range[1]
    # Font style
    x_label_font,y_label_font = 43,43
    x_title_font,y_title_font = 43,43
    # Sizes in pixels
    x_title_size,y_title_size = 14,14
    x_label_size,y_label_size = 12,12
    # Offsets relative to axis
    x_title_offset = 1.4
    y_title_offset = 1.6
    # Draw
    for i, h in enumerate(histograms):
        if i == 0:
            h.Draw('AP')
            # Set y-axis range
            ROOT.gPad.Update()
            g = h.GetPaintedGraph()
            g.SetMinimum(ymin)
            g.SetMaximum(ymax)
            g.GetXaxis().SetLimits(xmin, xmax)
            # Labels
            g.GetXaxis().SetLabelFont(x_label_font)
            g.GetYaxis().SetLabelFont(y_label_font)
            g.GetXaxis().SetLabelSize(0)
            g.GetYaxis().SetLabelSize(y_label_size)
            # Titles
            g.GetXaxis().SetTitleSize(0)
            g.GetYaxis().SetTitleFont(y_title_font)
            g.GetYaxis().SetTitleSize(y_title_size)
            ROOT.gPad.Update()
        else:
            h.Draw('same')
    # Legend
    lx1,ly1,lx2,ly2 = 0.5,0.65,0.9,0.9
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    leg.SetNColumns(2)
    for h, h_id in zip(histograms,descriptors):
        leg.AddEntry(h, h_id, 'lp')
    leg.Draw('same')
    # Labels
    draw_labels(short_title, variation_label)
    # Ratio
    p2.cd()    
    ratios = [get_th1_from_tefficiency(teff) for teff in histograms]
    for i, r in enumerate(ratios):
        if i > 0:
            r.Divide(ratios[0])
        r.GetXaxis().SetLabelFont(x_label_font)
        r.GetYaxis().SetLabelFont(y_label_font)
        r.GetXaxis().SetLabelSize(x_label_size)
        r.GetYaxis().SetLabelSize(y_label_size)
        r.GetYaxis().SetNdivisions(505)
        r.GetYaxis().SetTitle('Ratio')
        r.GetXaxis().SetTitleFont(x_title_font)
        r.GetYaxis().SetTitleFont(y_title_font)
        r.GetXaxis().SetTitleSize(x_title_size)
        r.GetYaxis().SetTitleSize(y_title_size)
        r.GetYaxis().SetTitleOffset(y_title_offset)
        r.GetXaxis().SetTitleOffset(x_title_offset)
        r.GetXaxis().SetRangeUser(xmin, xmax)
        r.SetMarkerSize(0.)
    if ratio_limits:
        ratio_min, ratio_max = ratio_limits[0], ratio_limits[1]
    elif len(ratios) > 1:
        ratio_min, ratio_max = get_min_bin_height(ratios[1:], xrange = [xmin, xmax]), get_max_bin_height(ratios[1:], xrange = [xmin, xmax])
    else:
        ratio_min, ratio_max = 0.8, 1.2
    ratios_err = []
    if len(histograms) > 1:
        for i, r in enumerate(ratios):
            if i == 0: continue
            elif i == 1:
                r.SetMinimum(0.9 * ratio_min)
                r.SetMaximum(1.1 * ratio_max)
                r.Draw('hist')
            else: r.Draw('hist,same')
            ratios_err.append(get_stat_err_hist(r, r.GetLineColor()))
    else:
        ratios[0].Draw('hist')
        ratios_err.append(get_stat_err_hist(ratios[0], ratios[0].GetLineColor()))
    for r_err in ratios_err:
        r_err.Draw('e2,same')
    unityLine = ROOT.TLine()
    unityLine.SetLineColor(ROOT.kBlack)
    xrange_min = ratios[0].GetXaxis().GetBinLowEdge(ratios[0].GetXaxis().GetFirst())
    xrange_max = ratios[0].GetXaxis().GetBinLowEdge(ratios[0].GetXaxis().GetLast()+1)
    if 0.9 * ratio_min < 1.:
        unityLine.DrawLine(xrange_min,1.0,xrange_max,1.0)
    # Save plots
    out_name = f'{h_name}.pdf'
    cnv.SaveAs(os.path.join(out_dir, out_name))
    cnv.Close()

def plot_histograms(out_dir, histograms, descriptors, h_name, short_title = '', variation_label = '', debug = False, show_entries = False, ratio_limits = None, plot_stat_err = False, x_range = None):
    # Generic method to take histograms and plot them
    # Scaling and axis range setting should already be done
    if debug:
        print(f'Plotting: {h_name}')

    # Setup pads and canvas
    ratio_frac = 0.3

    cnv = ROOT.TCanvas()
    cnv.cd()
    
    p2 = ROOT.TPad('p2','p2',0.,0.,1.,ratio_frac)
    p2.Draw()
    p2.SetTopMargin(0.03)
    p2.SetBottomMargin(ratio_frac)
    
    p1 = ROOT.TPad('p1','p1',0.,ratio_frac,1.,1.)
    p1.Draw()
    p1.SetBottomMargin(0.03)
    p1.cd()

    # Font style
    x_label_font,y_label_font = 43,43
    x_title_font,y_title_font = 43,43
    # Sizes in pixels
    x_title_size,y_title_size = 14,14
    x_label_size,y_label_size = 12,12
    # Offsets relative to axis
    x_title_offset = 1.4
    y_title_offset = 1.6
    # get relevant maximum
    y_max = get_max_bin_height(histograms)

    # Draw
    p1.cd()
    h_errs = []
    for i, h in enumerate(histograms):
        # Labels
        h.GetXaxis().SetLabelFont(x_label_font)
        h.GetYaxis().SetLabelFont(y_label_font)
        h.GetXaxis().SetLabelSize(0)
        h.GetYaxis().SetLabelSize(y_label_size)
        # Titles
        h.GetXaxis().SetTitleSize(0)
        h.GetYaxis().SetTitleFont(y_title_font)
        h.GetYaxis().SetTitleSize(y_title_size)
        if i == 0:
            h.SetMaximum(1.5 * y_max)
            if x_range:
               h.GetXaxis().SetRangeUser(x_range[0], x_range[1])
            h.Draw('hist')
            plot_ymax = h.GetMaximum()
        else:
            h.Draw('hist,same')
        if plot_stat_err:
            h_errs.append(get_stat_err_hist(h, h.GetLineColor()))
    if plot_stat_err:
        for h_err in h_errs:
            h_err.Draw('e2,same')

    # Legend
    lx1,ly1,lx2,ly2 = 0.7,0.6,0.9,0.9
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    for h, h_id in zip(histograms,descriptors):
        if show_entries:
            leg.AddEntry(h, f'{h_id} ({h.GetEntries():.1f}, {h.GetSumOfWeights():.1f})', 'l')
        else:
            leg.AddEntry(h, h_id, 'l')
    leg.Draw('same')

    # Draw pixel layer lines for relevant plots
    if 'jet_sv1_Lxy' in h_name:
        myLine = ROOT.TLine()
        myLine.SetLineColor(ROOT.kBlack)
        myLine.SetLineStyle(2)
        for layer, position in pixel_layers.items():
            myLine.DrawLine(position,0.,position,plot_ymax)

    # Labels
    if show_entries and variation_label == '':
        variation_label = 'MC (Entries, SumOfWeights)'
    draw_labels(short_title, variation_label)

    # Ratio
    p2.cd()    
    ratios = []
    ratios_err = []
    for i, h in enumerate(histograms):
        r = h.Clone()
        r.SetDirectory(0)
        r.Divide(histograms[0])
        r.GetXaxis().SetLabelFont(x_label_font)
        r.GetYaxis().SetLabelFont(y_label_font)
        r.GetXaxis().SetLabelSize(x_label_size)
        r.GetYaxis().SetLabelSize(y_label_size)
        r.GetYaxis().SetNdivisions(505)
        r.GetYaxis().SetTitle('Ratio')
        r.GetXaxis().SetTitleFont(x_title_font)
        r.GetYaxis().SetTitleFont(y_title_font)
        r.GetXaxis().SetTitleSize(x_title_size)
        r.GetYaxis().SetTitleSize(y_title_size)
        r.GetYaxis().SetTitleOffset(y_title_offset)
        r.GetXaxis().SetTitleOffset(x_title_offset)
        r.SetMarkerSize(0.)
        ratios.append(r)
    if ratio_limits:
        ratio_min, ratio_max = ratio_limits[0], ratio_limits[1]
    else:
        ratio_min, ratio_max = get_min_bin_height(ratios), get_max_bin_height(ratios)
    if len(histograms) > 1:
        for i, r in enumerate(ratios):
            if i == 0: continue
            elif i == 1:
                r.SetMinimum(0.9 * ratio_min)
                r.SetMaximum(1.1 * ratio_max)
                if x_range:
                    r.GetXaxis().SetRangeUser(x_range[0], x_range[1])
                r.Draw('hist')
            else: r.Draw('hist,same')
            if plot_stat_err:
                ratios_err.append(get_stat_err_hist(r, r.GetLineColor()))
    else:
        ratios[0].Draw('hist')
        if plot_stat_err:
            ratios_err.append(get_stat_err_hist(ratios[0], ratios[0].GetLineColor()))
    if plot_stat_err:
        for r_err in ratios_err:
            r_err.Draw('e2,same')

    unityLine = ROOT.TLine()
    unityLine.SetLineColor(ROOT.kBlack)
    xrange_min = ratios[0].GetXaxis().GetBinLowEdge(ratios[0].GetXaxis().GetFirst())
    xrange_max = ratios[0].GetXaxis().GetBinLowEdge(ratios[0].GetXaxis().GetLast()+1)
    unityLine.DrawLine(xrange_min,1.0,xrange_max,1.0)

    # Draw pixel layer lines for Lxy plots
    if 'jet_sv1_Lxy' in h_name:
        for layer, position in pixel_layers.items():
            myLine.DrawLine(position,ratio_min,position,ratio_max)

    # Save plots
    out_name = f'{h_name}.pdf'
    cnv.SaveAs(os.path.join(out_dir, out_name))
    cnv.Close()

def get_tree(files, samples, t_name, debug=False):
    t = ROOT.TChain(t_name)
    found_files = False
    for f in files:
        if any(s in f for s in samples):
            if debug: print(f'Adding file to tree: {f}')
            found_files = True
            t.Add(f)
    if not found_files:
        raise ValueError(f'No files found!\nfiles:\n{files}\nsamples:\n{samples}')
    print(f'Got {t.GetEntries()} events for tree {t_name}')
    return t

def get_rdf(files, samples, t_name, debug=False):
    rdf = ROOT.RDataFrame(get_tree(files, samples, t_name, debug).Clone())
    return rdf

def rdf_to_df(rdf, selection, variables):
    # get numpy arrays from rdf
    cols = rdf.Filter(selection).AsNumpy(variables)
    print(f'filtered rdf has entries: {len(cols[list(cols.keys())[0]])}')
    # get df from rdfs
    df = pd.DataFrame()
    for v in cols:
        df[v] = cols[v]
    return df

def get_significance(h_sig, h_bkg, range_min, range_max, debug = False):
    # Loop over histogram distribution
    start_bin = h_sig.FindBin(range_min)
    end_bin = h_sig.FindBin(range_max)
    sig_sum = 0
    for ibin in range(start_bin, end_bin+1):
        isig = h_sig.GetBinContent(ibin)
        ibkg = h_bkg.GetBinContent(ibin)
        if isig > 0 and ibkg > 0:
            sig_sum += 2 * ( (isig+ibkg) * ln(1+(isig/ibkg)) - isig )
    Z = sqrt(sig_sum)
    if debug:
        print(f'\tsignificance: {Z}')
    return Z

def fill_h(h, values, weights = None):
    if weights != None:
        if len(values) != len(weights):
            raise ValueError(f'Incompatible arrays: len(values) = {len(values)}, len(weights) = {len(weights)}')
        for v, w in zip(values, weights):
            h.Fill(v, w)
    else:
        for v in values:
            h.Fill(v)
    h.Sumw2()
    return h

def set_hist_color_style(h, c = ROOT.kBlack, s = 1, set_fill = True):
    h.SetLineColor(c)
    h.SetMarkerColor(c)
    h.SetLineStyle(s)
    if set_fill:
        h.SetFillColor(c)
    # from ATLAS style
    h.SetMarkerStyle(20)
    h.SetMarkerSize(1.2)
    h.SetLineWidth(2)