import ROOT
import plot_utils as pu

triggers = {
    '4b' : 'HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1J45p0ETA21_3J15p0ETA25',
    '4bmu' : 'HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1MU8F_2J15_J20',
    '4bmu_not4b' : 'HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1MU8F_2J15_J20 && !HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1J45p0ETA21_3J15p0ETA25',
}
labels = {
    '4b' : '4b',
    '4bmu' : '4bmu',
    '4bmu_not4b' : '4b+mu, !4b',
}
colors = {
    '4b' : ROOT.kBlack,
    '4bmu' : ROOT.kRed,
    '4bmu_not4b' : ROOT.kBlue,
}

if __name__ == '__main__':
    # get tree from ntuple file
    out_dir = '/Users/sebastienrettie/trigger/plots/'
    fname = '/Users/sebastienrettie/trigger/TriggerNtuple_HH4b_kL10.root'
    tname = 'trigDec'
    f = ROOT.TFile(fname, 'READ')
    t = ROOT.TChain(tname)
    t.Add(fname)
    # get histograms
    hists = {}
    efficiencies = {}
    nbins, mHH_min, mHH_max = 50, 0, 1000
    hists['h_all'] = ROOT.TH1D('h_all', 'h_all;mHH [GeV];Trigger Efficiency', nbins, mHH_min, mHH_max)
    t.Draw('truth_mHH/1000.>>h_all')
    for k in labels:
        hists[f'h_{k}_num'] = ROOT.TH1D(k, f'{k};mHH [GeV];Trigger Efficiency', nbins, mHH_min, mHH_max)
        pu.set_hist_color_style(hists[f'h_{k}_num'], colors[k], set_fill = False)
        t.Draw(f'truth_mHH/1000. >> {k}', triggers[k])
        efficiencies[k] = ROOT.TEfficiency(hists[f'h_{k}_num'], hists['h_all'])
        efficiencies[k].SetLineColor(colors[k])
    # make plots
    pu.set_ATLAS_style()
    pu.plot_efficiencies(out_dir, [efficiencies[k] for k in efficiencies], labels.values(), 'compare_4b_eff_new_new')