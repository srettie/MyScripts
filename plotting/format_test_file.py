import h5py
import pandas as pd
import numpy as np
import itertools

# Repackage preprocessed file into format usable by functions requiring test file specifications in the umami framework

njets = -1
ntestjets = 300000
verbose = False
debug = False

# Preprocessed file
in_filename = '/unix/atlastracking/srettie/retrain_DIPS/loose_highpt_odd_training/preprocessed/PFlow-hybrid-downsampled-merged.h5'
# Test file (same as preprocessed, just repackaged to satisfy requirements such as array types etc.)
out_filename = '/unix/atlastracking/srettie/retrain_DIPS/loose_highpt_odd_training/preprocessed/MC16d_hybrid-ext_odd_0_PFlow-preprocessed.h5'
# Original test file with desired specifications
ref_filename = '/unix/atlastracking/srettie/retrain_DIPS/loose_highpt/hybrids/MC16d_hybrid-ext_odd_0_PFlow-no_pTcuts-file_1.h5'

def convert_to_test_format(in_filename, out_filename):
    print('Input file:', in_filename)
    print('Output file:', out_filename)
    # Open preprocessed file
    in_data = h5py.File(in_filename, 'r')
    # Get input arrays
    bjets = in_data['bjets'][:njets]
    cjets = in_data['cjets'][:njets]
    ujets = in_data['ujets'][:njets]
    btrk  = in_data['btrk'][:njets]
    ctrk  = in_data['ctrk'][:njets]
    utrk  = in_data['utrk'][:njets]
    if debug:
        print('Input b/c/u jets shape (type {}/{}/{}): {}/{}/{}'.format(type(bjets), type(cjets), type(ujets), bjets.shape, cjets.shape, ujets.shape))
        print('Input b/c/u tracks shape (type {}/{}/{}): {}/{}/{}'.format(type(btrk), type(ctrk), type(utrk), btrk.shape, ctrk.shape, utrk.shape))
    if verbose:
        print('bjets\n', bjets)
        print('cjets\n', cjets)
        print('ujets\n', ujets)
        print('btrk\n', btrk)
        print('ctrk\n', ctrk)
        print('utrk\n', utrk)
    # Concatenate b/c/u jets
    jets = np.concatenate((bjets, cjets, ujets))
    tracks = np.concatenate((btrk, ctrk, utrk))
    # Shuffle to ensure uniform sampling of initial jets
    if verbose:
        print('Before shuffle')
        print('\njets\n', jets)
        print('\ntracks\n', tracks)
    np.random.seed(42)
    np.random.shuffle(jets)
    np.random.seed(42)
    np.random.shuffle(tracks)
    if verbose:
        print('After shuffle')
        print('\njets\n', jets)
        print('\ntracks\n', tracks)
    # Only keep 3e5 jets total for testing
    jets = jets[:ntestjets]
    tracks = tracks[:ntestjets]
    # Save properly formatted data to output
    with h5py.File(out_filename, 'w') as out_file:
        # Keep same shape as input file
        shape_jets = list(in_data['bjets'].shape)
        shape_tracks = list(in_data['btrk'].shape)
        # Number of jets will be minimum of (ntestjets, sum of b/c/u jets)
        shape_jets[0] = min(ntestjets, bjets.shape[0]+cjets.shape[0]+ujets.shape[0])
        shape_tracks[0] = min(ntestjets, btrk.shape[0]+ctrk.shape[0]+utrk.shape[0])
        if debug:
            print('Jets shape:', shape_jets)
            print('Tracks shape:', shape_tracks)
        out_file.create_dataset('jets', shape_jets, dtype=in_data['bjets'].dtype, data=jets, compression='gzip')
        out_file.create_dataset('tracks', shape_tracks, dtype=in_data['btrk'].dtype, data=tracks, compression='gzip')
    # Close input file
    in_data.close()
    
def compare_files(f1name,f2name):
    f1 = h5py.File(f1name,'r')
    f2 = h5py.File(f2name,'r')
    print('Types:')
    print('f1', type(f1))
    print('f2', type(f2))
    print('f1[\'jets\']', type(f1['jets']))
    print('f2[\'jets\']', type(f2['jets']))
    print('f1[\'tracks\']', type(f1['tracks']))
    print('f2[\'tracks\']', type(f2['tracks']))
    print('\nColumns:')
    df1 = pd.DataFrame(f1['jets'][:njets])
    df2 = pd.DataFrame(f2['jets'][:njets])
    for c1, c2 in itertools.zip_longest(df1.columns, df2.columns):
        if c1 != c2:
            print('COLUMNS DON\'T MATCH!! --> {} VS {}'.format(c1,c2))
        if verbose:
            print(c1, c2)

if __name__ == '__main__':
    convert_to_test_format(in_filename, out_filename)
    compare_files(out_filename, ref_filename)