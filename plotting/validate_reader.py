# Validate changes on master by comparing to reference
import sys
import os
import glob
import ROOT
import plot_utils

debug = False

out_dir = '/Users/sebastien/VHLegacy/Plots/r33_01_validation/'
hist_dir = '/Users/sebastien/VHLegacy/Histograms/'
tree_dir = '/Users/sebastien/VHLegacy/EasyTrees/'
tree_name = 'Nominal'
base_filename = 'hist-qqWlvHbbJ_PwPy8MINLO_{}_{}.root'
periods = ['a','d','e','ade']
jets = ['r33-01_em18','r33-01_pf18','r33-01_pf19']#'r32-15'
comparison_pairs = [
    ['r32-15','r33-01_em18'],
    ['r32-15','r33-01_pf18'],
    ['r32-15','r33-01_pf19'],
]
cutflows = [
    'CutFlow/PreselectionCutFlow',
    'CutFlow/Nominal/CutsResolved',
    'CutFlow/Nominal/CutsResolvedNoWeight'
]
histograms = [
    'qqWlvH125_2tag2jet_150_250ptv_SR_pTV',
    'qqWlvH125_2tag3jet_150_250ptv_SR_pTV',
    'qqWlvH125_2tag2jet_150_250ptv_SR_mBB',
    'qqWlvH125_2tag3jet_150_250ptv_SR_mBB'
]
tree_quantities = [
    'TriggerSF',
    'LeptonSF',
    'BTagSF',
    'MCEventWeight',
    'EventWeight',
    'LumiWeight',
    'JVTWeight',
    'PUWeight'
]
skeletons = {
    'TriggerSF'     : ROOT.TH1D('TriggerSF','TriggerSF;TriggerSF;Events',100,0,2),    
    'LeptonSF'      : ROOT.TH1D('LeptonSF','LeptonSF;LeptonSF;Events',100,0.6,1.1),
    'BTagSF'        : ROOT.TH1D('BTagSF','BTagSF;BTagSF;Events',80,0.6,1.4),
    'MCEventWeight' : ROOT.TH1D('MCEventWeight','MCEventWeight;MCEventWeight;Events',80,0,0.8),    
    'EventWeight'   : ROOT.TH1D('EventWeight','EventWeight;EventWeight;Events',100,0,0.01),
    'LumiWeight'    : ROOT.TH1D('LumiWeight','LumiWeight;LumiWeight;Events',100,0,0.02),
    'JVTWeight'     : ROOT.TH1D('JVTWeight','JVTWeight;JVTWeight;Events',100,0.8,1.8),    
    'PUWeight'      : ROOT.TH1D('PUWeight','PUWeight;PUWeight;Events',100,0,2),
}
colors = [ROOT.kBlack, ROOT.kRed, ROOT.kBlue, ROOT.kSpring-1]

def check_compatibility(h1, h2):
    # Check we have the same number of bins
    n1, n2 = h1.GetNbinsX(), h2.GetNbinsX()
    if n1 != n2:
        print('ERROR: h1 has {} bins, h2 has {} bins'.format(n1,n2))
        sys.exit()
    for i in range(1,n1+1):
        # Check name of cuts
        b1label = h1.GetXaxis().GetBinLabel(i)
        b2label = h2.GetXaxis().GetBinLabel(i)
        if b1label != b2label:
            print('ERROR: Bin {} has different labels: {} (h1) vs. {} (h2)'.format(i,b1label,b2label))
            sys.exit()
        
        # # Check number of events for this cut
        # b1 = h1.GetBinContent(i)
        # b2 = h2.GetBinContent(i)
        # difference = b2-b1
        # r = b1/b2
        # reldiff = difference/b1
        # diff.SetBinContent(i,r)
        
        # # Alert if large discrepancies
        # if 100*reldiff > 10:
        #     print('Comparing: {} for {}'.format(h_name, short_name))
        #     print('\t{}: {} vs. {} events ({:.2%} difference)'.format(b1label,b1,b2,reldiff))        

def print_cutflow(h):
    print('Cutflow for: {}'.format(h.GetName()))
    for i in range(1,h.GetNbinsX()+1):
        ilabel = h.GetXaxis().GetBinLabel(i)
        ival = h.GetBinContent(i)
        print('{}: {}'.format(ilabel,ival))
    
def fix_cutflow(h_orig, h_new):
    h_fixed = h_new.Clone()
    h_fixed.SetDirectory(0)
    prev_val = 0
    fixed = False
    for i in range(1, h_fixed.GetNbinsX()+1):
        ilabel = h_fixed.GetXaxis().GetBinLabel(i)
        if fixed:
            ival = h_orig.GetBinContent(i-1)
        else:
            ival = h_orig.GetBinContent(i)
        h_fixed.SetBinContent(i, ival)
        if ilabel == 'MbbPreCorrAbove50':
            h_fixed.SetBinContent(i, prev_val)
            fixed = True
            continue
        prev_val = ival
    if debug:
        print('ORIG:')
        print_cutflow(h_orig)
        print('NEW:')
        print_cutflow(h_new)
        print('FIXED:')
        print_cutflow(h_fixed)
    
    return h_fixed

def get_hist_from_tree(t, var):
    h = skeletons[var].Clone()
    h.SetName('h_{}'.format(var))
    t.Draw('{}>>h_{}'.format(var,var),'1')
    h.SetDirectory(0)
    return h

def get_hist_from_file(f, h_name):
    h = f.Get(h_name).Clone()
    h.SetDirectory(0)
    return h
    
def compare_histograms(histograms, ids, h_name, period):
    # Setup pads and canvas
    ratio_frac = 0.3

    cnv = ROOT.TCanvas()
    cnv.cd()
    
    p2 = ROOT.TPad('p2','p2',0.,0.,1.,ratio_frac)
    p2.Draw()
    p2.SetTopMargin(0.03)
    p2.SetBottomMargin(ratio_frac)
    
    p1 = ROOT.TPad('p1','p1',0.,ratio_frac,1.,1.)
    p1.Draw()
    p1.SetBottomMargin(0.03)
    p1.cd()

    # Font style
    x_label_font = 43
    y_label_font = 43
    x_title_font = 43
    y_title_font = 43    
    # Sizes in pixels
    x_title_size = 14
    y_title_size = 14
    x_label_size = 0.1
    y_label_size = 0.1
    # Offsets relative to axis
    x_title_offset = 5.0
    y_title_offset = 1.6
    
    if len(histograms) == 2:
        if 'CutsResolved' in h_name:
            histograms[0] = fix_cutflow(histograms[0],histograms[1])
        check_compatibility(histograms[0],histograms[1])
    
    # Style
    h_max = max([h.GetMaximum() for h in histograms])
    for i, h in enumerate(histograms):
        h.SetLineColor(colors[i])
        h.SetMinimum(0)
        h.SetMaximum(1.5*h_max)
        h.GetXaxis().SetLabelSize(0)

    # Draw
    p1.cd()
    for i, h in enumerate(histograms):
        if i == 0:
            h.Draw('hist')
        else:
            h.Draw('hist,same')

    # Legend
    lx1,ly1,lx2,ly2 = 0.4,0.7,0.9,0.9
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    for h, h_id in zip(histograms,ids):
        leg.AddEntry(h, '{} ({:.1f}, {:.1f})'.format(h_id,h.GetEntries(),h.GetSumOfWeights()), 'l')
    leg.Draw('same')
    
    # Labels
    plot_utils.draw_labels('Data period: {}'.format(period),'MC (Entries, SumOfWeights)')

    # Ratio
    p2.cd()
    ratio_min, ratio_max = 0.8, 1.2
    if 'BTagSF' in h_name or 'JVTWeight' in h_name:
        ratio_min, ratio_max = 0., 2.
    if 'EventWeight' in h_name or 'JVTWeight' in h_name:
        ratio_min, ratio_max = 0.5, 1.5
        
    ratios = []
    for i, h in enumerate(histograms):
        r = h.Clone()
        r.SetDirectory(0)
        r.Divide(histograms[0])
        r.SetLineColor(colors[i])
        r.GetXaxis().SetTitle(h_name.replace('/','_'))
        r.GetXaxis().SetLabelSize(x_label_size)
        r.GetYaxis().SetLabelSize(y_label_size)
        r.SetMinimum(ratio_min)
        r.SetMaximum(ratio_max)
        r.GetYaxis().SetTitle('Ratio')
        r.GetXaxis().SetTitleFont(x_title_font)
        r.GetYaxis().SetTitleFont(y_title_font)
        r.GetXaxis().SetTitleSize(x_title_size)
        r.GetYaxis().SetTitleSize(y_title_size)
        r.GetYaxis().SetTitleOffset(y_title_offset)
        r.GetXaxis().SetTitleOffset(x_title_offset)
        r.SetMarkerSize(0.)
        ratios.append(r)
        
    for i, r in enumerate(ratios):
        if i == 0: continue
        elif i == 1: r.Draw('hist')
        else: r.Draw('hist,same')
    
    unityLine = ROOT.TLine()
    unityLine.SetLineColor(ROOT.kBlack)
    unityLine.DrawLine(histograms[0].GetXaxis().GetXmin(),1.0,histograms[0].GetXaxis().GetXmax(),1.0)

    if len(histograms) == 2:
        out_name = '{}_{}_{}_{}.pdf'.format(h_name.split('/')[-1],ids[0], ids[1], period)
    else:
        out_name = '{}_{}.pdf'.format(h_name.split('/')[-1], period)
    cnv.SaveAs(plot_dir+out_name)
    cnv.Close()
    
if __name__ == '__main__':
    if not os.path.exists(out_dir): os.mkdir(out_dir)
    for pair in comparison_pairs:
        id_1, id_2 = pair[0], pair[1]
        plot_dir = out_dir + id_1 + '_vs_' + id_2 + '/'
        if not os.path.exists(plot_dir): os.mkdir(plot_dir)
        for period in periods:
            for h_name in cutflows+histograms:
                print('Comparing {} and {} ({}) period {}'.format(id_1, id_2, h_name, period))
                f1 = ROOT.TFile.Open(hist_dir+base_filename.format(period, id_1))
                f2 = ROOT.TFile.Open(hist_dir+base_filename.format(period, id_2))
                compare_histograms([get_hist_from_file(f1,h_name),get_hist_from_file(f2,h_name)], [id_1,id_2], h_name, period)
                f1.Close()
                f2.Close()

    plot_dir = out_dir + 'SF_comparisons/'
    if not os.path.exists(plot_dir): os.mkdir(plot_dir)
    for period in periods:
        for q in tree_quantities:
            files = [ROOT.TFile.Open(tree_dir+base_filename.format(period, j).replace('hist-','easyTree-')) for j in jets]
            trees = [f.Get(tree_name) for f in files]
            hists = [get_hist_from_tree(t, q) for t in trees]
            for h in hists:
                plot_utils.add_overflow(h)
                plot_utils.add_underflow(h)
            compare_histograms(hists, jets, q, period)
            for f in files:
                f.Close()