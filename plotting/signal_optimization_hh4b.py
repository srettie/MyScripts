import os
import sys
import argparse
import glob
import itertools
import array
import ROOT
import plot_utils as pu
import math
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

ROOT.gErrorIgnoreLevel = ROOT.kWarning
# ROOT.kPrint, ROOT.kInfo, ROOT.kWarning, ROOT.kError, ROOT.kBreak, ROOT.kSysError, ROOT.kFatal
debug = False
do_data_bkg = False
print_cutflows = False
force_slimming_creation = False
force_tgraph_generation = False
check_pass_fail_Xbb = False
plot_correlations = False
tree_name = 'fullmassplane'
lumi = {
    'a' : ROOT.TCut('36.20766'),
    'd' : ROOT.TCut('44.3074'),
    'e' : ROOT.TCut('58.4501'),
}
signal_dsids = {
    # All signals
    # 'signal'   : [502970,502971,502972,502973,502974,502975,502976,502977,502978,502979,502980,502981,507684],
    # SM VBF HH
    'SM'     : [502970],
    # k2v = 0 VBF HH
    'k2v0'   : [502971],
    # k2v = 0.5 VBF HH
    'k2v0p5' : [502972],
}
bkg_dsids = {
    'dijet'       : [364700,364701,364702,364703,364704,364705,364706,364707,364708,364709,364710,364711,364712],
    'dijet_bfilt' : [800285,800286,800287],
    'ttbar_HT'    : [407342,407343,407344,410429,410444],
}
ttbar_HT_samples = {p : [f'{d}_mc16{p}' for d in bkg_dsids['ttbar_HT']] for p in lumi}
dijet_bfilt_samples = {p : [f'{d}_mc16{p}' for d in bkg_dsids['dijet_bfilt']] for p in lumi}
signal_samples = {s : {p : [f'{d}_mc16{p}' for d in signal_dsids[s]] for p in lumi} for s in signal_dsids}

# weights: boosted analysis uses totalWeightBoosted and resolved analysis uses mc_sf[0]
weight = ROOT.TCut('totalWeightBoosted')
data_cuts = {
    'SR'                        : 'X_hhBoosted < 1.6',
    'not_SR'                    : 'X_hhBoosted >= 1.6',
    'cut_bottom_left'           : 'm_h1Boosted >= 125 || m_h2Boosted >= 125',
    'pass_Xbb'                  : 'HbbWP_h1Boosted <= 60 && HbbWP_h2Boosted <= 60',
    'pass_Xbb_loose'            : 'HbbWP_h1Boosted <= 70 && HbbWP_h2Boosted <= 70',
    'fail_Xbb'                  : 'HbbWP_h1Boosted > 60 && HbbWP_h2Boosted > 60',
    'fail_Xbb_loose'            : 'HbbWP_h1Boosted > 70 && HbbWP_h2Boosted > 70',
    'fail_Xbb1'                 : 'HbbWP_h1Boosted > 60 && HbbWP_h2Boosted <= 60',
    'fail_Xbb2'                 : 'HbbWP_h1Boosted <= 60 && HbbWP_h2Boosted > 60',
    'boostedPrecut'             : 'boostedPrecut',
    'passVBFJets'               : 'passVBFJets',
    'resolved_veto'             : '!(resolvedPrecut && kinematic_region==0 && ntag>=4 && X_wt_tag>1.5 && ((pass_vbf_sel && m_hh>400.) || !pass_vbf_sel))',
    'data_bkg_weight'           : ROOT.TCut('0.0002754055708430976'),
    'data_bkg_weight_loose'     : ROOT.TCut('0.0006395480597810252'),
    'data_bkg_weight_pass_fail' : ROOT.TCut('0.014548012012872227'),
    'data_bkg_weight_fail_pass' : ROOT.TCut('0.017248914075463497'),
}
# make sure things are blinded!
data_cuts['blinded']     = f'({data_cuts["not_SR"]}) && ({data_cuts["cut_bottom_left"]})'
data_cuts['CR']          = f'({data_cuts["boostedPrecut"]}) && ({data_cuts["passVBFJets"]}) && ({data_cuts["blinded"]})'
data_cuts['CR_pass_Xbb'] = f'({data_cuts["boostedPrecut"]}) && ({data_cuts["passVBFJets"]}) && ({data_cuts["blinded"]}) && ({data_cuts["pass_Xbb"]})'
data_cuts['CR_fail_Xbb'] = f'({data_cuts["boostedPrecut"]}) && ({data_cuts["passVBFJets"]}) && ({data_cuts["blinded"]}) && ({data_cuts["fail_Xbb"]})'
data_cuts['CR_pass_Xbb_loose'] = f'({data_cuts["boostedPrecut"]}) && ({data_cuts["passVBFJets"]}) && ({data_cuts["blinded"]}) && ({data_cuts["pass_Xbb_loose"]})'
data_cuts['CR_fail_Xbb_loose'] = f'({data_cuts["boostedPrecut"]}) && ({data_cuts["passVBFJets"]}) && ({data_cuts["blinded"]}) && ({data_cuts["fail_Xbb_loose"]})'
data_cuts['CR_pass_fail_Xbb'] = f'({data_cuts["boostedPrecut"]}) && ({data_cuts["passVBFJets"]}) && ({data_cuts["blinded"]}) && ({data_cuts["fail_Xbb2"]})'
data_cuts['CR_fail_pass_Xbb'] = f'({data_cuts["boostedPrecut"]}) && ({data_cuts["passVBFJets"]}) && ({data_cuts["blinded"]}) && ({data_cuts["fail_Xbb1"]})'
data_cuts['SR_fail_Xbb'] = f'({data_cuts["boostedPrecut"]}) && ({data_cuts["passVBFJets"]}) && ({data_cuts["SR"]}) && ({data_cuts["fail_Xbb"]})'
data_cuts['SR_fail_Xbb_loose'] = f'({data_cuts["boostedPrecut"]}) && ({data_cuts["passVBFJets"]}) && ({data_cuts["SR"]}) && ({data_cuts["fail_Xbb_loose"]})'
baseline_cuts = {
    'Initial'         : '1',
    'boostedPrecut'   : 'boostedPrecut',
    'passVBFJets'     : 'passVBFJets',
    'HbbWP_h1Boosted' : 'HbbWP_h1Boosted <= 60',
    'HbbWP_h2Boosted' : 'HbbWP_h2Boosted <= 60',
    'HbbWP_h1Boosted_loose' : 'HbbWP_h1Boosted <= 70',
    'HbbWP_h2Boosted_loose' : 'HbbWP_h2Boosted <= 70',
    'm_h1Boosted'     : 'm_h1Boosted > 50',
    'm_h2Boosted'     : 'm_h2Boosted > 50',
    'pt_h1Boosted'    : 'pt_h1Boosted > 450',
    'pt_h2Boosted'    : 'pt_h2Boosted > 250',
    'm_VBFjjBoosted'  : 'm_VBFjjBoosted > 1000',
    'deta_VBF'        : 'abs(eta_VBFj1Boosted-eta_VBFj2Boosted) > 3',
    'SR'              : 'X_hhBoosted < 1.6',
}
baseline_cuts_loose = baseline_cuts.copy()
baseline_cuts_loose['HbbWP_h1Boosted'] = 'HbbWP_h1Boosted <= 70'
baseline_cuts_loose['HbbWP_h2Boosted'] = 'HbbWP_h2Boosted <= 70'

slimming_selection_MC = f'{baseline_cuts["boostedPrecut"]} && {baseline_cuts["passVBFJets"]} && {baseline_cuts["HbbWP_h1Boosted"]} && {baseline_cuts["HbbWP_h2Boosted"]}'
slimming_selection_MC_pass_fail = f'{baseline_cuts["boostedPrecut"]} && {baseline_cuts["passVBFJets"]} && {data_cuts["fail_Xbb2"]}'
slimming_selection_MC_fail_pass = f'{baseline_cuts["boostedPrecut"]} && {baseline_cuts["passVBFJets"]} && {data_cuts["fail_Xbb1"]}'
correlation_selection = f'{baseline_cuts["boostedPrecut"]} && {baseline_cuts["passVBFJets"]} && {data_cuts["pass_Xbb_loose"]} && {data_cuts["resolved_veto"]} && {baseline_cuts["m_h1Boosted"]} && {baseline_cuts["m_h2Boosted"]} && {baseline_cuts["pt_h1Boosted"]} && {baseline_cuts["pt_h2Boosted"]}'
optimized_cuts_SM = {
    'Initial'         : ROOT.TCut('1'),
    'boostedPrecut'   : ROOT.TCut('boostedPrecut'),
    'passVBFJets'     : ROOT.TCut('passVBFJets'),
    'HbbWP_h1Boosted' : ROOT.TCut('HbbWP_h1Boosted <= 60'),
    'HbbWP_h2Boosted' : ROOT.TCut('HbbWP_h2Boosted <= 60'),
    'm_h1Boosted'     : ROOT.TCut('m_h1Boosted > 90'),
    'm_h2Boosted'     : ROOT.TCut('m_h2Boosted > 90'),
    'pt_h1Boosted'    : ROOT.TCut('pt_h1Boosted > 370'),
    'pt_h2Boosted'    : ROOT.TCut('pt_h2Boosted > 200'),
    'm_VBFjjBoosted'  : ROOT.TCut('m_VBFjjBoosted > 1700'),
    'deta_VBF'        : ROOT.TCut('abs(eta_VBFj1Boosted-eta_VBFj2Boosted) > 2.1'),
    'SR'              : ROOT.TCut('X_hhBoosted < 1.6'),
    'pt_vecsum_VBF'   : ROOT.TCut('pTvecsum_VBFBoosted < 120'),
    'dphi_hhBoosted'  : ROOT.TCut('abs(phi_h1Boosted-phi_h2Boosted) >= 1.2'),
}
optimized_cuts_k2v0 = {
    'Initial'         : ROOT.TCut('1'),
    'boostedPrecut'   : ROOT.TCut('boostedPrecut'),
    'passVBFJets'     : ROOT.TCut('passVBFJets'),
    'HbbWP_h1Boosted' : ROOT.TCut('HbbWP_h1Boosted <= 60'),
    'HbbWP_h2Boosted' : ROOT.TCut('HbbWP_h2Boosted <= 60'),
    'm_h1Boosted'     : ROOT.TCut('m_h1Boosted > 90'),
    'm_h2Boosted'     : ROOT.TCut('m_h2Boosted > 90'),
    'pt_h1Boosted'    : ROOT.TCut('pt_h1Boosted > 550'),
    'pt_h2Boosted'    : ROOT.TCut('pt_h2Boosted > 480'),
    'm_VBFjjBoosted'  : ROOT.TCut('m_VBFjjBoosted > 1200'),
    'deta_VBF'        : ROOT.TCut('abs(eta_VBFj1Boosted-eta_VBFj2Boosted) > 5.2'),
    'SR'              : ROOT.TCut('X_hhBoosted < 1.6'),
    'pt_vecsum_VBF'   : ROOT.TCut('pTvecsum_VBFBoosted < 455'),
    'dphi_hhBoosted'  : ROOT.TCut('abs(phi_h1Boosted-phi_h2Boosted) >= 2.8'),
}
optimized_cuts_k2v0p5 = {
    'Initial'         : ROOT.TCut('1'),
    'boostedPrecut'   : ROOT.TCut('boostedPrecut'),
    'passVBFJets'     : ROOT.TCut('passVBFJets'),
    'HbbWP_h1Boosted' : ROOT.TCut('HbbWP_h1Boosted <= 60'),
    'HbbWP_h2Boosted' : ROOT.TCut('HbbWP_h2Boosted <= 60'),
    'm_h1Boosted'     : ROOT.TCut('m_h1Boosted > 90'),
    'm_h2Boosted'     : ROOT.TCut('m_h2Boosted > 90'),
    'pt_h1Boosted'    : ROOT.TCut('pt_h1Boosted > 570'),
    'pt_h2Boosted'    : ROOT.TCut('pt_h2Boosted > 540'),
    'm_VBFjjBoosted'  : ROOT.TCut('m_VBFjjBoosted > 1400'),
    'deta_VBF'        : ROOT.TCut('abs(eta_VBFj1Boosted-eta_VBFj2Boosted) > 5.4'),
    'SR'              : ROOT.TCut('X_hhBoosted < 1.6'),
    'pt_vecsum_VBF'   : ROOT.TCut('pTvecsum_VBFBoosted < 290'),
    'dphi_hhBoosted'  : ROOT.TCut('abs(phi_h1Boosted-phi_h2Boosted) >= 2.9'),
}
optimized_cuts = {
    'SM'     : optimized_cuts_SM,
    'k2v0'   : optimized_cuts_k2v0,
    'k2v0p5' : optimized_cuts_k2v0p5,
}
baseline_significances = {}
# variables to optimize
to_optimize = {
    'm_h1Boosted'         : [10 * i for i in range(9, 15)],
    'm_h2Boosted'         : [10 * i for i in range(9, 14)],
    'pt_h1Boosted'        : [10 * i for i in range(25, 150)],
    'pt_h2Boosted'        : [10 * i for i in range(20, 150)],
    'm_VBFjjBoosted'      : [100 * i for i in range(40)],
    'deta_VBFjjBoosted'   : [i / 10 for i in range(0, 100)],
    'pTvecsum_VBFBoosted' : [5 * i for i in range(100)],
    'dphi_hhBoosted'      : [i / 10 for i in range(50)],
    'aplanarityBoosted'   : [i / 20 for i in range(21)],
    'planarityBoosted'    : [i / 20 for i in range(21)],
    'sphericityBoosted'   : [i / 20 for i in range(21)],
    'shapeCBoosted'       : [i / 20 for i in range(21)],
    'shapeDBoosted'       : [i / 20 for i in range(21)],
}
to_optimize.update({f'FWM{i}Boosted' : [i / 20 for i in range(21)] for i in range(10)})
to_optimize.update({f'HCM{i}Boosted' : [i / 20 for i in range(21)] for i in range(10)})
to_optimize.update({f'HCM{i}Boosted_fix' : [i / 20 for i in range(21)] for i in range(10)})

# variables to plot (variable name : [nbins, h_min, h_max])
vars = {
    'm_hhBoosted'         : [15, 300, 3300],
    'm_VBFjjBoosted'      : [25, 0, 5000],
    'm_h1Boosted'         : [7, 90, 160],
    'm_h2Boosted'         : [6, 90, 150],
    'pt_h1Boosted'        : [20, 0, 2000],
    'pt_h2Boosted'        : [20, 0, 2000],
    'eta_VBFj1Boosted'    : [20, -10, 10],
    'eta_VBFj2Boosted'    : [20, -10, 10],
    'deta_VBFjjBoosted'   : [20, -10, 10],
    'pTvecsum_VBFBoosted' : [10, 0, 400],
    'dphi_hhBoosted'      : [20, -2*math.pi, 2*math.pi],
    'dRmin_h1Boosted'     : [25, 1, 6],
    'dRmin_h2Boosted'     : [25, 1, 6],
    'aplanarityBoosted'   : [20, 0, 1],
    'planarityBoosted'    : [20, 0, 1],
    'sphericityBoosted'   : [20, 0, 1],
    'shapeCBoosted'       : [20, 0, 1],
    'shapeDBoosted'       : [20, 0, 1],
}
vars.update({f'FWM{i}Boosted' : [20, 0, 1] for i in range(10)})
vars.update({f'HCM{i}Boosted' : [20, 0, 1] for i in range(10)})
vars.update({f'HCM{i}Boosted_fix' : [20, 0, 1] for i in range(10)})

# colors
color_map = {
    'dijet_bfilt' : ROOT.kRed,
    'ttbar_HT'    : ROOT.kGreen+2,
    'bkg'         : ROOT.kGray,
    'data_bkg'    : ROOT.kBlack,
    'SM'          : ROOT.kViolet,
    'k2v0'        : ROOT.kBlue,
    'k2v0p5'      : ROOT.kCyan+1,
}
tree_branch_list = [
    'run_number',
    'event_number',
    'truth_mhh',
    'resolvedPrecut',
    'passResolvedSR',
    'resolvedCat',
    'mcChannelNumber',
    'boostedPrecut',
    'totalWeightBoosted',
    'm_hhBoosted',
    'pt_hhBoosted',
    'eta_hhBoosted',
    'phi_hhBoosted',
    'm_h1Boosted',
    'pt_h1Boosted',
    'eta_h1Boosted',
    'phi_h1Boosted',
    'Idx_h1Boosted',
    'HbbWP_h1Boosted',
    'dRmin_h1Boosted',
    'm_h2Boosted',
    'pt_h2Boosted',
    'eta_h2Boosted',
    'phi_h2Boosted',
    'Idx_h2Boosted',
    'HbbWP_h2Boosted',
    'dRmin_h2Boosted',
    'E_VBFj1Boosted',
    'pt_VBFj1Boosted',
    'eta_VBFj1Boosted',
    'phi_VBFj1Boosted',
    'Idx_VBFj1Boosted',
    'E_VBFj2Boosted',
    'pt_VBFj2Boosted',
    'eta_VBFj2Boosted',
    'phi_VBFj2Boosted',
    'Idx_VBFj2Boosted',
    'm_VBFjjBoosted',
    'pTvecsum_VBFBoosted',
    'passVBFJets',
    'X_hhBoosted'
]

tree_branch_list += ['aplanarityBoosted', 'planarityBoosted', 'sphericityBoosted', 'shapeCBoosted', 'shapeDBoosted']
tree_branch_list += [f'FWM{i}Boosted' for i in range(10)]
tree_branch_list += [f'HCM{i}Boosted' for i in range(10)]
tree_branch_list += [f'HCM{i}Boosted_fix' for i in range(10)]

def get_parser():
    p = argparse.ArgumentParser(description='Optimize hh4b signal', allow_abbrev=False)
    p.add_argument('-i', '--in_dir', dest='in_dir', default=None, required=True, help='Directory where input merged NNTs are stored')
    p.add_argument('-o', '--out_dir', dest='out_dir', default=None, required=True, help='Directory where plots are saved')
    p.add_argument('-d', '--debug', action='store_true', help='Print debug information')
    p.add_argument('-b', '--data_bkg', action='store_true', help='Use data NNTs for background estimation')
    p.add_argument('-c', '--print_cutflows', action='store_true', help='Print cutflows from large rdfs')
    p.add_argument('-g', '--force_tgraph', action='store_true', help='Generate root files with significance TGraphs')
    p.add_argument('-p', '--check_pass_fail_Xbb', action='store_true', help='Check significance of pass/fail Xbb regions')
    p.add_argument('-v', '--variable_correlations', action='store_true', help='Plot correlation map of variables')
    p.add_argument('-w', '--compare_xbb', action='store_true', help='Compare Xbb WP performance')
    return p

# selections
def get_selection(var_to_optimize, new_cut):
    boostedPrecut = ROOT.TCut(baseline_cuts['boostedPrecut'])
    # 2 large-R jets baseline
    largeR_jet1_mass_cut = ROOT.TCut(baseline_cuts['m_h1Boosted'])
    largeR_jet2_mass_cut = ROOT.TCut(baseline_cuts['m_h2Boosted'])
    largeR_jet1_pt_cut = ROOT.TCut(baseline_cuts['pt_h1Boosted'])
    largeR_jet2_pt_cut = ROOT.TCut(baseline_cuts['pt_h2Boosted'])
    # for optimization
    if var_to_optimize == 'm_h1Boosted':
        largeR_jet1_mass_cut = ROOT.TCut(f'm_h1Boosted > {new_cut}')
    if var_to_optimize == 'm_h2Boosted':
        largeR_jet2_mass_cut = ROOT.TCut(f'm_h2Boosted > {new_cut}')
    if var_to_optimize == 'pt_h1Boosted':
        largeR_jet1_pt_cut = ROOT.TCut(f'pt_h1Boosted > {new_cut}')
    if var_to_optimize == 'pt_h2Boosted':
        largeR_jet2_pt_cut = ROOT.TCut(f'pt_h2Boosted > {new_cut}')
    largeR_jet_mass_cuts = largeR_jet1_mass_cut + largeR_jet2_mass_cut
    largeR_jet_pt_cuts = largeR_jet1_pt_cut + largeR_jet2_pt_cut
    # Use the cut value for the 60% WP
    largeR_jet1_Hbb_cut = ROOT.TCut(baseline_cuts['HbbWP_h1Boosted'])
    largeR_jet2_Hbb_cut = ROOT.TCut(baseline_cuts['HbbWP_h2Boosted'])
    largeR_jet_Hbb_cuts = largeR_jet1_Hbb_cut + largeR_jet2_Hbb_cut
    largeR_jet_cuts = largeR_jet_mass_cuts + largeR_jet_pt_cuts + largeR_jet_Hbb_cuts
    # VBF baseline
    passVBFJets = ROOT.TCut(baseline_cuts['passVBFJets'])
    vbf_mjj_cut = ROOT.TCut(baseline_cuts['m_VBFjjBoosted'])
    vbf_deta_cut = ROOT.TCut(baseline_cuts['deta_VBF'])
    # for optimization
    if var_to_optimize == 'm_VBFjjBoosted':
        vbf_mjj_cut = ROOT.TCut(f'm_VBFjjBoosted > {new_cut}')
    if var_to_optimize == 'deta_VBFjjBoosted':
        vbf_deta_cut = ROOT.TCut(f'abs(eta_VBFj1Boosted-eta_VBFj2Boosted) > {new_cut}')
    vbf_cuts = passVBFJets + vbf_mjj_cut + vbf_deta_cut
    # SR baseline (eq. 2 of https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2022-035/)
    sr_cuts = ROOT.TCut(baseline_cuts['SR'])
    # pTvecsum
    pt_vecsum_VBF = ROOT.TCut('pTvecsum_VBFBoosted < 10000000')
    # for optimization
    if var_to_optimize == 'pTvecsum_VBFBoosted':
        pt_vecsum_VBF = ROOT.TCut(f'pTvecsum_VBFBoosted < {new_cut}')
    # dphi_hh
    dphi_hh = ROOT.TCut('abs(phi_h1Boosted-phi_h2Boosted) >= 0')
    if var_to_optimize == 'dphi_hhBoosted':
        dphi_hh = ROOT.TCut(f'abs(phi_h1Boosted-phi_h2Boosted) >= {new_cut}')
    # event shapes
    shape_cuts = ROOT.TCut('1')
    evt_shapes = ['aplanarityBoosted', 'planarityBoosted', 'sphericityBoosted', 'shapeCBoosted', 'shapeDBoosted']
    evt_shapes += [f'FWM{i}Boosted' for i in range(10)]
    evt_shapes += [f'HCM{i}Boosted' for i in range(10)]
    evt_shapes += [f'HCM{i}Boosted_fix' for i in range(10)]
    if var_to_optimize in evt_shapes:
        shape_cuts = ROOT.TCut(f'{var_to_optimize} >= {new_cut}')
    # final selection
    selection = boostedPrecut + largeR_jet_cuts + vbf_cuts + sr_cuts + pt_vecsum_VBF + dphi_hh + shape_cuts
    return selection

def get_hist(t, h_skeleton, var, process, selection, weight, suffix):
    h = h_skeleton.Clone(f'h_{var}{suffix}_{process}')
    draw_var = var
    # variables not explicitly in the NNT trees
    if var == 'deta_VBFjjBoosted':
        draw_var = 'eta_VBFj1Boosted-eta_VBFj2Boosted' 
    if var == 'dphi_hhBoosted':
        draw_var = 'phi_h1Boosted-phi_h2Boosted'
    draw_cmd = f'{draw_var} >> h_{var}{suffix}_{process}'
    if debug:
        print(f'drawing: {draw_cmd}')
        print(f'selection: {selection}')
        print(f'weight: {weight}')
    n_evts = t.Draw(draw_cmd, selection*weight)
    if debug:
        print(f'n_evts: {n_evts}')
    pu.add_overflow(h)
    pu.add_underflow(h)
    h.SetTitle(f'h_{var}{suffix}_{process}')
    h.SetLineWidth(2)
    h.SetDirectory(0)
    return h

def get_rdfs(in_dir, do_data_bkg = False):
    NNTs = glob.glob(os.path.join(in_dir, 'MC', '*/NanoNTuple.root'))
    rdfs = {}
    # get rdfs per process
    for s, dsids in signal_dsids.items():
        rdfs[s] = pu.get_rdf(NNTs, list(itertools.chain.from_iterable([signal_samples[s][p] for p in lumi])), tree_name, debug)
    rdfs['ttbar_HT'] = pu.get_rdf(NNTs, list(itertools.chain.from_iterable([ttbar_HT_samples[p] for p in lumi])), tree_name, debug)
    rdfs['dijet_bfilt'] = pu.get_rdf(NNTs, list(itertools.chain.from_iterable([dijet_bfilt_samples[p] for p in lumi])), tree_name, debug)
    # define lumi column (https://root.cern/doc/master/classROOT_1_1RDF_1_1RInterface.html#a680041c7ca9a752b814b27aa1387d759)
    ROOT.gInterpreter.Declare(
    """
    float lumi(unsigned int slot, const ROOT::RDF::RSampleInfo &id){
        if (id.Contains("mc16a")) return 36.20766;
        if (id.Contains("mc16d")) return 44.3074;
        if (id.Contains("mc16e")) return 58.4501;
        return -1;
    }
    """)
    ROOT.gInterpreter.Declare(
    """
    int label(unsigned int slot, const ROOT::RDF::RSampleInfo &id){
        bool is_signal = false;
        std::vector<std::string> dsids = {"502970","502971","502972","502973","502974","502975","502976","502977","502978","502979","502980","502981","507684"};
        for (auto dsid : dsids) {
            if (id.Contains(dsid)) is_signal = true;
        }
        if (is_signal) return 1;
        else return 0;
    }
    """)
    ROOT.gInterpreter.Declare(
    """
    int sample(unsigned int slot, const ROOT::RDF::RSampleInfo &id){
        bool is_signal = false;
        bool is_dijet_bfilt = false;
        bool is_ttbar_HT = false;
        std::vector<std::string> sig_dsids = {"502970","502971","502972","502973","502974","502975","502976","502977","502978","502979","502980","502981","507684"};
        std::vector<std::string> dijet_bfilt_dsids = {"800285","800286","800287"};
        std::vector<std::string> ttbar_HT_dsids = {"407342","407343","407344","410429","410444"};
        for (auto dsid : sig_dsids) {
            if (id.Contains(dsid)) is_signal = true;
        }
        for (auto dsid : dijet_bfilt_dsids) {
            if (id.Contains(dsid)) is_dijet_bfilt = true;
        }
        for (auto dsid : ttbar_HT_dsids) {
            if (id.Contains(dsid)) is_ttbar_HT = true;
        }
        if (is_signal) return 1;
        if (is_dijet_bfilt) return 2;
        if (is_ttbar_HT) return 3;
        return 0;
    }
    """)
    for s, dsids in signal_dsids.items():
        rdfs[s] = rdfs[s].DefinePerSample('lumi', 'lumi(rdfslot_, rdfsampleinfo_)')
        rdfs[s] = rdfs[s].DefinePerSample('label', 'label(rdfslot_, rdfsampleinfo_)')
        rdfs[s] = rdfs[s].DefinePerSample('sample', 'sample(rdfslot_, rdfsampleinfo_)')
    rdfs['ttbar_HT'] = rdfs['ttbar_HT'].DefinePerSample('lumi', 'lumi(rdfslot_, rdfsampleinfo_)')
    rdfs['dijet_bfilt'] = rdfs['dijet_bfilt'].DefinePerSample('lumi', 'lumi(rdfslot_, rdfsampleinfo_)')
    rdfs['ttbar_HT'] = rdfs['ttbar_HT'].DefinePerSample('label', 'label(rdfslot_, rdfsampleinfo_)')
    rdfs['dijet_bfilt'] = rdfs['dijet_bfilt'].DefinePerSample('label', 'label(rdfslot_, rdfsampleinfo_)')
    rdfs['ttbar_HT'] = rdfs['ttbar_HT'].DefinePerSample('sample', 'sample(rdfslot_, rdfsampleinfo_)')
    rdfs['dijet_bfilt'] = rdfs['dijet_bfilt'].DefinePerSample('sample', 'sample(rdfslot_, rdfsampleinfo_)')
    if do_data_bkg:
        # get data rdfs
        NNTs_data = glob.glob(os.path.join(in_dir, 'Data', 'data*.root'))
        print(f'CR selection: {data_cuts["CR"]}')
        data_samples = [f'data{p}.root' for p in range(15, 19)]
        rdfs['data_bkg'] = pu.get_rdf(NNTs_data, data_samples, 'fullmassplane', debug)
        n_CR = rdfs['data_bkg'].Filter(data_cuts['CR']).Count().GetValue()
        num_CR_pass_Xbb = rdfs['data_bkg'].Filter(data_cuts['CR_pass_Xbb']).Count().GetValue()
        num_CR_fail_Xbb = rdfs['data_bkg'].Filter(data_cuts['CR_fail_Xbb']).Count().GetValue()
        num_CR_pass_Xbb_loose = rdfs['data_bkg'].Filter(data_cuts['CR_pass_Xbb_loose']).Count().GetValue()
        num_CR_fail_Xbb_loose = rdfs['data_bkg'].Filter(data_cuts['CR_fail_Xbb_loose']).Count().GetValue()
        num_CR_pass_fail_Xbb = rdfs['data_bkg'].Filter(data_cuts['CR_pass_fail_Xbb']).Count().GetValue()
        num_CR_fail_pass_Xbb = rdfs['data_bkg'].Filter(data_cuts['CR_fail_pass_Xbb']).Count().GetValue()
        n_intermediate = n_CR - num_CR_pass_Xbb - num_CR_fail_Xbb
        print(f'Total events outside SR in data_bkg: {n_CR}')
        print(f'\tEvents passing Xbb ({data_cuts["pass_Xbb"]}): {num_CR_pass_Xbb} ({100*num_CR_pass_Xbb/n_CR:.2f}%)')
        print(f'\tEvents failing Xbb ({data_cuts["fail_Xbb"]}): {num_CR_fail_Xbb} ({100*num_CR_fail_Xbb/n_CR:.2f}%)')
        print(f'\tEvents passing loose Xbb ({data_cuts["pass_Xbb_loose"]}): {num_CR_pass_Xbb_loose} ({100*num_CR_pass_Xbb_loose/n_CR:.2f}%)')
        print(f'\tEvents failing loose Xbb ({data_cuts["fail_Xbb_loose"]}): {num_CR_fail_Xbb_loose} ({100*num_CR_fail_Xbb_loose/n_CR:.2f}%)')
        print(f'\tEvents with Xbb pass_fail ({data_cuts["fail_Xbb2"]}): {num_CR_pass_fail_Xbb} ({100*num_CR_pass_fail_Xbb/n_CR:.2f}%)')
        print(f'\tEvents with Xbb fail_pass ({data_cuts["fail_Xbb1"]}): {num_CR_fail_pass_Xbb} ({100*num_CR_fail_pass_Xbb/n_CR:.2f}%)')
        if n_intermediate == (num_CR_pass_fail_Xbb + num_CR_fail_pass_Xbb):
            if debug: print(f'\tSanity check passed!')
        else:
            raise ValueError(f'\tSanity check FAILED: {n_intermediate} != {num_CR_pass_fail_Xbb + num_CR_fail_pass_Xbb}')
        data_cuts['data_bkg_weight'] = ROOT.TCut(f'{num_CR_pass_Xbb / num_CR_fail_Xbb}')
        data_cuts['data_bkg_weight_loose'] = ROOT.TCut(f'{num_CR_pass_Xbb_loose / num_CR_fail_Xbb_loose}')
        data_cuts['data_bkg_weight_pass_fail'] = ROOT.TCut(f'{num_CR_pass_fail_Xbb / num_CR_fail_Xbb}')
        data_cuts['data_bkg_weight_fail_pass'] = ROOT.TCut(f'{num_CR_fail_pass_Xbb / num_CR_fail_Xbb}')
        print(f'data_bkg_weight is now {data_cuts["data_bkg_weight"]}')
        print(f'data_bkg_weight_loose is now {data_cuts["data_bkg_weight_loose"]}')
        print(f'data_bkg_weight_pass_fail is now {data_cuts["data_bkg_weight_pass_fail"]}')
        print(f'data_bkg_weight_fail_pass is now {data_cuts["data_bkg_weight_fail_pass"]}')
    return rdfs

def save_slimmed_trees(in_dir, do_data_bkg):
    print('Start slimming NNTs')
    # MC
    NNTs = glob.glob(os.path.join(in_dir, 'MC', '*/NanoNTuple.root'))
    for NNT in NNTs:
        NNT_slim = NNT.replace('.root', '_slimmed.root')
        if force_slimming_creation or not os.path.exists(NNT_slim):
            if debug: print(f'slimming: {NNT}')
            t = ROOT.TChain(tree_name)
            t.Add(NNT)
            if t.GetEntries() == 0:
                print(f'skipping 0 entries {NNT}')
                continue
            df = ROOT.RDataFrame(t)
            df.Filter(slimming_selection_MC).Snapshot(tree_name, NNT_slim, tree_branch_list)
        else:
            if debug: print(f'{NNT_slim} already exists; skipping!')
        NNT_slim_pass_fail = NNT.replace('.root', '_slimmed_pass_fail.root')
        if force_slimming_creation or not os.path.exists(NNT_slim_pass_fail):
            if debug: print(f'slimming pass_fail: {NNT}')
            t = ROOT.TChain(tree_name)
            t.Add(NNT)
            df = ROOT.RDataFrame(t)
            df.Filter(slimming_selection_MC_pass_fail).Snapshot(tree_name, NNT_slim_pass_fail, tree_branch_list)
        else:
            if debug: print(f'{NNT_slim_pass_fail} already exists; skipping!')
        NNT_slim_fail_pass = NNT.replace('.root', '_slimmed_fail_pass.root')
        if force_slimming_creation or not os.path.exists(NNT_slim_fail_pass):
            if debug: print(f'slimming fail_pass: {NNT}')
            t = ROOT.TChain(tree_name)
            t.Add(NNT)
            df = ROOT.RDataFrame(t)
            df.Filter(slimming_selection_MC_fail_pass).Snapshot(tree_name, NNT_slim_fail_pass, tree_branch_list)
        else:
            if debug: print(f'{NNT_slim_fail_pass} already exists; skipping!')
    # data
    if do_data_bkg:
        NNTs_data = glob.glob(os.path.join(in_dir, 'Data', 'data*.root'))
        for NNT in NNTs_data:
            if 'slimmed' in NNT or 'CR' in NNT: continue
            NNT_slim = NNT.replace('.root', '_slimmed_SR_fail_Xbb.root')
            if force_slimming_creation or not os.path.exists(NNT_slim):
                if debug: print(f'slimming: {NNT}')
                t = ROOT.TChain('fullmassplane')
                t.Add(NNT)
                df = ROOT.RDataFrame(t)
                df.Filter(data_cuts['SR_fail_Xbb']).Snapshot('fullmassplane', NNT_slim, tree_branch_list)
            else:
                print(f'{NNT_slim} already exists; skipping!')
            NNT_slim_loose = NNT.replace('.root', '_slimmed_SR_fail_Xbb_loose.root')
            if force_slimming_creation or not os.path.exists(NNT_slim_loose):
                print(f'slimming: {NNT}')
                t = ROOT.TChain('fullmassplane')
                t.Add(NNT)
                df = ROOT.RDataFrame(t)
                df.Filter(data_cuts['SR_fail_Xbb_loose']).Snapshot('fullmassplane', NNT_slim_loose, tree_branch_list)
            else:
                print(f'{NNT_slim} already exists; skipping!')
            NNT_slim_CR_pass_Xbb = NNT.replace('.root', '_CR_pass_Xbb.root')
            if force_slimming_creation or not os.path.exists(NNT_slim_CR_pass_Xbb):
                if debug: print(f'slimming CR_pass_Xbb: {NNT}')
                t = ROOT.TChain('fullmassplane')
                t.Add(NNT)
                df = ROOT.RDataFrame(t)
                df.Filter(data_cuts['CR_pass_Xbb']).Snapshot('fullmassplane', NNT_slim_CR_pass_Xbb, tree_branch_list)
            else:
                if debug: print(f'{NNT_slim_CR_pass_Xbb} already exists; skipping!')
            NNT_slim_CR_fail_Xbb = NNT.replace('.root', '_CR_fail_Xbb.root')
            if force_slimming_creation or not os.path.exists(NNT_slim_CR_fail_Xbb):
                if debug: print(f'slimming CR_fail_Xbb: {NNT}')
                t = ROOT.TChain('fullmassplane')
                t.Add(NNT)
                df = ROOT.RDataFrame(t)
                df.Filter(data_cuts['CR_fail_Xbb']).Snapshot('fullmassplane', NNT_slim_CR_fail_Xbb, tree_branch_list)
            else:
                if debug: print(f'{NNT_slim_CR_pass_Xbb} already exists; skipping!')
    print('done')

def get_bkg_trees(in_dir, do_data_bkg, check_pass_fail = ''):
    trees = {}
    NNTs = glob.glob(os.path.join(in_dir, 'MC', '*/NanoNTuple.root'))
    if check_pass_fail != '':
        NNTs = glob.glob(os.path.join(in_dir, 'MC', f'*/NanoNTuple_slimmed_{check_pass_fail}.root'))
    # get trees for individual periods
    for p in lumi:
        trees[f'ttbar_HT_{p}'] = pu.get_tree(NNTs, ttbar_HT_samples[p], tree_name, debug)
        trees[f'dijet_bfilt_{p}'] = pu.get_tree(NNTs, dijet_bfilt_samples[p], tree_name, debug)
    # get trees per process
    trees['ttbar_HT'] = pu.get_tree(NNTs, list(itertools.chain.from_iterable([ttbar_HT_samples[p] for p in lumi])), tree_name, debug)
    trees['dijet_bfilt'] = pu.get_tree(NNTs, list(itertools.chain.from_iterable([dijet_bfilt_samples[p] for p in lumi])), tree_name, debug)
    if do_data_bkg:
        # get data trees
        NNTs_data = glob.glob(os.path.join(in_dir, 'Data', 'data*_slimmed_SR_fail_Xbb.root'))
        NNTs_data_loose = glob.glob(os.path.join(in_dir, 'Data', 'data*_slimmed_SR_fail_Xbb_loose.root'))
        for p in list(range(15, 19)) + ['all']:
            if p == 'all':
                t_name = 'data_bkg'
                data_samples = [f'data{p}' for p in range(15, 19)]
            else:
                t_name = f'data{p}_bkg'
                data_samples = [f'data{p}']
            trees[t_name] = pu.get_tree(NNTs_data, data_samples, 'fullmassplane', debug)
            trees[f'{t_name}_loose'] = pu.get_tree(NNTs_data_loose, data_samples, 'fullmassplane', debug)
    return trees

def get_signal_trees(in_dir, check_pass_fail = ''):
    trees = {}
    NNTs = glob.glob(os.path.join(in_dir, 'MC', '*/NanoNTuple.root'))
    if check_pass_fail != '':
        NNTs = glob.glob(os.path.join(in_dir, 'MC', f'*/NanoNTuple_slimmed_{check_pass_fail}.root'))
    # loop over signals
    for s in signal_dsids:
        # get trees for individual periods
        for p in lumi:
            trees[f'{s}_{p}'] = pu.get_tree(NNTs, signal_samples[s][p], tree_name, debug)
        # get trees per process
        trees[s] = pu.get_tree(NNTs, list(itertools.chain.from_iterable([signal_samples[s][p] for p in lumi])), tree_name, debug)
    return trees

def get_trees(in_dir, do_data_bkg = False, check_pass_fail = ''):
    trees = get_signal_trees(in_dir, check_pass_fail)
    trees.update(get_bkg_trees(in_dir, do_data_bkg, check_pass_fail))
    return trees

def get_bkg_histograms(trees, h_skeleton, var_to_plot, selection, suffix, do_data_bkg, check_pass_fail = ''):
    hists = {}
    # add histograms with proper lumi scaling
    for i, p in enumerate(lumi):
        if i == 0:
            h_ttbar_HT = get_hist(trees[f'ttbar_HT_{p}'], h_skeleton, var_to_plot, 'ttbar_HT', selection, weight*lumi[p], suffix)
            h_dijet_bfilt = get_hist(trees[f'dijet_bfilt_{p}'], h_skeleton, var_to_plot, 'dijet_bfilt', selection, weight*lumi[p], suffix)
        else:
            h_ttbar_HT.Add(get_hist(trees[f'ttbar_HT_{p}'], h_skeleton, var_to_plot, 'ttbar_HT', selection, weight*lumi[p], suffix))
            h_dijet_bfilt.Add(get_hist(trees[f'dijet_bfilt_{p}'], h_skeleton, var_to_plot, 'dijet_bfilt', selection, weight*lumi[p], suffix))
    # set styles
    pu.set_hist_color_style(h_ttbar_HT, color_map['ttbar_HT'])
    pu.set_hist_color_style(h_dijet_bfilt, color_map['dijet_bfilt'])
    # background stack
    h_bkg_stack = ROOT.THStack('bkg_stack', f'bkg_stack;{var_to_plot};Events;')
    h_bkg_stack.Add(h_ttbar_HT)
    h_bkg_stack.Add(h_dijet_bfilt)
    # now that stack is drawn, get total background histogram from it
    h_bkg = h_bkg_stack.GetStack().Last().Clone()
    h_bkg.SetDirectory(0)
    pu.set_hist_color_style(h_bkg, color_map['bkg'], 2)
    hists[f'h_{var_to_plot}{suffix}_ttbar_HT'] = h_ttbar_HT
    hists[f'h_{var_to_plot}{suffix}_dijet_bfilt'] = h_dijet_bfilt
    hists[f'h_{var_to_plot}{suffix}_bkg'] = h_bkg
    hists[f'h_{var_to_plot}{suffix}_stack'] = h_bkg_stack
    # data
    if do_data_bkg:
        # don't apply Xbb cut on data
        data_selection = ROOT.TCut(data_cuts['SR_fail_Xbb']) + ROOT.TCut(selection.GetTitle().replace(baseline_cuts['HbbWP_h1Boosted'], '1').replace(baseline_cuts['HbbWP_h2Boosted'], '1'))
        data_weight = data_cuts['data_bkg_weight'] if check_pass_fail == '' else data_cuts[f'data_bkg_weight_{check_pass_fail}']
        h_data_bkg = get_hist(trees['data_bkg'], h_skeleton, var_to_plot, 'data_bkg', data_selection, data_weight, suffix)
        if suffix == '_loose':
            data_selection = ROOT.TCut(data_cuts['SR_fail_Xbb_loose']) + ROOT.TCut(selection.GetTitle().replace(baseline_cuts['HbbWP_h1Boosted_loose'], '1').replace(baseline_cuts['HbbWP_h2Boosted_loose'], '1'))
            data_weight = data_cuts['data_bkg_weight_loose']
            h_data_bkg = get_hist(trees['data_bkg_loose'], h_skeleton, var_to_plot, 'data_bkg', data_selection, data_weight, suffix)
        pu.set_hist_color_style(h_data_bkg, color_map['data_bkg'], set_fill = False)
        hists[f'h_{var_to_plot}{suffix}_data_bkg'] = h_data_bkg
    return hists

def get_signal_histograms(trees, h_skeleton, var_to_plot, selection, suffix):
    hists = {}
    # loop over signals
    for s in signal_dsids:
        # add histograms with proper lumi scaling
        for i, p in enumerate(lumi):
            if i == 0:
                h_signal = get_hist(trees[f'{s}_{p}'], h_skeleton, var_to_plot, s, selection, weight*lumi[p], suffix)
            else:
                h_signal.Add(get_hist(trees[f'{s}_{p}'], h_skeleton, var_to_plot, s, selection, weight*lumi[p], suffix))
        # set styles
        pu.set_hist_color_style(h_signal, color_map[s], set_fill = False)
        hists[f'h_{var_to_plot}{suffix}_{s}'] = h_signal
    return hists

def get_histograms(trees, var_to_plot, nbins, h_min, h_max, selection, suffix, do_data_bkg = False, check_pass_fail = ''):
    # get histograms
    h_skeleton = ROOT.TH1D('h_skeleton', f'h_skeleton;{var_to_plot};Events;', nbins, h_min, h_max)
    hists = get_signal_histograms(trees, h_skeleton, var_to_plot, selection, suffix)
    hists.update(get_bkg_histograms(trees, h_skeleton, var_to_plot, selection, suffix, do_data_bkg, check_pass_fail))
    return hists

def update_baseline_significances(histograms, do_data_bkg):
    for s in signal_dsids:
        baseline_significances[f'{s}_MC'] = pu.get_significance(
            histograms[f'h_m_hhBoosted_{s}'], histograms[f'h_m_hhBoosted_bkg'],
            vars['m_hhBoosted'][1], vars['m_hhBoosted'][2]
        )
        if do_data_bkg:
            baseline_significances[f'{s}_data_bkg'] = pu.get_significance(
                histograms[f'h_m_hhBoosted_{s}'], histograms[f'h_m_hhBoosted_data_bkg'],
                vars['m_hhBoosted'][1], vars['m_hhBoosted'][2]
            )

def plot_stack_and_hists(out_dir, histograms, signal_key, var_to_plot, suffix, short_title):
    h_bkg_stack        = histograms[f'h_{var_to_plot}{suffix}_stack']
    h_signal           = histograms[f'h_{var_to_plot}{suffix}_{signal_key}']
    h_ttbar_HT         = histograms[f'h_{var_to_plot}{suffix}_ttbar_HT']
    h_dijet_bfilt      = histograms[f'h_{var_to_plot}{suffix}_dijet_bfilt']
    h_bkg              = histograms[f'h_{var_to_plot}{suffix}_bkg']
    pu.plot_stack(
        out_dir, h_bkg_stack,
        f'h_{var_to_plot}{suffix}_stack',
        [h_signal], [signal_key],
        short_title = short_title, plot_stat_err = True
    )
    # redraw plot with significance if needed
    if var_to_plot == 'm_hhBoosted':
        Z = pu.get_significance(h_signal, h_bkg, vars[var_to_plot][1], vars[var_to_plot][2])
        Z_title = f'{short_title}, Z = {Z:.2f}' if short_title != '' else f'Z = {Z:.2f}'
        pu.plot_stack(
            out_dir, h_bkg_stack,
            f'h_{var_to_plot}{suffix}_stack',
            [h_signal], [signal_key],
            short_title = Z_title, plot_stat_err = True
        )
    pu.plot_histograms(
        out_dir, [pu.get_unit_normalized_hist(h) for h in [h_bkg, h_signal, h_ttbar_HT, h_dijet_bfilt]],
        [pu.legend_names[p] for p in ['bkg', signal_key, 'ttbar_HT', 'dijet_bfilt']],
        f'h_{var_to_plot}{suffix}_norm',
        short_title = short_title, plot_stat_err = True
    )
    pu.plot_single_hist(
        out_dir, h_signal, 
        f'h_{var_to_plot}{suffix}_{signal_key}',
        f'{pu.legend_names[signal_key]}, {short_title}',
        show_entries = True, plot_stat_err = True
    )
    pu.plot_single_hist(
        out_dir, h_ttbar_HT, 
        f'h_{var_to_plot}{suffix}_ttbar_HT',
        f'{pu.legend_names["ttbar_HT"]}, {short_title}',
        show_entries = True, plot_stat_err = True
    )
    pu.plot_single_hist(
        out_dir, h_dijet_bfilt, 
        f'h_{var_to_plot}{suffix}_dijet_bfilt',
        f'{pu.legend_names["dijet_bfilt"]}, {short_title}',
        show_entries = True, plot_stat_err = True
    )
    pu.plot_single_hist(
        out_dir, h_bkg, 
        f'h_{var_to_plot}{suffix}_bkg',
        f'{pu.legend_names["bkg"]}, {short_title}',
        show_entries = True, plot_stat_err = True
    )
    # data
    if do_data_bkg:
        h_data_bkg = histograms[f'h_{var_to_plot}{suffix}_data_bkg']
        pu.plot_stack(
            out_dir, h_bkg_stack,
            f'h_{var_to_plot}{suffix}_stack_data_bkg',
            [h_signal, h_data_bkg], [signal_key, 'data_bkg'],
            short_title, plot_stat_err = True
        )
        pu.plot_single_hist(
            out_dir, h_data_bkg,
            f'h_{var_to_plot}{suffix}_data_bkg',
            f'{pu.legend_names["data_bkg"]}, {short_title}',
            show_entries = True, plot_stat_err = True
        )
        pu.plot_histograms(
            out_dir,
            [pu.get_unit_normalized_hist(h) for h in [h_bkg, h_signal, h_ttbar_HT, h_dijet_bfilt, h_data_bkg]],
            [pu.legend_names[p] for p in ['bkg', signal_key, 'ttbar_HT', 'dijet_bfilt', 'data_bkg']],
            f'h_{var_to_plot}{suffix}_norm_data_bkg',
            short_title, plot_stat_err = True
        )
        # redraw plot with significance if needed
        if var_to_plot == 'm_hhBoosted':
            Z = pu.get_significance(h_signal, h_data_bkg, vars[var_to_plot][1], vars[var_to_plot][2])
            Z_title = f'{short_title}, Z = {Z:.2f}' if short_title != '' else f'Z = {Z:.2f}'
            pu.plot_stack(
                out_dir, h_bkg_stack,
                f'h_{var_to_plot}{suffix}_stack_data_bkg',
                [h_signal, h_data_bkg], [signal_key, 'data_bkg'],
                Z_title, plot_stat_err = True
            )

def plot_all_histograms(out_dir, histograms, signal_key, do_data_bkg):
    print(f'start plotting {signal_key}...')
    # make  N-1 plots
    for var_to_plot in to_optimize:
        print(f'plotting N-1: {var_to_plot}')
        out = os.path.join(out_dir, 'N1')
        suffix = '_N1'
        short_title = f'Baseline selection, no {var_to_plot} cut'
        plot_stack_and_hists(out, histograms, signal_key, var_to_plot, suffix, short_title)
    # make baseline plots
    for var_to_plot, binning in vars.items():
        print(f'plotting baseline: {var_to_plot}')
        out = out_dir
        suffix = ''
        short_title = 'Baseline selection'
        plot_stack_and_hists(out, histograms, signal_key, var_to_plot, suffix, short_title)
    # make optimization plots
    for var_to_plot in to_optimize:
        for new_cut in to_optimize[var_to_plot]:
            suffix = f'_{var_to_plot}_{new_cut}'
            # optimize significance with m_hhBoosted
            print(f'plotting selection: {var_to_plot} to {new_cut}')
            out = os.path.join(out_dir, 'optimizations')
            short_title = ''
            plot_stack_and_hists(out, histograms, signal_key, 'm_hhBoosted', suffix, short_title)
    # make optimized plots
    for var_to_plot, binning in vars.items():
        out = os.path.join(out_dir, 'optimized')
        suffix = f'_optimized_{signal_key}'
        short_title = f'Optimized {signal_key} selection'
        plot_stack_and_hists(out, histograms, signal_key, var_to_plot, suffix, short_title)

def get_tgraph_best_value(tg):
    x_list = list(tg.GetX())
    y_list = list(tg.GetY())
    i_max = y_list.index(max(y_list))
    return x_list[i_max]

def get_significance_tgraph(signal_key, optimized_var, scanned_values, significances, do_data_bkg = False):
    if len(scanned_values) != len(significances):
        raise ValueError(f'{len(scanned_values)} but {len(significances)} significances!')
    tg = ROOT.TGraph(len(significances), array.array('d', scanned_values), array.array('d',significances))
    tg.GetXaxis().SetTitle(optimized_var)
    tg.GetYaxis().SetTitle('Significance')
    tg.SetMarkerColor(color_map[signal_key])
    tg.SetLineColor(color_map[signal_key])
    # from ATLAS style
    tg.SetMarkerStyle(20)
    tg.SetMarkerSize(1.2)
    tg.SetLineWidth(2)
    if not do_data_bkg:
        tg.SetLineStyle(2)
    return tg

def print_cutflow(df, cuts, sample):
    print(f'\ncutflow for: {sample}')
    for i, (selection, cut) in enumerate(cuts.items()):
        if ('data_bkg' in sample or 'CR' in sample) and 'HbbWP' in selection:
            continue
        if 'CR' in sample and selection == 'SR':
            if sample == 'data_CR_pass_Xbb':
                curr = curr.Filter(data_cuts['pass_Xbb'], 'pass_Xbb')
            if sample == 'data_CR_fail_Xbb':
                curr = curr.Filter(data_cuts['fail_Xbb'], 'fail_Xbb')
            continue
        if 'CR' in sample and 'm_h' in selection:
            continue
        if i == 0:
            curr = df.Filter(cut, selection)
        else:
            curr = curr.Filter(cut, selection)
        if 'data_bkg' in sample and selection == 'boostedPrecut':
            if sample == 'data_bkg_loose':
                curr = curr.Filter(data_cuts['SR_fail_Xbb_loose'], 'SR_fail_Xbb_loose')
            else:
                curr = curr.Filter(data_cuts['SR_fail_Xbb'], 'SR_fail_Xbb')
        if 'CR' in sample and selection == 'passVBFJets':
            curr = curr.Filter(data_cuts['not_SR'], 'not_SR')
            curr = curr.Filter(data_cuts['cut_bottom_left'], 'cut_bottom_left')
    curr.Report().Print()

def get_sigs(sig_map, signal, var, do_data_bkg = False, debug = False):
    sigs = []
    if debug: print(f'get_sigs for {signal}, {var}, do_data_bkg = {do_data_bkg}')
    for k in sig_map:
        k_sig = k.replace('_data_bkg', '').split('_')[-1]
        k_var = '_'.join(k.replace('Z_', '').replace('_data_bkg', '').replace(f'_{k_sig}', '').split('_')[:-1])
        if debug: print(k)
        if k_sig != signal:
            if debug: print(f'skip 1: {k.replace("_data_bkg", "").split("_")[-1]} vs. {signal}')
            continue
        if k_var != var:
            if debug: print(f'skip 2: {k.replace("Z_", "").split("_")[0]} vs. {var}')
            continue
        if (not do_data_bkg and '_data_bkg' in k) or (do_data_bkg and '_data_bkg' not in k):
            if debug: print(f'skip 3: {do_data_bkg} vs. {"_data_bkg" in k}')
            continue
        sigs.append(sig_map[k])
    return sigs

def run_optimization(in_dir, out_file, do_data_bkg):
    print(f'Running optimization...')
    trees = get_trees(in_dir, do_data_bkg)
    hists = {}
    significances = {}
    # only run baseline plots once
    for var, binning in vars.items():
        print(f'running baseline for {var}')
        sel = get_selection('baseline', 0)
        suffix = ''
        hists.update(get_histograms(trees, var, binning[0], binning[1], binning[2], sel, suffix, do_data_bkg = do_data_bkg))
    for var_to_optimize in to_optimize:
        # make  N-1 plots
        print(f'running N-1 for {var_to_optimize}')
        sel = get_selection(var_to_optimize, 0)
        suffix = '_N1'
        binning = vars[var_to_optimize]
        if debug:
            print(f'N-1 {var_to_optimize} selection: {sel}')
        hists.update(get_histograms(trees, var_to_optimize, binning[0], binning[1], binning[2], sel, suffix, do_data_bkg = do_data_bkg))
        for new_cut in to_optimize[var_to_optimize]:
            print(f'setting: {var_to_optimize} to {new_cut}')
            sel = get_selection(var_to_optimize, new_cut)
            suffix = f'_{var_to_optimize}_{new_cut}'
            # optimize significance with m_hhBoosted
            binning = vars['m_hhBoosted']
            hists.update(get_histograms(trees, 'm_hhBoosted', binning[0], binning[1], binning[2], sel, suffix, do_data_bkg = do_data_bkg))
            # significances
            for s in signal_dsids:
                significances[f'Z{suffix}_{s}'] = pu.get_significance(
                    hists[f'h_m_hhBoosted{suffix}_{s}'], hists[f'h_m_hhBoosted{suffix}_bkg'],
                    vars['m_hhBoosted'][1], vars['m_hhBoosted'][2]
                )
                if do_data_bkg:
                    significances[f'Z{suffix}_{s}_data_bkg'] = pu.get_significance(
                        hists[f'h_m_hhBoosted{suffix}_{s}'], hists[f'h_m_hhBoosted{suffix}_data_bkg'],
                        vars['m_hhBoosted'][1], vars['m_hhBoosted'][2]
                    )
    if debug:
        print('significances:')
        for k, sig in significances.items():
            print(f'\t{k}: {sig}')
    # TODO: update relevant dictionary with optimized values automatically; for now we need to run/update/run
    for s in signal_dsids:
        optimized_sel = ROOT.TCut('1')
        for v, c in optimized_cuts[s].items():
            optimized_sel += c
        for var, binning in vars.items():
            suffix = f'_optimized_{s}'
            hists.update(get_histograms(trees, var, binning[0], binning[1], binning[2], optimized_sel, suffix, do_data_bkg = do_data_bkg))
    # now make significances tgraphs
    significance_tgraphs = {}
    for s in signal_dsids:
        for var_to_optimize in to_optimize:
            tg_key_stem = f'tg_{var_to_optimize}_{s}'
            sigs = get_sigs(significances, s, var_to_optimize)
            significance_tgraphs[f'{tg_key_stem}_MC'] = get_significance_tgraph(s, var_to_optimize, to_optimize[var_to_optimize], sigs)
            if do_data_bkg:
                sigs = get_sigs(significances, s, var_to_optimize, do_data_bkg = True)
                significance_tgraphs[f'{tg_key_stem}_data_bkg'] = get_significance_tgraph(s, var_to_optimize, to_optimize[var_to_optimize], sigs, do_data_bkg = do_data_bkg)
    # save to file for later manipulation
    f = ROOT.TFile.Open(out_file, 'RECREATE')
    for h_name, h in hists.items():
        h.Clone(h_name).Write()
    for tg_name, tg in significance_tgraphs.items():
        tg.Clone(tg_name).Write()
    f.Close()

def plot_significance_tgraphs(out_dir, significance_tgraphs, signal_key, do_data_bkg):
    # Now plot significances as a function of the cuts
    best_cut_values = {}
    print(f'Baseline significances for {signal_key}: {baseline_significances[f"{signal_key}_MC"]} (MC)')
    if do_data_bkg:
        print(f'Baseline significances for {signal_key}: {baseline_significances[f"{s}_data_bkg"]} (data_bkg)')
    for var_to_optimize in to_optimize:
        tg_key = f'tg_{var_to_optimize}_{signal_key}_MC'
        tg = significance_tgraphs[tg_key]
        best_cut_value = get_tgraph_best_value(tg)
        best_cut_values[f'{var_to_optimize}_MC'] = best_cut_value
        pu.plot_tgraphs(
            out_dir, [tg], ['bkg'], tg_key,
            short_title = f'Best {var_to_optimize}: {best_cut_value}',
            hline = baseline_significances[f'{signal_key}_MC']
        )
    # print optimized selection
    print(f'Optimized Selection for {signal_key} (MC):')
    for var, best_value in best_cut_values.items():
        if '_data_bkg' in var: continue
        print(f'\t{var}: {best_value}')
    if do_data_bkg:
        for var_to_optimize in to_optimize:
            tg_key = f'tg_{var_to_optimize}_{signal_key}_data_bkg'
            tg = significance_tgraphs[tg_key]
            best_cut_value = get_tgraph_best_value(tg)
            best_cut_values[f'{var_to_optimize}_data_bkg'] = best_cut_value
            pu.plot_tgraphs(
                out_dir, [tg], ['data_bkg'], tg_key,
                short_title = f'Best {var_to_optimize}: {best_cut_value}',
                hline = baseline_significances[f'{signal_key}_data_bkg']
            )
        # print optimized selection
        print(f'Optimized Selection for {signal_key} (data_bkg):')
        for var, best_value in best_cut_values.items():
            if '_MC' in var: continue
            print(f'\t{var}: {best_value}')
        # compare data_bkg to MC
        for var_to_optimize in to_optimize:
            tg_key_stem = f'tg_{var_to_optimize}_{signal_key}'
            pu.plot_tgraphs(
                out_dir, [significance_tgraphs[f'{tg_key_stem}_MC'], significance_tgraphs[f'{tg_key_stem}_data_bkg']],
                ['bkg', 'data_bkg'], f'{tg_key_stem}_comparison', short_title = f'Best {var_to_optimize}:',
                variation_label = f'{best_cut_values[f"{var_to_optimize}_MC"]} (MC), {best_cut_values[f"{var_to_optimize}_data_bkg"]} (DD)',
                hline = baseline_significances[f'{signal_key}_data_bkg']
            )

def plot_signal_shapes(histograms, out_dir):
    for var in vars:
        pu.plot_histograms(
            out_dir, [pu.get_unit_normalized_hist(histograms[f'h_{var}_{s}']) for s in signal_dsids],
            [pu.legend_names[s] for s in signal_dsids], f'h_{var}_signal_comparison',
            'Baseline selection', plot_stat_err = True
        )

def plot_signal_significances(significance_tgraphs, out_dir, do_data_bkg):
    # compare signal significance curves for each optimized variable
    for var_to_optimize in to_optimize:
        tg_key_stem = f'tg_{var_to_optimize}'
        pu.plot_tgraphs(
            out_dir, [significance_tgraphs[f'{tg_key_stem}_{s}_MC'] for s in signal_dsids],
            [s for s in signal_dsids], f'{tg_key_stem}_MC_comparison',
            short_title = f'Best {var_to_optimize}:',
            variation_label = ', '.join([f'{get_tgraph_best_value(significance_tgraphs[f"{tg_key_stem}_{s}_MC"])} ({s})' for s in signal_dsids]).rstrip(', ')
        )
        if do_data_bkg:
            pu.plot_tgraphs(
                out_dir,[significance_tgraphs[f'{tg_key_stem}_{s}_data_bkg'] for s in signal_dsids],
                [s for s in signal_dsids], f'{tg_key_stem}_data_bkg_comparison',
                short_title = f'Best {var_to_optimize}:',
                variation_label = ', '.join([f'{get_tgraph_best_value(significance_tgraphs[f"{tg_key_stem}_{s}_data_bkg"])} ({s})' for s in signal_dsids]).rstrip(', ')
            )

def get_pass_fail_selections():
    no_Xbb_sel = ROOT.TCut(get_selection('baseline', 0).GetTitle().replace(baseline_cuts['HbbWP_h1Boosted'], '1').replace(baseline_cuts['HbbWP_h2Boosted'], '1'))
    pass_fail_selections = {
        'pass_pass' : get_selection('baseline', 0),
        'pass_fail' : no_Xbb_sel + ROOT.TCut(data_cuts['fail_Xbb2']),
        'fail_pass' : no_Xbb_sel + ROOT.TCut(data_cuts['fail_Xbb1']),
    }
    return pass_fail_selections

def make_pass_fail_Xbb_histograms(out_file, do_data_bkg):
    print('making pass/fail Xbb histograms...')
    binning = vars['m_hhBoosted']
    pf_selections = get_pass_fail_selections()
    trees = {}
    hists = {}
    significances = {}
    combinations = [k for k in pf_selections]
    for check_pass_fail in combinations:
        print(f'checking {check_pass_fail} Xbb regions...')
        suffix = f'_{check_pass_fail}' if check_pass_fail != 'pass_pass' else ''
        sel = pf_selections[check_pass_fail]
        trees = get_trees(in_dir, do_data_bkg, check_pass_fail = check_pass_fail if check_pass_fail != 'pass_pass' else '')
        hists.update(get_histograms(trees, 'm_hhBoosted', binning[0], binning[1], binning[2], sel, suffix, do_data_bkg, check_pass_fail = check_pass_fail if check_pass_fail != 'pass_pass' else ''))
        for s in signal_dsids:
            # unweighted
            n_evt_sel_unweighted = trees[s].Draw("m_hhBoosted", sel)
            n_evt_sel_unweighted_ttbar = trees['ttbar_HT'].Draw("m_hhBoosted", sel)
            n_evt_sel_unweighted_dijet = trees['dijet_bfilt'].Draw("m_hhBoosted", sel)
            data_sel = ROOT.TCut(data_cuts['SR_fail_Xbb']) + ROOT.TCut(sel.GetTitle().replace(baseline_cuts['HbbWP_h1Boosted'], '1').replace(baseline_cuts['HbbWP_h2Boosted'], '1'))
            n_evt_sel_unweighted_data_bkg = trees['data_bkg'].Draw("m_hhBoosted", data_sel)
            # weighted
            n_evt_sel_weighted = hists[f'h_m_hhBoosted{suffix}_{s}'].Integral()
            n_evt_sel_weighted_bkg = hists[f'h_m_hhBoosted{suffix}_bkg'].Integral()
            n_evt_sel_weighted_data_bkg = hists[f'h_m_hhBoosted{suffix}_data_bkg'].Integral()
            print('events: weighted (unweighted)')
            print(f'\t{s}: {n_evt_sel_weighted:.2f} ({n_evt_sel_unweighted})')
            print(f'\tMC bkg: {n_evt_sel_weighted_bkg:.2f} ({n_evt_sel_unweighted_ttbar + n_evt_sel_unweighted_dijet})')
            print(f'\tDD-bkg: {n_evt_sel_weighted_data_bkg:.2f} ({n_evt_sel_unweighted_data_bkg})')
            # significances
            significances[f'Z{suffix}_{s}'] = pu.get_significance(
                hists[f'h_m_hhBoosted{suffix}_{s}'], hists[f'h_m_hhBoosted{suffix}_bkg'],
                vars['m_hhBoosted'][1], vars['m_hhBoosted'][2]
            )
            if do_data_bkg:
                significances[f'Z{suffix}_{s}_data_bkg'] = pu.get_significance(
                    hists[f'h_m_hhBoosted{suffix}_{s}'], hists[f'h_m_hhBoosted{suffix}_data_bkg'],
                    vars['m_hhBoosted'][1], vars['m_hhBoosted'][2]
                )
    print('significances:')
    for k, sig in significances.items():
        print(f'\t{k}: {sig}')
    # save to file for later manipulation
    f = ROOT.TFile.Open(out_file, 'RECREATE')
    for h_name, h in hists.items():
        h.Clone(h_name).Write()
    f.Close()

def plot_pass_fail_checks(out_dir, histograms, signal_key, do_data_bkg):
    print(f'start plotting pass/fail checks for {signal_key}...')
    significances = {}
    pf_selections = get_pass_fail_selections()
    combinations = [k for k in pf_selections]
    for check_pass_fail in combinations:
        print(f'plotting {check_pass_fail}')
        suffix = f'_{check_pass_fail}' if check_pass_fail != 'pass_pass' else ''
        short_title = check_pass_fail
        plot_stack_and_hists(out_dir, histograms, signal_key, 'm_hhBoosted', suffix, short_title)
        for s in signal_dsids:
            significances[f'Z{suffix}_{s}'] = pu.get_significance(
                histograms[f'h_m_hhBoosted{suffix}_{s}'], histograms[f'h_m_hhBoosted{suffix}_bkg'],
                vars['m_hhBoosted'][1], vars['m_hhBoosted'][2]
            )
            if do_data_bkg:
                significances[f'Z{suffix}_{s}_data_bkg'] = pu.get_significance(
                    histograms[f'h_m_hhBoosted{suffix}_{s}'], histograms[f'h_m_hhBoosted{suffix}_data_bkg'],
                    vars['m_hhBoosted'][1], vars['m_hhBoosted'][2]
                )
    print('significances:')
    for k, sig in significances.items():
        print(f'\t{k}: {sig}')

def get_df_from_rdfs(rdfs, samples, selection, variables):
    cols_tmp = {}
    for s in samples:
        cols_tmp[s] = rdfs[s].Filter(selection).AsNumpy(variables)
        print(f'filtered {s} has entries: {len(cols_tmp[s][list(cols_tmp[s].keys())[0]])}')
    # concatenate all samples into numpy arrays
    cols = {}
    for v in variables:
        cols[v] = np.concatenate([cols_tmp[s][v] for s in samples])
    # get df from rdfs
    df = pd.DataFrame()
    if 'totalWeightBoosted' not in cols:
        raise ValueError('no weights in rdfs!')
    if 'pt_hhBoosted' not in cols:
        raise ValueError('no pt_hhBoosted in rdfs!')
    if 'lumi' not in cols:
        raise ValueError('no lumi in rdfs!')
    # include luminosity in weights
    full_weights = cols['totalWeightBoosted'] * cols['lumi']
    for v in cols:
        if v in ['totalWeightBoosted', 'lumi']:
            continue
        df[v] = cols[v]
    return df, full_weights

def fix_relative_variables(df, rel_vars):
    for v in rel_vars:
        df[v] = df[v] / df['pt_hhBoosted']
        df.rename(columns = {v : f'rel_{v}'}, inplace = True)
    return df

def plot_variable_correlations(rdfs, selection, variables, out_dir, suffix = ''):
    print(f'suffix: {suffix}')
    # object allowing to get correlation coefficients with weights
    samples = ['k2v0', 'ttbar_HT', 'dijet_bfilt']
    df, full_weights = get_df_from_rdfs(rdfs, samples, selection, variables)
    # fix relative variables
    df = fix_relative_variables(df, ['pt_h1Boosted', 'pt_h2Boosted', 'm_h1Boosted', 'm_h2Boosted', 'pt_VBFj1Boosted'])
    # make column names less cumbersome for correlation plot
    df.rename(columns = {v : v.replace('Boosted', '') for v in df.columns}, inplace = True)
    from statsmodels.stats.weightstats import DescrStatsW
    dsw = DescrStatsW(df, weights = full_weights)
    scale = 2.25
    plt.figure(figsize=(8*scale, 6*scale))
    sns.heatmap(dsw.corrcoef, annot=True, annot_kws={"fontsize" : 6}, fmt='.2f', xticklabels=df.columns, yticklabels=df.columns)
    plt.title('Correlation Matrix', fontsize=16)
    plt.savefig(os.path.join(out_dir, f'variable_correlations{suffix}.pdf'), bbox_inches='tight')

def compare_xbb_wps(in_dir, do_data_bkg, histograms_file, out_dir, wp_a = '60', wp_b = '70'):
    # # compare cutflows
    # rdfs = get_rdfs(in_dir, do_data_bkg)
    # # print all cutflows from large RDFs
    # for sample in [s for s in signal_dsids] + ['ttbar_HT', 'dijet_bfilt']:
    #     print_cutflow(rdfs[sample], baseline_cuts, sample)
    #     print_cutflow(rdfs[sample], baseline_cuts_loose, f'{sample}_loose')
    # if do_data_bkg:
    #     print_cutflow(rdfs['data_bkg'], baseline_cuts, 'data_bkg')
    #     print_cutflow(rdfs['data_bkg'], baseline_cuts, 'data_bkg_loose')
    # compare significances
    trees = get_trees(in_dir, do_data_bkg)
    hists = {}
    significances = {}
    # only run baseline for now
    for var, binning in vars.items():
        if var != 'm_hhBoosted': continue
        sel = get_selection('baseline', 0)
        sel_loose = ROOT.TCut(get_selection('baseline', 0).GetTitle().replace(wp_a, wp_b))
        hists.update(get_histograms(trees, var, binning[0], binning[1], binning[2], sel, '', do_data_bkg = do_data_bkg))
        hists.update(get_histograms(trees, var, binning[0], binning[1], binning[2], sel_loose, '_loose', do_data_bkg = do_data_bkg))

    for suffix in ['', '_loose']:
        for s in signal_dsids:
            significances[f'Z_{s}'] = pu.get_significance(
                hists[f'h_m_hhBoosted{suffix}_{s}'], hists[f'h_m_hhBoosted{suffix}_bkg'],
                vars['m_hhBoosted'][1], vars['m_hhBoosted'][2]
            )
            if do_data_bkg:
                significances[f'Z{suffix}_{s}_data_bkg'] = pu.get_significance(
                    hists[f'h_m_hhBoosted{suffix}_{s}'], hists[f'h_m_hhBoosted{suffix}_data_bkg'],
                    vars['m_hhBoosted'][1], vars['m_hhBoosted'][2]
                )
    print('significances:')
    for k, sig in significances.items():
        print(f'\t{k}: {sig}')
    # save to file for later manipulation
    f = ROOT.TFile.Open(histograms_file, 'RECREATE')
    for h_name, h in hists.items():
        h.Clone(h_name).Write()
    f.Close()
    # now make the plots
    histograms = pu.load_from_file(histograms_file, 'TH1D')
    histograms.update(pu.load_from_file(histograms_file, 'THStack'))
    pu.set_ATLAS_style()
    for s in signal_dsids:
        short_title = 'Baseline selection'
        out = os.path.join(out_dir, s)
        os.makedirs(out, exist_ok = True)
        plot_stack_and_hists(out, histograms, s, 'm_hhBoosted', '', short_title)
        plot_stack_and_hists(out, histograms, s, 'm_hhBoosted', '_loose', f'{short_title} (loose Xbb)')

if __name__ == '__main__':

    parser = get_parser()
    args = parser.parse_args()
    # required arguments
    in_dir, out_dir = args.in_dir, args.out_dir
    if args.debug:
        debug = args.debug
    if args.data_bkg:
        print('running with data-driven background!')
        do_data_bkg = args.data_bkg
    if args.print_cutflows:
        print('printing cutflows')
        print_cutflows = args.print_cutflows
    if args.force_tgraph:
        print('forcing tgraph generation!')
        force_tgraph_generation = args.force_tgraph
    if args.check_pass_fail_Xbb:
        print('checking pass/fail Xbb regions')
        check_pass_fail_Xbb = args.check_pass_fail_Xbb
    if args.variable_correlations:
        print('plotting variable correlations')
        plot_correlations = args.variable_correlations
    if args.compare_xbb:
        print('comparing Xbb WPs')
        compare_xbb = args.compare_xbb

    ROOT.gROOT.SetBatch(1)
    if print_cutflows or plot_correlations:
        # get large rdfs for cutflows
        rdfs = get_rdfs(in_dir, do_data_bkg)
    if print_cutflows:
        # print all cutflows from large RDFs
        for sample in [s for s in signal_dsids] + ['ttbar_HT', 'dijet_bfilt']:
            print_cutflow(rdfs[sample], baseline_cuts, sample)
        if do_data_bkg:
            print_cutflow(rdfs['data_bkg'], baseline_cuts, 'data_bkg')
    if plot_correlations:
        # plot correlations
        corr_vars = ['m_hhBoosted', 'pt_h1Boosted', 'eta_h1Boosted', 'phi_h1Boosted', 'm_h1Boosted', 'HbbWP_h1Boosted', 'pt_h2Boosted', 'eta_h2Boosted', 'phi_h2Boosted', 'm_h2Boosted', 'HbbWP_h2Boosted', 'pt_hhBoosted', 'eta_hhBoosted', 'phi_hhBoosted', 'pt_VBFj1Boosted', 'eta_VBFj1Boosted', 'phi_VBFj1Boosted', 'E_VBFj1Boosted', 'pt_VBFj2Boosted', 'eta_VBFj2Boosted', 'phi_VBFj2Boosted', 'E_VBFj2Boosted', 'totalWeightBoosted', 'lumi']
        corr_vars += ['aplanarityBoosted', 'planarityBoosted', 'sphericityBoosted', 'shapeCBoosted', 'shapeDBoosted']
        corr_vars += [f'FWM{i}Boosted' for i in range(10)]
        corr_vars += [f'HCM{i}Boosted' for i in range(10)]
        plot_variable_correlations(rdfs, correlation_selection, corr_vars, out_dir)

    if compare_xbb:
        histograms_file = os.path.join(out_dir, 'hists_compare_xbb.root')
        compare_xbb_wps(in_dir, do_data_bkg, histograms_file, out_dir)
        sys.exit()
    # save smaller trees for optimizations if they don't exist or if we force them
    save_slimmed_trees(in_dir, do_data_bkg)
    if check_pass_fail_Xbb:
        histograms_file_pass_fail = os.path.join(out_dir, 'hists_pass_fail_Xbb.root')
        if not os.path.exists(histograms_file_pass_fail):
            make_pass_fail_Xbb_histograms(histograms_file_pass_fail, do_data_bkg)
        histograms_pass_fail = pu.load_from_file(histograms_file_pass_fail, 'TH1D')
        histograms_pass_fail.update(pu.load_from_file(histograms_file_pass_fail, 'THStack'))
        pu.set_ATLAS_style()
        for s in signal_dsids:
            out_dir_pass_fail = os.path.join(out_dir, s, 'pass_fail_checks')
            os.makedirs(out_dir_pass_fail, exist_ok = True)
            plot_pass_fail_checks(out_dir_pass_fail, histograms_pass_fail, s, do_data_bkg)
    # make output directories if they don't exist
    for s in signal_dsids:
        os.makedirs(os.path.join(out_dir, s), exist_ok = True)
        out_dirs = ['', 'N1', 'optimizations', 'optimized']
        for out in out_dirs:
            os.makedirs(os.path.join(out_dir, s, out), exist_ok = True)
    # run optimizations
    histograms_file = os.path.join(out_dir, 'hists.root')
    if not os.path.exists(histograms_file) or force_tgraph_generation:
        # run the optimization and produce tgraphs
        run_optimization(in_dir, histograms_file, do_data_bkg)
        sys.exit()
    else:
        print(f'using existing histogram file...')
    significance_tgraphs = pu.load_from_file(histograms_file, 'TGraph')
    histograms = pu.load_from_file(histograms_file, 'TH1D')
    histograms.update(pu.load_from_file(histograms_file, 'THStack'))
    update_baseline_significances(histograms, do_data_bkg)
    # only set style after gettting histograms to avoid histogram color loss...
    pu.set_ATLAS_style()
    for s in signal_dsids:
        plot_all_histograms(os.path.join(out_dir, s), histograms, s, do_data_bkg)
        plot_significance_tgraphs(os.path.join(out_dir, s), significance_tgraphs, s, do_data_bkg)
    # plot simple shape comparisons with baseline selection for signal
    os.makedirs(os.path.join(out_dir, 'signal_comparison'), exist_ok = True)
    plot_signal_shapes(histograms, os.path.join(out_dir, 'signal_comparison'))
    # compare significances for different signals
    plot_signal_significances(significance_tgraphs, os.path.join(out_dir, 'signal_comparison'), do_data_bkg)