"""Produce roc curves from tagger output and labels."""
import numpy as np
import pandas as pd
import h5py

from puma.hlplots import Results, Tagger
from puma.utils import logger
from puma.metrics import calc_eff
from puma import VarVsEff, VarVsEffPlot, Line2D, Line2DPlot


# load jets from evaluated results
out_dir = '/Users/sebastienrettie/CTIDE/gn1_evaluations/plots_pseudo/'
reference = 'nom'
# in GeV
min_pt_eval = '1000'
trainings = {
    'nom' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230223-T182422/ckpts/epoch=095-val_loss=0.76674__test_inclusive_testing_zprime_PFlow.h5',
    # 'nom_80' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230321-T063502/ckpts/epoch=095-val_loss=0.76241__test_inclusive_testing_zprime_PFlow.h5',
    'nomnom' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230306-T062324/ckpts/epoch=095-val_loss=0.78145__test_inclusive_testing_zprime_PFlow.h5',
    # 'nomnom_80' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230321-T104659/ckpts/epoch=095-val_loss=0.77947__test_inclusive_testing_zprime_PFlow.h5',
    'skip_ambi_roi' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230228-T135919/ckpts/epoch=098-val_loss=0.75035__test_inclusive_testing_zprime_PFlow.h5',
    # 'skip_ambi_roi_80' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230321-T131925/ckpts/epoch=098-val_loss=0.74358__test_inclusive_testing_zprime_PFlow.h5',
    # 'skip_ambi_roi_A' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230306-T061409/ckpts/epoch=097-val_loss=0.75093__test_inclusive_testing_zprime_PFlow.h5',
    # 'skip_ambi_roi_B' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230224-T082624/ckpts/epoch=096-val_loss=0.75011__test_inclusive_testing_zprime_PFlow.h5',
    # 'skip_ambi_roi_C' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230224-T083312/ckpts/epoch=098-val_loss=0.75078__test_inclusive_testing_zprime_PFlow.h5',
    # 'skip_ambi_roi_D' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230224-T132447/ckpts/epoch=093-val_loss=0.74974__test_inclusive_testing_zprime_PFlow.h5',
    # 'skip_ambi_roi_RF' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230306-T061856/ckpts/epoch=096-val_loss=0.75200__test_inclusive_testing_zprime_PFlow.h5',
    'pseudo_80' : '/Users/sebastienrettie/CTIDE/gn1_evaluations/GN1_20230407-T070010/ckpts/epoch=093-val_loss=0.61727__test_inclusive_testing_zprime_PFlow.h5',
}
labels = {
    'nom' : 'nom, relaxed SH',
    'nom_80' : 'nom, relaxed SH, 80 tracks',
    'nomnom' : 'nom',
    'nomnom_80' : 'nom, 80 tracks',
    'skip_ambi_roi' : 'skip_ambi_roi',
    'skip_ambi_roi_80' : 'skip_ambi_roi, 80 tracks',
    'skip_ambi_roi_A' : 'skip_ambi_roi_A',
    'skip_ambi_roi_B' : 'skip_ambi_roi_B',
    'skip_ambi_roi_C' : 'skip_ambi_roi_C',
    'skip_ambi_roi_D' : 'skip_ambi_roi_D',
    'skip_ambi_roi_RF' : 'skip_ambi_roi_RF',
    'pseudo_80' : 'pseudo, 80 tracks',
}
# https://rpubs.com/mjvoss/psc_viridis
colors = {
    'nom' : '#440154FF',
    'nom_80' : '#FDE725FF',
    'nomnom' : '#000000',
    'nomnom_80' : '#FDE725FF',
    'skip_ambi_roi' : '#2A788EFF',
    'skip_ambi_roi_80' : '#FDE725FF',
    'skip_ambi_roi_A' : '#414487FF',
    'skip_ambi_roi_B' : '#22A884FF',
    'skip_ambi_roi_C' : '#7AD151FF',
    'skip_ambi_roi_D' : '#FDE725FF',
    'skip_ambi_roi_RF' : '#FDE725FF',
    'pseudo_80' : '#7AD151FF',
}
# ensure we have relevant labels and colors
for t in trainings:
    assert t in labels
    assert t in colors

# fraction scans
fc_values = np.linspace(0., 1., 201)
SIG_EFF = 0.77
f_c = 0.05
base_tag = f'$\\sqrt{{s}}=13$ TeV\n$Z\'$, $p_{{T}} > {min_pt_eval}$ GeV\n$f_{{c}}$ = {f_c}'

# test to see if using the same test file for all trainings changes things, but for now use dedicated test file for each training

def wp_cut(cutmin, cutmax, cutvar, df, eff):
    bindf = df.query(f'{cutvar} > {cutmin}').query(f'{cutvar} < {cutmax}')
    dcut = np.percentile(bindf['D_b'], 100*(1-eff))
    return dcut

def calc_effs(fc_value: float, df):
    """Tagger efficiency for fixed working point

    Parameters
    ----------
    fc_value : float
        Value for the charm fraction used in discriminant calculation.

    Returns
    -------
    tuple
        Tuple of shape (, 3) containing (fc_value, ujets_eff, cjets_eff)
    """
    arr = df[['GN1_pu','GN1_pc','GN1_pb']].values
    is_b = df['HadronConeExclTruthLabelID'] == 5
    is_c = df['HadronConeExclTruthLabelID'] == 4
    is_light = df['HadronConeExclTruthLabelID'] == 0
    disc = arr[:, 2] / (fc_value * arr[:, 1] + (1 - fc_value) * arr[:, 0])
    ujets_eff = calc_eff(disc[is_b], disc[is_light], SIG_EFF)
    cjets_eff = calc_eff(disc[is_b], disc[is_c], SIG_EFF)

    return [fc_value, ujets_eff, cjets_eff]

def get_tagger_df(name, fname):
    with h5py.File(fname, 'r') as f:
        df = pd.DataFrame(
            f['jets'].fields(
                [
                    'GN1_pu',
                    'GN1_pc',
                    'GN1_pb',
                    'pt',
                    'eta',
                    'HadronConeExclTruthLabelID',
                    'n_tracks',
                    'n_truth_promptLepton',
                ]
            )[:]
        )
        # avoid inf warnings
        df = df.astype('float32')
    # remove all jets which are not trained on
    class_ids = [0, 4, 5]
    df.query(f'HadronConeExclTruthLabelID in {class_ids}', inplace=True)
    # only look at high-pT jets
    df.query(f'pt > {min_pt_eval}e3', inplace=True)
    # don't use jets with prompt leptons
    df.query('n_truth_promptLepton == 0', inplace=True)
    return df

def get_tagger(name, fname):
    print(f'getting tagger {name} ({fname})')
    df = get_tagger_df(name, fname)
    # WARNING: if you use 2 different data frames you need to specify the `is_light`,
    # `is_c` and `is_b` for each data frame separately and thus you cannot use these
    # args for each tagger the same applies to the `perf_var`
    tagger_args = {
        'is_flav': {
            'bjets': df['HadronConeExclTruthLabelID'] == 5,
            'cjets': df['HadronConeExclTruthLabelID'] == 4,
            'ujets': df['HadronConeExclTruthLabelID'] == 0,
        }
    }
    tagger = Tagger('GN1', **tagger_args)
    tagger.perf_var = df['pt'] / 1e3
    tagger.label = labels[name]
    tagger.f_c = f_c
    tagger.colour = colors[name]
    tagger.reference = (name == reference)
    tagger.extract_tagger_scores(df)
    return tagger

# create the Results object for c-jet signal plot use `signal='cjets'`
results = Results(signal='bjets')
taggers = {}
for n, fn in trainings.items():
    taggers[n] = get_tagger(n, fn)
    results.add(taggers[n])

logger.info('Start plotting')
results.sig_eff = np.linspace(0.35, 0.95, 20)
# results.sig_eff = np.linspace(0.6, 0.95, 20)
results.atlas_second_tag = base_tag

# tagger discriminant plots
logger.info('Plotting tagger discriminant plots.')
results.plot_discs(out_dir+'disc_b.pdf', bins_range=(-6,15))

# ROC curves
logger.info('Plotting ROC curves.')
results.plot_rocs(out_dir+'roc_b.pdf')

# eff/rej vs. variable plots
logger.info('Plotting efficiency/rejection vs pT curves.')
ptbins = np.linspace(int(min_pt_eval),5000,10)
# ptbins = np.linspace(0,250,10)
results.atlas_second_tag = base_tag + '\n70% $b$-jet eff. WP'
results.plot_var_perf(
    plot_name=out_dir+'beff70',
    working_point=0.7,
    bins=ptbins,
    fixed_eff_bin=False,
)
results.atlas_second_tag = base_tag + '\nFixed 70% $b$-jet eff. per bin'
results.plot_var_perf(
    plot_name=out_dir+'beff70fixed',
    working_point=0.7,
    bins=ptbins,
    fixed_eff_bin=True,
)
# no high-level API for fixed rej or frac plots, so do it manually
logger.info('Plotting manual efficiency/rejection vs pT and fc curves.')
plot_sig_eff = VarVsEffPlot(
    mode='sig_eff',
    ylabel='$b$-jets efficiency',
    xlabel=r'$p_{T}$ [GeV]',
    logy=False,
    atlas_second_tag=base_tag + '\nFixed $l$-jet rej. of 100 per bin',
    figsize=(6, 4.5),
    n_ratio_panels=1,
)
# fc scan
fc_plot = Line2DPlot()
fc_plot.ylabel = 'Light-flavour jets efficiency'
fc_plot.xlabel = '$c$-jets efficiency'
for n, fn in trainings.items():
    df = get_tagger_df(n, fn)
    t = taggers[n]
    is_b = df['HadronConeExclTruthLabelID'] == 5
    is_c = df['HadronConeExclTruthLabelID'] == 4
    is_light = df['HadronConeExclTruthLabelID'] == 0
    # add discriminant
    df['D_b'] = t.get_disc('bjets')
    # get disc cuts for 1% l-jet efficiency
    disc_cuts = [
        wp_cut(1e3*ptbins[ibin], 1e3*ptbins[ibin+1], 'pt', df[is_light], 0.01)
        for ibin in range(len(ptbins)-1)
    ]
    varvseff = VarVsEff(
        x_var_sig=t.perf_var[is_b],
        disc_sig=t.get_disc('bjets')[is_b],
        bins=ptbins,
        disc_cut = disc_cuts,
        label=labels[n],
        colour=colors[n]
    )
    plot_sig_eff.add(varvseff, reference = (n == reference))
    # fraction scans
    eff_results = np.array(list(map(calc_effs, fc_values, [df] * len(fc_values))))
    fc_line = Line2D(
        x_values=eff_results[:,2],
        y_values=eff_results[:,1],
        label=labels[n],
        colour=colors[n],
        linestyle='-',
    )
    marker_idx = list(fc_values).index(f_c)
    fc_marker = Line2D(
        x_values=eff_results[marker_idx,2],
        y_values=eff_results[marker_idx,1],
        colour=colors[n],
        marker='x',
        label=rf'$f_c={eff_results[marker_idx, 0]}$',
        markersize=15,
        markeredgewidth=2,
    )
    fc_plot.add(fc_line)
    fc_plot.add(fc_marker, is_marker=True)
plot_sig_eff.draw()
plot_sig_eff.savefig(out_dir+'pt_b_eff.pdf', transparent=False)
fc_plot.draw()
fc_plot.savefig(out_dir+'fc_scan.pdf')
