import os
import glob
import h5py
import ROOT
import pandas as pd
import numpy as np
import plot_utils as pu

# tdd files
in_dir = '/unix/atlastrackbtag/srettie/FTAG_preprocessing/tdd_ntuples/'
out_dir = '/unix/atlastrackbtag/srettie/FTAG_preprocessing/tdd_ntuples/tmp/'
out_file = os.path.join(out_dir, 'ntracks.root')
pts = ['incpt', 'lowpt', 'highpt']
flavs = ['b','c','u']
tdd_ntuples = {
    'nom' : 'user.srettie.800030.e7954_s3582_r12643.tdd.EMPFlow.22_2_108.23-02-20_nom_output.h5',
    'nomnom' : 'user.srettie.800030.e7954_s3582_r12643.tdd.EMPFlow.22_2_108.23-03-02_nomnom_output.h5',
    'skip_ambi_roi' : 'user.srettie.800030.e7954_s3582_r12643.tdd.EMPFlow.22_2_108.23-02-23_skip_ambi_roi_output.h5',
    # 'skip_ambi_roi_A' : 'user.srettie.800030.e7954_s3582_r12643.tdd.EMPFlow.22_2_108.23-02-28_skip_ambi_roi_A_output.h5',
    # 'skip_ambi_roi_B' : 'user.srettie.800030.e7954_s3582_r12643.tdd.EMPFlow.22_2_108.23-02-17_ski_ambi_roi_B_output.h5',
    # 'skip_ambi_roi_C' : 'user.srettie.800030.e7954_s3582_r12643.tdd.EMPFlow.22_2_108.23-02-18_ski_ambi_roi_C_output.h5',
    # 'skip_ambi_roi_D' : 'user.srettie.800030.e7954_s3582_r12643.tdd.EMPFlow.22_2_108.23-02-20_ski_ambi_roi_D_output.h5',
    'skip_ambi_roi_RF' : 'user.srettie.800030.e7954_s3582_r12643.tdd.EMPFlow.22_2_108.23-03-02_skip_ambi_roi_RF_output.h5',
    'pseudo' : 'user.srettie.800030.e7954_s3582_r12643.tdd.EMPFlow.23_0_15.23-03-16_pseudo_80_output.h5',
    'pseudo80' : 'user.srettie.800030.e7954_s3582_r12643.tdd.EMPFlow.23_0_20.23-04-05_pseudo80_output.h5',
}
colors = {
    'nom' : ROOT.kBlue,
    'nomnom' : ROOT.kBlack,
    'skip_ambi_roi' : ROOT.kGreen+1,
    'skip_ambi_roi_A' : ROOT.kPink-9,
    'skip_ambi_roi_B' : ROOT.kAzure+6,
    'skip_ambi_roi_C' : ROOT.kTeal+2,
    'skip_ambi_roi_D' : ROOT.kGray,
    'skip_ambi_roi_RF' : ROOT.kRed,
    'pseudo' : ROOT.kViolet,
    'pseudo80': ROOT.kTeal+2,
}
pt_labels = {
    'incpt'  : 'Inclusive p_{T}',
    'lowpt'  : 'Jet p_{T} < 250 GeV',
    'highpt' : 'Jet p_{T} #geq 250 GeV',
}
def get_jets_df(fname):
    # open files and fill histograms
    with h5py.File(fname, 'r') as f:
        df = pd.DataFrame(
            f['jets'].fields(['n_tracks', 'pt', 'HadronConeExclTruthLabelID', 'n_truth_promptLepton'])[:]
        )
        return df

def set_hist_colors(histos):
    for pt in pts:
        for flav in flavs:
            for k in tdd_ntuples:
                histos[f'{k}_{pt}_{flav}'].SetLineColor(colors[k])
                histos[f'{k}_{pt}_{flav}'].SetMarkerColor(colors[k])
                histos[f'{k}_{pt}_{flav}'].GetXaxis().SetTitle('Number of tracks per jet')
                histos[f'{k}_{pt}_{flav}'].GetYaxis().SetTitle('Number of jets')
                histos[f'{k}_{pt}_{flav}'].GetXaxis().SetRangeUser(0, 80)
    return histos

def get_histograms(outfile):
    # don't loop over ntuples if histograms exist
    if os.path.exists(outfile):
        return pu.load_from_file(outfile)
    # initialize histograms
    hists = {}
    for pt in pts:
        for flav in flavs:
            hists.update({f'{k}_{pt}_{flav}' : ROOT.TH1D(f'{k}_{pt}_{flav}', f'{k}_{pt}_{flav}', 100, 0, 100) for k in tdd_ntuples})
    for conf, dsname in tdd_ntuples.items():
        print(os.path.join(in_dir, dsname, '*'))
        all_files = glob.glob(os.path.join(in_dir, dsname, '*'))
        for infile in all_files:
            print(infile)
            df = get_jets_df(infile)
            is_b = df['HadronConeExclTruthLabelID'] == 5
            is_c = df['HadronConeExclTruthLabelID'] == 4
            is_u = df['HadronConeExclTruthLabelID'] == 0
            is_lowpt = df['pt'] < 250e3
            is_highpt = df['pt'] >= 250e3
            is_not_ejet = df['n_truth_promptLepton'] == 0
            masks = {}
            masks['incpt_b'] = is_not_ejet & is_b
            masks['incpt_c'] = is_not_ejet & is_c
            masks['incpt_u'] = is_not_ejet & is_u
            masks['lowpt_b'] = is_not_ejet & is_b & is_lowpt
            masks['lowpt_c'] = is_not_ejet & is_c & is_lowpt
            masks['lowpt_u'] = is_not_ejet & is_u & is_lowpt
            masks['highpt_b'] = is_not_ejet & is_b & is_highpt
            masks['highpt_c'] = is_not_ejet & is_c & is_highpt
            masks['highpt_u'] = is_not_ejet & is_u & is_highpt
            ntrack_arrays = {}
            for pt in pts:
                for flav in flavs:
                    key = f'{pt}_{flav}'
                    ntrack_arrays[key] = df[masks[key]]['n_tracks']
            for k, ntrackarr in ntrack_arrays.items():
                for ntrk in ntrackarr:
                    hists[f'{conf}_{k}'].Fill(ntrk)
    # save histograms
    f = ROOT.TFile.Open(outfile, 'RECREATE')
    f.cd()
    for hname, h in hists.items():
        h.Write(hname)
    # return histograms
    return hists

if __name__ == '__main__':
    # make plots
    histograms = set_hist_colors(get_histograms(out_file))
    pu.set_ATLAS_style()
    for pt in pts:
        for flav in flavs:
            hlist = [histograms[f'{k}_{pt}_{flav}'] for k in tdd_ntuples]
            descriptors = []
            for k in tdd_ntuples:
                if k == 'nom':
                    descriptors.append('nom, relaxed SH')
                elif k == 'nomnom':
                    descriptors.append('nom')
                else:
                    descriptors.append(k.replace(f'_{pt}','').replace(f'_{flav}',''))
            pu.plot_histograms(
                out_dir,
                hlist,
                descriptors,
                f'{pt}_{flav}jets',
                short_title = f'{pt_labels[pt]}, {flav}-jets',
                ratio_limits = [0, 2]
            )
