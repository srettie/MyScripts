import os
import sys
import ROOT
import glob
import subprocess
import plot_utils
import create_ipxd_templates
import draw_roc_curves
sys.path.append(os.path.join(os.path.dirname(__file__),'..','batch'))
import tracking_submitJobs
import process_trees
import produce_roc_curve

debug = True
verbose = False

# False to produce histograms, True to make pretty plots
do_fast = True
# hadd separate histogram files into final all_hists.root file
do_hadd = False
# Set to False for testing; simply print out the commands
do_submit = False

good_samples = tracking_submitJobs.good_samples

# ROOT.TColor.GetColor('#1f77b4'),#
# ROOT.TColor.GetColor('#ff7f0e'),#
# ROOT.TColor.GetColor('#2ca02c'),#
# ROOT.TColor.GetColor('#d62728'),#
# ROOT.TColor.GetColor('#9467bd'),#
# ROOT.TColor.GetColor('#8c564b'),#
# ROOT.TColor.GetColor('#e377c2'),#
# ROOT.TColor.GetColor('#7f7f7f'),#
# ROOT.TColor.GetColor('#bcbd22'),#
# ROOT.TColor.GetColor('#17becf'),#

colors = {
    'nom'                                     : ROOT.kBlack,
    'pseudo'                                  : ROOT.TColor.GetColor("#17becf"),
    'ideal'                                   : ROOT.TColor.GetColor("#ff7f0e"),
    'ref'                                     : ROOT.kGray,
    'nom_replaceWithTruth'                    : ROOT.TColor.GetColor("#e377c2"),
    'nom_replaceHFWithTruth'                  : ROOT.TColor.GetColor("#9467bd"),
    'nom_replaceFRAGWithTruth'                : ROOT.TColor.GetColor('#7f7f7f'),
    'nom_replaceFRAGHFWithTruth'              : ROOT.TColor.GetColor('#bcbd22'),
    'nom_replaceFRAGGEANTWithTruth'           : ROOT.TColor.GetColor('#1f77b4'),
    'nom_replaceFRAGHFGEANTWithTruth'         : ROOT.TColor.GetColor('#2ca02c'),
    'nom_replaceGEANTWithTruth'               : ROOT.TColor.GetColor('#d62728'),
    'nom_replaceHFGEANTWithTruth'             : ROOT.TColor.GetColor('#8c564b'),
    'nom_RF75'                                : ROOT.TColor.GetColor("#2ca02c"),
    'nom_RF75_replaceWithTruth'               : ROOT.TColor.GetColor("#d62728"),
    'nom_RF75_replaceWithTruth_removeNoMatch' : ROOT.TColor.GetColor("#17becf"),
    'nom_RF75_replaceHFWithTruth'             : ROOT.kSpring-1,
    'nom_RF75_replaceFRAGWithTruth'           : ROOT.kOrange+2,
    'nom_RF75_replaceFRAGHFWithTruth'         : ROOT.kYellow+1,
    'nom_RF75_replaceWithTruthIdeal'          : ROOT.TColor.GetColor("#ff7f0e"),
    'nom_RF90'                                : ROOT.TColor.GetColor("#e377c2"),
    'selectHF'                                : ROOT.TColor.GetColor("#9467bd"),
    'selectHF_replaceWithTruth'               : ROOT.TColor.GetColor("#e377c2"),
    'selectHF_emulateFT'                      : ROOT.TColor.GetColor("#2ca02c"),
    'selectHF_emulateFT_replaceWithTruth'     : ROOT.TColor.GetColor("#d62728"),
    'selectFRAG'                              : ROOT.kOrange+2,
    'selectFRAG_replaceWithTruth'             : ROOT.kSpring-1,
    'selectFRAGHF'                            : ROOT.TColor.GetColor('#7f7f7f'),
    'selectFRAGHF_replaceWithTruth'           : ROOT.TColor.GetColor('#bcbd22'),
    'selectFRAGHF_replaceFRAGWithTruth'       : ROOT.TColor.GetColor('#1f77b4'),
    'selectFRAGHF_replaceHFWithTruth'         : ROOT.TColor.GetColor('#8c564b'),
    'selectFRAGHFGEANT'                       : ROOT.kYellow+1,
    'pseudoNotReco'                           : ROOT.kBlack,
    'nom_RFMVA_loose'                         : ROOT.TColor.GetColor('#1f77b4'),
    'nom_RFMVA_tight'                         : ROOT.TColor.GetColor('#8c564b'),
    'group.perf-idtracking.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.e5362_e5984_s3126_r11212_IdealNomV5_EXT0' : ROOT.kBlack,
    'mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210' : ROOT.kRed,
    'extendedZp' : ROOT.kBlue,
    'extendedZp_rw_bcl_2d' : ROOT.kRed,
    'extendedZp_rw_bcl_3d' : ROOT.kSpring-1,
    'extendedZp_withCghost' : ROOT.kRed,
    'extendedZp_newLabels' : ROOT.kSpring-1,
}

legend_titles = {
    'nom'                                     : 'Nominal',
    'pseudo'                                  : 'Pseudo',
    'ideal'                                   : 'Ideal',
    'ref'                                     : 'BTagCalibRUN2-08-40',
    'nom_replaceWithTruth'                    : 'Nominal, replace with pseudo',
    'nom_replaceHFWithTruth'                  : 'Nominal, replace HF with pseudo',
    'nom_replaceFRAGWithTruth'                : 'Nominal, replace FRAG with pseudo',
    'nom_replaceFRAGHFWithTruth'              : 'Nominal, replace FRAG+HF with pseudo',
    'nom_replaceFRAGGEANTWithTruth'           : 'Nominal, replace FRAG+GEANT with pseudo',
    'nom_replaceFRAGHFGEANTWithTruth'         : 'Nominal, replace FRAG+HF+GEANT with pseudo',
    'nom_replaceGEANTWithTruth'               : 'Nominal, replace GEANTwith pseudo',
    'nom_replaceHFGEANTWithTruth'             : 'Nominal, replace HF+GEANT with pseudo',
    'nom_RF75'                                : 'Nominal, no fakes (TMP > 0.75)',
    'nom_RF75_replaceWithTruth'               : 'Nominal, no fakes (TMP > 0.75), replace with pseudo',
    'nom_RF75_replaceWithTruth_removeNoMatch' : 'Nominal, no fakes (TMP > 0.75), replace with pseudo (only matched tracks)',
    'nom_RF75_replaceHFWithTruth'             : 'Nominal, no fakes (TMP > 0.75), replace HF with pseudo',
    'nom_RF75_replaceFRAGWithTruth'           : 'Nominal, no fakes (TMP > 0.75), replace FRAG with pseudo',
    'nom_RF75_replaceFRAGHFWithTruth'         : 'Nominal, no fakes (TMP > 0.75), replace FRAG+HF with pseudo',
    'nom_RF75_replaceWithTruthIdeal'          : 'Nominal, no fakes (TMP > 0.75), replace with ideal',
    'nom_RF90'                                : 'Nominal, no fakes (TMP > 0.9)',
    'selectHF'                                : 'HF tracks',
    'selectHF_replaceWithTruth'               : 'HF tracks, replace with pseudo',
    'selectHF_emulateFT'                      : 'HF tracks (emulate FTAG)',
    'selectHF_emulateFT_replaceWithTruth'     : 'HF tracks (emulate FTAG), replace with pseudo',
    'selectFRAG'                              : 'FRAG tracks',
    'selectFRAG_replaceWithTruth'             : 'FRAG tracks, replace with pseudo',
    'selectFRAGHF'                            : 'FRAG+HF tracks',
    'selectFRAGHF_replaceWithTruth'           : 'FRAG+HF tracks, replace with pseudo',
    'selectFRAGHF_replaceFRAGWithTruth'       : 'FRAG+HF tracks, replace FRAG with pseudo',
    'selectFRAGHF_replaceHFWithTruth'         : 'FRAG+HF tracks, replace HF with pseudo',
    'selectFRAGHFGEANT'                       : 'FRAG+HF+GEANT tracks',
    'pseudoNotReco'                           : 'Unreconstructed pseudo tracks',
    'nom_RFMVA_loose'                         : 'Nominal, removing fakes with MVA (loose)',
    'nom_RFMVA_tight'                         : 'Nominal, removing fakes with MVA (tight)',
    'group.perf-idtracking.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.e5362_e5984_s3126_r11212_IdealNomV5_EXT0' : 'Z\' (no pileup)',
    'mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210' : 'Extended Z\'',
    'extendedZp' : 'Extended Z\'',
    'extendedZp_rw_bcl_2d' : 'Extended Z\' 2D Reweighting',
    'extendedZp_rw_bcl_3d' : 'Extended Z\' 3D Reweighting',
    'extendedZp_withCghost' : 'Extended Z\' (including C ghosts)',
    'extendedZp_newLabels' : 'Extended Z\' (new labels)',
}

track_categories_IPXD = ['0HitIn0HitNInExp2', '0HitIn0HitNInExpIn', '0HitIn0HitNInExpNIn', '0HitIn0HitNIn', '0HitInExp', '0HitIn', '0HitNInExp', '0HitNIn', 'InANDNInShared', 'PixShared', 'SctShared', 'InANDNInSplit', 'PixSplit', 'Good']
bjet_selections = ['_1bH','_isojet','_1bH_isojet','_1bH_isojet_largeFrag','_1bH_isojet_semilep_mu','_1bH_isojet_semilep_el','_1bH_isojet_hadronic','_leadingJet','_subleadingJet']

category_titles_IPXD = {
    0 : 'No hits in first two layers; expected hit in L0 and L1',
    1 : 'No hits in first two layers; expected hit in L0 and no expected hit in L1',
    2 : 'No hits in first two layers; no expected hit in L0 and expected hit in L1',
    3 : 'No hits in first two layers; no expected hit in L0 and L1',
    4 : 'No hit in L0; expected hit in L0',
    5 : 'No hit in L0; no expected hit in L0',
    6 : 'No hit in L1; expected hit in L1',
    7 : 'No hit in L1; no expected hit in L1',
    8 : 'Shared hit in both L0 and L1',
    9 : 'Shared pixel hits',
    10 : 'Two or more shared SCT hits',
    11 : 'Split hits in both L0 and L1',
    12 : 'Split pixel hit',
    13 : 'Good: a track not in any of the above categories',
}

def get_IPXD_title(var):
    title = ''

    if '_B_' in var: title += 'B '
    if '_C_' in var: title += 'C '
    if '_U_' in var: title += 'U '
    
    title += '(' + str(track_categories_IPXD.index(var.split('_')[-1])) + ' : ' + var.split('_')[-1] + ')'

    return title

def get_file_descriptor(filename):
    return filename.split('/')[-1].replace('.root','').split('Akt4EMTo_')[-1]

def get_hist_id(sample, jet_type, track_type, variable):
    return tracking_submitJobs.get_simple_name(sample) + '_' + tracking_submitJobs.get_short_jet_name(jet_type) + '_' + track_type + '_' + variable

def get_short_name(sample):
    return sample.replace('.Pythia8EvtGen_A14NNPDF23LO','').replace('user.gfacini.','').replace('.e5362','').replace('_e5984','').replace('_s3126','').replace('_r11212','').replace('.e7142','').replace('_JZ7WithSW','').replace('_EXT0','').replace('group.perf-idtracking.','')

def get_short_title(sample, jet_type):
    name = get_short_name(sample)
    title = ''

    if 'Zprime' in name:
        title += 'Z\''

    if '427080' in name and 'flatpT' in name:
        title += ' (flat p_{T}, 427080)'
    title += ' {}'.format(tracking_submitJobs.get_short_jet_name(jet_type))

    return title

def hadd_histograms(samples, tree_dir):
    # Save all relevant histograms to convenient file   
    print('About to hadd histograms for {} samples'.format(len(samples)))
    
    print('Start by hadding subjobs if necessary')
    n_sub_hadd = 0
    for sample in samples:
        if sample not in good_samples:
            continue
        for t in process_trees.tracks:
            # Only certain samples available for extended Z'
            if sample == tracking_submitJobs.zp_extended_sample and t not in tracking_submitJobs.tracks_extended_zp: continue
            cmd = 'hadd -f {}/{}/{}/hist_alljets_{}_{}.root '.format(tree_dir,sample,t,get_short_name(sample),t)
            cmd += tree_dir+'/'+sample+'/'+t+'/*/hist_*.root'
            if not do_submit:
                print(cmd)
            else:
                subprocess.call(cmd,shell=True)
            n_sub_hadd += 1
    print('hadded {} subjobs'.format(n_sub_hadd))
    print('Now hadd final file')
    cmd = 'hadd -f '+tree_dir+'/all_hists.root '
    for sample in samples:
        if sample not in good_samples:
                continue
        cmd += tree_dir+'/'+sample+'/*/hist_alljets_*.root '
    if not do_submit:
        print(cmd)
    else:
        subprocess.call(cmd,shell=True)

def submit_histogram_jobs(samples, tracks):
    print('Submitting histogram jobs!')
    nJobs = 0
    for sample in samples:
        if sample not in good_samples:
            continue
        print(sample)
        for j in tracking_submitJobs.jet_types:
            for track_type in tracks:
                # Only certain samples available for extended Z'
                if sample == tracking_submitJobs.zp_extended_sample and track_type not in tracking_submitJobs.tracks_extended_zp: continue
                subjobs = [s.split('/')[-1] for s in glob.glob(tree_dir+'/'+sample+'/'+track_type+'/*') if 'trk_' in s]
                for subjob in subjobs:
                    cmd = 'python /home/srettie/MyScripts/batch/process_trees.py -s {} -t {} -j {} -td {} -sj {}'.format(sample, track_type, j, tree_dir, subjob)
                    j_name = 'hist_{}_{}_{}_{}'.format(track_type, j, subjob, sample)
                    tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
                    nJobs += 1
                    if sample == tracking_submitJobs.zp_extended_sample:
                        tracking_submitJobs.torque_submit(cmd+' -rw bcl_2d','',j_name+'_rw_bcl_2d',do_submit)
                        tracking_submitJobs.torque_submit(cmd+' -rw bcl_3d','',j_name+'_rw_bcl_3d',do_submit)
                        nJobs += 2

    print('All done! Submitted a total of {} jobs.'.format(nJobs))

def plot_2D(histograms, var, track_type, sample, jet_type):
    # Plot 2D histogram for each sample, and its ratio w.r.t. nominal
    cnv = ROOT.TCanvas()
    cnv.cd()
    cnv.SetRightMargin(0.16)

    nRebin = 1
    if 'y_vs_x' in var:
        nRebin = 5
        
    # Get histogram and draw raw plot
    h_id = get_hist_id(sample, jet_type, track_type, var)
    sample_shortName = get_short_name(sample)
    sample_shortTitle = get_short_title(sample, jet_type)
    h = histograms[h_id].Clone()
    if 'jet_nHFtracks_vs_ntrk' in var:
        h.GetYaxis().SetRangeUser(0.,20.)
    h.GetXaxis().SetLabelSize(0.04)
    h.GetYaxis().SetLabelSize(0.04)
    h.RebinX(nRebin)
    h.RebinY(nRebin)
    h.Draw('colz')
    extraLabel = track_type+', '+sample_shortTitle
    selection = ''
    if '_bjet' in var: extraLabel += ', b-jets'
    if '_HF' in var: extraLabel += ', HF tracks'
    if '_nonHF' in var: extraLabel += ', non-HF tracks'
    if 'isojet' in var: selection += 'dR_{min} > 1.0' if selection == '' else ', dR_{min} > 1.0'
    if '1bH' in var: selection += 'N_{bH} = 1' if selection == '' else ', N_{bH} = 1'
    if 'jet_bH_pt_vs_jet_pt' in var and (sample != tracking_submitJobs.zp_extended_sample or '_rw_' in var):
        h.GetXaxis().SetRangeUser(0,1500)
        h.GetYaxis().SetRangeUser(0,1500)

    plot_utils.draw_labels(extraLabel, selection)
    cnv.SaveAs(plot_dir + 'h2_' + sample_shortName + '_' + var + '_' + track_type + '.pdf')

    # Draw ratio w.r.t. nominal
    draw_ratio = track_type != 'nom' or '_rw_' in var or '_1bH' in var or '_isojet' in var or '_leadingJet' in var or '_subleadingJet' in var or '_semilep' in var or '_hadronic' in var
    if draw_ratio:
        h_id_ref = get_hist_id(sample, jet_type, 'nom', var)
        if '_1bH' in var or '_isojet' in var or '_largeFrag' in var or '_leadingJet' in var or '_subleadingJet' in var or '_semilep' in var or '_hadronic' in var:
            h_id_ref = get_hist_id(sample, jet_type, 'nom', var.replace('_1bH','').replace('_isojet','').replace('_largeFrag','').replace('_leadingJet','').replace('_subleadingJet','').replace('_semilep_mu','').replace('_semilep_el','').replace('_hadronic',''))
        if '_rw_' in var:
            h_id_ref = get_hist_id(tracking_submitJobs.zp_ctide_sample, jet_type, 'nom', var.replace('_rw_bcl_2d','').replace('_rw_bcl_3d',''))

        if verbose:
            print('Ratio information:')
            print('\tNumerator: {}'.format(h_id))
            print('\tDenominator: {}'.format(h_id_ref))
        h_ref = histograms[h_id_ref].Clone().RebinX(nRebin).RebinY(nRebin)
        r = histograms[h_id].Clone().RebinX(nRebin).RebinY(nRebin)
        r.Divide(h_ref)
        r.GetZaxis().SetTitle('%s / nominal'%(track_type))
        r.GetXaxis().SetLabelSize(0.04)
        r.GetYaxis().SetLabelSize(0.04)
        num_name, denom_name = track_type, 'nom'
        if '_1bH' in var:
            num_name = '1bH'
        if '_isojet' in var:
            num_name = 'isojet'
        if '_leadingJet' in var:
            num_name = 'lead_jet'
        if '_subleadingJet' in var:
            num_name = 'sublead_jet'
        if '_1bH' in var and '_isojet' in var:
            num_name = '1bH_isojet'
        if '_semilep_mu' in var:
            num_name = '1bH_isojet_semilep_mu'
        if '_semilep_el' in var:
            num_name = '1bH_isojet_semilep_el'
        if '_hadronic' in var:
            num_name = '1bH_isojet_hadronic'
        if '_rw_bcl_2d' in var:
            denom_name = num_name
            num_name = '2D RW ' + num_name
        if '_rw_bcl_3d' in var:
            denom_name = num_name
            num_name = '3D RW ' + num_name

        r.GetZaxis().SetTitle('{} / {}'.format(num_name, denom_name))
        if 'jet_nHFtracks_vs_ntrk' in var:
            r.GetYaxis().SetRangeUser(0.,20.)
        if 'jet_bH_pt_vs_jet_pt' in var and (sample != tracking_submitJobs.zp_extended_sample or '_rw_' in var):
            r.GetXaxis().SetRangeUser(0,1500)
            r.GetYaxis().SetRangeUser(0,1500)
        cnv.SetLogz()
        r.SetMinimum(0.1)
        r.Draw('colz')
        extraLabel = sample_shortTitle
        if '_bjet' in var: extraLabel += ', b-jets'
        if '_HF' in var: extraLabel += ', HF tracks'
        if '_nonHF' in var: extraLabel += ', non-HF tracks'
        plot_utils.draw_labels(extraLabel, '')
        cnv.SaveAs(plot_dir + 'h2_' + sample_shortName + '_' + var + '_' + track_type + '_ratio.pdf')
        cnv.SetLogz(0)
    cnv.Close()
    
def plot_rate(histograms, var, sample, jet_type, tracks):

    # Setup pads and canvas
    ratio_frac = 0.3

    cnv = ROOT.TCanvas()
    cnv.cd()
    
    p2 = ROOT.TPad('p2','p2',0.,0.,1.,ratio_frac)
    p2.Draw()
    p2.SetTopMargin(0.03)
    p2.SetBottomMargin(ratio_frac)
    
    p1 = ROOT.TPad('p1','p1',0.,ratio_frac,1.,1.)
    p1.Draw()
    p1.SetBottomMargin(0.03)
    p1.cd()

    # Histogram styles and aesthetics
    ratio_min, ratio_max = 0.2, 1.0

    # Font style
    x_label_font = 43
    y_label_font = 43
    x_title_font = 43
    y_title_font = 43    
    # Sizes in pixels
    x_title_size = 14
    y_title_size = 14
    x_label_size = 12
    y_label_size = 12
    # Offsets relative to axis
    x_title_offset = 4.0
    y_title_offset = 1.6

    # Get rates, i.e. [sv1/jf]/all
    h_ids = []
    for track_type in tracks:
        h_id = get_hist_id(sample, jet_type, track_type, var)
        if track_type == 'extendedZp':
            h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var)
        if track_type == 'extendedZp_rw_bcl_2d':
            h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_rw_bcl_2d')
        if track_type == 'extendedZp_rw_bcl_3d':
            h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_rw_bcl_3d')
        h_ids.append(h_id)
    sample_shortName = get_short_name(sample)
    sample_shortTitle = get_short_title(sample, jet_type)

    pt_min, pt_max, nRebin = 0, 3000, 4

    pt_ranges = []
    selections = []

    if 'dRiso' in var:
        selections.append('')
    else:
        selections.append('')
        selections.append('_iso')

    for alg in process_trees.algos:
        for selection in selections:
            for pt_bin in list(process_trees.pt_bins) + ['']:
                if pt_bin == '':
                    pt_range = ''
                else:
                    pt_range = '_' + pt_bin
                p1.cd()
                rates_bjets = []
                ratios_bjets = []
                rates_ljets = []
                ratios_ljets = []
                for i, track_type in enumerate(tracks):
                    # Get rates
                    rates_bjets.append(histograms[h_ids[i]+'_bjet_'+alg+selection+pt_range].Clone().Rebin(nRebin))
                    rates_ljets.append(histograms[h_ids[i]+'_ljet_'+alg+selection+pt_range].Clone().Rebin(nRebin))
                    rates_bjets[i].Divide(histograms[h_ids[i]+'_bjet'+selection+pt_range].Clone().Rebin(nRebin))
                    rates_ljets[i].Divide(histograms[h_ids[i]+'_ljet'+selection+pt_range].Clone().Rebin(nRebin))

                    # Aesthetics
                    rates_bjets[i].GetYaxis().SetTitle(alg.upper()+' Rate')
                    rates_bjets[i].SetMinimum(0.)
                    rates_bjets[i].SetMaximum(1.5)
                    rates_bjets[i].SetLineColor(colors[track_type])
                    rates_bjets[i].SetMarkerColor(colors[track_type])
                    rates_ljets[i].SetLineColor(colors[track_type])
                    rates_ljets[i].SetMarkerColor(colors[track_type])
                    rates_ljets[i].SetLineStyle(2)
                    # Labels
                    rates_bjets[i].GetXaxis().SetLabelFont(x_label_font)
                    rates_bjets[i].GetYaxis().SetLabelFont(y_label_font)
                    rates_bjets[i].GetXaxis().SetLabelSize(0)
                    rates_bjets[i].GetYaxis().SetLabelSize(y_label_size)
                    rates_ljets[i].GetXaxis().SetLabelFont(x_label_font)
                    rates_ljets[i].GetYaxis().SetLabelFont(y_label_font)
                    rates_ljets[i].GetXaxis().SetLabelSize(0)
                    rates_ljets[i].GetYaxis().SetLabelSize(y_label_size)
                    # Titles
                    rates_bjets[i].GetXaxis().SetTitleSize(0)
                    rates_bjets[i].GetYaxis().SetTitleFont(y_title_font)
                    rates_bjets[i].GetYaxis().SetTitleSize(y_title_size)
                    rates_ljets[i].GetXaxis().SetTitleSize(0)
                    rates_ljets[i].GetYaxis().SetTitleFont(y_title_font)
                    rates_ljets[i].GetYaxis().SetTitleSize(y_title_size)


                    # Get ratios
                    ratios_bjets.append(rates_bjets[i].Clone())
                    ratios_bjets[i].Divide(rates_bjets[0])
                    ratios_bjets[i].GetYaxis().SetRangeUser(ratio_min, ratio_max)
                    ratios_bjets[i].GetYaxis().SetNdivisions(505)
                    ratios_ljets.append(rates_ljets[i].Clone())
                    ratios_ljets[i].Divide(rates_ljets[0])
                    ratios_ljets[i].GetYaxis().SetRangeUser(ratio_min, ratio_max)
                    ratios_ljets[i].GetYaxis().SetNdivisions(505)
                    # Labels
                    ratios_bjets[i].GetXaxis().SetLabelFont(x_label_font)
                    ratios_bjets[i].GetYaxis().SetLabelFont(y_label_font)
                    ratios_bjets[i].GetXaxis().SetLabelSize(x_label_size)
                    ratios_bjets[i].GetYaxis().SetLabelSize(y_label_size)
                    ratios_ljets[i].GetXaxis().SetLabelFont(x_label_font)
                    ratios_ljets[i].GetYaxis().SetLabelFont(y_label_font)
                    ratios_ljets[i].GetXaxis().SetLabelSize(x_label_size)
                    ratios_ljets[i].GetYaxis().SetLabelSize(y_label_size)
                    # Titles
                    ratios_bjets[i].GetYaxis().SetTitle('Ratio')
                    ratios_bjets[i].GetXaxis().SetTitleFont(x_title_font)
                    ratios_bjets[i].GetYaxis().SetTitleFont(y_title_font)
                    ratios_bjets[i].GetXaxis().SetTitleSize(x_title_size)
                    ratios_bjets[i].GetYaxis().SetTitleSize(y_title_size)
                    ratios_bjets[i].GetYaxis().SetTitleOffset(y_title_offset)
                    ratios_bjets[i].GetXaxis().SetTitleOffset(x_title_offset)
                    ratios_ljets[i].GetYaxis().SetTitle('Ratio')
                    ratios_ljets[i].GetXaxis().SetTitleFont(x_title_font)
                    ratios_ljets[i].GetYaxis().SetTitleFont(y_title_font)
                    ratios_ljets[i].GetXaxis().SetTitleSize(x_title_size)
                    ratios_ljets[i].GetYaxis().SetTitleSize(y_title_size)
                    ratios_ljets[i].GetYaxis().SetTitleOffset(y_title_offset)
                    ratios_ljets[i].GetXaxis().SetTitleOffset(x_title_offset)


                    if var == 'jet_rate_pt':
                        rates_bjets[i].GetXaxis().SetRangeUser(pt_min,pt_max)
                        rates_ljets[i].GetXaxis().SetRangeUser(pt_min,pt_max)
                        ratios_bjets[i].GetXaxis().SetRangeUser(pt_min,pt_max)
                        ratios_ljets[i].GetXaxis().SetRangeUser(pt_min,pt_max)
                
                # Legend
                lx1,ly1,lx2,ly2 = 0.53,0.6,0.9,0.9
                leg = ROOT.TLegend(lx1, ly1, lx2, ly2)

                # Plot rates for various jet flavours
                for i, track_type in enumerate(tracks):
                    if i == 0:
                        rates_bjets[i].Draw('hist')
                    else:
                        rates_bjets[i].Draw('hist,same')
                    rates_ljets[i].Draw('hist,same')
                    leg.AddEntry(rates_bjets[i], 'b-jets, {} tracks'.format(track_type),'l')
                    leg.AddEntry(rates_ljets[i], 'l-jets, {} tracks'.format(track_type),'l')
                leg.Draw('same')

                # Labels
                extra_label = ''
                if selection == '_iso':
                    extra_label += 'dRiso > 1'
                if selection == '_debugLxySmall':
                    extra_label += 'L_{xy}^{SV1} < 30 mm'
                if selection == '_debugLxyLarge':
                    extra_label += 'L_{xy}^{SV1} > 30 mm'
                if pt_range == '_0_1pt':
                    extra_label += 'p_{T} < 1 TeV'
                if pt_range == '_1_2pt':
                    extra_label += '1 TeV #leq p_{T} < 2 TeV'
                if pt_range == '_2pt':
                    extra_label += 'p_{T} #geq 2 TeV'
                plot_utils.draw_labels(sample_shortTitle, extra_label)

                # Back to p2
                p2.cd()
                for i, track_type in enumerate(tracks):
                    if i == 0:
                        continue
                    elif i == 1:
                        ratios_bjets[i].Draw('hist')
                    else:
                        ratios_bjets[i].Draw('hist,same')
                    ratios_ljets[i].Draw('hist,same')
                unityLine = ROOT.TLine()
                unityLine.SetLineColor(ROOT.kBlack)
                if var == 'jet_rate_pt':
                    unityLine.DrawLine(pt_min,1.0,pt_max,1.0)
                else:
                    unityLine.DrawLine(rates_bjets[0].GetXaxis().GetXmin(),1.0,rates_bjets[0].GetXaxis().GetXmax(),1.0)

                # Save output
                cnv.SaveAs(plot_dir + 'h_' + sample_shortName + '_' + var + '_' + alg + selection + pt_range + '.pdf')
    cnv.Close()

def plot_single_var(histograms, var, sample, jet_type, track_type):

    h_id = get_hist_id(sample, jet_type, track_type, var)

    # Ranges
    histograms[h_id].SetMaximum(1.5*histograms[h_id].GetMaximum())
    histograms[h_id].SetMinimum(0)

    # Axis titles
    if '0pull' in var or '0res' in var:
        histograms[h_id].GetXaxis().SetTitle(histograms[h_id].GetXaxis().GetTitle().replace('reco', track_type))

    # Labels    
    sample_shortName = get_short_name(sample)
    short_title = get_short_title(sample, jet_type)
    variation_label = ''
    if '_bjet' in var: short_title += ', b-jets'
    if '_cjet' in var: short_title += ', c-jets'
    if '_ljet' in var: short_title += ', l-jets'
    if '_HF' in var: short_title += ', HF tracks'
    if '_nonHF' in var: short_title += ', non-HF tracks'

    # Save plots
    h_name = 'h_' + sample_shortName + '_' + jet_type + '_' + var
    plot_utils.plot_single_hist(plot_dir, histograms[h_id], h_name, short_title, variation_label, debug, show_entries = True)

def rebin_1D(histograms, samples):
    for h_id, h in histograms.items():
        if '_vs_' in h_id: continue
        nRebin = 1
        if 'jet_ip2' in h_id or 'jet_ip3' in h_id: nRebin = 4
        if 'jet_sv1_m' in h_id or 'jet_sv1_efc' in h_id: nRebin = 2
        if 'jet_sv1_efc' in h_id: nRebin = 4
        if 'jet_sv1_L3d' in h_id or 'jet_sv1_sig3d' in h_id: nRebin = 2
        if nRebin == 1: continue
        if verbose: print('Rebinning {}'.format(h_id))
        histograms[h_id].Rebin(nRebin)

def add_1D_overunderflow(histograms, samples):
    for h_id, h in histograms.items():
        plot_utils.add_overflow(h)
        plot_utils.add_underflow(h)

def plot_comparison(histograms, var, sample, jet_type, tracks, do_zoom = False):

    to_scale = ['beff','leff','debugLxy','0pull','ip3d_','sv1_efc','sv1_Lxy','SipA0','jet_sv1_sig3d','ip2','ip3','jet_sv1_llr','jet_decayMode']
    scale_to_unity = any([check in var for check in to_scale])

    # Histogram styles and aesthetics
    ratio_min, ratio_max = 0.5, 1.5
    if 'jet_ip2_' in var or 'jet_ip3_' in var:
        ratio_min, ratio_max = 0.0, 3.0
        if tracks[-1] == 'selectHF_emulateFT': ratio_max = 2.0
    if 'jet_sv1_m_' in var or 'jet_sv1_efc_' in var:
        ratio_min = 0.
        if '_cjet' in var: ratio_min, ratio_max = 0.0, 1.0
        if '_ljet' in var: ratio_min, ratio_max = 0.0, 2.0
    if 'jet_sv1_ntrkv' in var:
        ratio_min, ratio_max = 0.,4.
        if '_bjet' in var: ratio_max = 2.5
    if 'jet_sv1_deltaR' in var: ratio_min, ratio_max = 0., 1.
    if 'jet_sv1_L3d' in var or 'jet_sv1_sig3d' in var: ratio_min = 0.0
    if 'jet_sv1_n2t' in var: ratio_min = 0.
    if 'jet_jf_efc' in var: ratio_max = 2.0
    if 'jet_jf_nvtx' in var: ratio_min, ratio_max = 0.,2.
    if 'jet_trk_ip3d_z0sig' in var: ratio_min = 0.3
    if 'jet_sv1_Nvtx' in var: ratio_min = 0.
    if 'jet_sv1_Lxy' in var:
        ratio_min, ratio_max = 0.,2.2
        if '_ljet' in var: ratio_max = 3.5
    if 'jet_ntrk' in var or 'jet_nOthertracks' in var: ratio_max = 2.
    if 'jet_trk_chi2' in var: ratio_min, ratio_max = 0.,8.
    if 'jet_trk_nPixHits' in var: ratio_max = 2.
    if 'jet_trk_nPixSCTHits' in var: ratio_max = 1.8
    if 'jet_nHFtracks' in var: ratio_min, ratio_max = 0.,3.
    if '0pull' in var: ratio_min  = 0.2
    if 'ip3d' in var: ratio_min, ratio_max = 0.25,1.5
    if '_beff_' in var or '_leff_' in var: ratio_min, ratio_max = 0.5, 1.5
    if 'jet_bH_nBHadron' in var: ratio_max = 4.
    if 'njets' in var: ratio_max = 3.

    ratio_lims = [ratio_min, ratio_max]

    xrange_min,xrange_max = histograms[get_hist_id(sample,jet_type,'nom',var)].GetXaxis().GetXmin(),histograms[get_hist_id(sample,jet_type,'nom',var)].GetXaxis().GetXmax()
    if 'jet_nHFtracks' in var: xrange_min, xrange_max = 0.,20.
    if 'jet_nPUFAKEtracks' in var: xrange_min, xrange_max = 0.,40.
    if 'jet_nLowPtTracks' in var: xrange_min, xrange_max = 0.,50.
    if 'jet_ntrk' in var: xrange_min, xrange_max = 0.,70.
    if 'jet_sv1_Lxy' in var: xrange_min = 20
    if do_zoom and 'SipA0' in var: xrange_min, xrange_max = -10,10
    if 'jet_pt_beff' in var or 'jet_pt_leff' in var: xrange_max = 2000
    if 'jet_decayMode' in var: xrange_max = 4

    # Set x-axis range (make sure to do this before any scaling to unity to avoid normalization issues, as integral changes depending on x-axis range)
    for track_type in tracks:
        h_id = get_hist_id(sample, jet_type, track_type, var)
        if track_type == 'extendedZp':
            h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var)
        if track_type == 'extendedZp_rw_bcl_2d':
            h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_rw_bcl_2d')
        if track_type == 'extendedZp_rw_bcl_3d':
            h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_rw_bcl_3d')
        if track_type == 'extendedZp_withCghost':
            if 'pT' in var:
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var.replace('semilep_mu','semilep_mu_withCghost').replace('semilep_el','semilep_el_withCghost').replace('hadronic','hadronic_withCghost'))
            else:
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_withCghost')
        if track_type == 'extendedZp_newLabels':
            if 'pT' in var:
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var.replace('semilep_mu','semilep_mu_newLabels').replace('semilep_el','semilep_el_newLabels').replace('hadronic','hadronic_newLabels'))
            else:
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_newLabels')
        histograms[h_id].GetXaxis().SetRangeUser(xrange_min,xrange_max)

    # Scale to unity if needed
    if scale_to_unity:
        for track_type in tracks:
            h_id = get_hist_id(sample, jet_type, track_type, var)
            if track_type == 'extendedZp':
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var)
            if track_type == 'extendedZp_rw_bcl_2d':
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_rw_bcl_2d')
            if track_type == 'extendedZp_rw_bcl_3d':
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_rw_bcl_3d')
            if track_type == 'extendedZp_withCghost':
                if 'pT' in var:
                    h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var.replace('semilep_mu','semilep_mu_withCghost').replace('semilep_el','semilep_el_withCghost').replace('hadronic','hadronic_withCghost'))
                else:
                    h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_withCghost')
            if track_type == 'extendedZp_newLabels':
                if 'pT' in var:
                    h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var.replace('semilep_mu','semilep_mu_newLabels').replace('semilep_el','semilep_el_newLabels').replace('hadronic','hadronic_newLabels'))
                else:
                    h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_newLabels')
            if histograms[h_id].Integral() > 0:
                histograms[h_id].Scale(1./histograms[h_id].Integral())
            histograms[h_id].GetYaxis().SetTitle('Arbitrary Units')

    # Get maxima for y-axis range and set them
    hists_to_check = []
    for t in tracks:
        if t == 'extendedZp':
            hists_to_check.append(histograms[get_hist_id(tracking_submitJobs.zp_extended_sample,jet_type,'nom',var)])
        elif t == 'extendedZp_rw_bcl_2d':
            hists_to_check.append(histograms[get_hist_id(tracking_submitJobs.zp_extended_sample,jet_type,'nom',var+'_rw_bcl_2d')])
        elif t == 'extendedZp_rw_bcl_3d':
            hists_to_check.append(histograms[get_hist_id(tracking_submitJobs.zp_extended_sample,jet_type,'nom',var+'_rw_bcl_3d')])
        elif t == 'extendedZp_withCghost':
            if 'pT' in var:
                hists_to_check.append(histograms[get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var.replace('semilep_mu','semilep_mu_withCghost').replace('semilep_el','semilep_el_withCghost').replace('hadronic','hadronic_withCghost'))])
            else:
                hists_to_check.append(histograms[get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_withCghost')])
        elif t == 'extendedZp_newLabels':
            if 'pT' in var:
                hists_to_check.append(histograms[get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var.replace('semilep_mu','semilep_mu_newLabels').replace('semilep_el','semilep_el_newLabels').replace('hadronic','hadronic_newLabels'))])
            else:
                hists_to_check.append(histograms[get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_newLabels')])
        else:
            hists_to_check.append(histograms[get_hist_id(sample,jet_type,t,var)])

    h_min, h_max = plot_utils.get_extrema(hists_to_check, xrange_min, xrange_max)
    if 'jet_pt_beff_jet_ip' in var: h_max = 1.4
    if 'jet_pt_beff_jet_sv1' in var: h_max = 1.0
    if 'jet_pt_leff_jet_' in var: h_max = 0.2

    h_to_plot, h_colors, h_descriptors = [], [], []
    # Setup colours and styles
    for i, track_type in enumerate(tracks):
        h_id = get_hist_id(sample, jet_type, track_type, var)
        if track_type == 'extendedZp':
            h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var)
        if track_type == 'extendedZp_rw_bcl_2d':
            h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_rw_bcl_2d')
        if track_type == 'extendedZp_rw_bcl_3d':
            h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_rw_bcl_3d')
        if track_type == 'extendedZp_withCghost':
            if 'pT' in var:
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var.replace('semilep_mu','semilep_mu_withCghost').replace('semilep_el','semilep_el_withCghost').replace('hadronic','hadronic_withCghost'))
            else:
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_withCghost')
        if track_type == 'extendedZp_newLabels':
            if 'pT' in var:
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var.replace('semilep_mu','semilep_mu_newLabels').replace('semilep_el','semilep_el_newLabels').replace('hadronic','hadronic_newLabels'))
            else:
                h_id = get_hist_id(tracking_submitJobs.zp_extended_sample, jet_type, 'nom', var+'_newLabels')
        histograms[h_id].SetMinimum(0)
        histograms[h_id].SetMaximum(1.7*h_max)
        if '_newLabels' in track_type:
            histograms[h_id].SetLineStyle(9)
        if 'SipA0' in var:
            histograms[h_id].GetXaxis().SetTitle('Track d_{0} [mm]')
            if not scale_to_unity:
                histograms[h_id].GetYaxis().SetTitle('Entries')
            if not do_zoom:
                histograms[h_id].SetMinimum(10e-7)
                histograms[h_id].SetMaximum(5000*histograms[h_id].GetMaximum())
                ROOT.gPad.SetLogy()
        if 'jet_decayMode' in var:
            histograms[h_id].GetXaxis().SetBinLabel(1,'Hadronic')
            histograms[h_id].GetXaxis().SetBinLabel(2,'Semileptonic (#mu)')
            histograms[h_id].GetXaxis().SetBinLabel(3,'Semileptonic (e)')
        # Change name of axis temporarily
        if '_beff_' in var: histograms[h_id].GetYaxis().SetTitle('b-jet efficiency')
        if '_leff_' in var: histograms[h_id].GetYaxis().SetTitle('l-jet efficiency')

        # Things to pass to the plotting function
        h_to_plot.append(histograms[h_id])
        h_colors.append(colors[track_type])
        h_descriptors.append(legend_titles[track_type])

    # Labels
    sample_shortName = get_short_name(sample)
    short_title = get_short_title(sample, jet_type)
    variation_label = ''
    if '_bjet' in var or '_beff_' in var: short_title += ', b-jets'
    if '_cjet' in var or '_ceff_' in var: short_title += ', c-jets'
    if '_ljet' in var or '_leff_' in var: short_title += ', l-jets'
    if '_HF' in var: short_title += ', HF tracks'
    if '_nonHF' in var: short_title += ', non-HF tracks'
    if 'SipA0' in var: variation_label = get_IPXD_title(var)
    if 'jet_sv1_sig3d_beff50' in var: variation_label = 'SV1 b-jet efficiency 50%'
    if 'jet_sv1_sig3d_leff04' in var: variation_label = 'SV1 l-jet efficiency 4%'
    if 'jet_ip2_beff50' in var: variation_label = 'IP2D b-jet efficiency 50%'
    if 'jet_ip2_leff04' in var: variation_label = 'IP2D l-jet efficiency 4%'
    if 'jet_ip3_beff50' in var: variation_label = 'IP3D b-jet efficiency 50%'
    if 'jet_ip3_leff04' in var: variation_label = 'IP3D l-jet efficiency 4%'
    if 'isojet' in var: variation_label = 'dR_{min} > 1.0'
    if '1bH' in var: variation_label = 'N_{bH} = 1'
    if 'isojet' in var and '1bH' in var: variation_label = 'N_{bH} = 1, dR_{min} > 1.0'
    if 'largeFrag' in var: variation_label += ', Frag > 1.0'
    if 'semilep_mu' in var: variation_label += ', Semileptonic (#mu) b decays'
    if 'semilep_el' in var: variation_label += ', Semileptonic (e) b decays'
    if 'hadronic' in var: variation_label += ', Hadronic b decays'
    if 'leadingJet' in var: variation_label = 'Leading jet'
    if 'subleadingJet' in var: variation_label = 'Subleading jet'

    for pt_bin in draw_roc_curves.pt_bins_titles:
        if pt_bin in var:
            variation_label += draw_roc_curves.pt_bins_titles[pt_bin]

    # Save plots
    if 'SipA0' in var:
        if do_zoom: h_name = 'h_' + sample_shortName + '_' + var.replace('SipA0','SipA0_'+str(track_categories_IPXD.index(var.split('_')[-1]))) + '_zoom'
        else: h_name = 'h_' + sample_shortName + '_' + var.replace('SipA0','SipA0_'+str(track_categories_IPXD.index(var.split('_')[-1])))
    else:
        h_name = 'h_' + sample_shortName + '_' + var

    plot_utils.plot_histograms(plot_dir, h_to_plot, h_descriptors, h_colors, h_name, short_title, variation_label, debug, ratio_limits = ratio_lims, show_entries=True)

def get_IPXD_histograms(sample, jet_type, tracks):
    if debug: print('Getting IPxD histograms')
    hists_IPXD = {}
    # Get relevant histograms
    for t in tracks + ['ref']:
        # V40 vs. V49 IPxD for EMTopo are the same, so use V49, which includes PFlow templates
        # Note that PFlow uses EMTopo templates anyways...
        if t == 'ref': filename = create_ipxd_templates.ref_hist_dir + sample + '/BTagCalibRUN2-08-49.root'
        else: filename = create_ipxd_templates.ref_hist_dir + sample + '/BTagCalibRUN2-08-49_{}.root'.format(t)

        if debug: print('Opening file: {}'.format(filename))
        f = ROOT.TFile.Open(filename, 'READ')

        for jflav in process_trees.jet_flavours:
            for cat in track_categories_IPXD:
                flav = jflav[0].upper()
                # Light-jets are named 'U' in IPXD ntuples
                if flav == 'L': flav = 'U'
                hist_to_get = 'IP2D/AntiKt4EMTopo/' + flav + '/' + cat + '/SipA0'
                if verbose: print('Getting histogram: {}'.format(hist_to_get))
                h = f.Get(hist_to_get)
                var = 'SipA0_' + flav + '_' + cat
                h_id = get_hist_id(sample, jet_type, t, var)
                hists_IPXD[h_id] = h.Clone()
                hists_IPXD[h_id].SetDirectory(0)
        f.Close()

    if debug:
        print('Obtained IPxD histograms')
        if verbose:
            print('IPxD histograms:')
            for h_id, h in hists_IPXD.items():
                print('{} : {}'.format(h_id, h))
    return hists_IPXD

def get_eff_histograms(filename):
    eff_hists = {}
    f = ROOT.TFile.Open(filename)
    for key in f.GetListOfKeys():
        k_name = key.GetName()
        h = f.Get(k_name)
        isTH1 = h.InheritsFrom(ROOT.TH1.Class())
        if verbose: print(k_name, isTH1)
        if isTH1:
            eff_hists[k_name] = h.Clone()
            eff_hists[k_name].SetDirectory(0)
    return eff_hists

def plot_IPXD_templates(hists_IPXD, sample, jet_type, tracks):
    # Plot single jet type, compare different track types, for each jet type
    for cat in track_categories_IPXD:
        plot_comparison(hists_IPXD, 'SipA0_B_'+cat, sample, jet_type, tracks + ['ref'])
        plot_comparison(hists_IPXD, 'SipA0_C_'+cat, sample, jet_type, tracks + ['ref'])
        plot_comparison(hists_IPXD, 'SipA0_U_'+cat, sample, jet_type, tracks + ['ref'])
        plot_comparison(hists_IPXD, 'SipA0_B_'+cat, sample, jet_type, tracks + ['ref'], True)
        plot_comparison(hists_IPXD, 'SipA0_C_'+cat, sample, jet_type, tracks + ['ref'], True)
        plot_comparison(hists_IPXD, 'SipA0_U_'+cat, sample, jet_type, tracks + ['ref'], True)


        
def print_IPXD_table(hists_IPXD, sample, jet_type, tracks):
    print('\nIPxD table for sample: {} ({})'.format(sample, jet_type))
    rates = {}
    totals = {}
    print('\n')
    print('-'*116)
    print('| {:<2} | {:<20} | {:^84} |'.format('','','Rate [%]'))
    print('| {:<2} | {:<20} | {:^26} | {:^26} | {:^26} |'.format('#','Descriptor','b-jets','c-jets','l-jets'))
    print('| {:<2} | {:<20} | {:^5},{:^5}, {:^5}, {:^5} | {:^5},{:^5}, {:^5}, {:^5} | {:^5},{:^5}, {:^5}, {:^5} |'.format('','','nom','pseudo','ideal','ref','nom','pseudo','ideal','ref','nom','pseudo','ideal','ref'))
    print('-'*116)
    for jflav in process_trees.jet_flavours:
        flav = jflav[0].upper()
        # Light-jets are named 'U' in IPXD ntuples
        if flav == 'L': flav = 'U'
        totals[flav] = [0] * len(tracks)
        rates[flav] = [[0]*len(track_categories_IPXD) for i in range(len(tracks))]
        for i, track_type in enumerate(tracks):
            for j, cat in enumerate(track_categories_IPXD):
                var = 'SipA0_' + flav + '_' + cat
                h_id = get_hist_id(sample, jet_type, track_type, var)
                h = hists_IPXD[h_id]
                n = h.GetEntries()
                if verbose: print(h_id,n)
                rates[flav][i][j] = n
                totals[flav][i] += n

    if verbose:
        print('rates', rates)
        print('totals', totals)

    # Sanity check
    for jflav in process_trees.jet_flavours:
        flav = jflav[0].upper()
        # Light-jets are named 'U' in IPxD ntuples
        if flav == 'L': flav = 'U'
        for i, track_type in enumerate(tracks):
            tot = totals[flav][i]
            test_sum = sum(rates[flav][i])
            if tot != test_sum:
                print('TOTAL NOT EQUAL TO SUM OF CATEGORIES!! --> {} {}'.format(jflav, track_type))
                sys.exit()

    # Print out pretty table
    for j, cat in enumerate(track_categories_IPXD):
        to_print = '| {:<2} | {:<20} | '.format(j, cat)
        for jflav in process_trees.jet_flavours:
            flav = jflav[0].upper()
            # Light-jets are named 'U' in IPXD ntuples
            if flav == 'L': flav = 'U'
            for i, track_type in enumerate(tracks):
                num = rates[flav][i][j]
                denom = totals[flav][i]
                rate = 100 * num / denom
                to_print += '{:>5.2f}'.format(rate)
                if verbose: to_print += ' ({:>10}/{:>10})'.format(num,denom)
                if i < (len(tracks) - 1): to_print += ', '
            to_print += ' | '
        print(to_print)
    print('-'*116)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        tree_dir = sys.argv[1]
        out_dir  = '/unix/atlasvhbb2/srettie/tracking/plots/'
    elif len(sys.argv) >= 3:
        tree_dir = sys.argv[1]
        out_dir  = sys.argv[2]
    else:
        # Default directories
        tree_dir = '/unix/atlasvhbb2/srettie/tracking/athenaOutputs_full/'
        out_dir  = '/unix/atlasvhbb2/srettie/tracking/plots/'

    all_hists = tree_dir+'/all_hists.root'

    samples = [s.split('/')[-1] for s in glob.glob(tree_dir+'/*') if s.split('/')[-1].split('_')[-1] == 'EXT0' or s.split('/')[-1].split('.')[0] == 'mc16_13TeV']

    print('Found {} samples:'.format(len(samples)))
    for s in samples:
        print('\t{}'.format(s))

    if do_hadd:
        print('hadding histogram files')
        hadd_histograms(samples, tree_dir)
        sys.exit()

    # Produce histograms from output trees
    if not do_fast:
        submit_histogram_jobs(samples, process_trees.tracks)
        sys.exit()
    
    # Create output directory if non-existent
    if not os.path.exists(out_dir): os.mkdir(out_dir)

    # Open hadded histogram file
    print('Getting histograms...')
    histograms = plot_utils.get_histograms(all_hists, verbose)

    if not len(histograms):
        print('ERROR! Histograms map is empty; exiting.')
        sys.exit()

    if verbose:
        print('Available histograms:')
        for h_name, h in histograms.items():
            print(h_name+': '+str(h))
        
    # Rebin relevant histograms
    rebin_1D(histograms, samples)

    # Add overflow/underflow
    add_1D_overunderflow(histograms, samples)

    print('Plotting!')
    if debug: print('All histograms: {}'.format(all_hists))

    # Produce plots
    for sample in samples:
        if sample not in good_samples:
            print('Skipping: {}'.format(sample))
            continue
        print('Sample: {}'.format(sample))

        for j in tracking_submitJobs.jet_types:
            # Setup plot directory structure
            base_path = out_dir + get_short_name(sample)
            if 'custom_ipxd' in tree_dir: base_path += '_custom_ipxd'
            if not os.path.exists(base_path): os.mkdir(base_path)
            base_path += '/'+j+'/'
            if not os.path.exists(base_path): os.mkdir(base_path)

            # Only plot IPxD templates for initial pass
            if 'custom_ipxd' not in tree_dir and 'mc16_13TeV' not in sample:
                print('Getting IPXD histograms...')
                hists_IPXD = get_IPXD_histograms(sample, j, process_trees.tracks)
                print_IPXD_table(hists_IPXD, sample, j, ['nom','pseudo','ideal','ref'])
                plot_dir = base_path + 'IPxD_templates/'
                if not os.path.exists(plot_dir): os.mkdir(plot_dir)
                plot_IPXD_templates(hists_IPXD, sample, j, ['nom','pseudo','ideal'])

            # Plot efficiency comparisons
            plot_dir = base_path + '/efficiencies/'
            if not os.path.exists(plot_dir): os.makedirs(plot_dir)
            # Get efficiency plots
            all_effs = tree_dir + '/all_roc_curves.root'
            if debug: print('Getting ROC efficiency histograms from ({}): {}'.format(j, all_effs))
            eff_hists = get_eff_histograms(all_effs)
            tracks = ['nom','pseudo','ideal','nom_RFMVA_loose','nom_RFMVA_tight','nom_RF75']
            if 'mc16_13TeV' in sample: tracks = ['nom','nom_RFMVA_loose','nom_RFMVA_tight']
            classifiers = produce_roc_curve.classifiers
            for classifier in classifiers:
                for var in ['jet_pt','jet_eta','jet_phi']:
                    for eff_type in ['beff','leff']:
                        for wp in ['beff50','leff04']:
                            tot_var = var + '_' + eff_type + '_' + classifier + '_' + wp
                            plot_comparison(eff_hists, tot_var, sample, j, tracks)

            # Plot kinematic variables etc.
            for var in process_trees.plot_skeletons:
                if var not in ['jet_bH_pt_frac','jet_bH_pt']:continue#,'jet_decayMode','njets','jet_bH_Lxy','jet_bH_pt_vs_jet_pt','jet_bH_dRjet','jet_pt']: continue
                # 2D plots
                if '_vs_' in var:
                    plot_dir = base_path + '/2D/'
                    if not os.path.exists(plot_dir): os.makedirs(plot_dir)
                    if var == 'jet_bH_pt_vs_jet_pt':
                        for sel in ['']+bjet_selections:
                            plot_2D(histograms, var+'_bjet'+sel, 'nom', sample, j)
                            plot_2D(histograms, var+'_bjet'+sel, 'nom', tracking_submitJobs.zp_extended_sample, j)
                            plot_2D(histograms, var+'_bjet'+sel+'_rw_bcl_2d', 'nom', tracking_submitJobs.zp_extended_sample, j)
                            plot_2D(histograms, var+'_bjet'+sel+'_rw_bcl_3d', 'nom', tracking_submitJobs.zp_extended_sample, j)
                        continue
                    for t in ['nom','pseudo','ideal']:
                        plot_2D(histograms, var, t, sample, j)
                        plot_2D(histograms, var+'_bjet', t, sample, j)
                    continue
                # Regular plot comparisons
                to_enum = [
                    ['nom','pseudo','ideal'],
                    ['nom','nom_RF75','nom_RF75_replaceWithTruth','nom_RF90'],
                    ['selectHF','selectHF_replaceWithTruth'],
                    ['nom','nom_replaceHFWithTruth','nom_replaceFRAGWithTruth','nom_replaceFRAGHFWithTruth','nom_replaceFRAGHFGEANTWithTruth','nom_replaceWithTruth','pseudo','ideal'],
                    ['nom','pseudo','ideal','nom_RF75','selectFRAG','selectFRAGHF','selectFRAGHFGEANT'],
                    ['nom','pseudo','ideal','nom_RFMVA_loose','nom_RFMVA_tight','nom_RF75'],
                    ['nom','extendedZp_rw_bcl_2d','extendedZp_rw_bcl_3d'],#'extendedZp'
                ]
                # Extended Z' sample only has nom and RFMVA
                if 'mc16_13TeV' in sample:
                    to_enum = [['extendedZp','extendedZp_withCghost','extendedZp_newLabels']]#[['nom','nom_RFMVA_loose','nom_RFMVA_tight']]

                for i, tracks in enumerate(to_enum):
                    if i < 6 and 'mc16_13TeV' not in sample: continue
                    if i == 0 : plot_dir = base_path + '/nom_pseudo_ideal/'
                    if i == 1 : plot_dir = base_path + '/nom_vs_RF75/'
                    if i == 2 : plot_dir = base_path + '/selectHF/'
                    if i == 3 : plot_dir = base_path + '/selectFRAGHF/'
                    if i == 4 : plot_dir = base_path + '/selectFRAGHF_vs_RF75/'
                    if i == 5 : plot_dir = base_path + '/RFMVA/'
                    if i == 6 or 'mc16_13TeV' in sample: plot_dir = base_path + '/PU_vs_noPU/'
                    if not os.path.exists(plot_dir): os.makedirs(plot_dir)
                    if '_rate_' in var:
                        plot_rate(histograms, var, sample, j, tracks)
                        continue
                    # Event-level variables are saved with specific weight designations
                    if var == 'njets' or 'PV' in var:
                        plot_comparison(histograms, var+'_weighted', sample, j, tracks)
                    if 'jet_bH' not in var:
                        # Only plot bH quantities for b-jets
                        plot_comparison(histograms, var, sample, j, tracks)
                    for jflav in process_trees.jet_flavours:
                        # Only plot bH quantities for b-jets
                        if '_bH_' in var and jflav != 'bjet': continue
                        # Additional 1bH and isojet selection for bH plots
                        if 'jet_bH' in var:
                            plot_comparison(histograms, var+'_'+jflav+'_1bH_isojet_semilep_mu', sample, j, tracks)
                            plot_comparison(histograms, var+'_'+jflav+'_1bH_isojet_semilep_el', sample, j, tracks)
                            plot_comparison(histograms, var+'_'+jflav+'_1bH_isojet_hadronic', sample, j, tracks)
                            for sel in bjet_selections:
                                plot_comparison(histograms, var+'_'+jflav+sel, sample, j, tracks)
                        # Event-level variables are saved with specific weight designations
                        if var == 'njets' or 'PV' in var:
                            plot_comparison(histograms, var+'_'+jflav+'_weighted', sample, j, tracks)
                        if var == 'njets' and jflav == 'bjet':
                            plot_comparison(histograms, var+'_'+jflav+'_semilep_mu', sample, j, tracks)
                            plot_comparison(histograms, var+'_'+jflav+'_semilep_el', sample, j, tracks)
                            plot_comparison(histograms, var+'_'+jflav+'_hadronic', sample, j, tracks)
                            plot_comparison(histograms, var+'_'+jflav+'_semilep_mu_weighted', sample, j, tracks)
                            plot_comparison(histograms, var+'_'+jflav+'_semilep_el_weighted', sample, j, tracks)
                            plot_comparison(histograms, var+'_'+jflav+'_hadronic_weighted', sample, j, tracks)
                        if var == 'jet_decayMode' and jflav == 'bjet':
                            plot_comparison(histograms, var+'_'+jflav+'_leadingJet', sample, j, tracks)
                            plot_comparison(histograms, var+'_'+jflav+'_subleadingJet', sample, j, tracks)

                        # Plot everything without weights
                        plot_comparison(histograms, var+'_'+jflav, sample, j, tracks)
                    # Now plot pT-binned histograms
                    for pt_bin in list(process_trees.pt_bins):
                        # Event-level variables don't make sense in pT bins
                        if '_rate_' in var or 'njets' in var or 'PV' in var: continue
                        # Only plot bH quantities for b-jets
                        if 'jet_bH' not in var:
                            plot_comparison(histograms, var+'_'+pt_bin, sample, j, tracks)
                        for jflav in process_trees.jet_flavours:
                            if '_bH_' in var and jflav != 'bjet': continue
                            plot_comparison(histograms, var+'_'+jflav+'_'+pt_bin, sample, j, tracks)
                            if 'jet_bH' in var:
                                plot_comparison(histograms, var+'_'+jflav+'_1bH_isojet_semilep_mu_'+pt_bin, sample, j, tracks)
                                plot_comparison(histograms, var+'_'+jflav+'_1bH_isojet_semilep_el_'+pt_bin, sample, j, tracks)
                                plot_comparison(histograms, var+'_'+jflav+'_1bH_isojet_hadronic_'+pt_bin, sample, j, tracks)
                                for sel in bjet_selections:
                                    plot_comparison(histograms, var+'_'+jflav+sel+'_'+pt_bin, sample, j, tracks)
                            if var == 'jet_decayMode' and jflav == 'bjet':
                                plot_comparison(histograms, var+'_'+jflav+'_leadingJet_'+pt_bin, sample, j, tracks)
                                plot_comparison(histograms, var+'_'+jflav+'_subleadingJet_'+pt_bin, sample, j, tracks)

                # Plot single variable, i.e. unreconstructed pseudo tracks for no-pileup sample
                if 'mc16_13TeV' in sample: continue
                plot_dir = base_path + '/pseudoNotReco/'
                if not os.path.exists(plot_dir): os.makedirs(plot_dir)
                if '_rate_' in var:
                    plot_rate(histograms, var, sample, j, ['pseudoNotReco'])
                    continue
                plot_single_var(histograms, var, sample, j, 'pseudoNotReco')
                if '_trk_' in var:
                    plot_single_var(histograms, var+'_HF', sample, j, 'pseudoNotReco')
                for jflav in process_trees.jet_flavours:
                    plot_single_var(histograms, var+'_'+jflav, sample, j, 'pseudoNotReco')

    print('Done!')