import os
import ROOT
import plot_utils as pu

labels = {
    'nom'           : 'Nominal',
    'sisp'          : 'SiSP Tracks (input to ambi)',
    # 'pseudo'        : 'Pseudo Tracks',
    # 'skip_ambi_roi' : 'Skip Ambi ROI',
    # 'skip_ambi_roi_2uniqueSi' : 'Skip Ambi ROI (2 Si)',
    # 'skip_ambi_roi_2uniqueSi_recover' : 'Skip Ambi ROI (2 Si, recover)',
    # 'skip_ambi_roi_3uniqueSCT_recover' : 'Skip Ambi ROI (3 SCT)',
    'recoPlusPseudoFromB' : 'Nominal + Pseudotracks FromB',
    'recoPlusROIPseudoFromB' : 'Nominal + Pseudotracks FromB (in ROI)',
}
origin_labels = {
    'B' : 'B-hadron',
    'C' : 'D-hadron',
    'Tau' : 'Tau',
    'Jet' : 'Jet',
    'Track' : 'All',
}
filepath = '/Users/sebastienrettie/CTIDE/analyzed_ntuples/CA_23.0.22/with_rois/'
filenames = [f'{filepath}/{d}/{d}.root' for d in labels]
out_dir = '/Users/sebastienrettie/CTIDE/plots_with_rois/'
colors = {
    'nom' : ROOT.kBlack,
    'sisp' : ROOT.kBlue,
    'pseudo' : ROOT.kGreen,
    'skip_ambi_roi' : ROOT.kBlue,
    'skip_ambi_roi_2uniqueSi' : ROOT.kRed,
    'skip_ambi_roi_2uniqueSi_recover' : ROOT.kGray,
    'skip_ambi_roi_3uniqueSCT_recover' : ROOT.kMagenta,
    'recoPlusPseudoFromB' : ROOT.kGray,
    'recoPlusROIPseudoFromB' : ROOT.kMagenta,
}
marker_styles = {
    'Reco' : 20,
    'SiSP' : 23,
    'Pseudo' : 22,
}
line_styles = {
    'Reco' : 1,
    'SiSP' : 3,
    'Pseudo' : 2,
}
marker_colors = {
    'Reco' : colors['nom'],
    'SiSP' : colors['sisp'],
    'Pseudo' : colors['pseudo'],
}
jet_flav_colors = {
    'B' : ROOT.kGreen,
    'C' : ROOT.kBlue,
    'U' : ROOT.kBlack,
}
h_names = {}
# kinematic plots
prefixes = [
    # 'h_closestHadROIEt',
    # 'h_closestHadROIdR',
    'h_pt',
    # 'h2_pT_dR',
    # 'h2_pT_dR_zoom',
    # 'h2_Et_dR',
    # 'h2_Et_dR_zoom',
    # 'h2_pT_Et',
    # 'h2_pT_Et_zoom',
    'h_nROIsNearTrack',
    'h_ambiScore',
    'h_TMP',
    'h_closestROIdR',
]
for p in prefixes:
    folder = 'TrkObserverTool' if p == 'h_ambiScore' else 'TrkInformation'
    h_names[f'{p}_btracks'] = f'TrackInformation/ReconstructablePseudo/From_B/{folder}/{p}_From_B'
    h_names[f'{p}_othertracks'] = f'TrackInformation/ReconstructablePseudo/From_Other/{folder}/{p}_From_Other'

# b/c eff
for o in ['B', 'C']:
    h_names[f'eff_num_{o}'] = f'Efficiency/From_{o}/p_recoEfficiency_pT{o}_From_{o}'
    h_names[f'eff_den_{o}'] = f'Efficiency/From_{o}/p_recoEfficiency_pT_ALL{o}_From_{o}'

# tau eff
h_names[f'eff_num_Tau'] = f'Efficiency/From_Tau/p_recoEfficiency_pT_From_Tau'
h_names[f'eff_den_Tau'] = f'Efficiency/From_Tau/p_recoEfficiency_pT_ALL_From_Tau'

# jet eff
h_names['eff_num_Jet'] = 'Efficiency/From_All/p_recoEfficiency_JetpT5_From_All'
h_names['eff_den_Jet'] = 'Efficiency/From_All/p_recoEfficiency_JetpT5_ALL_From_All'

# general track eff
h_names['eff_num_Track'] = 'Efficiency/From_All/p_recoEfficiency_pT_From_All'
h_names['eff_den_Track'] = 'Efficiency/From_All/p_recoEfficiency_pT_ALL_From_All'

# fake rates
fake_rate_mods = {
    'default' : 'All tracks',
    'lowpT_default' : 'All tracks',
    'nonHF' : 'Non-HF tracks',
    'indR01' : 'dR(track, jet) < 0.1',
    'outdR01' : 'dR(track, jet) > 0.1',
    'lowpT_indR01' : 'dR(track, jet) < 0.1',
    'lowpT_outdR01' : 'dR(track, jet) > 0.1',
    'indR02' : 'dR(track, jet) < 0.2',
    'outdR02' : 'dR(track, jet) > 0.2',
    'lowpT_indR02' : 'dR(track, jet) < 0.2',
    'lowpT_outdR02' : 'dR(track, jet) > 0.2',
}
h_names['fake_rate_default_num'] = 'Efficiency/From_All/p_fracFakeTracks5_pT_From_All'
h_names['fake_rate_default_den'] = 'Efficiency/From_All/p_fracFakeTracks5_pT_ALL_From_All'
h_names['fake_rate_lowpT_default_num'] = 'Efficiency/From_All/p_fracFakeTracks5_lowpT_pT_From_All'
h_names['fake_rate_lowpT_default_den'] = 'Efficiency/From_All/p_fracFakeTracks5_lowpT_pT_ALL_From_All'
h_names['fake_rate_nonHF_num'] = 'Efficiency/From_All/p_fracFakeTracks5nonHF_pT_From_All'
h_names['fake_rate_nonHF_den'] = 'Efficiency/From_All/p_fracFakeTracks5_pT_ALL_From_All'
h_names['fake_rate_indR01_num'] = 'Efficiency/From_All/p_fracFakeTracks5_indR01_pT_From_All'
h_names['fake_rate_indR01_den'] = 'Efficiency/From_All/p_fracFakeTracks5_indR01_pT_ALL_From_All'
h_names['fake_rate_outdR01_num'] = 'Efficiency/From_All/p_fracFakeTracks5_outdR01_pT_From_All'
h_names['fake_rate_outdR01_den'] = 'Efficiency/From_All/p_fracFakeTracks5_outdR01_pT_ALL_From_All'
h_names['fake_rate_lowpT_indR01_num'] = 'Efficiency/From_All/p_fracFakeTracks5_indR01_lowpT_pT_From_All'
h_names['fake_rate_lowpT_indR01_den'] = 'Efficiency/From_All/p_fracFakeTracks5_indR01_lowpT_pT_ALL_From_All'
h_names['fake_rate_lowpT_outdR01_num'] = 'Efficiency/From_All/p_fracFakeTracks5_outdR01_lowpT_pT_From_All'
h_names['fake_rate_lowpT_outdR01_den'] = 'Efficiency/From_All/p_fracFakeTracks5_outdR01_lowpT_pT_ALL_From_All'
h_names['fake_rate_indR02_num'] = 'Efficiency/From_All/p_fracFakeTracks5_indR02_pT_From_All'
h_names['fake_rate_indR02_den'] = 'Efficiency/From_All/p_fracFakeTracks5_indR02_pT_ALL_From_All'
h_names['fake_rate_outdR02_num'] = 'Efficiency/From_All/p_fracFakeTracks5_outdR02_pT_From_All'
h_names['fake_rate_outdR02_den'] = 'Efficiency/From_All/p_fracFakeTracks5_outdR02_pT_ALL_From_All'
h_names['fake_rate_lowpT_indR02_num'] = 'Efficiency/From_All/p_fracFakeTracks5_indR02_lowpT_pT_From_All'
h_names['fake_rate_lowpT_indR02_den'] = 'Efficiency/From_All/p_fracFakeTracks5_indR02_lowpT_pT_ALL_From_All'
h_names['fake_rate_lowpT_outdR02_num'] = 'Efficiency/From_All/p_fracFakeTracks5_outdR02_lowpT_pT_From_All'
h_names['fake_rate_lowpT_outdR02_den'] = 'Efficiency/From_All/p_fracFakeTracks5_outdR02_lowpT_pT_ALL_From_All'

# duplicates
h_names['n_duplicates'] = 'h_nDuplicates'
h_names['n_duplicatesNoFakes'] = 'h_nDuplicatesNoFakes'
h_names['rej_locations_from_b'] = 'h_rejectionLocations_from_b'
h_names['2D_rej_locations_from_b'] = 'h2_rejectionLocations_from_b'

# hadronic ROI quantities
h_names['nROIs'] = 'ROIs/h_nROIs'
h_names['2D_nJets_vs_nROIs'] = 'ROIs/h2_nJets_vs_nROIs'
h_names['nROIsPerJet'] = 'ROIs/h_nROIsPerJet'
h_names['nROIsPerJet_jet_flav_B'] = 'ROIs/h_nROIsPerJet_jet_flav_B'
h_names['nROIsPerJet_jet_flav_C'] = 'ROIs/h_nROIsPerJet_jet_flav_C'
h_names['nROIsPerJet_jet_flav_U'] = 'ROIs/h_nROIsPerJet_jet_flav_U'
# need to rerun ambi-solver-analyzer for these...
h_names['nTracksInROI_Reco'] = 'ROIs/h_nRecoTracksInROI'
h_names['nTracksInROI_SiSP'] = 'ROIs/h_nSiSPTracksInROI'
h_names['nTracksInROI_Pseudo'] = 'ROIs/h_nPseudoTracksInROI'
h_names['nClustersInROI_Reco'] = 'ROIs/h_nRecoClustersInROI'
h_names['nClustersInROI_SiSP'] = 'ROIs/h_nSiSPClustersInROI'
h_names['nClustersInROI_Pseudo'] = 'ROIs/h_nPseudoClustersInROI'
h_names['nMissingPseudoClustersInROI'] = 'ROIs/h_nMissingPseudoClustersInROI'
h_names['nIncompletePseudoTracksInROI'] = 'ROIs/h_nIncompletePseudoTracksInROI'
h_names['nIncompleteFromBPseudoTracksInROI'] = 'ROIs/h_nIncompleteFromBPseudoTracksInROI'

# track properties
pt_ranges = {
    'incpT' : 'Inclusive track p_{T}',
    'lowpT' : 'Track p_{T} < 10 GeV',
    'medpT' : '10 GeV #leq track p_{T} < 50 GeV',
    'highpT' : 'Track p_{T} #geq 50 GeV',
}
track_quantities = [
    'numberOfInnermostPixelLayerHits',
    'numberOfInnermostPixelLayerSharedHits',
    'numberOfNextToInnermostPixelLayerHits',
    'numberOfNextToInnermostPixelLayerSharedHits',
    'numberOfPixelHits',
    'numberOfPixelSharedHits',
    'numberOfPixelHoles',
    'numberOfSCTHits',
    'numberOfSCTSharedHits',
    'numberOfSCTHoles',
    # 'numberOfSiHits',
    'numSharedModules',
    'numberOfUniquePixelHits',
    'numberOfUniqueSCTHits',
    'numberOfUniqueHits',
    'd0',
    'd0Unc',
    'd0Pull',
    'z0',
    'z0Unc',
    'z0Pull'
]
rejection_locations = {
    0  : 'acceptedTrack',
    1  : 'stillBeingProcessed',
    2  : 'trackScoreZero',
    3  : 'duplicateTrack',
    4  : 'subtrackCreated',
    5  : 'refitFailed',
    6  : 'bremRefitFailed',
    7  : 'bremRefitSubtrackCreated',
    8  : 'bremRefitTrackScoreZero',
    9  : 'refitTrackScoreZero',
    10 : 'TSOSRejectedHit',
    11 : 'TSOSOutlierShared',
    12 : 'pixelSplitButTooManyShared2Ptc',
    13 : 'pixelSplitButTooManyShared3Ptc',
    14 : 'tooManySharedRecoverable',
    15 : 'tooManySharedNonRecoverable',
    16 : 'sharedSCT',
    17 : 'sharedHitsBadChi2',
    18 : 'sharedHitsNotEnoughUniqueHits',
    19 : 'firstHitSharedAndPixIBL',
    20 : 'firstHitSharedAndExtraShared',
    21 : 'sharedHitsNotEnoughUniqueSiHits',
    22 : 'sharedIBLSharedWithNoIBLTrack',
    23 : 'sharedPixelSharedWithDifferentIBLTrack',
    24 : 'tooManySharedAfterIncreasingShared',
    25 : 'notEnoughSiHits',
    26 : 'notEnoughTRTHits',
    27 : 'notEnoughUniqueSiHits',
    28 : 'tooFewHits',
    29 : 'failedSubtrackCreation',
    30 : 'subtrackCreatedWithRecoveredShared',
    31 : 'other',
}
for q in track_quantities + ['TMP']:
    for pt in pt_ranges:
        ptsuffix = '' if pt == 'incpT' else f'_{pt}'
        h_names[f'{q}_{pt}'] = f'TrackInformation/ReconstructedRecoTracks{ptsuffix}/From_All/TrkInformation/h_{q}_From_All'
        h_names[f'{q}_{pt}_b'] = f'TrackInformation/ReconstructedRecoTracks{ptsuffix}/From_B/TrkInformation/h_{q}_From_B'

def get_hists_from_file(fname, key):
    print(f'reading file {fname}')
    f = ROOT.TFile(fname, 'READ')
    histos = {}
    for hkey, hname in h_names.items():
        histos[f'{key}_{hkey}'] = f.Get(hname)
        histos[f'{key}_{hkey}'].SetDirectory(0)
    # plot distributions for nominal configuration
    for hname, h in histos.items():
        col = ROOT.kRed if 'othertracks' in hname else colors[key]
        if 'jet_flav_' in hname:
            col = jet_flav_colors[hname.split('jet_flav_')[-1][0]]
        if 'nTracksInROI' in hname or 'nClustersInROI' in hname:
            prefix = hname.split('_')[-1]
            col = marker_colors[prefix]
        if 'nIncompletePseudo' in hname:
            col = ROOT.kRed
        pu.set_hist_color_style(h, col, set_fill = False)
        # pu.add_overflow(h)
        # pu.add_underflow(h)
        # if 'eff_' in hname or 'fake_rate' in hname:
        #     h.Rebin(4)
        if 'TMP' in hname:
            h.Rebin(4)
        if 'nROIsNearTrack' in hname:
            h.GetXaxis().SetRangeUser(0, 7)
        if hname == 'nom_nROIs':
            h.GetXaxis().SetRangeUser(0, 15)
            h.GetXaxis().SetTitle('Number of hadronic ROIs in event')
        if hname == 'nom_2D_nJets_vs_nROIs':
            h.GetXaxis().SetTitle('Number of hadronic ROIs in event')
            h.GetXaxis().SetRangeUser(0, 15)
            h.GetYaxis().SetTitle('Number of jets in event')
            h.GetYaxis().SetRangeUser(0, 15)
            h.GetZaxis().SetTitle('Events')
        if 'nTracksInROI' in hname:
            h.GetXaxis().SetTitle('Number of tracks per ROI')
            h.GetYaxis().SetTitle('Entries')
            h.SetBinContent(1, 0)
        if 'nClustersInROI' in hname:
            h.GetXaxis().SetTitle('Number of clusters per ROI')
            h.GetYaxis().SetTitle('Entries')
            h.GetXaxis().SetRangeUser(0, 250)
            h.SetBinContent(1, 0)
        if 'nIncomplete' in hname:
            h.GetXaxis().SetTitle('Number of incomplete pseudo tracks per ROI')
        if 'numSharedModules' in hname:
            h.GetXaxis().SetTitle('Number of Shared Modules')
            h.GetXaxis().SetTitle('Number of Shared Modules')
    if key == 'nom':
        # make plots
        pu.set_ATLAS_style()
        for p in prefixes:
            if 'h2' not in p:
                pu.plot_histograms(out_dir, [histos[f'{key}_{p}_btracks'], histos[f'{key}_{p}_othertracks']], ['b tracks', 'other tracks'], f'{p}_comparison_nom', short_title = 'Non-reconstructed, Reconstructable Tracks')
                pu.plot_histograms(out_dir, [pu.get_unit_normalized_hist(histos[f'{key}_{p}_btracks']), pu.get_unit_normalized_hist(histos[f'{key}_{p}_othertracks'])], ['b tracks', 'other tracks'], f'{p}_comparison_nom_norm', short_title = 'Non-reconstructed, Reconstructable Tracks', ratio_limits = [0, 2])
                if p == 'h_pt':
                    histos[f'{key}_{p}_btracks'].GetXaxis().SetRangeUser(0,500)
                    histos[f'{key}_{p}_othertracks'].GetXaxis().SetRangeUser(0,500)
                    pu.plot_histograms(out_dir, [histos[f'{key}_{p}_btracks'], histos[f'{key}_{p}_othertracks']], ['b tracks', 'other tracks'], f'{p}_comparison_nom_zoom', short_title = 'Non-reconstructed, Reconstructable Tracks')
                    pu.plot_histograms(out_dir, [pu.get_unit_normalized_hist(histos[f'{key}_{p}_btracks']), pu.get_unit_normalized_hist(histos[f'{key}_{p}_othertracks'])], ['b tracks', 'other tracks'], f'{p}_comparison_nom_norm_zoom', short_title = 'Non-reconstructed, Reconstructable Tracks', ratio_limits = [0, 2])
            else:
                pu.plot_2D(out_dir, histos[f'{key}_{p}_btracks'], f'{p}_track_ROI_btracks_nom', short_title = 'b tracks')
                pu.plot_2D(out_dir, histos[f'{key}_{p}_othertracks'], f'{p}_track_ROI_othertracks_nom', short_title = 'other tracks')
    return histos

def plot_trajectories(iroi, tgraphs):
    print(f'plotting trajectories for roi {iroi}')
    n_tracks = {p : 0 for p in marker_styles}
    n_clusters = {p : 0 for p in marker_styles}
    leg_keys = {p : '' for p in marker_styles}
    cnv = ROOT.TCanvas()
    cnv.cd()
    lx1,ly1,lx2,ly2 = 0.5,0.7,0.8,0.9
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    # get min/max y-values from tgraphs
    y_min = min([ROOT.TMath.MinElement(len(tgraphs[tgname].GetY()), tgraphs[tgname].GetY()) for tgname in tgraphs if f'roi_{iroi}_' in tgname], default = 0)
    y_max = max([ROOT.TMath.MaxElement(len(tgraphs[tgname].GetY()), tgraphs[tgname].GetY()) for tgname in tgraphs if f'roi_{iroi}_' in tgname], default = 999)
    x_min = min([ROOT.TMath.MinElement(len(tgraphs[tgname].GetX()), tgraphs[tgname].GetX()) for tgname in tgraphs if f'roi_{iroi}_' in tgname], default = 0)
    x_max = max([ROOT.TMath.MaxElement(len(tgraphs[tgname].GetX()), tgraphs[tgname].GetX()) for tgname in tgraphs if f'roi_{iroi}_' in tgname], default = 999)
    # set plotting limits based on quadrant
    if y_min > 0 and y_max > 0:
        y_min = 0
        y_max = 1.5 * y_max
    if y_min < 0 and y_max < 0:
        y_min = -1.1 * abs(y_min)
        y_max = 200
    if x_min > 0 and x_max > 0:
        x_min = 0
        x_max = 1.05 * x_max
    if x_min < 0 and x_max < 0:
        x_max = 0
        x_min = -1.05 * abs(x_min)
    first = True
    for tgname, tg in tgraphs.items():
        if f'roi_{iroi}_' not in tgname:
            continue
        prefix = tgname.split('_')[0]
        n_tracks[prefix] += 1
        n_clusters[prefix] += tg.GetN()
        leg_keys[prefix] = tgname
        if tg.GetN() == 0:
            raise ValueError('zero clusters!')
        if first:
            tg.GetXaxis().SetLimits(x_min, x_max)
            tg.GetYaxis().SetRangeUser(y_min, y_max)
            tg.GetXaxis().SetTitle('Global X')
            tg.GetYaxis().SetTitle('Global Y')
            tg.Draw('ALP')
            first = False
        else:
            tg.Draw('LP,same')
    pu.draw_labels(f'ROI {iroi}', '')
    # don't save empty plots...
    if sum(n_clusters.values()) == 0:
        print('No clusters, skipping trajectory drawing!')
        return
    for prefix, leg_key, ntracks, nclusters in zip(n_tracks.keys(), leg_keys.values(), n_tracks.values(), n_clusters.values()):
        if nclusters > 0:
            leg.AddEntry(tgraphs[leg_key], f'{prefix} ({ntracks} tracks, {nclusters} clusters)')
    leg.Draw('same')
    out_name = f'roi_{iroi}.pdf'
    cnv.SaveAs(os.path.join(out_dir, 'trajectories', out_name))
    cnv.Close()


if __name__ == '__main__':
    histograms, efficiencies, fake_rates = {}, {}, {}
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)
    if not os.path.isdir(os.path.join(out_dir, 'trajectories')):
        os.mkdir(os.path.join(out_dir, 'trajectories'))
    tgraphs = pu.load_from_file(os.path.join(filepath, 'trajectories_ntuple/user.srettie.33322583.EXT0._001112.hist.root'), 'TGraph')
    print('got graphs')
    for k, tg in tgraphs.items():
        # print(f'{k}: {tg}')
        prefix = k.split('_')[0]
        tg.SetMarkerStyle(marker_styles[prefix])
        tg.SetMarkerColor(marker_colors[prefix])
        tg.SetLineStyle(line_styles[prefix])
        tg.SetLineColor(marker_colors[prefix])
    pu.set_ATLAS_style()
    for iroi in range(50):
        plot_trajectories(iroi, tgraphs)

    for f, k in zip(filenames, labels):
        histograms.update(get_hists_from_file(f, k))
        # efficiencies
        for o in origin_labels:
            efficiencies[f'{k}_{o}'] = ROOT.TEfficiency(histograms[f'{k}_eff_num_{o}'], histograms[f'{k}_eff_den_{o}'])
            efficiencies[f'{k}_{o}'].GetTotalHistogram().GetXaxis().SetTitle(f'{origin_labels[o]}'+' p_{T} [GeV]')
            efficiencies[f'{k}_{o}'].SetLineColor(colors[k])
            efficiencies[f'{k}_{o}'].SetMarkerColor(colors[k])
        # fake rates
        for fr in fake_rate_mods:
            fake_rates[f'{k}_{fr}'] = ROOT.TEfficiency(histograms[f'{k}_fake_rate_{fr}_num'], histograms[f'{k}_fake_rate_{fr}_den'])
            fake_rates[f'{k}_{fr}'].GetTotalHistogram().GetXaxis().SetTitle('Track p_{T} [GeV]')
            fake_rates[f'{k}_{fr}'].SetLineColor(colors[k])
            fake_rates[f'{k}_{fr}'].SetMarkerColor(colors[k])
    pu.set_ATLAS_style()
    for o in origin_labels:
        v_label = f'{origin_labels[o]} tracks'
        yrange = [0.4, 1.3]
        if o == 'Jet':
            v_label +=' with TMP > 0.5'
            yrange = [0.85, 1.1]
        if o == 'Tau':
            yrange = [0.8, 1.2]
        pu.plot_efficiencies(
            out_dir,
            [efficiencies[f'{k}_{o}'] for k in labels if 'pseudo' not in k],
            [labels[k] for k in labels if 'pseudo' not in k],
            f'eff_pT_{o}',
            short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
            variation_label = v_label,
            x_range = [0, 3000],
            y_range = yrange
        )
    for fr in fake_rate_mods:
        pu.plot_efficiencies(
            out_dir,
            [fake_rates[f'{k}_{fr}'] for k in labels if 'pseudo' not in k],
            [labels[k] for k in labels if 'pseudo' not in k],
            f'fake_rate_pT_{fr}',
            short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
            variation_label = fake_rate_mods[fr],
            x_range = [0, 100] if 'lowpT' in fr else [0, 3000],
            # y_range = [0, 0.4]
        )
    for q in track_quantities:
        for pt in pt_ranges:
            pu.plot_histograms(
                out_dir,
                [pu.get_unit_normalized_hist(histograms[f'{k}_{q}_{pt}']) for k in labels],
                labels.values(),
                f'{q}_{pt}',
                short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
                variation_label = 'All tracks, ' + pt_ranges[pt],
                ratio_limits = [0, 4] if 'Shared' in q else None
            )
            pu.plot_histograms(
                out_dir,
                [pu.get_unit_normalized_hist(histograms[f'{k}_{q}_{pt}_b']) for k in labels],
                labels.values(),
                f'{q}_{pt}_b',
                short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
                variation_label = 'B hadron tracks, ' + pt_ranges[pt],
                ratio_limits = [0, 4] if 'Shared' in q else None
            )
    # don't plot pseudo tracks for TMP
    for pt in pt_ranges:
        for k in labels:
            histograms[f'{k}_TMP_{pt}'].GetYaxis().SetTitle("Tracks")
            histograms[f'{k}_TMP_{pt}_b'].GetYaxis().SetTitle("Tracks")
        pu.plot_histograms(
            out_dir,
            [pu.get_unit_normalized_hist(histograms[f'{k}_TMP_{pt}']) for k in labels if 'pseudo' not in k],
            [labels[k] for k in labels if 'pseudo' not in k],
            f'TMP_{pt}',
            short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
            variation_label = pt_ranges[pt],
            x_range = [0, 1.0],
            ratio_limits = [0, 4] if 'Shared' in q else None
        )
        pu.plot_histograms(
            out_dir,
            [pu.get_unit_normalized_hist(histograms[f'{k}_TMP_{pt}_b']) for k in labels if 'pseudo' not in k],
            [labels[k] for k in labels if 'pseudo' not in k],
            f'TMP_{pt}_b',
            short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
            variation_label = pt_ranges[pt] + ' (b tracks)',
            x_range = [0, 1.0],
            ratio_limits = [0, 4] if 'Shared' in q else None
        )
        pu.plot_histograms(
            out_dir,
            [histograms[f'{k}_TMP_{pt}'] for k in labels if 'pseudo' not in k],
            [labels[k] for k in labels if 'pseudo' not in k],
            f'TMP_{pt}_no_norm',
            short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
            variation_label = pt_ranges[pt],
            x_range = [0, 1.0],
            ratio_limits = [0, 4] if 'Shared' in q else None
        )
        pu.plot_histograms(
            out_dir,
            [histograms[f'{k}_TMP_{pt}_b'] for k in labels if 'pseudo' not in k],
            [labels[k] for k in labels if 'pseudo' not in k],
            f'TMP_{pt}_b_no_norm',
            short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
            variation_label = pt_ranges[pt] + ' (b tracks)',
            x_range = [0, 1.0],
            ratio_limits = [0, 4] if 'Shared' in q else None
        )
    pu.plot_histograms(
        out_dir,
        [pu.get_unit_normalized_hist(histograms[f'{k}_n_duplicates']) for k in labels],
        labels.values(),
        'n_duplicates',
        short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
        x_range = [0, 30],
        ratio_limits = [0,5],
    )
    pu.plot_histograms(
        out_dir,
        [pu.get_unit_normalized_hist(histograms[f'{k}_n_duplicatesNoFakes']) for k in labels],
        labels.values(),
        'n_duplicatesNoFakes',
        short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
        variation_label = 'TMP #geq 0.5',
        x_range = [0, 20],
        ratio_limits = [0,5],
    )
    # fix rejection location axis labels
    for k in labels:
        for rej_loc, label in rejection_locations.items():
            histograms[f'{k}_rej_locations_from_b'].GetXaxis().SetBinLabel(rej_loc+1, str(rej_loc))
        histograms[f'{k}_rej_locations_from_b'].SetBinContent(1, 0)
        histograms[f'{k}_rej_locations_from_b'].SetBinContent(5, 0)
    pu.plot_histograms(
        out_dir,
        [pu.get_unit_normalized_hist(histograms[f'{k}_rej_locations_from_b']) for k in labels],
        labels.values(),
        'rej_locations_from_b',
        short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
        variation_label = 'B hadron tracks',
        x_range = [0,32],
        ratio_limits = [0,5],
    )
    for k in labels:
        histograms[f'{k}_2D_rej_locations_from_b'].GetYaxis().SetTitle('Entries per track')
        histograms[f'{k}_2D_rej_locations_from_b'].GetXaxis().SetRangeUser(0,32)
        histograms[f'{k}_2D_rej_locations_from_b'].GetYaxis().SetRangeUser(0,20)
        for ybin in range(1, histograms[f'{k}_2D_rej_locations_from_b'].GetNbinsY()+1):
            histograms[f'{k}_2D_rej_locations_from_b'].SetBinContent(histograms[f'{k}_2D_rej_locations_from_b'].GetBin(1, ybin), 0)
            histograms[f'{k}_2D_rej_locations_from_b'].SetBinContent(histograms[f'{k}_2D_rej_locations_from_b'].GetBin(5, ybin), 0)
        pu.plot_2D(
            out_dir,
            histograms[f'{k}_2D_rej_locations_from_b'],
            f'2D_rej_locations_from_b_{k}',
            short_title = 'Extended Z\', p_{T}^{track} > 500 MeV',
            variation_label = 'B hadron tracks',
        )
    # roi plots
    pu.plot_single_hist(
        out_dir,
        histograms['nom_nROIs'],
        'h_nROIs',
        short_title = 'Extended Z\', E_{T}^{ROI} > 150 GeV',
    )
    pu.plot_2D(
        out_dir,
        histograms['nom_2D_nJets_vs_nROIs'],
        '2D_nJets_vs_nROIs',
        short_title = 'Extended Z\', E_{T}^{ROI} > 150 GeV',
    )
    flavours = ['B', 'C', 'U']
    pu.plot_histograms(
        out_dir,
        [pu.get_unit_normalized_hist(histograms[f'nom_nROIsPerJet_jet_flav_{f}']) for f in flavours],
        [f'{f.lower()}-jets' for f in flavours],
        'h_nROIsPerJet',
        short_title = 'Extended Z\', E_{T}^{ROI} > 150 GeV',
        x_range = [0,6],
    )
    track_types = ['Reco', 'SiSP', 'Pseudo']
    pu.plot_histograms(
        out_dir,
        [histograms[f'nom_nTracksInROI_{t}'] for t in track_types],
        [f'{t.lower()} tracks' for t in track_types],
        'h_nTracksInROI',
        short_title = 'Extended Z\', E_{T}^{ROI} > 150 GeV',
        x_range = [0, 40],
        ratio_limits = [0, 2],
    )
    pu.plot_histograms(
        out_dir,
        [histograms[f'nom_nClustersInROI_{t}'] for t in track_types],
        [f'{t.lower()} tracks' for t in track_types],
        'h_nClustersInROI',
        short_title = 'Extended Z\', E_{T}^{ROI} > 150 GeV',
        ratio_limits = [0, 2],

    )
    pu.plot_single_hist(
        out_dir,
        pu.get_unit_normalized_hist(histograms['nom_nMissingPseudoClustersInROI']),
        'h_nMissingPseudoClustersInROI',
        short_title = 'Extended Z\', E_{T}^{ROI} > 150 GeV',
        x_range = [0, 25],
    )
    pu.plot_histograms(
        out_dir,
        [pu.get_unit_normalized_hist(h) for h in [histograms['nom_nIncompletePseudoTracksInROI'], histograms['nom_nIncompleteFromBPseudoTracksInROI']]],
        ['All pseudo tracks', 'Pseudo tracks FromB'],
        'h_nIncompletePseudoTracksInROI',
        short_title = 'Extended Z\', E_{T}^{ROI} > 150 GeV',
        x_range = [0, 10],
    )
