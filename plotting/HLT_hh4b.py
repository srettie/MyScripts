import os
import glob
import math
import ROOT
import plot_utils as pu

# useful abbreviations: https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Trigger/TriggerCommon/TriggerMenuMT/python/HLT/Jet/JetRecoCommon.py#0021
# etaRangeAbbrev = {
#    "j":"0eta320", # default
#    "a":"0eta490",
#    "c":"0eta240",
#    "f":"320eta490"
# }
# HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1J45p0ETA21_3J15p0ETA25
# HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1MU8F_2J15_J20
samples = {
    'user.srettie.mc21_13p6TeV.601479.PhPy8EG_HH4b_cHHH01d0.e8472_e8455_s3873_s3874_r13983_20230609_MYSTREAM' : 'SM HH4b',
    'user.srettie.mc21_13p6TeV.601480.PhPy8EG_HH4b_cHHH10d0.e8472_e8455_s3873_s3874_r13983_20230609_MYSTREAM' : '#kappa_{#lambda} = 10 HH4b',
}
HLT_decisions = [
    'HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1J45p0ETA21_3J15p0ETA25',
    'HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1MU8F_2J15_J20',
]
debug = False
verbose = False
do_fast = True

colors = {
    'HLT_j80' : ROOT.kBlack,
    'HLT_j60' : ROOT.kRed,
    '4b' : ROOT.kGray,
    '4b_sanity' : ROOT.kGreen,
    '4bmu' : ROOT.kBlack,
    '4bmu_sanity' : ROOT.kGray,
    '4bmu_loose' : ROOT.kCyan,
    '4bmu_loose_not4b' : ROOT.kCyan,
    '4bmu_not4b' : ROOT.kBlack,
    '4b_tight20'    : ROOT.kBlack,
    '4b_tight25'    : ROOT.kBlue,
    '4b_tight30'    : ROOT.kRed,
    '4b_tight35'    : ROOT.kOrange+2,
    'L14b'          : ROOT.kOrange,
    'L14b_sanity'   : ROOT.kGray,
    'L14b_tight20'  : ROOT.kCyan,
    'L14b_tight25'  : ROOT.kCyan,
    'L14b_tight30'  : ROOT.kOrange+2,
    'L14b_tight35'  : ROOT.kCyan,
    'L14bmu'        : ROOT.kOrange,
    'L14bmu_sanity' : ROOT.kOrange,
    '4b_OR_4bmu'    : ROOT.kBlack,
    '4b_OR_4bmu_loose' : ROOT.kCyan,
    '4bmu_looser'   : ROOT.kBlue,
    '4bmu_looser_not4b'    : ROOT.kBlue,
    '4b_OR_4bmu_looser' : ROOT.kBlue,
    '4bmu_tight'   : ROOT.kRed,
    '4bmu_tight_not4b'    : ROOT.kRed,
    '4b_OR_4bmu_tight' : ROOT.kRed,
    '4bmu_medium'   : ROOT.kMagenta,
    '4bmu_medium_not4b'    : ROOT.kMagenta,
    '4b_OR_4bmu_medium' : ROOT.kMagenta,
}

def get_quantity(event, container_name, quantity):
    # get an array of quantity values from container_name
    arr = getattr(event, f'{container_name}.{quantity}')
    if quantity in ['pt', 'et8', 'et6', 'et4']:
        arr = [i/1000. for i in arr]
    return arr

def get_dl1_score(pb, pc, pu, fc):
    return math.log( pb / ((pu * (1 - fc)) + (fc * pc)) )

def get_jets_tagger_scores(event, container_name, tagger_name, fc = 0.018):
    pb = get_quantity(event, container_name, f'{tagger_name}_pb')
    pc = get_quantity(event, container_name, f'{tagger_name}_pc')
    pu = get_quantity(event, container_name, f'{tagger_name}_pu')
    scores = []
    for pib, pic, piu in zip(pb, pc, pu):
        scores.append(get_dl1_score(pib, pic, piu, fc))
    return scores

def dump_event(event):
    truth_mHH = getattr(event, 'truth_mHH') / 1000.
    jetpts = get_quantity(event, 'HLTJets', 'pt')
    jetetas = get_quantity(event, 'HLTJets', 'eta')
    jetphis = get_quantity(event, 'HLTJets', 'phi')
    njets = len(jetpts)
    print(f'njets = {njets}, truth_mHH = {truth_mHH}')
    for ijet in range(njets):
        print(f'\tpt/eta/phi: {jetpts[ijet]}/{jetetas[ijet]}/{jetphis[ijet]}')

def pass_HLT_j80(event):
    jetpts = get_quantity(event, 'HLTJets', 'pt')
    for ijet in range(len(jetpts)):
        if jetpts[ijet] > 80:
            return True
    return False

def pass_HLT_j60(event):
    jetpts = get_quantity(event, 'HLTJets', 'pt')
    for ijet in range(len(jetpts)):
        if jetpts[ijet] > 60:
            return True
    return False

def get_N_jXc_020jvt(event, X):
    jetpts = get_quantity(event, 'HLTJets', 'pt')
    jetetas = get_quantity(event, 'HLTJets', 'eta')
    jetjvts = get_quantity(event, 'HLTJets', 'Jvt')
    N = 0
    if debug: print(f'\tjXc_020jvt looking at {len(jetpts)} jets')
    for i in range(len(jetpts)):
        if debug: print(f'\t\tnew jet: pt = {jetpts[i]}, eta = {jetetas[i]}, jvt = {jetjvts[i]}')
        if jetpts[i] >= X and abs(jetetas[i]) <= 2.4 and jetjvts[i] >= 0.2:
            if debug: print(f'\t\tgood jet: {i}')
            N += 1
    if debug: print(f'\t\tevent has {N} jXc_020jvt jets with pT >= {X}')
    return N

def get_N_jXc_020jvt_bdl1d77_pf_ftf(event, X):
    jetpts = get_quantity(event, 'HLTJets', 'pt')
    jetetas = get_quantity(event, 'HLTJets', 'eta')
    jetjvts = get_quantity(event, 'HLTJets', 'Jvt')
    jetdl1d = get_jets_tagger_scores(event, 'HLTJets', 'DL1d20211216')
    N = 0
    if debug: print(f'\tjXc_020jvt_bdl1d77_pf_ftf looking at {len(jetpts)} jets')
    for i in range(len(jetpts)):
        if debug: print(f'\t\tnew jet: pt = {jetpts[i]}, eta = {jetetas[i]}, jvt = {jetjvts[i]}, dl1d = {jetdl1d[i]}')
        # https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Trigger/TrigHypothesis/TrigBjetHypo/python/TrigBjetBtagHypoTool.py#0023
        if jetpts[i] >= X and abs(jetetas[i]) <= 2.4 and jetjvts[i] >= 0.2 and jetdl1d[i] > 2.157:
            if debug: print(f'\t\tgood jet: {i}')
            N += 1
    if debug: print(f'\t\tevent has {N} jXc_020jvt_bdl1d77_pf_ftf jets with pT >= {X}')
    return N

def pass_presel_X(event, X):
    jetpts = get_quantity(event, 'OfflineJets', 'pt')
    jetetas = get_quantity(event, 'OfflineJets', 'eta')
    jetdl1d = get_jets_tagger_scores(event, 'OfflineJets', 'DL1dv01')
    N_c20 = 0
    N_c20b85 = 0
    if debug: print(f'\tpresel looking at {len(jetpts)} jets')
    for i in range(len(jetpts)):
        if debug: print(f'\t\tnew jet: pt = {jetpts[i]}, eta = {jetetas[i]}, dl1d = {jetdl1d[i]}')
        # https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Trigger/TrigHypothesis/TrigBjetHypo/python/TrigBjetBtagHypoTool.py#0026
        if jetpts[i] >= X and abs(jetetas[i]) <= 2.4 and jetdl1d[i] > 0.634:
            if debug: print(f'\t\tgood jet b85: {i}')
            N_c20b85 += 1
        if jetpts[i] >= X and abs(jetetas[i]) <= 2.4:
            if debug: print(f'\t\tgood jet: {i}')
            N_c20 += 1
    decision = (N_c20 >= 4) and (N_c20b85 >= 2)
    if debug: print(f'\t\tpresel N_c20b85 = {N_c20b85}, N_c20 = {N_c20}, decision = {decision}')
    return decision

def pass_L1jets(event):
    # L1J45p0ETA21_3J15p0ETA25
    jetpts = get_quantity(event, 'LVL1Jets', 'et8')
    jetetas = get_quantity(event, 'LVL1Jets', 'eta')
    N_L1J45p0ETA21 = 0
    N_L1J15p0ETA25 = 0
    for i in range(len(jetpts)):
        if jetpts[i] > 45 and abs(jetetas[i]) <= 2.1:
            N_L1J45p0ETA21 += 1
        if jetpts[i] > 15 and abs(jetetas[i]) <= 2.5:
            N_L1J15p0ETA25 += 1
    return (N_L1J45p0ETA21 > 0) and (N_L1J15p0ETA25 >= 3)

def pass_L14bmu_sanity(event):
    # L1MU8F_2J15_J20
    # "F" : require Full-station (three-station) big-wheel coincidence for endcap
    # https://twiki.cern.ch/twiki/bin/view/Atlas/TriggerNamingRun3
    jetpts = get_quantity(event, 'LVL1Jets', 'et8')
    muonpts = get_quantity(event, 'LVL1Muons', 'pt')
    N_L1J15 = 0
    N_L1J20 = 0
    N_L1MU8F = 0
    for i in range(len(jetpts)):
        if jetpts[i] > 15:
            N_L1J15 += 1
        if jetpts[i] > 20:
            N_L1J20 += 1
    for i in range(len(muonpts)):
        if muonpts[i] > 8:
            N_L1MU8F += 1    
    return (N_L1MU8F > 0) and (N_L1J15 >= 2) and (N_L1J20 > 0)

def pass_L1jets_tight20(event):
    jetpts = get_quantity(event, 'LVL1Jets', 'et8')
    jetetas = get_quantity(event, 'LVL1Jets', 'eta')
    N_L1J45p0ETA21 = 0
    N_L1J20p0ETA25 = 0
    N_L1J15p0ETA25 = 0
    for i in range(len(jetpts)):
        if jetpts[i] > 45 and abs(jetetas[i]) <= 2.1:
            N_L1J45p0ETA21 += 1
        if jetpts[i] > 20 and abs(jetetas[i]) <= 2.5:
            N_L1J20p0ETA25 += 1
        if jetpts[i] > 15 and abs(jetetas[i]) <= 2.5:
            N_L1J15p0ETA25 += 1
    return (N_L1J45p0ETA21 > 0) and (N_L1J20p0ETA25 >= 2) and (N_L1J15p0ETA25 >= 3)

def pass_L1jets_tight25(event):
    jetpts = get_quantity(event, 'LVL1Jets', 'et8')
    jetetas = get_quantity(event, 'LVL1Jets', 'eta')
    N_L1J45p0ETA21 = 0
    N_L1J25p0ETA25 = 0
    N_L1J15p0ETA25 = 0
    for i in range(len(jetpts)):
        if jetpts[i] > 45 and abs(jetetas[i]) <= 2.1:
            N_L1J45p0ETA21 += 1
        if jetpts[i] > 25 and abs(jetetas[i]) <= 2.5:
            N_L1J25p0ETA25 += 1
        if jetpts[i] > 15 and abs(jetetas[i]) <= 2.5:
            N_L1J15p0ETA25 += 1
    return (N_L1J45p0ETA21 > 0) and (N_L1J25p0ETA25 >= 2) and (N_L1J15p0ETA25 >= 3)

def pass_L1jets_tight30(event):
    jetpts = get_quantity(event, 'LVL1Jets', 'et8')
    jetetas = get_quantity(event, 'LVL1Jets', 'eta')
    N_L1J45p0ETA21 = 0
    N_L1J30p0ETA25 = 0
    N_L1J15p0ETA25 = 0
    for i in range(len(jetpts)):
        if jetpts[i] > 45 and abs(jetetas[i]) <= 2.1:
            N_L1J45p0ETA21 += 1
        if jetpts[i] > 30 and abs(jetetas[i]) <= 2.5:
            N_L1J30p0ETA25 += 1
        if jetpts[i] > 15 and abs(jetetas[i]) <= 2.5:
            N_L1J15p0ETA25 += 1
    return (N_L1J45p0ETA21 > 0) and (N_L1J30p0ETA25 >= 2) and (N_L1J15p0ETA25 >= 3)

def pass_L1jets_tight35(event):
    jetpts = get_quantity(event, 'LVL1Jets', 'et8')
    jetetas = get_quantity(event, 'LVL1Jets', 'eta')
    N_L1J45p0ETA21 = 0
    N_L1J35p0ETA25 = 0
    N_L1J15p0ETA25 = 0
    for i in range(len(jetpts)):
        if jetpts[i] > 45 and abs(jetetas[i]) <= 2.1:
            N_L1J45p0ETA21 += 1
        if jetpts[i] > 35 and abs(jetetas[i]) <= 2.5:
            N_L1J35p0ETA25 += 1
        if jetpts[i] > 15 and abs(jetetas[i]) <= 2.5:
            N_L1J15p0ETA25 += 1
    return (N_L1J45p0ETA21 > 0) and (N_L1J35p0ETA25 >= 2) and (N_L1J15p0ETA25 >= 3)

def pass_4b_base_sanity(event):
    # HLT
    # j20c_020jvt
    if get_N_jXc_020jvt(event, 20) < 4:
        if debug: print('failed 20')
        return False
    # j28c_020jvt
    if get_N_jXc_020jvt(event, 28) < 3:
        if debug: print('failed 28')
        return False
    # j55c_020jvt
    if get_N_jXc_020jvt(event, 55) < 2:
        if debug: print('failed 55')
        return False
    # j80c_020jvt
    if get_N_jXc_020jvt(event, 80) < 1:
        if debug: print('failed 80')
        return False
    # SHARED (i.e. "use the jets to the left to try the selection on the right")
    # 2j20c_020jvt_bdl1d77_pf_ftf
    if get_N_jXc_020jvt_bdl1d77_pf_ftf(event, 20) < 2:
        # TODO: check that the jets passing here are the same as the ones passing the above...
        if debug: print('failed btag')
        return False
    # presel2c20XX2c20b85 ('XX' is just a separator)
    if not pass_presel_X(event, 20):
        if debug: print('failed presel')
        return False
    return True

def pass_4b_HLT_loose(event):
    # HLT
    # j20c_020jvt
    if get_N_jXc_020jvt(event, 20) < 4:
        if debug: print('failed 20')
        return False
    # j28c_020jvt
    if get_N_jXc_020jvt(event, 28) < 2:
        if debug: print('failed 28')
        return False
    # j55c_020jvt
    if get_N_jXc_020jvt(event, 55) < 1:
        if debug: print('failed 55')
        return False
    # j80c_020jvt
    # first try removing 4th jet requirement
    # if get_N_jXc_020jvt(event, 60) < 1:
    #     if debug: print('failed 60')
    #     return False
    # SHARED (i.e. "use the jets to the left to try the selection on the right")
    # 2j20c_020jvt_bdl1d77_pf_ftf
    if get_N_jXc_020jvt_bdl1d77_pf_ftf(event, 20) < 2:
        # TODO: check that the jets passing here are the same as the ones passing the above...
        if debug: print('failed btag')
        return False
    # presel2c20XX2c20b85 ('XX' is just a separator)
    if not pass_presel_X(event, 20):
        if debug: print('failed presel')
        return False
    return True

def pass_4b_HLT_looser(event):
    # HLT
    # j20c_020jvt
    if get_N_jXc_020jvt(event, 20) < 4:
        if debug: print('failed 20')
        return False
    # j28c_020jvt
    if get_N_jXc_020jvt(event, 28) < 2:
        if debug: print('failed 28')
        return False
    # j55c_020jvt
    if get_N_jXc_020jvt(event, 40) < 1:
        if debug: print('failed 40')
        return False
    # j80c_020jvt
    # first try removing 4th jet requirement
    # if get_N_jXc_020jvt(event, 60) < 1:
    #     if debug: print('failed 60')
    #     return False
    # SHARED (i.e. "use the jets to the left to try the selection on the right")
    # 2j20c_020jvt_bdl1d77_pf_ftf
    if get_N_jXc_020jvt_bdl1d77_pf_ftf(event, 20) < 2:
        # TODO: check that the jets passing here are the same as the ones passing the above...
        if debug: print('failed btag')
        return False
    # presel2c20XX2c20b85 ('XX' is just a separator)
    if not pass_presel_X(event, 20):
        if debug: print('failed presel')
        return False
    return True

def pass_4b_HLT_tight(event):
    # HLT
    # j20c_020jvt
    if get_N_jXc_020jvt(event, 20) < 4:
        if debug: print('failed 20')
        return False
    # j28c_020jvt
    if get_N_jXc_020jvt(event, 40) < 2:
        if debug: print('failed 40')
        return False
    # j55c_020jvt
    if get_N_jXc_020jvt(event, 55) < 1:
        if debug: print('failed 55')
        return False
    # j80c_020jvt
    # first try removing 4th jet requirement
    # if get_N_jXc_020jvt(event, 60) < 1:
    #     if debug: print('failed 60')
    #     return False
    # SHARED (i.e. "use the jets to the left to try the selection on the right")
    # 2j20c_020jvt_bdl1d77_pf_ftf
    if get_N_jXc_020jvt_bdl1d77_pf_ftf(event, 20) < 2:
        # TODO: check that the jets passing here are the same as the ones passing the above...
        if debug: print('failed btag')
        return False
    # presel2c20XX2c20b85 ('XX' is just a separator)
    if not pass_presel_X(event, 20):
        if debug: print('failed presel')
        return False
    return True

def pass_4b_HLT_medium(event):
    # HLT
    # j20c_020jvt
    if get_N_jXc_020jvt(event, 20) < 4:
        if debug: print('failed 20')
        return False
    # j28c_020jvt
    if get_N_jXc_020jvt(event, 40) < 2:
        if debug: print('failed 40')
        return False
    # j80c_020jvt
    # first try removing 4th jet requirement
    # if get_N_jXc_020jvt(event, 60) < 1:
    #     if debug: print('failed 60')
    #     return False
    # SHARED (i.e. "use the jets to the left to try the selection on the right")
    # 2j20c_020jvt_bdl1d77_pf_ftf
    if get_N_jXc_020jvt_bdl1d77_pf_ftf(event, 20) < 2:
        # TODO: check that the jets passing here are the same as the ones passing the above...
        if debug: print('failed btag')
        return False
    # presel2c20XX2c20b85 ('XX' is just a separator)
    if not pass_presel_X(event, 20):
        if debug: print('failed presel')
        return False
    return True

def pass_4b_sanity(event):
    # rebuild 4b decision as a sanity check
    if not pass_4b_base_sanity(event):
        if debug: print('failed base from 4b')
        return False
    # L1J45p0ETA21_3J15p0ETA25 ('p' is just a separator)
    if not pass_L1jets(event):
        if debug: print('failed L1jets')
        return False
    return True

def pass_4b_tight20(event):
    # build 4b decision with tighter L1
    if not pass_4b_base_sanity(event):
        if debug: print('failed base from 4b')
        return False
    if not pass_L1jets_tight20(event):
        if debug: print('failed L1jets')
        return False
    return True

def pass_4b_tight25(event):
    # build 4b decision with tighter L1
    if not pass_4b_base_sanity(event):
        if debug: print('failed base from 4b')
        return False
    if not pass_L1jets_tight25(event):
        if debug: print('failed L1jets')
        return False
    return True

def pass_4b_tight30(event):
    # build 4b decision with tighter L1
    if not pass_4b_base_sanity(event):
        if debug: print('failed base from 4b')
        return False
    if not pass_L1jets_tight30(event):
        if debug: print('failed L1jets')
        return False
    return True

def pass_4b_tight35(event):
    # build 4b decision with tighter L1
    if not pass_4b_base_sanity(event):
        if debug: print('failed base from 4b')
        return False
    if not pass_L1jets_tight35(event):
        if debug: print('failed L1jets')
        return False
    return True

def pass_4bmu_sanity(event):
    if not pass_4b_base_sanity(event):
        if debug: print('failed base from 4b')
        return False
    if not pass_L14bmu_decision(event):
        if debug: print('failed L1muons')
        return False        
    return True

def pass_4bmu_loose(event):
    if not pass_4b_HLT_loose(event):
        if debug: print('failed HLT loose from 4b')
        return False
    if not pass_L14bmu_decision(event):
        if debug: print('failed L1muons')
        return False        
    return True

def pass_4bmu_looser(event):
    if not pass_4b_HLT_looser(event):
        if debug: print('failed HLT looser from 4b')
        return False
    if not pass_L14bmu_decision(event):
        if debug: print('failed L1muons')
        return False        
    return True

def pass_4bmu_tight(event):
    if not pass_4b_HLT_tight(event):
        if debug: print('failed HLT tight from 4b')
        return False
    if not pass_L14bmu_decision(event):
        if debug: print('failed L1muons')
        return False        
    return True

def pass_4bmu_medium(event):
    if not pass_4b_HLT_medium(event):
        if debug: print('failed HLT medium from 4b')
        return False
    if not pass_L14bmu_decision(event):
        if debug: print('failed L1muons')
        return False        
    return True

def pass_L14b_sanity(event):
    # rebuild L1 decision as a sanity check
    # L1J45p0ETA21_3J15p0ETA25 ('p' is just a separator)
    if not pass_L1jets(event):
        if debug: print('failed L1jets')
        return False
    return True

def pass_4b_decision(event):
    decision = getattr(event, 'HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1J45p0ETA21_3J15p0ETA25')
    return bool(decision)

def pass_4bmu_decision(event):
    decision = getattr(event, 'HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1MU8F_2J15_J20')
    return bool(decision)

def pass_L14b_decision(event):
    decision = getattr(event, 'L1_J45p0ETA21_3J15p0ETA25')
    return bool(decision)

def pass_L14bmu_decision(event):
    decision = getattr(event, 'L1_MU8F_2J15_J20')
    return bool(decision)

def pass_4bmu_not4b(event):
    return (pass_4bmu_decision(event) and not pass_4b_decision(event))

def pass_4bmu_loose_not4b(event):
    return (pass_4bmu_loose(event) and not pass_4b_decision(event))

def pass_4bmu_looser_not4b(event):
    return (pass_4bmu_looser(event) and not pass_4b_decision(event))

def pass_4bmu_tight_not4b(event):
    return (pass_4bmu_tight(event) and not pass_4b_decision(event))

def pass_4bmu_medium_not4b(event):
    return (pass_4bmu_medium(event) and not pass_4b_decision(event))

def pass_4b_OR_4bmu(event):
    return (pass_4bmu_decision(event) or pass_4b_decision(event))

def pass_4b_OR_4bmu_loose(event):
    return (pass_4bmu_loose(event) or pass_4b_decision(event))

def pass_4b_OR_4bmu_looser(event):
    return (pass_4bmu_looser(event) or pass_4b_decision(event))

def pass_4b_OR_4bmu_tight(event):
    return (pass_4bmu_tight(event) or pass_4b_decision(event))

def pass_4b_OR_4bmu_medium(event):
    return (pass_4bmu_medium(event) or pass_4b_decision(event))

def get_trigger_histograms(triggers, t, sample_short):
    histos, efficiencies = {}, {}
    # try getting histograms if they exist already
    if os.path.exists(histogram_file) and do_fast:
        print(f'using existing histogram file: {histogram_file}')
        histfile = ROOT.TFile(histogram_file, 'READ')
        for trigger in triggers:
            histos[f'h_{sample_short}_{trigger}_pass'] = histfile.Get(f'h_{sample_short}_{trigger}_pass').Clone(f'h_{sample_short}_{trigger}_pass')
            histos[f'h_{sample_short}_{trigger}_all'] = histfile.Get(f'h_{sample_short}_{trigger}_all').Clone(f'h_{sample_short}_{trigger}_all')
            efficiencies[f'eff_{sample_short}_{trigger}'] = histfile.Get(f'eff_{sample_short}_{trigger}').Clone(f'eff_{sample_short}_{trigger}')
            histos[f'h_{sample_short}_{trigger}_pass'].SetDirectory(0)
            histos[f'h_{sample_short}_{trigger}_all'].SetDirectory(0)
            efficiencies[f'eff_{sample_short}_{trigger}'].SetDirectory(0)
        return histos, efficiencies
    # fill the trigger histograms from ntuples
    mHH_pass = { trig : [] for trig in triggers }
    mHH_all = { trig : [] for trig in triggers }
    for i in range(nevents):
        if i % 10000 == 0 or debug:
            print(f'processing event {i}')
        t.GetEntry(i)
        truth_mHH = getattr(t, 'truth_mHH') / 1000.
        if verbose:
            dump_event(t)
        for hlt in triggers:
            if triggers[hlt](t):
                if debug:
                    print(f'passed {hlt}')
                mHH_pass[hlt].append(truth_mHH)
                mHH_all[hlt].append(truth_mHH)
            else:
                if debug:
                    print(f'failed {hlt}')
                mHH_all[hlt].append(truth_mHH)
    # fill efficiency histograms
    nbins, mHH_min, mHH_max = 50, 0, 1000
    for trigger in triggers:
        histos[f'h_{sample_short}_{trigger}_pass'] = ROOT.TH1D(f'h_{sample_short}_{trigger}_pass', f'h_{sample_short}_{trigger}_pass;mHH [GeV];Trigger Efficiency', nbins, mHH_min, mHH_max)
        histos[f'h_{sample_short}_{trigger}_all'] = ROOT.TH1D(f'h_{sample_short}_{trigger}_all', f'h_{sample_short}_{trigger}_all;mHH [GeV];Trigger Efficiency', nbins, mHH_min, mHH_max)
        for mHH in mHH_pass[trigger]:
            histos[f'h_{sample_short}_{trigger}_pass'].Fill(mHH)
        for mHH in mHH_all[trigger]:
            histos[f'h_{sample_short}_{trigger}_all'].Fill(mHH)
        efficiencies[f'eff_{sample_short}_{trigger}'] = ROOT.TEfficiency(histos[f'h_{sample_short}_{trigger}_pass'], histos[f'h_{sample_short}_{trigger}_all'])
        efficiencies[f'eff_{sample_short}_{trigger}'].SetLineColor(colors[trigger])
    # save histograms to file
    histfile = ROOT.TFile(histogram_file, 'RECREATE')
    histfile.cd()
    for trigger in triggers:
        histos[f'h_{sample_short}_{trigger}_pass'].Write(f'h_{sample_short}_{trigger}_pass')
        histos[f'h_{sample_short}_{trigger}_all'].Write(f'h_{sample_short}_{trigger}_all')
        efficiencies[f'eff_{sample_short}_{trigger}'].Write(f'eff_{sample_short}_{trigger}')
    return histos, efficiencies

if __name__ == '__main__':
    out_dir = '/Users/sebastienrettie/trigger/plots/'
    pu.set_ATLAS_style()
    for sample in samples:
        sample_short = '.'.join(sample.split('.')[3:5])
        histogram_file = f'/Users/sebastienrettie/trigger/plots/histograms_{sample_short}.root'
        fnames = glob.glob(f'/Users/sebastienrettie/trigger/ntuples/{sample}/*.root')
        tname = 'myTree'
        t = ROOT.TChain(tname)
        for fname in fnames:
            t.Add(fname)
        nevents = t.GetEntries()
        if debug: print(f'nevents = {nevents}')
        trigger_selections = {
            'HLT_j80'    : pass_HLT_j80,
            'HLT_j60'    : pass_HLT_j60,
            # L1
            'L14b'          : pass_L14b_decision,
            'L14b_sanity'   : pass_L14b_sanity,
            'L14bmu'        : pass_L14bmu_decision,
            'L14bmu_sanity' : pass_L14bmu_sanity,
            # full chains
            '4b'            : pass_4b_decision,
            '4b_sanity'     : pass_4b_sanity,
            '4bmu'          : pass_4bmu_decision,
            '4bmu_sanity'   : pass_4bmu_sanity,
            '4bmu_not4b'    : pass_4bmu_not4b,
            '4b_OR_4bmu'    : pass_4b_OR_4bmu,
            # for now, just remove 4th jet requirement
            '4bmu_loose'    : pass_4bmu_loose,
            '4bmu_loose_not4b'    : pass_4bmu_loose_not4b,
            '4b_OR_4bmu_loose' : pass_4b_OR_4bmu_loose,
            # now loosen the subleading jet as well
            '4bmu_looser'   : pass_4bmu_looser,
            '4bmu_looser_not4b'    : pass_4bmu_looser_not4b,
            '4b_OR_4bmu_looser' : pass_4b_OR_4bmu_looser,
            # more variations
            '4bmu_tight'   : pass_4bmu_tight,
            '4bmu_tight_not4b'    : pass_4bmu_tight_not4b,
            '4b_OR_4bmu_tight' : pass_4b_OR_4bmu_tight,
            '4bmu_medium'   : pass_4bmu_medium,
            '4bmu_medium_not4b'    : pass_4bmu_medium_not4b,
            '4b_OR_4bmu_medium' : pass_4b_OR_4bmu_medium,
            # for tightening studies
            '4b_tight20'    : pass_4b_tight20,
            '4b_tight25'    : pass_4b_tight25,
            '4b_tight30'    : pass_4b_tight30,
            '4b_tight35'    : pass_4b_tight35,
            'L14b_tight20'  : pass_L1jets_tight20,
            'L14b_tight25'  : pass_L1jets_tight25,
            'L14b_tight30'  : pass_L1jets_tight30,
            'L14b_tight35'  : pass_L1jets_tight35,
        }
        
        hists, efficiencies = get_trigger_histograms(trigger_selections, t, sample_short)
        for trigger in trigger_selections:
            # set colors
            hists[f'h_{sample_short}_{trigger}_pass'].SetLineColor(colors[trigger])
            hists[f'h_{sample_short}_{trigger}_all'].SetLineColor(colors[trigger])
            efficiencies[f'eff_{sample_short}_{trigger}'].SetLineColor(colors[trigger])
            efficiencies[f'eff_{sample_short}_{trigger}'].SetMarkerSize(0.)
            if 'not4b' in trigger or '_OR_' in trigger:
                efficiencies[f'eff_{sample_short}_{trigger}'].SetLineStyle(2)
            # choose what to actually plot
            pu.plot_single_hist(out_dir, hists[f'h_{sample_short}_{trigger}_pass'], f'{trigger}_pass', f'{trigger}_pass')
            pu.plot_single_hist(out_dir, hists[f'h_{sample_short}_{trigger}_all'], f'{trigger}_all', f'{trigger}_all')
        triggers_to_compare = [
            'HLT_j80',
            'HLT_j60',
            # L1
            'L14b',
            'L14b_sanity',
            'L14bmu',
            'L14bmu_sanity',
            # full chains
            '4bmu',
            '4b',
            '4b_sanity',
            '4bmu_sanity',
            '4bmu_not4b',
            # for now, just remove 4th jet requirement
            '4bmu_loose',
            '4bmu_loose_not4b',
            # for tightening studies
            '4b_tight20',
            '4b_tight25',
            '4b_tight30',
            '4b_tight35',
            'L14b_tight20',
            'L14b_tight25',
            'L14b_tight30',
            'L14b_tight35',
        ]
        triggers_to_compare_tightenL1 = [
            '4b',
            '4b_sanity',
            '4b_tight20',
            '4b_tight25',
            '4b_tight30',
            '4b_tight35',
        ]
        triggers_to_compare_muon = [
            '4b',
            '4b_sanity',
            '4bmu',
            '4bmu_not4b',
            '4bmu_loose',
            '4bmu_loose_not4b',
            '4bmu_looser',
            '4bmu_looser_not4b',
            '4bmu_tight',
            '4bmu_tight_not4b',
            '4bmu_medium',
            '4bmu_medium_not4b',
        ]
        triggers_to_compare_muon_OR = [
            '4b',
            '4b_sanity',
            '4bmu',
            '4b_OR_4bmu',
            '4bmu_loose',
            '4b_OR_4bmu_loose',
            '4bmu_looser',
            '4b_OR_4bmu_looser',
            '4bmu_tight',
            '4b_OR_4bmu_tight',
            '4bmu_medium',
            '4b_OR_4bmu_medium',
        ]
        pu.plot_efficiencies(out_dir, [efficiencies[f'eff_{sample_short}_{trigger}'] for trigger in triggers_to_compare_tightenL1], triggers_to_compare_tightenL1, f'HLT_comparison_{sample_short}_tightenL1', short_title=samples[sample])
        pu.plot_efficiencies(out_dir, [efficiencies[f'eff_{sample_short}_{trigger}'] for trigger in triggers_to_compare_muon], triggers_to_compare_muon, f'HLT_comparison_{sample_short}_muon', short_title=samples[sample])
        pu.plot_efficiencies(out_dir, [efficiencies[f'eff_{sample_short}_{trigger}'] for trigger in triggers_to_compare_muon_OR], triggers_to_compare_muon_OR, f'HLT_comparison_{sample_short}_muon_OR', short_title=samples[sample])
