import os
import sys
import glob
import ROOT
import plot_utils

debug = True

# Reader outputs
trees_path = '/Users/sebastien/1LMedPtVStudies/reader_outputs/run/Reader_1L_33-05_*_Resolved_D_D/fetch/data-MVATree/*.root'
tree_name = 'Nominal'

# Output directory
out_dir = '/Users/sebastien/1LMedPtVStudies/CROptimization/'

# Signal samples:
signal = [
    'qqZvvHbbJ_PwPy8MINLO',
    'qqZllHbbJ_PwPy8MINLO',
    'qqWlvHbbJ_PwPy8MINLO',
    'ggZvvHbb_PwPy8',
    'ggZllHbb_PwPy8'
]

# Diboson samples:
diboson = [
    'WqqZvv_Sh221',
    'WqqZll_Sh221',
    'WlvZqq_Sh221',
    'ZqqZvv_Sh221',
    'ZqqZll_Sh221',
    'ggZqqZvv_Sh222',
    'ggZqqZll_Sh222',
    'WqqWlv_Sh221',
    'ggWqqWlv_Sh222'
]

# Various keys used for fitting
fit_keys = [
    'signal_2J',
    'signal_3J',
    'diboson_2J',
    'diboson_3J'
]
pTV_regions = ['75pTV150','150pTV250','250pTVinf']
pTV_region_titles = {
    '75pTV150'  : '$75~\GeV < \pTV < 150~\GeV$',
    '150pTV250' : '$150~\GeV < \pTV < 250~\GeV$',
    '250pTVinf' : '$\pTV > 250~\GeV$'
}
# 2D dRBB vs pTV distributions
h2 = {}
# 1D quantile distributions
quantiles = {}
# Quantile fits
fits = {}
ref_fits = {}
ref_fits_params = {
    'signal_2J' : [8.70286e-01, 1.37867e+00, -7.95322e-03],
    'signal_3J' : [7.63283e-01, 1.33179e+00, -7.30396e-03],
    'diboson_2J' : [3.99879e-01, 7.88279e-01, -1.02303e-02],
    'diboson_3J' : [4.20840e-01, 2.67742e-01, -8.08892e-03]
}
# Quantile fractions
qfrac = {
    'signal_2J' : 0.95,
    'signal_3J' : 0.85,
    'diboson_2J' : 0.1,
    'diboson_3J' : 0.1
}
# Yields for tables
yields = {}

# Binning
pTV_nbins = 250
pTV_min = 0
pTV_max = 500
dRBB_nbins = 250
dRBB_min = 0
dRBB_max = 5
# 150-485, 75-485, 75-150
fit_min, fit_max = 75, 485
draw_min, draw_max = 75, 500
h2_skeleton = ROOT.TH2D('h2_skeleton', 'h2_skeleton', pTV_nbins, pTV_min, pTV_max, dRBB_nbins, dRBB_min, dRBB_max)

# Selection
sel_2J = ROOT.TCut('nJets == 2')
sel_3J = ROOT.TCut('nJets == 3')
sel_75pTV150 = ROOT.TCut('pTV > 75 && pTV < 150')
sel_150pTV250 = ROOT.TCut('pTV > 150 && pTV < 250')
sel_250pTVinf = ROOT.TCut('pTV > 250')
region_cuts = {
    '2J_inclusive' : sel_2J,
    '2J_75pTV150'  : sel_2J + sel_75pTV150,
    '2J_150pTV250' : sel_2J + sel_150pTV250,
    '2J_250pTVinf' : sel_2J + sel_250pTVinf,
    '3J_inclusive' : sel_3J,
    '3J_75pTV150'  : sel_3J + sel_75pTV150,
    '3J_150pTV250' : sel_3J + sel_150pTV250,
    '3J_250pTVinf' : sel_3J + sel_250pTVinf,
}

# Aesthetics
table_titles = {
    'signal_2J' : '2-jet',
    'signal_3J' : '3-jet',
    'diboson_2J' : '2-jet',
    'diboson_3J' : '3-jet'    
}
caption_base = 'Cuts defining the {} $\Delta R$ control region. Events with this region are removed from the signal region, such that the two regions are fully orthogonal'
yields_caption = 'Split of $VH$ signal events into signal and control regions for each \pTV and number of jets category. Numbers are given as \% of total signal events in that given analysis category.'
fit_range_caption = '(Fit range: ${}~\GeV < \pTV < {}~\GeV$) '.format(fit_min, fit_max)
table_captions = {
    'CRLow' : fit_range_caption + caption_base.format('low'),
    'CRHigh' : fit_range_caption + caption_base.format('high'),
    'CRLow_ref' : '(Moriond 2020 Result) ' + caption_base.format('low'),
    'CRHigh_ref' : '(Moriond 2020 Result) ' + caption_base.format('high'),
    'yields' : fit_range_caption + yields_caption,
    'yields_ref' : '(Moriond 2020 Result) ' + yields_caption
}

def get_tree(files, samples, t_name):
    t = ROOT.TChain(t_name)
    for f in files:
        if any(s in f for s in samples):
            if debug: print('Adding file to tree: {}'.format(f))
            t.Add(f)
    print('Got {} events for tree {}'.format(t.GetEntries(), t_name))
    return t

def get_2d_histogram(t, x, y, selection, h_name):
    h2 = h2_skeleton.Clone(h_name)
    h2.GetXaxis().SetTitle(x)
    h2.GetYaxis().SetTitle(y)
    draw_cmd = '{}:{}>>{}'.format(y, x, h_name)
    print('Drawing: {}'.format(draw_cmd))
    nevts = t.Draw(draw_cmd, selection, 'colz')
    print('Drew {} events for {}'.format(nevts, h_name))
    plot_utils.plot_2D(out_dir, h2, h_name, short_title = h_name, variation_label = '')
    return h2

def get_analysis_region_cut(base_reg, ana_reg, use_ref_fits = False):
    nJet = '2J' if '2J' in base_reg else '3J'
    # Get relevant functions
    lower_fit = ref_fits['diboson_'+nJet] if use_ref_fits else fits['diboson_'+nJet]
    upper_fit = ref_fits['signal_'+nJet] if use_ref_fits else fits['signal_'+nJet]
    # Get cuts
    lower_cut = 'dRBB {} {} + TMath::Exp({}+{}*pTV)'.format('>' if 'SR' in ana_reg else '<', lower_fit.GetParameter(0), lower_fit.GetParameter(1), lower_fit.GetParameter(2))
    upper_cut = 'dRBB {} {} + TMath::Exp({}+{}*pTV)'.format('<' if 'SR' in ana_reg else '>', upper_fit.GetParameter(0), upper_fit.GetParameter(1), upper_fit.GetParameter(2))
    # Return base cut and function cut
    if 'SR' in ana_reg:
        return region_cuts[base_reg] + ROOT.TCut(lower_cut) + ROOT.TCut(upper_cut)
    elif 'CRLow' in ana_reg:
        return region_cuts[base_reg] + ROOT.TCut(lower_cut)
    elif 'CRHigh' in ana_reg:
        return region_cuts[base_reg] + ROOT.TCut(upper_cut)
    else:
        raise Exception('Unknown analysis region: {}'.format(ana_reg))

def get_region_yield(t, selection):
    print('Selection: {}'.format(selection))
    nevts = t.Draw('dRBB:pTV', selection, 'colz')
    print('Obtained {} events!'.format(nevts))
    return nevts

def get_document_start():
    doc_start = r"""
    \documentclass{article}
    \usepackage{xspace}
    \usepackage{amsmath}
    \usepackage{multicol}
    \usepackage{geometry}
    \usepackage{graphics}
    \newcommand*{\pTV}{\ensuremath{p_{\text{T}}^{\text{V}}}\xspace}
    \newcommand*{\GeV}{\ensuremath{\text{Ge\kern -0.1em V}}\xspace}
    \begin{document}
    """
    return doc_start

def get_document_end():
    doc_end = r"""
    \end{document}
    """
    return doc_end

def get_fit_table_start():
    tab_start = r"""
    \begin{table}
    \begin{center}
    \begin{tabular}{ c | c }
    \hline\hline
    Category & Cut \\
    \hline
    """
    return tab_start

def get_yield_table_start():
    tab_start = r"""
    \begin{table}
    \begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{ c | ccc | ccc}
    \hline\hline
    \multicolumn{7}{c}{1 Lepton VH Signal}\\
    \hline
     & \multicolumn{3}{c|}{2-jet} & \multicolumn{3}{c}{3-jet} \\
     & Low $\Delta R$ CR & SR & High $\Delta R$ CR & Low $\Delta R$ CR & SR & High $\Delta R$ CR \\
    """
    return tab_start

def get_table_end(CR = ''):
    tab_end = r"""
    \hline\hline
    \end{tabular}
    """
    if 'yields' in CR:
        tab_end += '}\n'
    tab_end += r"""\caption{"""
    tab_end += table_captions[CR] if CR in table_captions else ''
    tab_end += r"""}"""
    tab_end += r"""
    \end{center}
    \end{table}
    """
    return tab_end

def get_fit_table_line(fit_key, fit):
    line = table_titles[fit_key] + ' & $\Delta R '
    line += ' > ' if 'signal' in fit_key else ' < '
    line += ' {:.2f} '.format(fit.GetParameter(0))
    line += r' + e^{'
    line += '{:.3f} {:.5f}'.format(fit.GetParameter(1), fit.GetParameter(2))
    line += r' \times \pTV}'
    line += r'$ \\'
    return line

def get_fit_table(CR):
    print('Making table: {}\n'.format(CR))
    tab = get_fit_table_start()
    if 'CRHigh' in CR:
        fit = ref_fits['signal_2J'] if 'ref' in CR else fits['signal_2J']
        tab += get_fit_table_line('signal_2J', fit) + '\n'
        fit = ref_fits['signal_3J'] if 'ref' in CR else fits['signal_3J']
        tab += get_fit_table_line('signal_3J', fit) + '\n'
    elif 'CRLow' in CR:
        fit = ref_fits['diboson_2J'] if 'ref' in CR else fits['diboson_2J']
        tab += get_fit_table_line('diboson_2J', fit) + '\n'
        fit = ref_fits['diboson_3J'] if 'ref' in CR else fits['diboson_3J']
        tab += get_fit_table_line('diboson_3J', fit) + '\n'
    else:
        raise Exception('Unrecognized CR: {}'.format(CR))
    tab += get_table_end(CR)
    
    return tab

def get_yield_table_line(pTV_region, use_ref_fits = False):
    line = pTV_region_titles[pTV_region]
    line += ' & {:.2f}\%'.format(100*yields['2J_{}_CRLow{}'.format(pTV_region, '_ref' if use_ref_fits else '')]/yields['2J_{}'.format(pTV_region)])
    line += ' & {:.2f}\%'.format(100*yields['2J_{}_SR{}'.format(pTV_region, '_ref' if use_ref_fits else '')]/yields['2J_{}'.format(pTV_region)])
    line += ' & {:.2f}\%'.format(100*yields['2J_{}_CRHigh{}'.format(pTV_region, '_ref' if use_ref_fits else '')]/yields['2J_{}'.format(pTV_region)])
    line += ' & {:.2f}\%'.format(100*yields['3J_{}_CRLow{}'.format(pTV_region, '_ref' if use_ref_fits else '')]/yields['3J_{}'.format(pTV_region)])
    line += ' & {:.2f}\%'.format(100*yields['3J_{}_SR{}'.format(pTV_region, '_ref' if use_ref_fits else '')]/yields['3J_{}'.format(pTV_region)])
    line += ' & {:.2f}\%'.format(100*yields['3J_{}_CRHigh{}'.format(pTV_region, '_ref' if use_ref_fits else '')]/yields['3J_{}'.format(pTV_region)])
    line += r' \\'
    # line += '\n'
    return line

def get_yield_table(use_ref_fits = False):
    tab = get_yield_table_start()
    for pTV_region in pTV_regions:
        tab += get_yield_table_line(pTV_region, use_ref_fits) + '\n'
    if use_ref_fits:
        tab += get_table_end('yields_ref')
    else:
        tab += get_table_end('yields')        
    return tab

def make_tables():
    tables = get_document_start()

    # Fit tables
    tables += get_fit_table('CRLow')
    tables += get_fit_table('CRHigh')
    tables += get_fit_table('CRLow_ref')
    tables += get_fit_table('CRHigh_ref')
    # Yield tables
    tables += get_yield_table()
    tables += get_yield_table(True)
    
    tables += get_document_end()
    # Print for convenience
    print(tables)
    # Save to file
    with open(out_dir+'/tables/all.tex', 'w') as f:
        f.write(tables)

def make_individual_tables():
    for tname in ['CRLow','CRHigh','CRLow_ref','CRHigh_ref','yields','yields_ref']:
        tab = get_document_start()
        if 'yields' in tname:
            tab += get_yield_table('ref' in tname)
        else:
            tab += get_fit_table(tname)
        tab += get_document_end()    
        with open(out_dir+'/tables/{}.tex'.format(tname), 'w') as f:
            f.write(tab)
        print(tab)
    
if __name__ == '__main__':
        
    out_dir += '/fit_range_{}pTV{}/'.format(fit_min, fit_max)
    if not os.path.exists(out_dir): os.mkdir(out_dir)

    # Get relevant trees
    reader_files = glob.glob(trees_path)
    t_signal = get_tree(reader_files, signal, tree_name)
    t_diboson = get_tree(reader_files, diboson, tree_name)
    
    print('Obtained {} signal events and {} diboson events'.format(t_signal.GetEntries(), t_diboson.GetEntries()))

    cnv = ROOT.TCanvas()
    cnv.cd()
    cnv.SetRightMargin(0.16)

    for k in fit_keys:
        t = t_signal if 'signal' in k else t_diboson
        sel = sel_2J if '2J' in k else sel_3J
        h2[k] = get_2d_histogram(t, 'pTV', 'dRBB', sel, k)
        quantiles[k] = h2[k].QuantilesX(qfrac[k])
        # Sanity check with quantiles
        h2[k].Draw('colz')
        quantiles[k].Draw('same')
        plot_utils.draw_labels(k, '')
        cnv.SaveAs(out_dir + 'sanity_{}.pdf'.format(k))
        
    # Full plots (no fit)
    h2['signal_2J'].Draw('colz')
    quantiles['signal_2J'].Draw('same')
    quantiles['diboson_2J'].Draw('same')
    plot_utils.draw_labels('signal_2J', '')
    cnv.SaveAs(out_dir+'signal_2J_quantiles.pdf')

    h2['signal_3J'].Draw('colz')
    quantiles['signal_3J'].Draw('same')
    quantiles['diboson_3J'].Draw('same')
    plot_utils.draw_labels('signal_3J', '')
    cnv.SaveAs(out_dir+'signal_3J_quantiles.pdf')

    for k in fit_keys:
        # Fit quantiles and print result
        fits[k] = ROOT.TF1('fit_{}'.format(k), '[0]+exp([1]+[2]*x)', fit_min, fit_max)
        fits[k].SetLineColor(ROOT.kCyan)
        fits[k].SetLineWidth(2)
        quantiles[k].Fit('fit_{}'.format(k), 'ERM')
        fits[k].SetRange(draw_min, draw_max)
        # Reproduce reference fits
        ref_fits[k] = ROOT.TF1('ref_fit_{}'.format(k), '[0]+exp([1]+[2]*x)', fit_min, fit_max)
        ref_fits[k].SetLineColor(ROOT.kRed)
        ref_fits[k].SetLineWidth(2)
        for i in range(3):
            ref_fits[k].SetParameter(i,ref_fits_params[k][i])
        ref_fits[k].SetRange(draw_min, draw_max)
        # Sanity check with fits
        h2[k].Draw('colz')
        quantiles[k].Draw('same')
        fits[k].Draw('same')
        ref_fits[k].Draw('same')
        plot_utils.draw_labels(k, '')
        cnv.SaveAs(out_dir + 'sanity_{}_fit.pdf'.format(k))

    # Print relative fit parameter differences
    for k in fit_keys:        
        print('Parameter differences for {} (old, new, diff):'.format(k))
        for i in range(3):
            old_param = ref_fits[k].GetParameter(i)
            new_param = fits[k].GetParameter(i)
            rel_diff = 100 * (old_param - new_param)/old_param
            print('\t [{}]: {:.5f}, {:.5f}, {:.2f}%'.format(i, old_param, new_param, rel_diff))

    # Legend
    lx1,ly1,lx2,ly2 = 0.45,0.82,0.82,0.92
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)

    # Full plots (with fit)
    h2['signal_2J'].Draw('colz')
    quantiles['signal_2J'].Draw('same')
    fits['signal_2J'].Draw('same')
    ref_fits['signal_2J'].Draw('same')
    quantiles['diboson_2J'].Draw('same')
    fits['diboson_2J'].Draw('same')
    ref_fits['diboson_2J'].Draw('same')
    plot_utils.draw_labels('signal_2J, Fit range: [{}, {}] GeV'.format(fit_min,fit_max), '')
    leg.AddEntry(fits['signal_2J'], 'New Fit (range: {} - {} GeV)'.format(fit_min, fit_max),'l')
    leg.AddEntry(ref_fits['signal_2J'], 'Old Fit','l')
    leg.Draw('same')
    cnv.SaveAs(out_dir+'signal_2J_quantiles_fit.pdf')

    h2['signal_3J'].Draw('colz')
    quantiles['signal_3J'].Draw('same')
    fits['signal_3J'].Draw('same')
    ref_fits['signal_3J'].Draw('same')
    quantiles['diboson_3J'].Draw('same')
    fits['diboson_3J'].Draw('same')
    ref_fits['diboson_3J'].Draw('same')
    plot_utils.draw_labels('signal_3J, Fit range: [{}, {}] GeV'.format(fit_min,fit_max), '')
    leg.Clear()
    leg.AddEntry(fits['signal_3J'], 'New Fit (range: {} - {} GeV)'.format(fit_min, fit_max),'l')
    leg.AddEntry(ref_fits['signal_3J'], 'Old Fit','l')
    leg.Draw('same')
    cnv.SaveAs(out_dir+'signal_3J_quantiles_fit.pdf')

    cnv.Close()
    
    # Get yields for tables
    for base_reg in region_cuts:
        yields[base_reg] = get_region_yield(t_signal, region_cuts[base_reg])
        if 'inclusive' not in base_reg:
            for ana_reg in ['CRLow', 'CRHigh', 'SR']:
                ana_reg_cut = get_analysis_region_cut(base_reg, ana_reg)
                ana_reg_cut_ref = get_analysis_region_cut(base_reg, ana_reg, use_ref_fits=True)
                yields[base_reg+'_'+ana_reg] = get_region_yield(t_signal, ana_reg_cut)
                yields[base_reg+'_'+ana_reg+'_ref'] = get_region_yield(t_signal, ana_reg_cut_ref)
    # Sanity check
    for base_reg in region_cuts:
        if 'inclusive' in base_reg:
            sum_regions = sum([yields[base_reg[:3]+r] for r in pTV_regions])
        else:
            sum_regions = yields[base_reg+'_CRLow'] + yields[base_reg+'_SR'] + yields[base_reg+'_CRHigh']
        if yields[base_reg] != sum_regions:
            raise Exception('Sanity check failed for {}: region yield = {}, sum of CR/SR yields = {}'.format(base_reg, yields[base_reg], sum_regions))

    for r, y in yields.items():
        print('Region (yield): {} ({})'.format(r,y))



    # Create yields tables
    if not os.path.exists(out_dir+'/tables/'): os.mkdir(out_dir+'/tables/')
    make_tables()
    make_individual_tables()