import os
import ROOT
import argparse
import plot_utils

out_dir = 'plots'
base_selections = ['hi', 'me', 'lo']
add_selections = [s + '_p100' for s in base_selections] + [s + '_m100' for s in base_selections]
all_selections = base_selections + add_selections
labels = {
    'hi' : '1400 < Jet p_{T} < 1600 GeV',
    'me' : '800 < Jet p_{T} < 1000 GeV',
    'lo' : '200 < Jet p_{T} < 400 GeV',
    'hi_p100' : '1500 < Jet p_{T} < 1700 GeV',
    'me_p100' : '900 < Jet p_{T} < 1100 GeV',
    'lo_p100' : '300 < Jet p_{T} < 500 GeV',
    'hi_m100' : '1300 < Jet p_{T} < 1500 GeV',
    'me_m100' : '700 < Jet p_{T} < 900 GeV',
    'lo_m100' : '100 < Jet p_{T} < 300 GeV',
}
colors = {
    'hi' : 2,
    'me' : 4,
    'lo' : 8,
    'hi_p100' : 2,
    'me_p100' : 4,
    'lo_p100' : 8,
    'hi_m100' : 2,
    'me_m100' : 4,
    'lo_m100' : 8,
}

def get_parser():
    p = argparse.ArgumentParser(description='Debug AOD files')
    p.add_argument('-d', '--data_histograms', dest='data_histograms', default=None, help='Input file containing data histograms')
    p.add_argument('-m', '--mc_histograms', dest='mc_histograms', default=None, help='Input file containing mc histograms')
    p.add_argument('-a', '--mc_additional', dest='mc_additional', default=None, help='Additional MC file for comparison')
    p.add_argument('-o', '--out_dir', dest='out_dir', default=None, help='Directory where plots are saved')
    return p

def get_histograms(hist_filename):
    hists = {}
    f = ROOT.TFile.Open(hist_filename)
    for key in f.GetListOfKeys():
        k_name = key.GetName()
        h = f.Get(k_name)
        isTH1 = h.InheritsFrom(ROOT.TH1.Class())
        if isTH1:
            hists[k_name] = h.Clone()
            hists[k_name].SetDirectory(0)
            for c in colors:
                if c in k_name:
                    hists[k_name].SetLineColor(colors[c])
                    hists[k_name].SetMarkerColor(colors[c])
    return hists

def get_max_bin_height(histograms):
    return max([h.GetMaximum() for h in histograms])

def add_scaled_histograms(hists, only_base = False):
    print('scaling')
    max_height = 0
    max_height_pT = 0
    selections = base_selections if only_base else all_selections
    for sel in selections:
        nj = hists[f'h1_jetPt_{sel}'].GetEntries()
        print(f'Number of jets ({sel}): {nj}')
        h = hists[f'h1_dRtj_{sel}'].Clone()
        h.SetDirectory(0)
        h.SetLineColor(colors[sel])
        h.SetMarkerColor(colors[sel])
        h.GetYaxis().SetTitle('dN/dA')
        h_pT = hists[f'h1_jetPt_{sel}'].Clone()
        h_pT.SetDirectory(0)
        h_pT.SetLineColor(colors[sel])
        h_pT.SetMarkerColor(colors[sel])
        h_pT.GetYaxis().SetTitle('A.U.')
        # scale to number of jets
        if nj > 0:
            h.Scale(1/nj)
            h_pT.Scale(1/nj)
            if h_pT.GetMaximum() > max_height_pT:
                max_height_pT = h_pT.GetMaximum()
        else:
            print('Skipping division by zero!')
        # scale to partial area
        for ibin in range(1, h.GetNbinsX() + 1):
            # calculate partial jet area
            r_low, r_high = h.GetBinLowEdge(ibin), h.GetBinLowEdge(ibin + 1)
            area = r_high**2 - r_low**2
            tot_area = 0.4**2
            norm_area = area / tot_area
            # apply jet area scaling
            cont = h.GetBinContent(ibin)
            if norm_area > 0:
                h.SetBinContent(ibin, cont/norm_area)
                if cont/norm_area > max_height:
                    max_height = cont/norm_area
            else:
                print(f'Warning: non-physical partial jet area in bin {ibin}')
                h.SetBinContent(ibin, 0)
        hists[f'h1_dRtj_{sel}_scaled'] = h
        hists[f'h1_jetPt_{sel}_scaled'] = h_pT
    for sel in selections:
        hists[f'h1_dRtj_{sel}_scaled'].SetMaximum(1.5*max_height)
        hists[f'h1_jetPt_{sel}_scaled'].SetMaximum(1.5*max_height_pT)
    return hists

def draw_plots(hists, hist_type, var):
    print('plotting')
    # canvas
    cnv = ROOT.TCanvas()
    cnv.cd()
    # legend
    lx1,ly1,lx2,ly2 = 0.53,0.6,0.9,0.9
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    for sel in base_selections:
        h_name = f'h1_{var}_{sel}_scaled'
        hists[h_name].Draw('hist, same')
        if var == 'jetPt':
            leg.AddEntry(hists[h_name], labels[sel] + f', {hists[h_name].GetEntries():.0f} jets','l')
        else:
            leg.AddEntry(hists[h_name], labels[sel],'l')
    leg.Draw('same')
    if hist_type == 'data':
        plot_utils.draw_labels('data18 AOD', '')
    elif 'mc' in hist_type:
        plot_utils.draw_labels(f'{hist_type} extended Z\' AOD', '')
    else:
        raise Exception(f'Unknown hist_type: {hist_type}')
    cnv.SaveAs(os.path.join(out_dir, f'{var}_{hist_type}.pdf'))

def draw_comparison(data, mc, var, mc_add = None):
    print('plotting comparison')
    # canvas
    cnv = ROOT.TCanvas()
    cnv.cd()
    # legend
    lx1,ly1,lx2,ly2 = 0.53,0.6,0.9,0.9
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    if mc_add:
        max_height = get_max_bin_height(
            [mc[f'h1_{var}_{sel}_scaled'] for sel in base_selections] +
            [data[f'h1_{var}_{sel}_scaled'] for sel in base_selections] +
            [mc_add[f'h1_{var}_{sel}_scaled'] for sel in base_selections]
        )
    else:
        max_height = get_max_bin_height(
            [mc[f'h1_{var}_{sel}_scaled'] for sel in base_selections] +
            [data[f'h1_{var}_{sel}_scaled'] for sel in base_selections]
        )
    for sel in base_selections:
        h_name = f'h1_{var}_{sel}_scaled'
        if max_height > mc[h_name].GetMaximum():
            mc[h_name].SetMaximum(max_height)
        mc[h_name].Draw('hist, same')
        data[h_name].Draw('same, p')
        if mc_add:
            mc_add[h_name].SetLineStyle(2)
            mc_add[h_name].Draw('hist, same')
        leg.AddEntry(mc[h_name], labels[sel], 'l')
    leg.Draw('same')
    if mc_add:
        plot_utils.draw_labels('#minus mc20, #bullet data18', '-- mc16')
        cnv.SaveAs(os.path.join(out_dir, f'{var}_comparison_add.pdf'))
    else:
        plot_utils.draw_labels('#minus mc20, #bullet data18', '')
        cnv.SaveAs(os.path.join(out_dir, f'{var}_comparison.pdf'))

def draw_sanity_checks(data, mc, var):
    print('plotting sanity checks')
    sanity_checks = {
        'hi_m100' : ['hi_m100', 'me', 'lo'],
        'me_p100' : ['hi', 'me_p100', 'lo'],
        'me_p100_hi_m100' : ['hi_m100', 'me_p100', 'lo'],
    }
    for check, san_sel in sanity_checks.items():
        # canvas
        cnv = ROOT.TCanvas()
        cnv.cd()
        # legend
        lx1,ly1,lx2,ly2 = 0.53,0.6,0.9,0.9
        leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
        max_height = get_max_bin_height(
            [mc[f'h1_{var}_{sel}_scaled'] for sel in san_sel] +
            [data[f'h1_{var}_{sel}_scaled'] for sel in san_sel]
        )
        for sel in san_sel:
            h_name = f'h1_{var}_{sel}_scaled'
            if max_height > mc[h_name].GetMaximum():
                mc[h_name].SetMaximum(max_height)
            mc[h_name].Draw('hist, same')
            data[h_name].Draw('same, p')
            leg.AddEntry(mc[h_name], labels[sel], 'l')
        leg.Draw('same')
        plot_utils.draw_labels('#minus mc20, #bullet data18', '')
        cnv.SaveAs(os.path.join(out_dir, f'{var}_comparison_{check}.pdf'))

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    if args.out_dir:
        out_dir = args.out_dir
    os.makedirs(out_dir, exist_ok = True)
    if args.data_histograms:
        data_hists = add_scaled_histograms(get_histograms(args.data_histograms))
        draw_plots(data_hists, 'data', 'dRtj')
        draw_plots(data_hists, 'data', 'jetPt')
    if args.mc_histograms:
        mc_hists = add_scaled_histograms(get_histograms(args.mc_histograms))
        draw_plots(mc_hists, 'mc20', 'dRtj')
        draw_plots(mc_hists, 'mc20', 'jetPt')
    if args.mc_additional:
        mc_hists_add = add_scaled_histograms(get_histograms(args.mc_additional), only_base = True)
        draw_plots(mc_hists_add, 'mc16', 'dRtj')
        draw_plots(mc_hists_add, 'mc16', 'jetPt')
    if args.data_histograms and args.mc_histograms:
        draw_comparison(data_hists, mc_hists, 'dRtj')
        draw_comparison(data_hists, mc_hists, 'jetPt')
        draw_sanity_checks(data_hists, mc_hists, 'dRtj')
        draw_sanity_checks(data_hists, mc_hists, 'jetPt')
    if args.data_histograms and args.mc_histograms and args.mc_additional:
        draw_comparison(data_hists, mc_hists, 'dRtj', mc_hists_add)
        draw_comparison(data_hists, mc_hists, 'jetPt', mc_hists_add)