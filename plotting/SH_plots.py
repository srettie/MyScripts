import os
import glob
import argparse
import numpy as np
import pandas as pd
import uproot
import ROOT
import plot_utils as pu

flavours = ['b', 'c', 'u']
mc_campaigns = ['e8448_a899_r13144_p5631', 'e8448_a899_r13145_p5631', 'e8448_a899_r13167_p5631']
ntuple_name = 'ntuple.root'
tree_name = 'AnalysisMiniTree'
jet_prefix = 'recojet_antikt4'
tagger_score_cuts = {
    'GN2v00' : {
        'beff_60'  : 5.407,
        'beff_70'  : 3.885,
        'beff_77'  : 2.899,
        'beff_85'  : 1.637,
        'beff_90'  : 0.523,
        'urej_143' : 1.796,
        'urej_192' : 2.038,
    },
    'DL1r' : {
        'beff_60' : 4.441,
        'beff_70' : 3.041,
        'beff_77' : 1.921,
        'beff_85' : 0.167,
        'beff_90' : -0.942,
        'urej_143' : 1.919,
        'urej_192' : 2.197,
    },
    'GN120220509' : {
        'beff_60' : 5.146,
        'beff_70' : 3.672,
        'beff_77' : 2.629,
        'beff_85' : 1.28,
        'beff_90' : 0.071,
        'urej_143' : 1.804,
        'urej_192' : 2.063,
    },
    'DL1dv01' : {
        'beff_60' : 4.891,
        'beff_70' : 3.533,
        'beff_77' : 2.498,
        'beff_85' : 0.976,
        'beff_90' : -0.227,
        'urej_143' : 1.899,
        'urej_192' : 2.146,
    },
}
fc_values = {
    'GN2v00' : 0.1,
    'DL1r' : 0.018,
    'GN120220509' : 0.05,
    'DL1dv01' : 0.018,
}

def get_parser():
    p = argparse.ArgumentParser(description='Submit QC jobs')
    p.add_argument('-i', '--in_dir', dest = 'in_dir', required = True, help = 'Input file directory')
    p.add_argument('-o', '--out_dir', dest = 'out_dir', required = True, help = 'Where to save plots')
    return p

def get_masses(s):
    # given a sample name s, extract X and S masses
    m_X = s[s.find('XHS'):].split('_')[1]
    m_S = s[s.find('XHS'):].split('_')[2]
    return m_X[1:], m_S[1:]

def get_combined_samples(samples):
    # combine MC campaigns for sample names
    return ['.'.join(s.split('.')[:-1]) for s in samples if mc_campaigns[0] in s]

def add_tagger_scores(df):
    epsilon = 1e-10
    for tagger in tagger_score_cuts:
        fc = fc_values[tagger]
        # add scores
        df[f'{tagger}_score'] = np.log(np.divide(df[f'{tagger}_pb'] + epsilon, fc*df[f'{tagger}_pc'] + (1-fc)*df[f'{tagger}_pu'] + epsilon))
        # add pass/fail WP flags
        for wp, cut in tagger_score_cuts[tagger].items():
            df[f'{tagger}_FixedCutBEff_{wp}'] = df[f'{tagger}_score'] > cut
    return df

def add_n_jets(df):
    df = df.join(
        pd.DataFrame(
            df.groupby(level = 0).size(),
            columns = ['n_jets']
        )
    )
    for tagger in tagger_score_cuts:
        for wp in tagger_score_cuts[tagger]:
            col_name = f'n_jets_{tagger}_{wp}'
            df = df.join(
                pd.DataFrame(
                    df[df[f'{tagger}_FixedCutBEff_{wp}']].groupby(level = 0).size(),
                    columns = [col_name]
                )
            )
            # na means zero jets here
            df[col_name].fillna(0, inplace = True)
            # cast n jets to int
            df[col_name] = df[col_name].astype('int')
    return df

def add_total_efficiencies(effs, samples):
    # should have 240 total efficiencies for now (36/3=12 samples times 4 taggers times 5 WPs)
    # combine a/d/e pass/fail numbers to get total efficiency
    for s in get_combined_samples(samples):
        print(f'combined sample: {s}')
        for t in tagger_score_cuts:
            for wp in tagger_score_cuts[t]:
                pass_combined = 0
                total_combined = 0
                for c in mc_campaigns:
                    pass_combined += int(effs[f'{s}.{c}_{t}_{wp}_pass'])
                    total_combined += int(effs[f'{s}.{c}_{t}_{wp}_total'])
                effs[f'{s}_{t}_{wp}_pass_combined'] = pass_combined
                effs[f'{s}_{t}_{wp}_total_combined'] = total_combined
                effs[f'{s}_{t}_{wp}_eff_combined'] = pass_combined / total_combined
    return effs

def get_efficiencies(args, samples):
    effs = {}
    out_file = os.path.join(args.out_dir, 'efficiencies.txt')
    # don't compute efficiencies if already done
    if os.path.isfile(out_file):
        print(f'reading efficiencies from {out_file}')
        with open(out_file, 'r') as f:
            for l in f.readlines():
                l = l.strip('\n').split(': ')
                k, v = l[0], l[1]
                effs[k] = v
        return effs
    # columns to load from ntuples
    columns = []
    for tagger in tagger_score_cuts:
        columns += [f'{jet_prefix}_{tagger}_p{flav}' for flav in flavours]
    for sample in samples:
        # get sample masses
        m_X, m_S = get_masses(sample)
        print(f'sample: {sample} (m_X = {m_X}, m_S = {m_S})')
        # get df
        df = uproot.open(f'{os.path.join(args.in_dir, sample, ntuple_name)}:{tree_name}').arrays(columns, library = 'pd')
        df.rename(columns = {c : c.replace(f'{jet_prefix}_', '') for c in columns}, inplace = True)
        # add tagger scores using probabilities
        df = add_tagger_scores(df)
        df = add_n_jets(df)
        # calculate efficiencies
        n_total = len(df.groupby(level = 0))
        for tagger in tagger_score_cuts:
            for wp in tagger_score_cuts[tagger]:
                n_jets_mask = df[f'n_jets_{tagger}_{wp}']>=4
                n_pass = len(df[n_jets_mask].groupby(level = 0))
                effs[f'{sample}_{tagger}_{wp}_pass'] = n_pass
                effs[f'{sample}_{tagger}_{wp}_total'] = n_total
                effs[f'{sample}_{tagger}_{wp}_eff'] = n_pass/n_total
    with open(out_file, 'w') as f:
        for k, v in effs.items():
            f.write(f'{k}: {v}\n')
    return effs

def get_eff_map(samples, effs, tagger, wp):
    h2 = ROOT.TH2D(f'{tagger}_{wp}', f'{tagger}_{wp};m_{{X}} [GeV];m_{{S}} [GeV];Signal Efficiency', 65, 0, 6500, 16, 0, 1600)
    for s in get_combined_samples(samples):
        m_X, m_S = get_masses(s)
        eff = effs[f'{s}_{tagger}_{wp}_eff_combined']
        h2.Fill(float(m_X), float(m_S), float(eff))
    h2.SetDirectory(0)
    return h2


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    # make output directory
    os.makedirs(args.out_dir, exist_ok=True)
    # get ntuples and calculate efficiencies
    samples = [os.path.basename(s) for s in glob.glob(os.path.join(args.in_dir, "*"))]
    efficiencies = get_efficiencies(args, samples)
    efficiencies = add_total_efficiencies(efficiencies, samples)
    # make plots
    eff_maps = {}
    for t in tagger_score_cuts:
        for wp in tagger_score_cuts[t]:
            eff_maps[f'{t}_{wp}'] = get_eff_map(samples, efficiencies, t, wp)
    pu.set_ATLAS_style()
    for k, h2 in eff_maps.items():
        pu.plot_2D(args.out_dir, h2, k)
    # take relevant ratio to see improvement
    h2_GN2_over_DL1r_urej_143 = eff_maps['GN2v00_urej_143'].Clone('h2_GN2_over_DL1r_urej_143')
    h2_GN2_over_DL1r_urej_143.Divide(eff_maps['DL1r_urej_143'])
    h2_GN2_over_DL1r_urej_143.GetZaxis().SetTitle('Signal Efficiency Improvement (GN2/DL1r)')
    pu.plot_2D(args.out_dir, h2_GN2_over_DL1r_urej_143, 'GN2_over_DL1r_urej_143')
