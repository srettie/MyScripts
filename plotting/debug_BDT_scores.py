import uproot
import xgboost as xgb
import matplotlib.pyplot as plt
import numpy as np

variables = [
    'pt_h1','eta_h1','phi_h1',
    'pt_h2','eta_h2','phi_h2',
    'pt_hh','eta_hh','phi_hh','m_hh',
    'pt_VBFj1','eta_VBFj1','phi_VBFj1','E_VBFj1',
    'pt_VBFj2','eta_VBFj2','phi_VBFj2','E_VBFj2'
]
res_truth_masses = ['1000','1100','1200','1300','1400','1500','1600','1800','2000','2500','3000','4000','5000']
def addBDTScores(df, modelfile):
    model = xgb.Booster()
    model.load_model(modelfile)
    dmatrix = xgb.DMatrix(df[[f'{v}Boosted' for v in variables]], feature_names = variables)
    df[f'BDT_{modelfile.split("/")[-1].replace(".model","").replace(".json","")}'] = model.predict(dmatrix)
    return df

def addBDTScoresRes(df, modelfile, truthmass):
    vars = ['true_mass',
            'pt_h1Boosted',
            'eta_h1Boosted',
            'pt_h2Boosted',
            'eta_h2Boosted',
            'pt_hhBoosted',
            'eta_hhBoosted',
            'pt_VBFj1Boosted',
            'eta_VBFj1Boosted',
            'E_VBFj1Boosted',
            'pt_VBFj2Boosted',
            'eta_VBFj2Boosted',
            'E_VBFj2Boosted',
            'm_hhBoosted']
    df['true_mass'] = int(truthmass)
    model = xgb.Booster()
    model.load_model(modelfile)
    df[f'BDT_res_{truthmass}'] = model.predict(xgb.DMatrix(df[vars]))
    return df

if __name__ == '__main__':
    # get df from NNT
    fname = '/Users/sebastienrettie/HH/nano_ntuples_with_shapes_SEP22/merged/MC/502971_mc16e/reconstructed.root'
    tname = 'fullmassplane'
    mname = '/Users/sebastienrettie/HH/limits/hh4b-vbf-limits/models/k2V0.model'
    mname_res = '/Users/sebastienrettie/HH/limits/hh4b-vbf-limits/models/pBDT_resonant.model'
    t = uproot.open(fname)[tname]
    df = t.arrays([f'{v}Boosted' for v in variables]+['BDT_score']+[f'BDT_score_res_{m}' for m in res_truth_masses], library='pd')
    # add BDT scores
    df = addBDTScores(df, mname)
    for truthmass in res_truth_masses:
        df = addBDTScoresRes(df, mname_res, truthmass)
    # print first event
    print('first event:')
    for v in variables:
        print(f'\t{v} = {df[f"{v}Boosted"][0]}')
    print(f'\t-->score = {df["BDT_k2V0"][0]} vs. {df["BDT_score"][0]}')
    print('\t-->res scores:')
    for truthmass in res_truth_masses:
        print(f'\t\t{df[f"BDT_res_{truthmass}"][0]} vs. {df[f"BDT_score_res_{truthmass}"][0]}')
    plt.hist(df['BDT_k2V0'], bins=np.linspace(0,1,100))
    plt.savefig('nonres_from_python.pdf')
    plt.clf()
    plt.hist(df['BDT_score'], bins=np.linspace(0,1,100))
    plt.savefig('nonres_from_cpp.pdf')
    plt.clf()
    for truthmass in res_truth_masses:
        plt.hist(df[f'BDT_res_{truthmass}'], bins=np.linspace(0,1,100))
        plt.savefig(f'res_{truthmass}_from_python.pdf')
        plt.clf()
        plt.hist(df[f'BDT_score_res_{truthmass}'], bins=np.linspace(0,1,100))
        plt.savefig(f'res_{truthmass}_from_cpp.pdf')
        plt.clf()
