import os
import sys
import glob
import array
import ROOT
import datetime as dt
import plot_utils
import tracking_plots
import draw_roc_curves
sys.path.append(os.path.join(os.path.dirname(__file__),'..','batch'))
import tracking_submitJobs
import produce_roc_curve
import process_trees
import plot_reweighted

# Produce weights to reweight tree2 to tree1 using distribution of reweight_quantity
plot_dir = '/unix/atlastracking/srettie/reweight_plots/'
weights_file = plot_dir + 'weights.root'
jet_type = 'AntiKt4EMPFlowJets'
debug = True
do_fast = True
do_submit = False
add_weights = False

base_dir1 = '/unix/atlasvhbb2/srettie/tracking/athenaOutputs_full_custom_ipxd/group.perf-idtracking.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.e5362_e5984_s3126_r11212_IdealNomV5_EXT0/nom/'
base_dir2 = '/unix/atlasvhbb2/srettie/tracking/athenaOutputs_full_custom_ipxd/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210/nom/'
fname1 = base_dir1 + 'all_flav_427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.e5362_e5984_s3126_r11212_IdealNomV5_Akt4EMPf_nom.root'
fname2 = base_dir2 + 'all_flav_427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.merge.AOD.e6928_e5984_s3126_r10201_r10210_Akt4EMPf_nom.root'

reweight_quantity    = 'jet_pt'
reweight_quantity_2d = 'jet_eta'
reweight_quantity_bH = 'jet_bH_pt'

def get_weights_hist(fname1, fname2, reweight_quantity, jflav, do_fast = True):

    is_2d = '_2d' in jflav or ('_3d' in jflav and 'bjet' not in jflav)
    is_3d = '_3d' in jflav and 'bjet' in jflav
    if debug: print('jflav, is_2d, is_3d = {}, {}, {}'.format(jflav, is_2d, is_3d))
    if do_fast:
        # Just return pre-calculated histogram
        f = ROOT.TFile(weights_file)
        if is_2d:
            h_name = 'weights_{}_vs_{}_{}'.format(reweight_quantity, reweight_quantity_2d, jflav)
        elif is_3d:
            h_name = 'weights_{}_vs_{}_vs_{}_{}'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav)
        else:
            h_name = 'weights_{}_{}'.format(reweight_quantity, jflav)
        if debug: print('Retreiving: {}'.format(h_name))
        h = f.Get(h_name)
        h.SetDirectory(0)
        return h

    # Labels for single-variable histogram
    short_title = 'AntiKt4EMPFlowJets'
    if 'bjet' in jflav: short_title += ', b-jets'
    if 'cjet' in jflav: short_title += ', c-jets'
    if 'ljet' in jflav: short_title += ', l-jets'
    if 'all'  in jflav: short_title += ', all jets'

    # Get both trees; for now, h1 will be no pileup Z', h2 will be extended Z' with pileup
    t1 = produce_roc_curve.get_tree(fname1, jet_type, base_dir1)
    t2 = produce_roc_curve.get_tree(fname2, jet_type, base_dir2)

    if debug:
        print('t1 has {} entries, t2 has {} entries (Jet flavour {})'.format(t1.GetEntries(), t2.GetEntries(), jflav))
    # Get both pT spectra
    h_skeleton = process_trees.plot_skeletons[reweight_quantity].Clone()
    if is_2d:
        h_skeleton = process_trees.plot_skeletons['{}_vs_{}'.format(reweight_quantity, reweight_quantity_2d)].Clone()
    if is_3d:
        # 3D reweighting only for b-jets
        h_skeleton = process_trees.plot_skeletons['{}_vs_{}_vs_{}'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d)].Clone()
    h1 = h_skeleton.Clone('h1')
    h2 = h_skeleton.Clone('h2')



    if jflav in produce_roc_curve.flavour_cuts or '_bcl_2d' in jflav or '_bcl_3d' in jflav:
        flav_cut1 = produce_roc_curve.flavour_cuts[jflav.replace('_bcl_2d_bH','').replace('_bcl_2d','').replace('_bcl_3d','')]
        flav_cut2 = produce_roc_curve.flavour_cuts[jflav.replace('_bcl_2d_bH','').replace('_bcl_2d','').replace('_bcl_3d','')]
    elif jflav == 'all':
        flav_cut1 = ROOT.TCut('1')
        flav_cut2 = ROOT.TCut('1')
    else:
        raise ValueError('Unrecognised jflav: {}'.format(jflav))

    if 'bjet' in jflav:
        flav_cut1 += ROOT.TCut(produce_roc_curve.do_1bH_cut_str)
        flav_cut2 += ROOT.TCut(produce_roc_curve.do_1bH_cut_str)

    if debug: print('Flavour cuts applied: t1 = {}, t2 = {}'.format(flav_cut1, flav_cut2))

    if is_2d:
        if 'jet_pt' in reweight_quantity or 'jet_bH_pt' in reweight_quantity:
            n1selected = t1.Draw('{}/1000.:{}>>h1'.format(reweight_quantity, reweight_quantity_2d), produce_roc_curve.baseline_cut + flav_cut1)
            n2selected = t2.Draw('{}/1000.:{}>>h2'.format(reweight_quantity, reweight_quantity_2d), produce_roc_curve.baseline_cut + flav_cut2)
        else:
            n1selected = t1.Draw('{}:{}>>h1'.format(reweight_quantity, reweight_quantity_2d), produce_roc_curve.baseline_cut + flav_cut1)
            n2selected = t2.Draw('{}:{}>>h2'.format(reweight_quantity, reweight_quantity_2d), produce_roc_curve.baseline_cut + flav_cut2)
    elif is_3d:
        n1selected = t1.Draw('{}/1000.:{}/1000.:{}>>h1'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d), produce_roc_curve.baseline_cut + flav_cut1)
        n2selected = t2.Draw('{}/1000.:{}/1000.:{}>>h2'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d), produce_roc_curve.baseline_cut + flav_cut2)
    else:
        if 'jet_pt' in reweight_quantity or 'jet_bH_pt' in reweight_quantity:
            n1selected = t1.Draw('{}/1000.>>h1'.format(reweight_quantity), produce_roc_curve.baseline_cut + flav_cut1)
            n2selected = t2.Draw('{}/1000.>>h2'.format(reweight_quantity), produce_roc_curve.baseline_cut + flav_cut2)
        else:
            n1selected = t1.Draw('{}>>h1'.format(reweight_quantity), produce_roc_curve.baseline_cut + flav_cut1)
            n2selected = t2.Draw('{}>>h2'.format(reweight_quantity), produce_roc_curve.baseline_cut + flav_cut2)

    if debug:
        print('Selected: h1 has {} jets, h2 has {} jets'.format(n1selected, n2selected))
        print('h1max: {}, h2max: {}'.format(h1.GetMaximum(), h2.GetMaximum()))

    if is_2d:
        plot_utils.plot_2D(plot_dir, h1, 'h_{}_vs_{}_orig_{}_num'.format(reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h2, 'h_{}_vs_{}_orig_{}_denom'.format(reweight_quantity, reweight_quantity_2d, jflav), short_title)
    elif is_3d:
        plot_utils.plot_2D(plot_dir, h1.Project3DProfile('xy'), 'h_{}_vs_{}_vs_{}_orig_{}_num_xy'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h2.Project3DProfile('xy'), 'h_{}_vs_{}_vs_{}_orig_{}_denom_xy'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h1.Project3DProfile('xz'), 'h_{}_vs_{}_vs_{}_orig_{}_num_xz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h2.Project3DProfile('xz'), 'h_{}_vs_{}_vs_{}_orig_{}_denom_xz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h1.Project3DProfile('yz'), 'h_{}_vs_{}_vs_{}_orig_{}_num_yz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h2.Project3DProfile('yz'), 'h_{}_vs_{}_vs_{}_orig_{}_denom_yz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
    else:
        plot_utils.plot_histograms(plot_dir, [h1,h2],['Z\' (no pileup)','Extended Z\' (with pileup)'], plot_reweighted.colors, 'h_{}_orig_{}'.format(reweight_quantity, jflav))

    h1_normalized = h1.Clone('h1norm')
    h1_normalized.Scale(1./h1_normalized.Integral())
    h2_normalized = h2.Clone('h2norm')
    h2_normalized.Scale(1./h2_normalized.Integral())

    if is_2d:
        plot_utils.plot_2D(plot_dir, h1_normalized, 'h_{}_vs_{}_norm_{}_num'.format(reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h2_normalized, 'h_{}_vs_{}_norm_{}_denom'.format(reweight_quantity, reweight_quantity_2d, jflav), short_title)
    elif is_3d:
        plot_utils.plot_2D(plot_dir, h1_normalized.Project3DProfile('xy'), 'h_{}_vs_{}_vs_{}_norm_{}_num_xy'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h2_normalized.Project3DProfile('xy'), 'h_{}_vs_{}_vs_{}_norm_{}_denom_xy'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h1_normalized.Project3DProfile('xz'), 'h_{}_vs_{}_vs_{}_norm_{}_num_xz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h2_normalized.Project3DProfile('xz'), 'h_{}_vs_{}_vs_{}_norm_{}_denom_xz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h1_normalized.Project3DProfile('yz'), 'h_{}_vs_{}_vs_{}_norm_{}_num_yz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h2_normalized.Project3DProfile('yz'), 'h_{}_vs_{}_vs_{}_norm_{}_denom_yz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
    else:
        plot_utils.plot_histograms(plot_dir, [h1_normalized,h2_normalized],['Z\' (no pileup)','Extended Z\' (with pileup)'], plot_reweighted.colors, 'h_{}_norm_{}'.format(reweight_quantity, jflav))

    # Produce ratios for weights
    if is_2d:
        h_weight = h1.Clone('weights_{}_vs_{}_{}'.format(reweight_quantity, reweight_quantity_2d, jflav))
    elif is_3d:
        h_weight = h1.Clone('weights_{}_vs_{}_vs_{}_{}'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav))
    else:
        h_weight = h1.Clone('weights_{}_{}'.format(reweight_quantity, jflav))
    h_weight.Divide(h2)
    if not is_2d and not is_3d:
        h_min, h_max = plot_utils.get_extrema([h_weight], h_weight.GetXaxis().GetXmin(), h_weight.GetXaxis().GetXmax())
        h_weight.SetMaximum(1.5*h_max)

    # Plot actual weights distribution
    if is_2d:
        plot_utils.plot_2D(plot_dir, h_weight, 'h_weights_{}_vs_{}_{}'.format(reweight_quantity, reweight_quantity_2d, jflav), short_title)
    elif is_3d:
        plot_utils.plot_2D(plot_dir, h_weight.Project3DProfile('xy'), 'h_weights_{}_vs_{}_vs_{}_{}_xy'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h_weight.Project3DProfile('xz'), 'h_weights_{}_vs_{}_vs_{}_{}_xz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h_weight.Project3DProfile('yz'), 'h_weights_{}_vs_{}_vs_{}_{}_yz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
    else:
        plot_utils.plot_single_hist(plot_dir, h_weight, 'h_weights_{}_{}'.format(reweight_quantity, jflav), short_title)

    # Sanity check
    h_sanity = h2.Clone('sanity')
    h_sanity.Multiply(h_weight)
    if is_2d:
        plot_utils.plot_2D(plot_dir, h_sanity, 'h_{}_vs_{}_sanity_{}'.format(reweight_quantity, reweight_quantity_2d, jflav), short_title)
    elif is_3d:
        plot_utils.plot_2D(plot_dir, h_sanity.Project3DProfile('xy'), 'h_{}_vs_{}_vs_{}_sanity_{}_xy'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h_sanity.Project3DProfile('xz'), 'h_{}_vs_{}_vs_{}_sanity_{}_xz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
        plot_utils.plot_2D(plot_dir, h_sanity.Project3DProfile('yz'), 'h_{}_vs_{}_vs_{}_sanity_{}_yz'.format(reweight_quantity_bH, reweight_quantity, reweight_quantity_2d, jflav), short_title)
    else:
        plot_utils.plot_histograms(plot_dir, [h_sanity, h1], ['Sanity Check', 'Original'], plot_reweighted.colors, 'h_sanity_{}_{}'.format(reweight_quantity, jflav))

    f_out = ROOT.TFile(weights_file, 'UPDATE')

    f_out.cd()
    h_weight.Write()
    f_out.Close()

    return h_weight

if __name__ == "__main__":

    # Get weights histogram
    if not do_fast and os.path.exists(weights_file):
        # Delete old histograms file as we are creating a new one
        if debug: print('Deleting old weights file!')
        os.remove(weights_file)

    h_weights = {}
    h_weights['all'] = get_weights_hist(fname1, fname2, reweight_quantity, 'all', do_fast)
    for jflav in process_trees.jet_flavours:
        h_weights[jflav] = get_weights_hist(fname1, fname2, reweight_quantity, jflav, do_fast)
        h_weights[jflav+'_bcl_2d'] = get_weights_hist(fname1, fname2, reweight_quantity, jflav+'_bcl_2d', do_fast)
        if jflav == 'bjet':
            h_weights[jflav+'_bcl_2d_bH'] = get_weights_hist(fname1, fname2, reweight_quantity_bH, jflav+'_bcl_2d_bH', do_fast)
        else:
            h_weights[jflav+'_bcl_2d_bH'] = get_weights_hist(fname1, fname2, reweight_quantity, jflav+'_bcl_2d_bH', do_fast)
        h_weights[jflav+'_bcl_3d'] = get_weights_hist(fname1, fname2, reweight_quantity, jflav+'_bcl_3d', do_fast)

    # Add weights to new ttree files
    if add_weights:
        if not os.path.exists(fname2):
            if debug: print('File {} does not exists, will TChain subdirectories...'.format(fname2))
            filenames = [fn for fn in glob.glob(base_dir2 + 'trk_*/flav_{}.root'.format(tracking_submitJobs.get_short_jet_name(jet_type)))]
        else:
            if debug: print('Opening file: {}'.format(fname2))
            filenames = [fname2]
        nJobs = 0
        for fn in filenames:
            cmd = 'python /home/srettie/MyScripts/batch/add_weights.py -f {}'.format(fn)
            j_name = 'add_weights_{}'.format(fn.split('/')[-2].replace('trk_',''))
            tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
            nJobs += 1
        print('Submitted weights addition jobs for {} files!'.format(nJobs))
        sys.exit()

    # Get comparison plots
    # Get both trees; for now, h1 will be no pileup Z', h2 will be extended Z' with pileup
    t1 = produce_roc_curve.get_tree(fname1, jet_type, base_dir1)
    t2 = produce_roc_curve.get_tree(fname2, jet_type, base_dir2, with_weights = True)
    trees = [t1,t2]
    nplots = 0
    for rw in ['NA','jet_pt','jet_pt_bcl','jet_pt_bcl_2d','jet_pt_bcl_2d_bH','jet_pt_bcl_3d']:
        for var in ['jet_pt', 'jet_eta', 'jet_phi','sv1_llr']:
            cmd = 'python /home/srettie/MyScripts/batch/plot_reweighted.py -o {} -f1 {} -f2 {} -v {} -rw {}'.format(plot_dir, fname1, fname2, var, rw)
            j_name = 'plot_rw_{}_{}'.format(var, rw)
            tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
            nplots += 1
            for jflav in process_trees.jet_flavours:
                cmd = 'python /home/srettie/MyScripts/batch/plot_reweighted.py -o {} -f1 {} -f2 {} -v {} -rw {} -jf {}'.format(plot_dir, fname1, fname2, var, rw, jflav)
                j_name = 'plot_rw_{}_{}_{}'.format(var, rw, jflav)
                tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
                nplots += 1
                if jflav == 'bjet' and var == 'jet_pt':
                    cmd = 'python /home/srettie/MyScripts/batch/plot_reweighted.py -o {} -f1 {} -f2 {} -v {} -rw {} -jf {}'.format(plot_dir, fname1, fname2, 'jet_bH_pt', rw, jflav)
                    j_name = 'plot_rw_{}_{}_{}'.format('jet_bH_pt', rw, jflav)
                    tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
                    cmd = 'python /home/srettie/MyScripts/batch/plot_reweighted.py -o {} -f1 {} -f2 {} -v {} -rw {} -jf {}'.format(plot_dir, fname1, fname2, 'jet_bH_pt_frac', rw, jflav)
                    j_name = 'plot_rw_{}_{}_{}'.format('jet_bH_pt_frac', rw, jflav)
                    tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
                    nplots += 2
            for ptbin in process_trees.pt_bins:
                cmd = 'python /home/srettie/MyScripts/batch/plot_reweighted.py -o {} -f1 {} -f2 {} -v {} -rw {} -pt {}'.format(plot_dir, fname1, fname2, var, rw, ptbin)
                j_name = 'plot_rw_{}_{}_{}'.format(var, rw, ptbin)
                tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
                nplots += 1
                for jflav in process_trees.jet_flavours:
                    cmd = 'python /home/srettie/MyScripts/batch/plot_reweighted.py -o {} -f1 {} -f2 {} -v {} -rw {} -jf {} -pt {}'.format(plot_dir, fname1, fname2, var, rw, jflav, ptbin)
                    j_name = 'plot_rw_{}_{}_{}_{}'.format(var, rw, jflav, ptbin)
                    tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
                    nplots += 1
                    if jflav == 'bjet' and var == 'jet_pt':
                        cmd = 'python /home/srettie/MyScripts/batch/plot_reweighted.py -o {} -f1 {} -f2 {} -v {} -rw {} -jf {} -pt {}'.format(plot_dir, fname1, fname2, 'jet_bH_pt', rw, jflav, ptbin)
                        j_name = 'plot_rw_{}_{}_{}_{}'.format('jet_bH_pt', rw, jflav, ptbin)
                        tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
                        cmd = 'python /home/srettie/MyScripts/batch/plot_reweighted.py -o {} -f1 {} -f2 {} -v {} -rw {} -jf {} -pt {}'.format(plot_dir, fname1, fname2, 'jet_bH_pt_frac', rw, jflav, ptbin)
                        j_name = 'plot_rw_{}_{}_{}_{}'.format('jet_bH_pt_frac', rw, jflav, ptbin)
                        tracking_submitJobs.torque_submit(cmd,'',j_name,do_submit)
                        nplots += 2

    print('Done! Submitted {} plots.'.format(nplots))