from __future__ import division
import ROOT

# Use ATLAS Style
ROOT.gROOT.SetBatch(1)
ROOT.gROOT.LoadMacro("~/atlasrootstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("~/atlasrootstyle/AtlasLabels.C")
ROOT.gROOT.LoadMacro("~/atlasrootstyle/AtlasUtils.C")
ROOT.SetAtlasStyle()

out_dir = '/unix/atlastracking/srettie/debug_plots/retrain_DIPS/stats_comparison/'

debug = False
x_values = [1/2**n for n in range(7)]
y_values_highpt_test_file = {
    'lrej_loose'        : [8.757057490212247,8.52802675585285,7.846750369276225,7.13772253946927,6.239612391719283,5.309152994086787,4.539253035212024],
    'crej_loose'        : [2.8952093824542837,2.873125986322991,2.8078591395431065,2.749378637722199,2.5820357512187946,2.432201502922352,2.2773564746045425],
    'lrej_loose_highpt' : [8.81152809454697,7.87291589477585,7.222637661454798,6.110131314099497,5.099760000000003,4.255331931510968,3.440483579350733],
    'crej_loose_highpt' : [2.9365570079639807,2.800121759748794,2.7410137381594666,2.625476941565273,2.4553679300946896,2.2727367298639853,2.085805666276824],
}
y_values_highpt_test_file_preprocessed = {
    'lrej_loose'        : [7.825662264593272,7.527435357787139,6.993365921787716,6.267288315914644,5.487697956052393,4.859049005337219,4.13497667120856],
    'crej_loose'        : [2.867217536007348,2.8399215663985937,2.7593881157499474,2.7302133704887606,2.5538972655251753,2.4146812931909367,2.2578342107046856],
    'lrej_loose_highpt' : [7.902856691919199,7.1624231154341365,6.3483359746434305,5.417929019692712,4.602674878205722,3.9178827119439807,3.215753644595726],
    'crej_loose_highpt' : [2.9142073953108625,2.790595068554358,2.715155137749283,2.582073741053668,2.443672820638222,2.236210253082416,2.0594757233533945],
}
colors = {
    'lrej_loose'        : ROOT.kBlack,
    'crej_loose'        : ROOT.TColor.GetColor('#2ca02c'),
    'lrej_loose_highpt' : ROOT.TColor.GetColor('#1f77b4'),
    'crej_loose_highpt' : ROOT.TColor.GetColor('#ff7f0e'),
}
legend_titles = {
    'lrej_loose'        : 'Inclusive training (l-jets)',
    'lrej_loose_highpt' : 'High-pT training (l-jets)',
    'crej_loose'        : 'Inclusive training (c-jets)',
    'crej_loose_highpt' : 'High-pT training (c-jets)',
}

def get_graph(x,y):
    g = ROOT.TGraph(len(x))
    for i, (ix,iy) in enumerate(zip(x,y)):
        if debug: print(i,ix,iy)
        g.SetPoint(i,ix,iy)
    return g

def get_all_graphs(x_vals, y_vals):
    # Get all TGraphs associated to y_vals
    graphs = {}
    for k, yval in y_vals.items():
        graphs[k] = get_graph(x_vals, yval)
        graphs[k].SetLineColor(colors[k])
        graphs[k].SetMarkerColor(colors[k])
        graphs[k].SetMinimum(0)
        graphs[k].GetXaxis().SetTitle('Fraction of statistics')
        graphs[k].GetYaxis().SetTitle('Rejection @ 70% b-jet efficiency')
    # Minima for later drawing
    graphs['lrej_loose'].SetMinimum(3)
    graphs['crej_loose'].SetMinimum(2)
    return graphs
    
def plot_graphs(graphs, suffix = ''):   
    cnv = ROOT.TCanvas()
    cnv.cd()
    # Light-jets    
    graphs['lrej_loose'].Draw('AC*')
    graphs['lrej_loose_highpt'].Draw('SAMEC*')
    # Legend
    lx1,ly1,lx2,ly2 = 0.53,0.4,0.9,0.7
    leg = ROOT.TLegend(lx1, ly1, lx2, ly2)
    leg.AddEntry(graphs['lrej_loose'],legend_titles['lrej_loose'],'l')
    leg.AddEntry(graphs['lrej_loose_highpt'],legend_titles['lrej_loose_highpt'],'l')
    leg.Draw('same')
    if suffix == '_test_preprocessed':
        ROOT.myText(0.53, 0.3, 1, 'Preprocessed test sample')
    cnv.SaveAs(out_dir+'stats_comparison_ljet{}.pdf'.format(suffix))
    # c-jets
    graphs['crej_loose'].Draw('AC*')
    graphs['crej_loose_highpt'].Draw('SAMEC*')
    leg.Clear()
    leg.AddEntry(graphs['crej_loose'],legend_titles['crej_loose'],'l')
    leg.AddEntry(graphs['crej_loose_highpt'],legend_titles['crej_loose_highpt'],'l')
    leg.Draw('same')
    if suffix == '_test_preprocessed':
        ROOT.myText(0.53, 0.3, 1, 'Preprocessed test sample')
    cnv.SaveAs(out_dir+'stats_comparison_cjet{}.pdf'.format(suffix))
    # Fit
    # for k, g in graphs.items():
    #     fn = ROOT.TF1('fn','log([0]*x)',0,1)
    #     fn.SetLineColor(colors[k])
    #     fit_result = g.Fit(fn,'RMS')

if __name__ == "__main__":
    graphs = get_all_graphs(x_values, y_values_highpt_test_file)
    plot_graphs(graphs)
    graphs = get_all_graphs(x_values, y_values_highpt_test_file_preprocessed)
    plot_graphs(graphs, suffix = '_test_preprocessed')