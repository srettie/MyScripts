import sys
import os
import plot_utils
import ROOT
import subprocess
import shutil
sys.path.append(os.path.join(os.path.dirname(__file__),'..','batch'))
import tracking_submitJobs

debug = True
tracks = tracking_submitJobs.tracks
templates_base = '/unix/atlasvhbb2/srettie/tracking/athenaOutputs_full/'
ref_hist_dir = '/unix/atlasvhbb2/srettie/tracking/reference_histograms/'
jet_collections = ['AntiKt4EMPFlow_BTagging201903']#'AntiKt4EMTopo'
good_samples = tracking_submitJobs.good_samples

def load_root_macros():
    iptag_dir = '/unix/atlasvhbb2/srettie/tracking/Projects/IPtagTuning/'
    calibprep_dir = '/unix/atlasvhbb2/srettie/tracking/Projects/calibFilePreparator/'
    os.chdir(iptag_dir)
    ROOT.gROOT.LoadMacro(iptag_dir + 'IPxDStandaloneTool.cxx+')
    ROOT.gROOT.LoadMacro(iptag_dir + 'tuning_IP3D_new.c+')
    os.chdir(calibprep_dir)
    ROOT.gROOT.LoadMacro(calibprep_dir + 'calibMod.C+')

def create_templates(sample, track_type, jet_type):
    # Actually create the templates from the athena outputs
    if debug: print('Creating template for: {} ({})'.format(track_type, jet_type))
    # For now the macro has the folder structure for the input file as follows:
    # "/unix/atlasvhbb2/srettie/tracking/athenaOutputs_full/"+sample+"/"+track_type+"/all_flav_"+simple_name+"_"+abrv+"_"+track_type+".root"
    # With output file as follows:
    # "/unix/atlasvhbb2/srettie/tracking/athenaOutputs_full/"+sample+"/"+track_type+"/ip3d_tuning_V49_zprime_"+jet_type+"_"+track_type+".root"
    ROOT.tuning_IP3D(sample, tracking_submitJobs.get_simple_name(s), track_type, jet_type)

def create_calibration_file(sample, track_type):
    # Merge new templates with BTAG calibration file
    if debug: print('Creating calibration file for: {}'.format(track_type))
    # Get original calibration file; copied from srettie@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/perf-flavtag/ReferenceHistograms/
    reference_calib_filename = ref_hist_dir + sample + '/BTagCalibRUN2-08-49.root'
    calib_filename = reference_calib_filename.replace('.root','_{}.root'.format(track_type))
    tmp_filename = calib_filename.replace('.root','_tmp.root')
    shutil.copyfile(reference_calib_filename, tmp_filename)
    # Remove the GUID and IPxD folders within that file
    ROOT.removeGUID(tmp_filename)
    ROOT.removeTagger(tmp_filename, 'IP2D')
    ROOT.removeTagger(tmp_filename, 'IP3D')

    # Add the new templates
    new_templates = templates_base+sample+'/'+track_type+'/ip3d_tuning_V49_zprime_'+track_type+'.root'
    cmd = 'hadd -f {} {} {}'.format(calib_filename, tmp_filename, new_templates)
    print(cmd)
    subprocess.call(cmd, shell=True)

    # Remove temporary file
    os.remove(tmp_filename)

    # Add GUID
    cmd = 'coolHist_setFileIdentifier.sh {}'.format(calib_filename)
    subprocess.call(cmd, shell=True)
    
    return

def merge_jet_templates(sample,track_type):
    combined_file = templates_base+sample+'/'+track_type+'/ip3d_tuning_V49_zprime_'+track_type+'.root'
    cmd = 'hadd -f {} '.format(combined_file)
    for j in jet_collections:
        cmd += templates_base+sample+'/'+track_type+'/ip3d_tuning_V49_zprime_'+j+'_'+track_type+'.root '
    print(cmd)
    subprocess.call(cmd, shell=True)

if __name__ == "__main__":

    load_root_macros()
    for s in good_samples:
        print('Creating IPxD template for sample: {}'.format(s))
        for t in tracks:
            for j in jet_collections:
                create_templates(s, t, j)
            # Merge jet types here...
            merge_jet_templates(s,t)
    print('IPxD templates created, now create calibration files...')
    for s in good_samples:
        for t in tracks:
            create_calibration_file(s, t)
    print('Done!')