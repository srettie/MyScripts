import os
import glob
import ROOT
import array
import argparse
import numpy as np
import plot_utils as pu

force_slimming_creation = False
debug = True
tree_names = {
    'SR' : 'sig',
    'VR' : 'validation',
    'CR' : 'control',
}
tree_branch_list = [
    'run_number','event_number','truth_mhh','resolvedPrecut','passResolvedSR','resolvedCat','mcChannelNumber','boostedPrecut','totalWeightBoosted','m_hhBoosted','pt_hhBoosted','eta_hhBoosted','phi_hhBoosted','m_h1Boosted','pt_h1Boosted','eta_h1Boosted','phi_h1Boosted','Idx_h1Boosted','HbbWP_h1Boosted','dRmin_h1Boosted','m_h2Boosted','pt_h2Boosted','eta_h2Boosted','phi_h2Boosted','Idx_h2Boosted','HbbWP_h2Boosted','dRmin_h2Boosted','E_VBFj1Boosted','pt_VBFj1Boosted','eta_VBFj1Boosted','phi_VBFj1Boosted','Idx_VBFj1Boosted','E_VBFj2Boosted','pt_VBFj2Boosted','eta_VBFj2Boosted','phi_VBFj2Boosted','Idx_VBFj2Boosted','m_VBFjjBoosted','pTvecsum_VBFBoosted','passVBFJets','X_hhBoosted', 'BDT_score', 'BDT_scoreNoPhi_nonres']
tree_branch_list += [f'BDT_score_res_{m}' for m in [1000,1100,1200,1300,1400,1500,1600,1800,2000,2500,3000,4000,5000]]

CR_VR = '(sqrt( pow((m_h1Boosted-124)/(0.1*log(m_h1Boosted)),2) + pow((m_h2Boosted-117)/(0.1*log(m_h2Boosted)),2) ))'
selections = {
    'boostedPrecut' : 'boostedPrecut',
    'passVBFJets'   : 'passVBFJets',
    'SR'            : 'X_hhBoosted < 1.6',
    'VR'            : f'(X_hhBoosted >= 1.6) && ({CR_VR} < 100)',
    'CR'            : f'(X_hhBoosted >= 1.6) && ({CR_VR} >= 100) && ({CR_VR} < 170)',
    'CR_inclusive'  : f'{CR_VR} < 170',
    '0F_WP60'       : 'HbbWP_h1Boosted <= 60 && HbbWP_h2Boosted <= 60',
    '0F_WP70'       : 'HbbWP_h1Boosted <= 70 && HbbWP_h2Boosted <= 70',
    '1F_WP60'       : '(HbbWP_h1Boosted > 60 && HbbWP_h2Boosted <= 60) || (HbbWP_h1Boosted <= 60 && HbbWP_h2Boosted > 60)',
    '1F_WP70'       : '(HbbWP_h1Boosted > 70 && HbbWP_h2Boosted <= 70) || (HbbWP_h1Boosted <= 70 && HbbWP_h2Boosted > 70)',
    '2F_WP60'       : 'HbbWP_h1Boosted > 60 && HbbWP_h2Boosted > 60',
    '2F_WP70'       : 'HbbWP_h1Boosted > 70 && HbbWP_h2Boosted > 70',
    'baseline'      : 'boostedPrecut && passVBFJets && !passResolvedSR && m_h1Boosted > 50 && m_h2Boosted > 50 && pt_h1Boosted > 450 && pt_h2Boosted > 250 && m_VBFjjBoosted > 1000 && abs(eta_VBFj1Boosted-eta_VBFj2Boosted) > 3 && pt_VBFj1Boosted > 20 && pt_VBFj2Boosted > 20'
}
slimming_selection_60 = f'({selections["boostedPrecut"]}) && ({selections["passVBFJets"]}) && ({selections["1F_WP60"]})'
slimming_selection_70 = f'({selections["boostedPrecut"]}) && ({selections["passVBFJets"]}) && ({selections["1F_WP70"]})'
slimming_selection_60_0F = f'({selections["boostedPrecut"]}) && ({selections["passVBFJets"]}) && ({selections["0F_WP60"]})'
bdt_variables = [
    'pt_h1','eta_h1','phi_h1',
    'pt_h2','eta_h2','phi_h2',
    'pt_hh','eta_hh','phi_hh','m_hh',
    'pt_VBFj1','eta_VBFj1','phi_VBFj1','E_VBFj1',
    'pt_VBFj2','eta_VBFj2','phi_VBFj2','E_VBFj2'
]

def get_parser():
    p = argparse.ArgumentParser(description='Optimize hh4b signal', allow_abbrev=False)
    p.add_argument('-i', '--in_dir', dest='in_dir', default=None, required=True, help='Directory where input merged NNTs are stored')
    p.add_argument('-o', '--out_dir', dest='out_dir', default=None, required=True, help='Directory where plots are saved')
    p.add_argument('-d', '--debug', action='store_true', help='Print debug information')
    return p

def slim_NNT(orig_name, new_name, tree_name, selection, cols_to_add=None):
    if force_slimming_creation or not os.path.exists(new_name):
        if debug: print(f'slimming: {orig_name}')
        t = ROOT.TChain(tree_name)
        t.Add(orig_name)
        if t.GetEntries() == 0:
            print(f'skipping 0 entries {orig_name}')
            return
        df = ROOT.RDataFrame(t)
        df.Filter(selection).Snapshot(tree_name, new_name, tree_branch_list)
    else:
        if debug: print(f'{new_name} already exists; skipping!')

def save_slimmed_trees(in_dir):
    print(f'Start slimming NNTs: {in_dir}')
    NNTs_data = glob.glob(os.path.join(in_dir, 'data', 'data*.root'))
    for NNT in NNTs_data:
        if '1F' in NNT: continue
        if '0F' in NNT: continue
        slim_NNT(NNT, NNT.replace('.root', '_1F_WP60.root'), tree_names['SR'], slimming_selection_60)
        slim_NNT(NNT, NNT.replace('.root', '_0F_WP60.root'), tree_names['SR'], slimming_selection_60_0F)
        slim_NNT(NNT, NNT.replace('.root', '_1F_WP70.root'), tree_names['SR'], slimming_selection_70)
    print('done slimming trees')

def print_norm_sys(yields):
    print(f'norm_sys: {100*(((yields["VR_0F_WP60"]/yields["VR_1F_WP60"])/(yields["CR_0F_WP60"]/yields["CR_1F_WP60"]))-1):.3f}%')

def get_region_numbers(in_dir, skip_calculations=False):
    print(f'CR selection: {selections["CR"]}')
    print(f'VR selection: {selections["VR"]}')
    if skip_calculations:
        yields = {
            'SR_0F_WP60' : 0,
            'SR_1F_WP60' : 2626,
            'SR_2F_WP60' : 74989,
            'VR_0F_WP60' : 59,
            'VR_1F_WP60' : 6279,
            'VR_2F_WP60' : 180895,
            'CR_0F_WP60' : 125,
            'CR_1F_WP60' : 14866,
            'CR_2F_WP60' : 443429,
            'SR_0F_WP70' : 27,
            'SR_1F_WP70' : 3846,
            'SR_2F_WP70' : 73742,
            'VR_0F_WP70' : 121,
            'VR_1F_WP70' : 9215,
            'VR_2F_WP70' : 177897,
            'CR_0F_WP70' : 266,
            'CR_1F_WP70' : 21792,
            'CR_2F_WP70' : 436362,
        }
        print_norm_sys(yields)
        return yields
    NNTs_data = glob.glob(os.path.join(in_dir, 'Data', 'data*.root'))
    data_samples = [f'data{p}.root' for p in range(15, 19)]
    yields = {}
    for wp in ['60', '70']:
        for r in ['SR', 'VR', 'CR']:
            rdf = pu.get_rdf(NNTs_data, data_samples, tree_names[r], debug)
            for xbb in ['0F', '1F', '2F']:
                yields[f'{r}_{xbb}_WP{wp}'] = rdf.Filter(f'({selections[f"{xbb}_WP{wp}"]}) && ({selections[r]}) && ({selections["baseline"]})').Count().GetValue()
    for k, v in yields.items():
        print(f'{k}: {v}')
    return yields

def save_yields_to_files(in_dir, yields):
    NNTs_data = glob.glob(os.path.join(in_dir, 'Data', 'data*.root'))
    for NNT in NNTs_data:
        if 'WP' not in NNT: continue
        f = ROOT.TFile.Open(NNT, 'UPDATE')
        for k, v in yields.items():
            if 'WP60' in NNT and 'WP60' in k:
                p = ROOT.TParameter(float)(k.replace('_WP60',''), v)
                p.Write()
            if 'WP70' in NNT and 'WP70' in k:
                p = ROOT.TParameter(float)(k.replace('_WP70',''), v)
                p.Write()
        f.Close()

def get_n_binning(h, n = 3):
    if n <= 0:
        raise ValueError('Make sure number of bins is at least 1!')
    total = h.Integral()
    n_per_bin = total / n
    bin_ctr = 0
    ibin_ctr = 0
    binning = [h.GetBinLowEdge(1)]
    bin_contents = []
    bins_to_merge = []
    print(f'total events: {total}')
    print(f'events per bin for {n} bins: {n_per_bin}')
    # loop over bins
    for ibin in range(1, h.GetNbinsX()+1):
        print(f'bin: {ibin}, n_in_bin: {h.GetBinContent(ibin)}')
        ibin_ctr += 1
        bin_ctr += h.GetBinContent(ibin)
        if bin_ctr > n_per_bin:
            print(f'\tbin_ctr = {bin_ctr}, merging {ibin_ctr} bins')
            binning.append(h.GetBinLowEdge(ibin+1))
            bin_contents.append(bin_ctr)
            bins_to_merge.append(ibin_ctr)
            bin_ctr = 0
            ibin_ctr = 0
    print(f'\tbin_ctr = {bin_ctr}, merging remaining {ibin_ctr} bins')
    binning.append(h.GetBinLowEdge(h.GetNbinsX()+1))
    bin_contents.append(bin_ctr)
    bins_to_merge.append(ibin_ctr)
    print(f'binning: {binning}')
    print(f'bin_contents: {bin_contents}')
    print(f'bins_to_merge: {bins_to_merge}')
    return bins_to_merge

def addBDTScores(df, modelfile):
    import xgboost as xgb
    model=xgb.Booster()
    model.load_model(modelfile)
    dmatrix=xgb.DMatrix(df[[f'{v}Boosted' for v in bdt_variables]],feature_names=bdt_variables)
    df[f'BDT_{modelfile.split("/")[-1].replace(".model","")}'] = model.predict(dmatrix)
    return df

def get_baseline_df(df):
    mask = (df['boostedPrecut']) & \
           (df['passVBFJets']) & \
           (~df['passResolvedSR']) & \
           (df['m_h1Boosted'] > 50) & \
           (df['m_h2Boosted'] > 50) & \
           (df['pt_h1Boosted'] > 450) & \
           (df['pt_h2Boosted'] > 250) & \
           (df['m_VBFjjBoosted'] > 1000) & \
           (np.absolute(df['eta_VBFj1Boosted']-df['eta_VBFj2Boosted']) > 3) & \
           (df['pt_VBFj1Boosted'] > 20) & \
           (df['pt_VBFj2Boosted'] > 20)
    return df[mask]

def get_VR_df(df):
    df = get_baseline_df(df)
    mask = (df['X_hhBoosted'] >= 1.6) & (df['CR_VR'] < 100)
    return df[mask]

def get_SR_df(df):
    df = get_baseline_df(df)
    mask = df['X_hhBoosted'] < 1.6
    return df[mask]

def addCRVR(df):
    df.insert(0, 'CR_VR', np.sqrt( np.power((df['m_h1Boosted']-124)/(0.1*np.log(df['m_h1Boosted'])),2) +
                                        np.power((df['m_h2Boosted']-117)/(0.1*np.log(df['m_h2Boosted'])),2) ), False)
    return df

def produce_shape_syst_hist(in_dir, yields, binning, the_bins):
    print(f'\nproducing shape systematic for: {binning} = {the_bins}')
    isBDT = 'BDT' in binning
    fit_var = 'BDT_scoreNoPhi_nonres' if isBDT else 'm_hhBoosted'
    x_title = 'BDT Score' if isBDT else 'm_{HH} [GeV]'
    # initialize histograms
    h_result = ROOT.TH1D('h_skeleton', f'h_skeleton;{x_title}', len(the_bins)-1, array.array('d', the_bins))
    h_bkg = h_result.Clone('h_bkg')
    h_VR_0F = h_result.Clone('h_VR_0F')
    h_VR_1F = h_result.Clone('h_VR_1F')
    h_SR_1F = h_result.Clone('h_SR_1F')
    h_shape = h_result.Clone('h_shape')
    N_CR_0F = yields['CR_0F_WP60']
    N_CR_1F = yields['CR_1F_WP60']
    N_VR_0F = yields['VR_0F_WP60']
    N_VR_1F = yields['VR_1F_WP60']
    # get input names
    NNTs_data = glob.glob(os.path.join(in_dir, 'Data', 'data*.root'))
    data_samples = [f'data{p}.root' for p in range(15, 19)]
    # define selections
    selection_0F = ROOT.TCut(f'({selections["baseline"]})&&({selections["0F_WP60"]})')
    selection_1F = ROOT.TCut(f'({selections["baseline"]})&&({selections["1F_WP60"]})')
    t_VR = pu.get_tree(NNTs_data, data_samples, tree_names['VR'])
    t_SR = pu.get_tree(NNTs_data, data_samples, tree_names['SR'])
    t_VR.Draw(f'{fit_var} >> h_VR_0F', selection_0F)
    t_VR.Draw(f'{fit_var} >> h_VR_1F', selection_1F)
    t_SR.Draw(f'{fit_var} >> h_SR_1F', selection_1F)
    t_SR.Draw(f'{fit_var} >> h_bkg', selection_1F)
    h_bkg.Scale(N_CR_0F/N_CR_1F)
    vals = []
    for ibin in range(1, h_result.GetNbinsX()+1):
        val = ((h_VR_0F.GetBinContent(ibin)/N_VR_0F)/(h_VR_1F.GetBinContent(ibin)/N_VR_1F)) - 1
        h_result.SetBinContent(ibin, val)
        h_shape.SetBinContent(ibin, (1+val)*h_bkg.GetBinContent(ibin))
        vals.append(val)
    # get norm factor
    norm = h_bkg.Integral() / h_shape.Integral()
    vals = [norm * i for i in vals]
    print(f'{binning} vals: {vals}')
    pu.plot_single_hist(out_dir, h_bkg, f'h_bkg_{binning}', short_title = f'SR, 1F, {binning}', plot_stat_err = True)
    pu.plot_single_hist(out_dir, h_VR_0F, f'h_VR_0F_{binning}', short_title = f'VR, 0F, {binning}', plot_stat_err = True)
    pu.plot_single_hist(out_dir, h_VR_1F, f'h_VR_1F_{binning}', short_title = f'VR, 1F, {binning}', plot_stat_err = True)
    pu.plot_single_hist(out_dir, h_SR_1F, f'h_SR_1F_{binning}', short_title = f'SR, 1F, {binning}', plot_stat_err = True)
    pu.plot_single_hist(out_dir, h_result, f'h_result_{binning}', short_title = f'Shape Syst Values, {binning}', plot_stat_err = True, y_range = [-2,3])
    pu.plot_single_hist(out_dir, h_shape, f'h_shape_{binning}', short_title = f'Shape Syst, {binning}', plot_stat_err = True)
    # plots for INT note
    pu.set_hist_color_style(h_VR_1F, ROOT.kBlue, set_fill = False)
    pu.set_hist_color_style(h_VR_0F, ROOT.kRed, set_fill = False)
    pu.plot_histograms(out_dir, [pu.get_unit_normalized_hist(h) for h in [h_SR_1F, h_VR_1F]], ['Data SR, 1F', 'Data VR, 1F'], f'h_{"BDT" if "BDT" in binning else "mHH"}_k2V0SR1FVR1F_{binning}', plot_stat_err = True)
    pu.plot_histograms(out_dir, [pu.get_unit_normalized_hist(h) for h in [h_VR_1F, h_VR_0F]], ['Data VR, 1F', 'Data VR, 0F'], f'h_{"BDT" if "BDT" in binning else "mHH"}_k2V0VR1FVR0F_{binning}', plot_stat_err = True)
    plot_shape_uncertainty(h_bkg, vals, binning)
    h_bkg.SetDirectory(0)
    return (h_bkg, vals)

def plot_shape_uncertainty(h, vals, binning):
    h_shape = h.Clone()
    assert len(vals) == h.GetNbinsX()
    for ibin, val in enumerate(vals):
        h_shape.SetBinContent(ibin+1, val*h.GetBinContent(ibin+1))
    h_shape_up = h + h_shape
    h_shape_down = h - h_shape
    pu.set_hist_color_style(h_shape_up, ROOT.kRed, set_fill = False)
    pu.set_hist_color_style(h_shape_down, ROOT.kBlue, set_fill = False)
    pu.plot_histograms(out_dir, [h, h_shape_up, h_shape_down], ['Nominal', '+1#sigma', '-1#sigma'], f'h_{"BDT" if "BDT" in binning else "mHH"}_k2V0ShapeSys_{binning}', plot_stat_err = True)

def print_run_commands():
    configs = [
        # 'NR_WP60_DDbkg_DDnormshapeSys_nominal',
        # 'NR_WP60_DDbkg_DDnormshapeSys_optimal',
        # 'NR_WP60_DDbkg_DDnormshapeSys_optimalfromthree',
        # 'NR_WP60_DDbkg_DDnormshapeSys_BDT',
        # 'NR_mHH',
        # 'NR_BDT',
        # 'NR_mHH_sys',
        'NR_BDT_sys',
    ]
    script_flags = {
        'make_workspace'        : ['--roowksp'],
        'fit_workspace'         : [''],
        'make_limit_plots'      : ['-muscan -xsscan -e pdf'],
        'make_pullimpact_plots' : ['-e pdf'],
        'make_dist_plots'       : ['--variations 1 1 1 --variations 1 0 1 --signal-scalings 100 1 -e pdf'],
        'make_syst_plots'       : ['--systematics DD_boosted_norm DD_boosted_shape -e pdf'],
        'run_pulls'             : ['--pulls --postfit --corrs'],
        'make_fit_plots'        : ['--pre --individual -e pdf', '--post --individual -e pdf'],
    }
    for config in configs:
        cmd = f'mkdir -p workspaces/{config} results/{config} plots/{config};'
        for script in script_flags:
            for opts in script_flags[script]:
                cmd += f'python hh4b-vbf-limits/{script}.py -c hh4b-vbf-limits/config/boosted_vbf_4b/{config}.yaml {opts};'
        if 'DDnormSys' in config or config in ['NR_mHH','NR_BDT','NR_mHH_sys']:
            cmd += f'python hh4b-vbf-limits/make_syst_plots.py -c hh4b-vbf-limits/config/boosted_vbf_4b/{config}.yaml --systematics DD_boosted_norm -e pdf;'
        if 'DDshapeSys' in config or config in ['NR_mHH','NR_BDT','NR_mHH_sys']:
            cmd += f'python hh4b-vbf-limits/make_syst_plots.py -c hh4b-vbf-limits/config/boosted_vbf_4b/{config}.yaml --systematics DD_boosted_shape -e pdf;'
        if 'DDnormshapeSys' in config or config in ['NR_mHH','NR_BDT','NR_mHH_sys']:
            cmd += f'python hh4b-vbf-limits/make_syst_plots.py -c hh4b-vbf-limits/config/boosted_vbf_4b/{config}.yaml --systematics DD_boosted_norm DD_boosted_shape -e pdf;'
        if config in ['NR_mHH_sys','NR_BDT_sys']:
            cmd += f'python hh4b-vbf-limits/make_syst_plots.py -c hh4b-vbf-limits/config/boosted_vbf_4b/{config}.yaml --systematics _bkg -e pdf;'
            cmd += f'python hh4b-vbf-limits/make_syst_plots.py -c hh4b-vbf-limits/config/boosted_vbf_4b/{config}.yaml --systematics mc -e pdf --num-per-plot 1;'
        print(cmd)

def get_2D_hist(t, h_skeleton, x, y, process, selection, weight, suffix, debug=False):
    h2 = h_skeleton.Clone(f'h2_{x}_vs_{y}{suffix}_{process}')
    draw_var = f'{y}:{x}'
    draw_cmd = f'{draw_var} >> h2_{x}_vs_{y}{suffix}_{process}'
    if debug:
        print(f'drawing: {draw_cmd}')
        print(f'selection: {selection}')
        print(f'weight: {weight}')
    n_evts = t.Draw(draw_cmd, selection*weight)
    if debug:
        print(f'n_evts: {n_evts}')
    h2.SetTitle(f'h2_{x}_vs_{y}{suffix}_{process}')
    h2.SetDirectory(0)
    return h2

def plot_2D_distributions(t):
    # print numbers
    regions = {
        'SR' : 1.6,
        'VR' : 100,
        'CR' : 170,
    }
    Xbb_sels = [f'{i}F_WP60' for i in range(3)]
    for r in list(regions.keys()):
        for xbb in Xbb_sels:
            cut = ROOT.TCut(f'({selections["baseline"]})&&({selections[r]})&&({selections[xbb]})')
            print(cut.GetTitle())
            n = t.Draw('m_hhBoosted',cut)
            print(f'{r}_{xbb}: {n}')
    contours = {
        'SR' : ROOT.TF2('SR', 'sqrt(pow(((x-124)/(1500/x)),2)+pow(((y-117)/(1900/y)),2))', 40, 220, 40, 220),
        'VR' : ROOT.TF2('VR', 'sqrt(pow((x-124)/(0.1*log(x)),2)+pow((y-117)/(0.1*log(y)),2))', 40, 220, 40, 220),
        'CR' : ROOT.TF2('CR', 'sqrt(pow((x-124)/(0.1*log(x)),2)+pow((y-117)/(0.1*log(y)),2))', 40, 220, 40, 220),
    }
    for r, c in regions.items():
        contours[r].SetContour(1, array.array('d', [c]))
    h2_skeleton = ROOT.TH2D('h2', 'h2;m_h1Boosted [GeV];m_h2Boosted [GeV]', 170, 50, 220, 170, 50, 220)
    baseline = ROOT.TCut(selections['baseline'])
    CR_inclusive = ROOT.TCut(selections['CR_inclusive'])
    weight = ROOT.TCut('1')
    h2s = {}
    h2s['all'] = get_2D_hist(t, h2_skeleton, 'm_h1Boosted', 'm_h2Boosted', 'all', baseline+CR_inclusive, weight, '')
    pu.plot_2D(out_dir, h2s['all'], 'all', f'all: {h2s["all"].GetEntries()}', contours = contours.values())
    for xbb in Xbb_sels:
        xbb_cut = ROOT.TCut(selections[xbb])
        h2s[xbb] = get_2D_hist(t, h2_skeleton, 'm_h1Boosted', 'm_h2Boosted', xbb, baseline+xbb_cut+CR_inclusive, weight, '')
        pu.plot_2D(out_dir, h2s[xbb], xbb, f'{xbb}: {h2s[xbb].GetEntries()}', contours = contours.values())
        for r in regions:
            r_cut = ROOT.TCut(selections[r])
            h2s[f'{xbb}_{r}'] = get_2D_hist(t, h2_skeleton, 'm_h1Boosted', 'm_h2Boosted', f'{xbb}_{r}', baseline+xbb_cut+r_cut, weight, '')
            pu.plot_2D(out_dir, h2s[f'{xbb}_{r}'], f'{xbb}_{r}', f'{xbb}_{r}: {h2s[f"{xbb}_{r}"].GetEntries()}', contours = contours.values())

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    # required arguments
    in_dir, out_dir = args.in_dir, args.out_dir
    if args.debug:
        debug = args.debug
    print_run_commands()
    # save smaller trees for optimizations if they don't exist or if we force them
    save_slimmed_trees(in_dir)
    yields = get_region_numbers(in_dir, skip_calculations=True)
    # save yields to slimmed files
    # save_yields_to_files(in_dir, yields)
    pu.set_ATLAS_style()
    # map with bkg distribution and shape sys values as a tuple (h_bkg, vals)
    shape_vals = {}
    bins = {
        'nominal' : [100*i for i in range(5, 26)],
        'optimal' : [500.0, 759, 877, 1027, 1091, 1180, 1270, 1445, 1860, 2500.0],
        'three' : [500.0, 877.0, 1180.0, 2500.0],
        'BDT' : [0.0, 0.227, 0.251, 0.31, 0.357, 0.461, 0.574, 0.687, 1.0],
        'BDTtwo' : [0.0, 0.461, 1.0],
        'BDTTest' : [0.0, 0.113333, 0.295, 0.475, 0.685, 1.0],
        'BDTtwoTest' : [0.0, 0.475, 1.0],
    }
    for b, the_bins in bins.items():
        shape_vals[b] = produce_shape_syst_hist(in_dir, yields, b, the_bins)

    # optimized binning shape sys from 3 bins
    three_bin_vals = shape_vals['three'][1]
    assert len(three_bin_vals) == 3
    # bins_to_merge = get_n_binning(shape_vals['optimal'][0])
    bins_to_merge = [2, 3, 4]
    print(f'bins_to_merge: {bins_to_merge}')
    optimalfromthree_vals = []
    for i in range(0, len(bins_to_merge)):
        optimalfromthree_vals += bins_to_merge[i]*[three_bin_vals[i]]
    shape_vals['optimalfromthree'] = (shape_vals['optimal'][0], optimalfromthree_vals)
    plot_shape_uncertainty(shape_vals['optimalfromthree'][0], shape_vals['optimalfromthree'][1], 'optimalfromthree')

    # optimized BDT binning shape sys for 2 bins
    two_bin_vals = shape_vals['BDTtwo'][1]
    assert len(two_bin_vals) == 2
    bins_to_merge = [5, 3]
    print(f'bins_to_merge: {bins_to_merge}')
    optimalbdtfromtwo_vals = []
    for i in range(0, len(bins_to_merge)):
        optimalbdtfromtwo_vals += bins_to_merge[i]*[two_bin_vals[i]]
    shape_vals['BDTfromtwo'] = (shape_vals['BDT'][0], optimalbdtfromtwo_vals)
    for b, v in shape_vals.items():
        print(f'{b}: {v[1]}')
    plot_shape_uncertainty(shape_vals['BDTfromtwo'][0], shape_vals['BDTfromtwo'][1], 'BDTfromtwo')
